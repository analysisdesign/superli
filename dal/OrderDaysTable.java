package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modules.Order;
import modules.ProvidedProduct;

public class OrderDaysTable implements IDBManager{

	private Connection conn;
	private String name;

	public OrderDaysTable()
	{
		this.conn = null;
		this.name = "ORDERS_DAYS";
	}
	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		createTable(this.name);
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {
		Statement stmt;
		try {
			stmt = this.conn.createStatement();
			String sql = 
					"CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (OrderID    TEXT      NOT NULL , "
					+ " Day     TEXT    NOT NULL , "
					+ "primary key (OrderID , DAY ),"
					+ "foreign key (OrderID) references SupplierOrder(OrderID))";

			stmt.executeUpdate(sql);
			stmt.close();
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create Order Days table");
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean clearTable() {
		try {
			conn.createStatement().executeUpdate("DELETE FROM " + this.name + ";");
//			conn.createStatement().execute("VACUUM;");
		} catch (SQLException e) {
			System.out.println("ERROR: "+this.getClass()+".clearTable()");
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public boolean closeConnection() {
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public void insertHardCodedDefaultData() {
		// TODO Auto-generated method stub
		
	}
	
	public boolean CheckIfExistOrderIDInOrderDaysTableByOrderID(String orderID) {
			try {
				Statement stmt  = conn.createStatement();
				ResultSet rs    = stmt.executeQuery("SELECT * "
						+ "FROM "+this.name
						+ " WHERE OrderID = '"+orderID+"'");
				// loop through the result set
				if (rs.next()) {
					return true;
					}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			return false;
	}
	/**
	 * Filter the list from orders that are not permanent.
	 * @param allOrdersThatHasSpecificProduct
	 * @return arrayList of permanent orders.
	 */
	public ArrayList<Order> getPermanentOrders(ArrayList<Order> allOrdersThatHasSpecificProduct) {
		for (Order order : allOrdersThatHasSpecificProduct) {
			if(!CheckIfExistOrderIDInOrderDaysTableByOrderID(order.getOrderNumber()))
				allOrdersThatHasSpecificProduct.remove(order);
		}
		return allOrdersThatHasSpecificProduct;
	}
	public List<Integer> getDaysByInput(String deliveryDays) {
		if(!checkValidInputDeliveryDays(deliveryDays))
			return null;
		String[] days = deliveryDays.split(",");
		ArrayList<Integer> ans = new ArrayList<Integer>();
		for (String day : days) 
		{
			if(ans.contains(day)) // duplicate delivery days.
			{
				System.out.println("Duplicate delivery day "+day);
				return null;
			}
			ans.add(Integer.parseInt(day));
		}
		return ans;
	}
	public boolean checkValidInputDeliveryDays(String deliveryDays) 
	{
		String[] days = deliveryDays.split(",");
		if(days.length == 0 || days.length > 7) // too many delivery days.
			return false;
		for (String day : days) 
		{
			try
			{
				int dayInNumber = Integer.parseInt(day);// day should be a number
				if(dayInNumber <1 || dayInNumber > 7)
					throw new Exception(); // invalid day.
			}
			catch(Exception e)
			{
				System.out.println(day +" is not valid input for a day in the week");
				return false;
			}
		}
		return true;
	}
	public boolean updateOrder(Order o)
	{
		try {
			PreparedStatement stmt = conn.prepareStatement(
					"DELETE FROM "+this.name
					+" WHERE OrderID = '"
					+ o.getOrderNumber()+"'");
			stmt.executeUpdate();
			for(Integer i:o.getDeliveyDays())
			{
				stmt = conn.prepareStatement(
						"insert into "+this.name+" (OrderID , Day) VALUES(?,?)");
				stmt.setString(1, o.getOrderNumber());
				stmt.setString(2, i+"");
				stmt.executeUpdate();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public List<Integer> getOrderDeliveryDays(String orderID) {
		List<Integer> ans = new ArrayList<Integer>();
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery("SELECT * "
					+ " FROM "+this.name
					+ " WHERE OrderID = '"+orderID+"'");
			// loop through the result set
			while (rs.next()) {
				ans.add(rs.getInt("Day"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return ans;
	}
	/**
	 * Check if there are days for delievery for the order
	 * return true if this order is permanent
	 * else return false
	 * @param orderID
	 * @return
	 */
	public boolean checkIfIsPermanentOrder(String orderID)
	{
		if(getOrderDeliveryDays(orderID).size() > 0)
			return true;
		return false;
	}
	
}
