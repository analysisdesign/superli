package dal;

import java.sql.*;

/**
 * The Data Base
 *
 */
public class DbConnectionSingelton {

	/**
	 * The connection to the data base
	 */
	private static Connection instance;

	/**
	 * Singleton constructor   
	 */
	private DbConnectionSingelton() {
		try {
			Class.forName("org.sqlite.JDBC");
			instance = DriverManager.getConnection("jdbc:sqlite:superli.db");
			System.out.println("Opened database successfully");
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Error: database connection fail");
			e.printStackTrace();	
		}
	}

	/**
	 * Singleton getter
	 * @return Connection of the data base
	 */
	public static Connection getInstance()
	{
		if (instance == null) {
			synchronized (DbConnectionSingelton.class) {
				if (instance == null) {
					new DbConnectionSingelton();

					//TODO: check that this working after change, and if so - delete this
					//instance = DbConnectionSingelton.instance;
				}
			}
		}
		return instance;
	}
}
