package dal;

import java.io.InputStream;
import java.security.Security;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

import DataAccessLayer.DBAccess;
import Deliveries.Layers.PLI;
import Employees.PresentationLayer.Menu;
import app.main.MainMenu;
import reports.ExpiredReport;
import reports.ShortageReport;
import modules.*;

public class DataBase {
	/**
	 * The Data Base
	 */
	private static DataBase db;
	private static PLI pli;

	/**
	 * The path of the Data Base
	 */
	final String ADDRESS = "Super Li, Beer Sheva";

	Connection conn;
	public SupplierTable supplierDBM;
	public ContactsTable contactsDBM;
	public ProductSupplierTable productSupplierDBM;
	public SupplierOrderTable supplierOrderDBM;
	public OrdersTable ordersDBM;
	public OrderDaysTable orderDaysDBM;

	public ShopsTable shopsTable;

	public Supplier_Supply_DaysTable supplier_Supply_DaysTable;

	public IDBManager productTable;
	public IDBManager categoryTable;
	public IDBManager productDiscountTable;
	public IDBManager categoryDiscountTable;
	public IDBManager productInCategoryTable;
	public IDBManager supplierDiscountTable;

	/**
	 * Collects all the DB managers into one collection
	 */
	IDBManager[] managers;

	/**
	 * singleton constructor
	 */
	private DataBase() {
		supplierDBM = new SupplierTable();
		supplierDBM.connectToDB();
		contactsDBM = new ContactsTable();
		contactsDBM.connectToDB();
		productSupplierDBM = new ProductSupplierTable();// product-supplier
		productSupplierDBM.connectToDB();
		supplierOrderDBM = new SupplierOrderTable();// Supplier-order
		supplierOrderDBM.connectToDB();
		ordersDBM = new OrdersTable();// orders
		ordersDBM.connectToDB();
		orderDaysDBM=new OrderDaysTable();
		orderDaysDBM.connectToDB();
		///////////////////

		shopsTable=new ShopsTable();
		shopsTable.connectToDB();
		shopsTable.createTable("SHOPS");

		supplier_Supply_DaysTable=new Supplier_Supply_DaysTable();
		supplier_Supply_DaysTable.connectToDB();
		supplier_Supply_DaysTable.createTable("SUPPLIER_SUPPLY_DAYS");

		productTable = new ProductTable();
		productTable.connectToDB();
		productTable.createTable("PRODUCTS");
		categoryTable = new CategoryTable();
		categoryTable.connectToDB();
		categoryTable.createTable("CATEGORIES");
		productDiscountTable = new ProductDiscountTable();
		productDiscountTable.connectToDB();
		productDiscountTable.createTable("PRODUCT_DISCOUNTS");
		categoryDiscountTable = new CategoryDiscountTable();
		categoryDiscountTable.connectToDB();
		categoryDiscountTable.createTable("CATEGORY_DISCOUNTS");
		productInCategoryTable = new ProductInCategoryTable();
		productInCategoryTable.connectToDB();
		productInCategoryTable.createTable("PRODUCT_CATEGORIES");
		supplierDiscountTable = new SupplierDiscountTable();
		supplierDiscountTable.connectToDB();
		supplierDiscountTable.createTable("SUPPLIER_DISCOUNTS");
		this.conn = DbConnectionSingelton.getInstance();


		managers = new IDBManager[] { this.contactsDBM, this.orderDaysDBM, this.ordersDBM, 
				this.productSupplierDBM, this.supplierDBM, this.supplierOrderDBM,this.productTable,
				this.categoryTable,this.productDiscountTable,this.categoryDiscountTable,
				this.productInCategoryTable,this.supplierDiscountTable};

		/*this.shopsTable.insertTable(new Shop(1, "ramhal 1"));
		this.shopsTable.insertTable(new Shop(2, "ramhal 2"));
		this.shopsTable.insertTable(new Shop(3, "ramhal 3"));
		this.shopsTable.insertTable(new Shop(4, "ramhal 4"));

		((ProductTable)productTable).addProductsToInventory("Milk 1L","Tara","10/05/2017",100,1);
		((ProductTable)productTable).addProductsToInventory("Koteg 500gr","Tnuva","10/05/2017",20,1);
		((ProductTable)productTable).addProductsToInventory("Koteg 100gr","Tnuva","10/05/2017",50,1);
		((ProductTable)productTable).addProductsToInventory("Danny 100gr","Tara","10/05/2017",30,1);
		((ProductTable)productTable).addProductsToInventory("Bamba 100gr","Osem","22/05/2017",100,1);
		((ProductTable)productTable).addProductsToInventory("Bamba 100gr","Shush","22/05/2017",50,1);
		((ProductTable)productTable).addProductsToInventory("Bisly 100gr","Osem","22/05/2017",100,1);
		((ProductTable)productTable).addProductsToInventory("Salami 200gr","Zogloveck","15/05/2017",100,1);

		((ProductTable)productTable).addProductsToInventory("Milk 1L","Tara","10/05/2017",150,2);
		((ProductTable)productTable).addProductsToInventory("Koteg 500gr","Tnuva","10/05/2017",40,2);
		((ProductTable)productTable).addProductsToInventory("Koteg 100gr","Tnuva","10/05/2017",50,2);
		((ProductTable)productTable).addProductsToInventory("Danny 100gr","Tara","10/05/2017",45,2);
		((ProductTable)productTable).addProductsToInventory("Bamba 100gr","Osem","22/05/2017",150,2);
		((ProductTable)productTable).addProductsToInventory("Bamba 100gr","Shush","22/05/2017",40,2);
		((ProductTable)productTable).addProductsToInventory("Bisly 100gr","Osem","22/05/2017",120,2);
		((ProductTable)productTable).addProductsToInventory("Salami 200gr","Zogloveck","15/05/2017",100,2);

		((ProductTable)productTable).addProductsToInventory("Milk 1L","Tara","10/05/2017",100,3);
		((ProductTable)productTable).addProductsToInventory("Koteg 500gr","Tnuva","10/05/2017",20,3);
		((ProductTable)productTable).addProductsToInventory("Koteg 100gr","Tnuva","10/05/2017",50,3);
		((ProductTable)productTable).addProductsToInventory("Danny 100gr","Tara","10/05/2017",30,3);
		((ProductTable)productTable).addProductsToInventory("Bamba 100gr","Osem","22/05/2017",100,3);
		((ProductTable)productTable).addProductsToInventory("Bamba 100gr","Shush","22/05/2017",50,3);
		((ProductTable)productTable).addProductsToInventory("Bisly 100gr","Osem","22/05/2017",100,3);
		((ProductTable)productTable).addProductsToInventory("Salami 200gr","Zogloveck","15/05/2017",100,3);

		((ProductTable)productTable).addProductsToInventory("Milk 1L","Tara","10/05/2017",100,4);
		((ProductTable)productTable).addProductsToInventory("Koteg 500gr","Tnuva","10/05/2017",20,4);
		((ProductTable)productTable).addProductsToInventory("Koteg 100gr","Tnuva","10/05/2017",50,4);
		((ProductTable)productTable).addProductsToInventory("Danny 100gr","Tara","10/05/2017",30,4);
		((ProductTable)productTable).addProductsToInventory("Bamba 100gr","Osem","22/05/2017",100,4);
		((ProductTable)productTable).addProductsToInventory("Bamba 100gr","Shush","22/05/2017",50,4);
		((ProductTable)productTable).addProductsToInventory("Bisly 100gr","Osem","22/05/2017",100,4);
		((ProductTable)productTable).addProductsToInventory("Salami 200gr","Zogloveck","15/05/2017",100,4);


		((CategoryTable)categoryTable).insertTable(new Category("Dairy","-"));
		((CategoryTable)categoryTable).insertTable(new Category("Dairy Milk","Dairy"));
		((CategoryTable)categoryTable).insertTable(new Category("Dairy Cheese","Dairy"));
		((CategoryTable)categoryTable).insertTable(new Category("Dairy Yogurt","Dairy"));
		((CategoryTable)categoryTable).insertTable(new Category("Snack","-"));
		((CategoryTable)categoryTable).insertTable(new Category("Meat","-"));
		((CategoryTable)categoryTable).insertTable(new Category("Meat Sliced","Meat"));

		((CategoryDiscountTable)categoryDiscountTable).insertTable(new CategoryDiscount("Dairy Cheese",30,"30/04/2017","30/05/2017"));
		((ProductDiscountTable)productDiscountTable).insertTable(new ProductDiscount("Salami","Zogloveck",50,"30/04/2017","30/05/2017"));

		supplierDBM.addSupplier(new Supplier("1", "Roy&Co", "001", "Credit", false));
		supplierDBM.addSupplier(new Supplier("2", "HotBrazil", "002", "Credit", true));
		supplierDBM.addSupplier(new Supplier("3", "Orsh", "003", "Credit", false));
		supplierDBM.addSupplier(new Supplier("4", "Idanoosh", "004", "Credit", false));

		contactsDBM.add(new Supplier("1", "", "", "", false), new Contact("1", "Roy", "Ygael", "050512341"));
		contactsDBM.add(new Supplier("2", "", "", "", false), new Contact("2", "Yaniv", "Daye", "051512341"));
		contactsDBM.add(new Supplier("3", "", "", "", false), new Contact("3", "Or", "Ben Shushan", "052512341"));
		contactsDBM.add(new Supplier("4", "", "", "", false), new Contact("4", "Idan", "Izcovitch", "053512341"));

		addProduct(new Supplier("1", "", "", "", false), new ProvidedProduct("Milk 1L", "Tara", "2", "1", "1",1));
		addProduct(new Supplier("1", "", "", "", false), new ProvidedProduct("Koteg 500gr", "Tnuva", "3", "1", "2",1));
		addProduct(new Supplier("1", "", "", "", false), new ProvidedProduct("Koteg 100gr", "Tnuva", "1", "1", "3",1));
		addProduct(new Supplier("1", "", "", "", false), new ProvidedProduct("Danny 100gr", "Tara", "3", "1", "4",1));
		addProduct(new Supplier("1", "", "", "", false), new ProvidedProduct("Bamba 100gr", "Osem", "1.5", "1", "5",1));
		addProduct(new Supplier("1", "", "", "", false), new ProvidedProduct("Bamba 100gr", "Shush", "1", "1", "6",1));
		addProduct(new Supplier("1", "", "", "", false), new ProvidedProduct("Bisly 100gr", "Osem", "1.5", "1", "7",1));
		addProduct(new Supplier("1", "", "", "", false), new ProvidedProduct("Salami 200gr", "Zogloveck", "2", "1", "8",1));


		addProduct(new Supplier("2", "", "", "", false), new ProvidedProduct("Milk 1L", "Tara", "2", "1", "1",2));
		addProduct(new Supplier("2", "", "", "", false), new ProvidedProduct("Koteg 500gr", "Tnuva", "3", "1", "2",2));
		addProduct(new Supplier("2", "", "", "", false), new ProvidedProduct("Koteg 100gr", "Tnuva", "1", "1", "3",2));
		addProduct(new Supplier("2", "", "", "", false), new ProvidedProduct("Danny 100gr", "Tara", "3", "1", "4",2));
		addProduct(new Supplier("2", "", "", "", false), new ProvidedProduct("Bamba 100gr", "Osem", "1.5", "1", "5",2));
		addProduct(new Supplier("2", "", "", "", false), new ProvidedProduct("Bamba 100gr", "Shush", "1", "1", "6",2));
		addProduct(new Supplier("2", "", "", "", false), new ProvidedProduct("Bisly 100gr", "Osem", "1.5", "1", "7",2));
		addProduct(new Supplier("2", "", "", "", false), new ProvidedProduct("Salami 200gr", "Zogloveck", "2", "1", "8",2));



		addSupplierDiscount("Milk 1L,Tara,2,50,25/05/2017,14/06/2017,10");
		addSupplierDiscount("Koteg 100gr,Tnuva,2,50,25/05/2017,14/06/2017,10");


		addProductInCategory("Milk 1L,Dairy Milk,Tara,4,5,1",1);
		addProductInCategory("Koteg 500gr,Dairy Cheese,Tnuva,6,1,1",1);
		addProductInCategory("Koteg 100gr,Dairy Cheese,Tnuva,3,2,1",1);
		addProductInCategory("Danny 100gr,Dairy Yogurt,Tara,6,2,1",1);
		addProductInCategory("Bamba 100gr,Snack,Osem,4,10,1",1);
		addProductInCategory("Bamba 100gr,Snack,Shush,3,5,1",1);
		addProductInCategory("Salami 200gr,Meat Sliced,Zogloveck,3,11,1",1);
		addProductInCategory("Bisly 100gr,Snack,Osem,4,10,1",1);

		addProductInCategory("Milk 1L,Dairy Milk,Tara,4,5,1",2);
		addProductInCategory("Koteg 500gr,Dairy Cheese,Tnuva,6,1,1",2);
		addProductInCategory("Koteg 100gr,Dairy Cheese,Tnuva,3,2,1",2);
		addProductInCategory("Danny 100gr,Dairy Yogurt,Tara,6,2,1",2);
		addProductInCategory("Bamba 100gr,Snack,Osem,4,10,1,1",2);
		addProductInCategory("Bamba 100gr,Snack,Shush,3,5,1",2);
		addProductInCategory("Salami 200gr,Meat Sliced,Zogloveck,3,11,1",2);
		addProductInCategory("Bisly 100gr,Snack,Osem,4,10,1",2);

		addProductInCategory("Milk 1L,Dairy Milk,Tara,4,5,1",3);
		addProductInCategory("Koteg 500gr,Dairy Cheese,Tnuva,6,1,1",3);
		addProductInCategory("Koteg 100gr,Dairy Cheese,Tnuva,3,2,1",3);
		addProductInCategory("Danny 100gr,Dairy Yogurt,Tara,6,2,1",3);
		addProductInCategory("Bamba 100gr,Snack,Osem,4,10,1",3);
		addProductInCategory("Bamba 100gr,Snack,Shush,3,5,1",3);
		addProductInCategory("Salami 200gr,Meat Sliced,Zogloveck,3,11,1",3);
		addProductInCategory("Bisly 100gr,Snack,Osem,4,10,1",3);

		addProductInCategory("Milk 1L,Dairy Milk,Tara,4,5,1",4);
		addProductInCategory("Koteg 500gr,Dairy Cheese,Tnuva,6,1,1",4);
		addProductInCategory("Koteg 100gr,Dairy Cheese,Tnuva,3,2,1",4);
		addProductInCategory("Danny 100gr,Dairy Yogurt,Tara,6,2,1",4);
		addProductInCategory("Bamba 100gr,Snack,Osem,4,10,1",4);
		addProductInCategory("Bamba 100gr,Snack,Shush,3,5,1",4);
		addProductInCategory("Salami 200gr,Meat Sliced,Zogloveck,3,11,1",4);
		addProductInCategory("Bisly 100gr,Snack,Osem,4,10,1",4);*/

	}

	/**
	 * initiates the data base
	 * 
	 * @return data base
	 */
	public static DataBase getDataBase() 
	{
		if(DataBase.pli == null)
		{
			DBAccess dbAccess = Menu.dbAccess;
			DataBase.pli = new PLI(dbAccess);
		}
		if (db != null) {
			return db;
		} else {
			db = new DataBase();
			return db;
		}
	}

	public void setPLI(PLI pli){
		this.pli=pli;
	}



	/**
	 * Clears all records in table at the database. for testing use only.
	 * 
	 * @author Idan Izicovich
	 * @param dbManager
	 *            database table manager
	 * @return false case of SQL exception
	 */
	public boolean clearAllTables() {

		for (IDBManager dbManager : managers) {
			if (!dbManager.clearTable())
				return false;
		}

		return true;
	}

	private boolean validateDate(String date) {
		String[] splitedDate = date.split("/");
		if (splitedDate.length != 3)
			return false;
		String day = splitedDate[0];
		String month = splitedDate[1];
		String year = splitedDate[2];
		if (!checkBetween(1, 31, day) && !checkBetween(1, 12, month) && !checkBetween(0, 2017, year))
			return false;
		return true;

	}

	private boolean checkBetween(int start, int end, String string) {
		int number = Integer.parseInt(string);
		return start <= number && number <= end;
	}


	public boolean addSupplier(Supplier newSupplier) {
		return supplierDBM.addSupplier(newSupplier);
	}

	public List<Supplier> getAllSuppliers() {
		return supplierDBM.getAll();
	}

	public Supplier getSupplierById(String id) {
		return supplierDBM.getByID(id);
	}

	public boolean updateSupplier(Supplier s, String fieldName, String input) {
		return supplierDBM.update(s, fieldName, input);
	}

	public boolean updateContact(Supplier s, Contact c, String fieldName, String input) {
		return contactsDBM.update(s, c, fieldName, input);
	}

	public boolean addContact(Supplier s, Contact newcontact) {
		return contactsDBM.add(s, newcontact);
	}

	public List<ProvidedProduct> getAllProducts(Supplier s) {
		return productSupplierDBM.getAll(s);
	}

	public boolean addProduct(Supplier s, ProvidedProduct product) {
		return  productSupplierDBM.add(s, product);
	}

	public boolean updateProductPrice(Supplier s, ProvidedProduct product, String input) {
		return productSupplierDBM.update(s, product, "price", input);
	}

	public ProvidedProduct getProvidedProductByNameManufacturerAndSupplier(String productName,String manufacturer,String supplierD) {
		return productSupplierDBM.getProvidedProductByNameManufacturerAndSupplier(productName,manufacturer,supplierD);
	}

	public List<Order> getAllOrders(Supplier s) {
		return supplierOrderDBM.getAll(s);
	}

	public int orderPrice(Order selected) {
		return ((SupplierDiscountTable)(supplierDiscountTable)).CalculatePrice(selected);
	}

	/**
	 * Retrieve all contacts of a supplier
	 * 
	 * @author Idan Izicovich
	 * @param id
	 *            the id of the supplier
	 * @return a list of contacts, otherwise empty Array.
	 */
	public List<Contact> getAllContactsBySupplierID(String id) {
		return getSupplierById(id).getContacts();
	}

	/**
	 * <b>Idan: <i>some improvements</i></b> 2017-04-04<br>
	 * making the function simpler to maintain using collection of managers
	 */
	public void close() {


		for (IDBManager manager : this.managers) {
			manager.closeConnection();
		}

		// supplierDBM.closeConnection();
		// contactsDBM.closeConnection();
		// productDBM.closeConnection();
		// productSupplierDBM.closeConnection();
		// discountDBM.closeConnection();
		// supplierOrderDBM.closeConnection();
		// ordersDBM.closeConnection();
	}

	public void insertHardCodedDefaultData() {
		for (IDBManager manager : this.managers) {
			manager.insertHardCodedDefaultData();
		}
	}

	public void printGeneralInventoryReport(int shopID){
		((ProductTable) this.productTable).printGeneralInventoryReport(shopID);
		System.out.println();
		System.out.println();
	}

	public boolean printSpecificCategorizedReport(String catName,int shopID){
		try{
			if(!checkIfCategoryNameExists(catName)){
				return false;
			}
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT P.NAME,P.MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP AS P JOIN (SELECT * "
					+ " FROM PRODUCT_CATEGORIES"
					+ " WHERE CATEGORY_NAME LIKE '"+catName+"%' AND SHOP_ID = '"+shopID+"') AS PC"
					+ " WHERE P.NAME = PC.PRODUCT_NAME AND P.MANUFACTURER = PC.MANUFACTURER AND P.SHOP_ID = '"+shopID+"'"
					+ " GROUP BY P.NAME,P.MANUFACTURER");


			System.out.println("---------------Products in Superli---------------");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Product [ Name = " + rs.getString("P.NAME")+", Manufacturer = " + rs.getString("P.MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
			}

			System.out.println();
			System.out.println();

			rs  = stmt.executeQuery("SELECT P.NAME,P.MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP AS P JOIN (SELECT * "
					+ " FROM PRODUCT_CATEGORIES"
					+ " WHERE CATEGORY_NAME LIKE '"+catName+"%' AND SHOP_ID = '"+shopID+"') AS PC"
					+ " WHERE P.NAME = PC.PRODUCT_NAME AND P.MANUFACTURER = PC.MANUFACTURER AND P.LOCATION = 'WAREHOUSE'  AND P.SHOP_ID = '"+shopID+"'"
					+ " GROUP BY P.NAME,P.MANUFACTURER");


			System.out.println("---------------Products in warehouse---------------");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Product [ Name = " + rs.getString("P.NAME")+", Manufacturer = " + rs.getString("P.MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
			}

			System.out.println();
			System.out.println();

			rs  = stmt.executeQuery("SELECT P.NAME,P.MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP AS P JOIN (SELECT * "
					+ " FROM PRODUCT_CATEGORIES"
					+ " WHERE CATEGORY_NAME LIKE '"+catName+"%' AND SHOP_ID = '"+shopID+"') AS PC"
					+ " WHERE P.NAME = PC.PRODUCT_NAME AND P.MANUFACTURER = PC.MANUFACTURER AND P.LOCATION = 'STORE'  AND P.SHOP_ID = '"+shopID+"'"
					+ " GROUP BY P.NAME,P.MANUFACTURER");


			System.out.println("---------------Products in store---------------");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Product [ Name = " + rs.getString("P.NAME")+", Manufacturer = " + rs.getString("P.MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
			}
			System.out.println();
			System.out.println();
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return true;
	}

	public boolean checkIfCategoryNameExists(String cat){
		ArrayList<Category> allCat =((CategoryTable) categoryTable).getAllCategories();
		for (Category category : allCat) {
			if(cat.equals(category.getCategoryName()))
				return true;
		}
		return false;
	}

	public void closeConnection(){
		productTable.closeConnection();
		categoryTable.closeConnection();
		productDiscountTable.closeConnection();
		categoryDiscountTable.closeConnection();
		productInCategoryTable.closeConnection();
	}

	public void printAllCategories() {
		ArrayList<Category> allCat =((CategoryTable) categoryTable).getAllCategories();
		for (Category category : allCat) {
			System.out.println(category.getCategoryName());	
		}
		System.out.println();
	}

	public boolean printMultipleCategorizedReport(String nextLine,int shopID) {
		String[] tmp = nextLine.split(",");
		HashSet<String> allCategories = new HashSet<String>();
		HashSet<String> toRemove = new HashSet<String>();
		for (String string : tmp) {
			if(checkIfCategoryNameExists(string)){
				allCategories.add(string);
			}
			else{
				System.out.println("Category "+string+" does'nt exist. Enter any key to continue to main menu");
				return false;
			}
		}
		for (String cat : allCategories) 
		{
			for (String otherCat : allCategories) 
			{
				if(!otherCat.equals(cat) && otherCat.contains(cat))
				{
					toRemove.add(otherCat);
				}
			}
		}
		for (String string : toRemove) {
			allCategories.remove(string);
		}
		try{
			Statement stmt  = conn.createStatement();
			ResultSet rs;
			System.out.println("---------------Products in Superli---------------");
			for (String catName : allCategories){
				rs  = stmt.executeQuery("SELECT P.NAME,P.MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
						+ " FROM PRODUCTS_IN_SHOP AS P JOIN (SELECT * "
						+ " FROM PRODUCT_CATEGORIES"
						+ " WHERE CATEGORY_NAME LIKE '"+catName+"%' AND SHOP_ID = '"+shopID+"') AS PC"
						+ " WHERE P.NAME = PC.PRODUCT_NAME AND P.MANUFACTURER = PC.MANUFACTURER"
						+ " GROUP BY P.NAME,P.MANUFACTURER");

				// loop through the result set
				while (rs.next()) {
					System.out.println("Product [ Name = " + rs.getString("P.NAME")+", Manufacturer = " + rs.getString("P.MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
				}
			}
			System.out.println();
			System.out.println();

			System.out.println("---------------Products in warehouse---------------");
			for (String catName : allCategories){
				rs  = stmt.executeQuery("SELECT P.NAME,P.MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
						+ " FROM PRODUCTS_IN_SHOP AS P JOIN (SELECT * "
						+ " FROM PRODUCT_CATEGORIES"
						+ " WHERE CATEGORY_NAME LIKE '"+catName+"%' AND SHOP_ID = '"+shopID+"') AS PC"
						+ " WHERE P.NAME = PC.PRODUCT_NAME AND P.MANUFACTURER = PC.MANUFACTURER AND P.LOCATION = 'WAREHOUSE'"
						+ " GROUP BY P.NAME,P.MANUFACTURER");


				// loop through the result set
				while (rs.next()) {
					System.out.println("Product [ Name = " + rs.getString("P.NAME")+", Manufacturer = " + rs.getString("P.MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
				}
			}

			System.out.println();
			System.out.println();

			System.out.println("---------------Products in store---------------");
			for (String catName : allCategories){
				rs  = stmt.executeQuery("SELECT P.NAME,P.MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
						+ " FROM PRODUCTS_IN_SHOP AS P JOIN (SELECT * "
						+ " FROM PRODUCT_CATEGORIES"
						+ " WHERE CATEGORY_NAME LIKE '"+catName+"%' AND SHOP_ID = '"+shopID+"') AS PC"
						+ " WHERE P.NAME = PC.PRODUCT_NAME AND P.MANUFACTURER = PC.MANUFACTURER AND P.LOCATION = 'STORE'"
						+ " GROUP BY P.NAME,P.MANUFACTURER");

				// loop through the result set
				while (rs.next()) {
					System.out.println("Product [ Name = " + rs.getString("P.NAME")+", Manufacturer = " + rs.getString("P.MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
				}
			}
			System.out.println();
			System.out.println();
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return true;
	}

	public void printDamagedReport(int shopID) {
		((ProductTable) this.productTable).printDamagedReport(shopID);
	}

	public boolean addProductsToInventory(String input,int shopID)
	{
		String productName;
		String manufacturer;
		String expDate;
		int amount;

		if(!checkValidInputInsertProduct(input))
		{
			System.out.println("invalid input, please try again");
			return false;
		}
		String[] arr = input.split(",");
		productName = arr[0];
		manufacturer = arr[1];
		expDate = arr[2];
		amount = Integer.parseInt(arr[3]);

		if(((ProductInCategoryTable)productInCategoryTable).checkIfExist(productName,manufacturer))
		{
			return ((ProductTable)productTable).addProductsToInventory(productName,manufacturer,expDate,amount,shopID);
		}
		else
		{
			System.out.println("This product is new, please enter category for product and more info before");
			return false;
		}
	}

	public boolean checkValidInputInsertProduct(String input) 
	{
		String[] arr = input.split(",");
		if(arr.length != 4)
			return false;
		//check valid amount
		if(!((ProductTable) productTable).checkIfIsNumber(arr[3]))
		{
			return false;
		}
		//if exp date is '-', there is no exp date.
		if(!(((ProductTable) productTable).checkIfIsDate(arr[2]) || arr[2].equals("-")))
		{
			return false;
		}

		return true;
	}

	public boolean checkValidInputInsertProductDiscount(String input) 
	{
		String[] arr = input.split(",");
		if(arr.length != 5)
		{
			System.out.println("Invalid input format");
			return false;
		}
		//check if product and manufacturer exists
		if(!((ProductInCategoryTable)productInCategoryTable).checkIfExist(arr[0],arr[1]))
		{
			System.out.println("Product doesn't exists");
			return false;
		}
		//check valid discount
		if(!((ProductTable) productTable).checkIfIsNumber(arr[2]))
		{
			System.out.println(arr[2]+ " is not a number");
			return false;
		}
		//check start date
		if(!((ProductTable) productTable).checkIfIsDate(arr[3]))
		{
			System.out.println(arr[3]+ " is not a valid date format");
			return false;
		}
		//check end date
		if(!((ProductTable) productTable).checkIfIsDate(arr[4]))
		{
			System.out.println(arr[4]+ " is not a valid date format");
			return false;
		}
		//check if end date is after start date
		if(!(((ProductDiscountTable)productDiscountTable).compareBetweenDates(arr[3],arr[4]) != 1))
		{
			System.out.println( "end date is before start date");
			return false;
		}
		if(!((ProductDiscountTable)productDiscountTable).checkCollision (arr[3],arr[4],arr[0],arr[1]))
		{
			System.out.println("Failed to add discount, Discount for this product already exists in this dates.");
			return false;
		}

		return true;
	}
	public boolean checkValidInputInsertCategoryDiscount(String input) 
	{
		String[] arr = input.split(",");
		if(arr.length != 4)
		{
			System.out.println("Invalid input format");
			return false;
		}
		//check if category exists
		if(!((CategoryTable)categoryTable).checkIfExist(arr[0]))
		{
			System.out.println("Category doesn't exists");
			return false;
		}
		//check valid discount
		if(!((ProductTable) productTable).checkIfIsNumber(arr[1]))
		{
			System.out.println(arr[1]+ " is not a number");
			return false;
		}
		//check start date
		if(!((ProductTable) productTable).checkIfIsDate(arr[2]))
		{
			System.out.println(arr[2]+ " is not a valid date format");
			return false;
		}
		//check end date
		if(!((ProductTable) productTable).checkIfIsDate(arr[3]))
		{
			System.out.println(arr[3]+ " is not a valid date format");
			return false;
		}
		//check if end date is after start date
		if(!(((ProductDiscountTable)productDiscountTable).compareBetweenDates(arr[2],arr[3]) != 1))
		{
			System.out.println( "end date is before start date");
			return false;
		}
		//check collision with more discount
		if(!((CategoryDiscountTable)categoryDiscountTable).checkCollision(arr[2],arr[3],arr[0]))
		{
			System.out.println("Failed to add discount, Discount for this category already exists in this dates.");
			return false;
		}

		return true;
	}

	public boolean addCategoryDiscount(String input) {
		if(!checkValidInputInsertCategoryDiscount(input))
			return false;
		String[] arr = input.split(",");
		return categoryDiscountTable.insertTable(new CategoryDiscount(arr[0], Integer.parseInt(arr[1]), arr[2], arr[3]));
	}

	public boolean addProductDiscount(String input)
	{
		if(!checkValidInputInsertProductDiscount(input))
			return false;
		String[] arr = input.split(",");
		return productDiscountTable.insertTable(new ProductDiscount(arr[0], arr[1], Integer.parseInt(arr[2]), arr[3], arr[4]));
	}


	public void makeUrgentOrder(ProductInCategory p,int shopID) 
	{
		int todayDayNumber = getNumberByDay(LocalDateTime.now().getDayOfWeek().toString());
		//get the closest order by the product and today's number 
		Order order = getCloseOrderByProdutAndDay(p, todayDayNumber,shopID);
		int numOfDaysUntilDelievery = 7;
		if(order != null)
		{
			numOfDaysUntilDelievery = 7;
		}
		//get the closest delivery day by today's number.
		int dailyConsumption = p.getDailyConsumption();
		int amountInOrder = dailyConsumption * numOfDaysUntilDelievery;
		if(amountInOrder > 0)
		{
			//get the best supplier(lower price after discount) by product and amount.
			ProvidedProduct providedProduct = new ProvidedProduct(p.getProduct(), p.getManufacturer(), "-1","-1", "-1", p.getWeight());
			Supplier supplier = ((SupplierDiscountTable) supplierDiscountTable).getBestSupplier(providedProduct,amountInOrder,shopID,dailyConsumption,null);
			if(supplier == null)
				return;
			providedProduct = productSupplierDBM.getProvidedProductByNameManufacturerAndSupplier
					(p.getProduct(),p.getManufacturer(),supplier.getID());
			//create order by product,amount,supplier and date.
			Order urgentOrder = createUrgentOrder(providedProduct,amountInOrder, supplier,shopID,dailyConsumption,false);
			if(urgentOrder != null)
			{
				ordersDBM.addOrder(urgentOrder);
				printOrderForm(urgentOrder);
			}
			else
				System.out.println("There is no available transportation for this order");
		}
	}

	/**
	 * After updating an order(adding amount or adding new product to the order 
	 * we create urgent order to complete the order for this week.
	 * @param providedProduct
	 * @param shopID
	 * @param amount
	 */
	public void makeOrderAfterUpdate(ProvidedProduct  providedProduct,int shopID,int amount)
	{
		Supplier supplier = ((SupplierDiscountTable) supplierDiscountTable).getBestSupplier(providedProduct,amount,shopID,0,null);
		//create order by product,amount,supplier and date.
		providedProduct.setSupplierID(supplier.getID());
		//daily consumption is 0 because the number of products is not changed(this order is just after updating order details)
		Order urgentOrder = createUrgentOrder(providedProduct,amount, supplier,shopID,0,false);
		if(urgentOrder != null)
		{
			ordersDBM.addOrder(urgentOrder);
			printOrderForm(urgentOrder);
		}
		else
		{
			System.out.println("The order is cancelled");
			PrintShortageReport(shopID);
		}

	}

	public void printOrder(String orderNum,int shopID) {
		printOrderForm(ordersDBM.getByOrderNumber(orderNum,shopID));	
	}

	private void printOrderForm(Order order) 
	{
		System.out.println("Order Details");
		System.out.println("Order Number : "+order.getOrderNumber());
		System.out.println("Supplier Name : "+order.getSuplier().getName());
		System.out.println("Supplier Number :"+ order.getSuplier().getID());
		System.out.println("Address : "+ this.shopsTable.getAddressByID(order.getShopID()));
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate localDate = LocalDate.now();
		System.out.println("Order Date : "+ dtf.format(localDate));
		int numberOfCotacts = order.getSuplier().getContacts().size();
		if(numberOfCotacts == 0)
			System.out.println("There are no contacts for this suppliers");
		else
		{
			int contactIndex = (int) (Math.random() * (numberOfCotacts - 1));
			System.out.println("Contact : "+ order.getSuplier().getContacts().get(contactIndex).getPhoneNumber());
		}
		System.out.println();
		Map<ProvidedProduct, Integer> products = order.getProducts();
		for (ProvidedProduct pd : products.keySet()) {
			System.out.println("Catalog Number : "+pd.getCatalogNumber());
			System.out.println("Product Name : "+pd.getProduct_name());
			System.out.println("Manufacturer : " +pd.getManufacturer());
			System.out.println("Amount : " +products.get(pd));
			System.out.println("Price before discount : "+pd.getCostPrice() );
			System.out.println("Price after discount : " + getSellPriceAfterSupplierDiscount(pd.getProduct_name(),pd.getManufacturer(),pd.getSupplierID(),products.get(pd)));
		}
	}
	private double getSellPriceAfterSupplierDiscount(String product_name, String manufacturer, String supplierID,int amount) {
		return ((SupplierDiscountTable)supplierDiscountTable).getSellPriceAfterSupplierDiscount(product_name, manufacturer, supplierID, amount);
	}

	/**
	 * create urgent order
	 * @param p
	 * @param amountInOrder
	 * @param supplier
	 * @return urgent order that doesn't have delivery days.
	 */
	public Order createUrgentOrder(ProvidedProduct p, int amountInOrder, Supplier supplier, int ShopID,int dailyConsumption,boolean AfterChangeDriverShift) 
	{
		Order o = new Order();
		o.setDays(new ArrayList<Integer>());
		String orderNumber = (ordersDBM.getMaxOrderNumber() + 1) +"";
		o.setOrderNumber(orderNumber);
		o.setSuplier(supplier);
		o.setShopID(ShopID);
		Map<ProvidedProduct, Integer> products = new HashMap<ProvidedProduct, Integer>();
		products.put(p, amountInOrder);
		o.setProducts(products);
		if(supplier.isProvideTransport())
			return o;
		else
		{
			//check with delievery when is the closest delievery for this shopID
			ArrayList<Integer> supplierSupplyDays = supplier_Supply_DaysTable.getSupplyDays(supplier.getID()); 
			Calendar cal =  Calendar.getInstance();
			int Day_In_Week = cal.get(Calendar.DAY_OF_WEEK);
			int i = Day_In_Week + 1;
			HashMap<Order, Integer> orderByDays = new HashMap<Order, Integer>();
			int counter = 1;
			while((i % 7) != (Day_In_Week % 7))
			{
				if(supplierSupplyDays.contains(i % 7 + 1))
				{
					Order toAdd = new Order();
					Map<ProvidedProduct, Integer> newDayProducts = new HashMap<ProvidedProduct, Integer>();
					newDayProducts.put(p, dailyConsumption * (i - Day_In_Week));
					toAdd.setProducts(newDayProducts);
					toAdd.setSuplier(supplier);
					toAdd.setOrderNumber(orderNumber);
					toAdd.setDays(new ArrayList<Integer>());
					orderByDays.put(toAdd,i);
				}
				i++;
			}
			Order ans = pli.AddUrgentOrderToDelivery(orderByDays); //Ikutiel function
			if(ans != null)
				return ans;
			if(ans == null && !AfterChangeDriverShift)
				return NoDelieveryForOrderMenu(p, amountInOrder, supplier, ShopID, dailyConsumption, supplierSupplyDays);
		}
		return null;
	}
	/**
	 * This function is a menu in case there is an urgent order
	 * and it is not possible to deliver it to the shop.
	 * @param p
	 * @param amountInOrder
	 * @param supplier
	 * @param ShopID
	 * @param dailyConsumption
	 */
	public Order NoDelieveryForOrderMenu(ProvidedProduct p, int amountInOrder, Supplier supplier, int ShopID,int dailyConsumption, ArrayList<Integer> ordersDay) 
	{
		Scanner sc =new Scanner(System.in);
		System.out.println("There is no available delievery for this order");
		System.out.println("1.Cancel the Order.");
		System.out.println("2.Change Drivers Shifts");
		String option =sc.nextLine();
		if(option.equals("1"))
			return null;
		if(option.equals("2"))
		{
			String orderNumber = (ordersDBM.getMaxOrderNumber() + 1) +"";
			HashMap<Order, Integer> orderByDays = new HashMap<Order, Integer>();
			for (Integer integer : ordersDay)
			{
				Order toAdd = new Order();
				Map<ProvidedProduct, Integer> newDayProducts = new HashMap<ProvidedProduct, Integer>();
				newDayProducts.put(p, dailyConsumption * integer);
				toAdd.setProducts(newDayProducts);
				toAdd.setSuplier(supplier);
				toAdd.setOrderNumber(orderNumber);
				toAdd.setDays(new ArrayList<Integer>());
				toAdd.setShopID(ShopID);
				orderByDays.put(toAdd, integer);
			}
			if(pli.changeDriversShift(orderByDays)) //Ikutiel and Tomer function
				return createUrgentOrder(p, amountInOrder, supplier, ShopID, dailyConsumption,true);
		}
		return null;
	}
	/**
	 * search the closest order for the product p by today day.
	 * @param ProductInCategory p
	 * @param int todayDayNumber
	 * @return closest order for a specific product.
	 */
	private Order getCloseOrderByProdutAndDay(ProductInCategory p, int todayDayNumber,int shopID) {
		Order ans = null;
		int minNumOfDaysUntilDelievery = 7;
		ArrayList<Order> allOrdersThatHasSpecificProduct = ordersDBM.getAllOrdersByProductNameAndManufacturer
				(p.getProduct(),p.getManufacturer(),shopID);
		ArrayList<Order> getAllPermanentOrders = orderDaysDBM.getPermanentOrders(allOrdersThatHasSpecificProduct);
		for (Order order : getAllPermanentOrders) {


			if(minNumOfDaysUntilDelievery > getNumOfDaysUntilCloseDelievery(order,todayDayNumber))
			{
				ans = order;
				minNumOfDaysUntilDelievery = getNumOfDaysUntilCloseDelievery(order,todayDayNumber);
			}
		}		
		return ans;
	}


	private int getNumOfDaysUntilCloseDelievery(Order order, int todayDayNumber) 
	{
		int minNumOfDaysUntilDelievery = 7;
		for (int delieveryDay : order.getDeliveyDays()) 
		{
			int numOfDaysUntilDelievery = delieveryDay - todayDayNumber;
			if( numOfDaysUntilDelievery < 0)
			{
				numOfDaysUntilDelievery = 7 + numOfDaysUntilDelievery;
			}
			minNumOfDaysUntilDelievery = Math.min(numOfDaysUntilDelievery, minNumOfDaysUntilDelievery);
		}
		return minNumOfDaysUntilDelievery;
	}

	private int getNumberByDay(String dayInWeek)
	{
		switch (dayInWeek.toLowerCase()) 
		{
		case "sunday":  return 1;
		case "monday":  return 2;
		case "tuesday":  return 3;
		case "wednesday":  return 4;
		case "thursday":  return 5;
		case "friday":  return 6;
		case "saturday":  return 7;
		}
		return -1;
	}

	public boolean removeProductById(String id) {
		if(!((ProductTable)productTable).checkIfExistByID(id)){
			return false;
		}
		//check if product got to minimal
		Product p = ((ProductTable)productTable).getProductById(id);
		try{
			Statement stmt  = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT NAME,MANUFACTURER,SHOP_ID FROM PRODUCTS_IN_SHOP WHERE ID = '"+id+"'" );
			rs.next();
			String Name = rs.getString("Name");
			String MANUFACTURER = rs.getString("MANUFACTURER");
			int shopID = rs.getInt("SHOP_ID");

			Statement stmt2  = conn.createStatement();
			ResultSet rs2  = stmt2.executeQuery
					("SELECT MINIMAL_AMOUNT FROM PRODUCT_CATEGORIES WHERE SHOP_ID = '"+shopID+"' AND MANUFACTURER = '"+MANUFACTURER+"'"
							+ " AND PRODUCT_NAME = '"+Name+ "'");
			rs2.next();
			int MINIMAL_AMOUNT = rs2.getInt("MINIMAL_AMOUNT");
			Statement stmt3  = conn.createStatement();
			ResultSet rs3  = stmt3.executeQuery
					("SELECT COUNT(*) AS NUMBER_OF_PRODUCTS FROM PRODUCTS_IN_SHOP WHERE SHOP_ID = '"+shopID+"' AND MANUFACTURER = '"+MANUFACTURER+"'"
							+ " AND Name = '"+Name+ "'");
			rs3.next();
			int NUMBER_OF_PRODUCTS = rs3.getInt("NUMBER_OF_PRODUCTS");
			if(Name.equals(p.getName()) && MANUFACTURER.equals(p.getManufacturer()))
			{
				if (NUMBER_OF_PRODUCTS-1== MINIMAL_AMOUNT)
				{
					System.out.println("Got to minimal amount");
					ProductInCategory pic = ((ProductInCategoryTable) productInCategoryTable).getProductInCategoryByNameManufacturerAndShopID(p.getName(),p.getManufacturer(),shopID);
					makeUrgentOrder(pic,shopID);
				}
			}

		}
		catch(SQLException e)
		{
		}
		((ProductTable)productTable).removeByID(id);
		return true;
	}


	public Product getProductById(String id){
		return ((ProductTable)productTable).getProductById(id);
	}

	/**
	 * Compares between date1 and date2. 
	 * If date1 > date2 --> return 1
	 * If date1 < date2 --> return -1
	 * If date1 == date2 --> return 0
	 * @param date1
	 * @param date2
	 * @return
	 */
	private int compareDate(String date1,String date2){
		String[] date1Parsed = date1.split("/");
		String[] date2Parsed = date2.split("/");
		int[] date1Int = new int[3];
		int[] date2Int = new int[3];
		for(int i=2; i>=0 ;i--){
			date1Int[i]= Integer.parseInt(date1Parsed[i]);
			date2Int[i]= Integer.parseInt(date2Parsed[i]);
			if(date1Int[i]>date2Int[i]){
				return 1;
			}
			if(date1Int[i]<date2Int[i]){
				return -1;
			}
		}
		return 0;
	}

	public boolean addCategory(String input) 
	{
		if(!checkValidInputInsertCategory(input))
			return false;
		String[] arr = input.split(",");
		return categoryTable.insertTable(new Category(arr[0], arr[1]));
	}

	private boolean checkValidInputInsertCategory(String input) 
	{
		String[] arr = input.split(",");
		if(arr.length != 2)
		{
			System.out.println("Invalid input format");
			return false;
		}
		//check if category exists
		if(((CategoryTable)categoryTable).checkIfExist(arr[0]))
		{
			System.out.println("Category already exists");
			return false;
		}
		//check if parent category exists
		if(!((CategoryTable)categoryTable).checkIfExist(arr[1]))
		{
			//if parent category is '-' there is no parent category for this category
			if(!arr[1].equals("-"))
			{
				System.out.println("Parent Category doesn't exists");
				return false;
			}
		}
		//check if parent category is substring of the sub category.
		if(!arr[1].equals("-"))
		{
			if(!arr[0].contains(arr[1]))
			{
				System.out.println("Parent Category must be prefix of the sub category");
				return false;
			}
		}

		return true;
	}

	public boolean checkIfExistByID(String id) {
		if(!((ProductTable)productTable).checkIfExistByID(id))
			return false;
		return true;
	}

	public boolean updateStatusById(int id,boolean isDamaged,String val){
		if(!((ProductTable)productTable).updateStatusById(id,isDamaged,val))
			return false;
		return true;
	}
	public boolean addProductInCategory(String input, int shopID) {
		//if(!checkValidInputInsertProductInCategory(input))
		//	return false;
		String[] arr = input.split(",");
		String productName = arr[0];
		String category = arr[1];
		String manufacturer = arr[2];
		double sellPrice = Double.parseDouble(arr[3]);
		int dailyConsumption = Integer.parseInt(arr[4]);
		int weight = Integer.parseInt(arr[5]);
		//get the static order for this product
		ArrayList<Order> allPermanentOrders =  getPermanentOrdersByProductNameAndManufacturer(productName,manufacturer,shopID);
		int avgDeliveryTimeInDays = getAvgDeliveryTimeInDays(allPermanentOrders);
		int minimalAmount = avgDeliveryTimeInDays * dailyConsumption;
		return productInCategoryTable.insertTable(new ProductInCategory(productName, category, manufacturer, sellPrice, minimalAmount, dailyConsumption , weight , shopID));
	}

	public Map<ProvidedProduct,Integer> inserNewOrder(String orderdProducts,int shopID) {
		String [] products = orderdProducts.split(",");
		Map<ProvidedProduct,Integer> wantedProducts = new HashMap<ProvidedProduct,Integer>();
		for (String string : products) {
			String[] arr = string.split(":");
			if(!checkValidInputInsertNewOrder(string)){
				System.out.println("Invalid input format");
				return null;
			}
			ProvidedProduct toAdd = new ProvidedProduct(arr[0],arr[1],"0","-1","-1",-1);
			wantedProducts.put(toAdd,Integer.parseInt(arr[2]));
		}
		return wantedProducts;
	}

	private boolean checkValidInputInsertNewOrder(String orderdProducts) {
		String[] arr = orderdProducts.split(":");
		if(arr.length != 3)
		{
			System.out.println("Invalid input format");
			return false;
		}
		//check if product exists with the suppliers
		if(!productSupplierDBM.checkIfExist(arr[0],arr[1]))
		{
			System.out.println("Suppliers don't have product "+arr[0]+" "+arr[1]);
			return false;
		}
		//check if amount is entered corectly
		if(!((ProductTable) productTable).checkIfIsNumber(arr[2]))
		{
			System.out.println(arr[2]+ " is not a number");
			return false;
		}
		return true;
	}

	private ArrayList<Order> getPermanentOrdersByProductNameAndManufacturer(String productName, String manufacturer,int shopID) {
		ArrayList<Order> allOrderForProduct = ordersDBM.getAllOrdersByProductNameAndManufacturer(productName, manufacturer,shopID);
		return orderDaysDBM.getPermanentOrders(allOrderForProduct);
	}

	private int getAvgDeliveryTimeInDays(ArrayList<Order> allOrder) 
	{
		int sum = 1;
		for (Order order : allOrder) {
			sum = sum + order.getDeliveyDays().size();
		}
		return 7 /sum;
	}

	private boolean checkValidInputInsertProductInCategory(String input)
	{
		String[] arr = input.split(",");
		if(arr.length != 5)
		{
			System.out.println("Invalid input format");
			return false;
		}
		//check if product exists
		if(((ProductInCategoryTable)productInCategoryTable).checkIfExist(arr[0],arr[2]))
		{
			System.out.println("product already exists");
			return false;
		}
		//check if exists supplier for this product
		if(!productSupplierDBM.checkIfExistsSupplierForProduct(arr[0],arr[2]))
		{
			System.out.println("There is no supplier for this product.");
			return false;
		}
		//check if category exists
		if(!((CategoryTable) categoryTable).checkIfExist(arr[1]))
		{
			System.out.println("category doesn't exists.");
			return false;
		}
		//check sell price
		if(!((ProductInCategoryTable) productInCategoryTable).checkifIsDoubleAndPossitive(arr[3]))
		{
			System.out.println(arr[3]+ " is not a valid price");
			return false;
		}
		//check valid daily consumption
		if(!((ProductTable) productTable).checkIfIsNumber(arr[4]))
		{
			System.out.println(arr[4]+ " is not a number");
			return false;
		}
		return true;
	}

	public void printProductPrice(String input) 
	{
		if(checkValidInputPrintProductPrice(input))
		{
			String[] arr = input.split(",");
			String product = arr[0];
			String manufacturer = arr[1];
			String category = ((ProductInCategoryTable) productInCategoryTable).getCategoryByProductNameAndManufacturer(product,manufacturer);
			double sellPrice = ((ProductInCategoryTable) productInCategoryTable).getSellPriceByProductNameAndManufacturer(product,manufacturer);
			if(sellPrice <= 0)
			{
				System.out.println("An error accourd, sell price need to be possitive");
				return;
			}
			Calendar cal =  Calendar.getInstance();
			int currYear = cal.get(Calendar.YEAR);
			int currMonth = cal.get(Calendar.MONTH) ;
			int currDay = cal.get(Calendar.DAY_OF_MONTH);
			String currDate = currDay+"/"+currMonth+"/"+currYear;
			ArrayList<ProductDiscount> arrProductDiscount = ((ProductDiscountTable)productDiscountTable).getAllProductDiscountByProductNameAndManufacturerAndDate(product,manufacturer,currDate);
			int max = 0;
			for (ProductDiscount productDiscount : arrProductDiscount) 
			{
				if(productDiscount.getDiscount() > max)
					max = productDiscount.getDiscount();
			}
			ArrayList<CategoryDiscount> arrCategoryDiscount = ((CategoryDiscountTable) categoryDiscountTable).getAllCategoryDiscountByCategoryAndDate(category,currDate);
			for (CategoryDiscount categoryDiscount : arrCategoryDiscount) 
			{
				if(categoryDiscount.getDiscount() > max)
					max = categoryDiscount.getDiscount();
			}
			if(max > 0)
			{
				System.out.println("sell price before discount : "+sellPrice +", after discount : " + (sellPrice - sellPrice * max / 100));
			}
			else
			{
				System.out.println("sell price : "+sellPrice);
			}
		}
	}

	private boolean checkValidInputPrintProductPrice(String input) 
	{
		String[] arr = input.split(",");
		if(arr.length != 2)
		{
			System.out.println("Invalid input format");
			return false;
		}
		//check if product exists
		if(!((ProductInCategoryTable)productInCategoryTable).checkIfExist(arr[0],arr[1]))
		{
			System.out.println("product doesn't exists");
			return false;
		}
		return true;
	}

	public boolean checkIfExistByNameAndManufacturer(String product){
		String[] productParse= product.split(",");
		if(productParse.length!=2){
			System.out.println("Incorrect format!");
			return false;
		}
		if(((ProductInCategoryTable)productInCategoryTable).checkIfExist(productParse[0],productParse[1]))
			return true;
		System.out.println(product+" does'nt exist in the system");
		return false;
	}

	/*
	 * if is number return true
	 */
	public boolean checkIfIsPrice(String toCheck)
	{
		try{Double.parseDouble(toCheck);}
		catch(Exception e){
			return false;
		}
		return true;
	}

	public boolean updatePriceByNameAndManufacturer(String product, double updatePrice,boolean isSell){
		if(updatePrice<=0){
			System.out.println("Invalid price!");
			return false;
		}
		String[] productParse= product.split(",");
		if(isSell)
		{
			if(((ProductInCategoryTable)productInCategoryTable).updatePriceByNameAndManufacturer(productParse[0],productParse[1],updatePrice))
				return true;
		}
		else{
		}

		System.out.println("Somthing went wrong please try again");
		return false;
	}

	public int getLastAddedId(){
		return ((ProductTable)productTable).getLastAddedId();
	}

	public boolean tearDown(int productId, String prodName,String prodManu,String catName){
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeUpdate("DELETE FROM PRODUCTS_IN_SHOP WHERE ID='"+productId+"'");
			stmt.executeUpdate("DELETE FROM CATEGORIES WHERE NAME='"+catName+"'");
			stmt.executeUpdate("DELETE FROM PRODUCT_CATEGORIES WHERE PRODUCT_NAME='"+prodName+"' AND MANUFACTURER='"+prodManu+"'");
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	public boolean addSupplierDiscount(String input) {

		if(!checkValidInputInsertSupplierDiscount(input))
			return false;
		String[] arr = input.split(",");
		String product = arr[0];
		String manufacturer = arr[1];
		String supplierID = arr[2];
		double discount = Double.parseDouble(arr[3]);
		int minimalAmount = Integer.parseInt(arr[6]);
		String startDate = arr[4];
		String endDate = arr[5];

		return supplierDiscountTable.insertTable(new SupplierDiscount
				(product, manufacturer, supplierID, discount, startDate,endDate,minimalAmount));
	}

	private boolean checkValidInputInsertSupplierDiscount(String input) {
		String[] arr = input.split(",");
		if(arr.length != 7)
		{
			System.out.println("Invalid input format");
			return false;
		}
		//check if product exists

		if(productSupplierDBM.getProvidedProductByNameManufacturerAndSupplier(arr[0],arr[1],arr[2])==null)
		{
			System.out.println("product doesn't exists");
			return false;
		}
		//check valid discount
		if(!((ProductTable) productTable).checkIfDouble(arr[3]))
		{
			System.out.println(arr[3]+ " is not a number");
			return false;
		}
		return true;
	}

	public boolean removeSupplierDiscount(String input) {
		if(!checkValidInputRemoveSupplierDiscount(input))
			return false;
		String[] arr = input.split(",");
		String product = arr[0];
		String manufacturer = arr[1];
		String supplierID = arr[2];
		int discount = Integer.parseInt(arr[3]);
		int minimalAmount = Integer.parseInt(arr[4]);
		String startDate = arr[5];
		String endDate = arr[6];
		return ((SupplierDiscountTable) supplierDiscountTable).removeFromTable(new SupplierDiscount
				(product, manufacturer, supplierID, discount, startDate,endDate,minimalAmount));
	}

	private boolean checkValidInputRemoveSupplierDiscount(String input) 
	{
		String[] arr = input.split(",");
		if(arr.length != 7)
		{
			System.out.println("Invalid input format");
			return false;
		}
		//check if supplier discount exists

		String product = arr[0];
		String manufacturer = arr[1];
		String supplierID = arr[2];
		int discount = Integer.parseInt(arr[3]);
		int minimalAmount = Integer.parseInt(arr[4]);
		String startDate = arr[5];
		String endDate = arr[6];

		if(!((SupplierDiscountTable)supplierDiscountTable).checkIfExist(new SupplierDiscount
				(product, manufacturer, supplierID, discount, startDate,endDate,minimalAmount)))
		{
			System.out.println("Supplier discount doesn't exists");
			return false;
		}
		return true;
	}

	public boolean checkIfCategoryDiscExist(String catName){
		return ((CategoryDiscountTable)categoryDiscountTable).checkIfDiscExist(catName);
	}

	public boolean checkIfProductDiscExist(String productName,String manufacturer){
		return ((ProductDiscountTable)productDiscountTable).checkIfDiscExist(productName,manufacturer);
	}

	public boolean removeDiscountByProductNameManufacturerStartTimeEndTime(String productName,String manufacturer,String startTime,String endTime){
		return ((ProductDiscountTable)productDiscountTable).removeDiscountByProductNameManufacturerStartTimeEndTime(productName,manufacturer, startTime, endTime);
	}

	public boolean removeDiscountByCategoryNameStartTimeEndTime(String catName,String startTime,String endTime){
		return ((CategoryDiscountTable)categoryDiscountTable).removeDiscountByCategoryNameStartTimeEndTime(catName, startTime, endTime);
	}

	public double getSellPriceByProductNameAndManufacturer(String product, String manufacturer){
		return ((ProductInCategoryTable)productInCategoryTable).getSellPriceByProductNameAndManufacturer(product, manufacturer);
	}

	public void printProductWithID(String input,int shopID) 
	{
		if(checkValidInputPrintProductPrice(input))
		{
			String[] arr = input.split(",");
			String product = arr[0];
			String manufacturer = arr[1];
			((ProductTable)this.productTable).printProductsByNameAndManufacturer(product, manufacturer,shopID);
		}
	}
	/*public boolean checkIfExistSupplierDiscount(String productName, String manufacturer, String supplierName){
		return ((SupplierDiscountTable)supplierDiscountTable).checkIfExist(productName, manufacturer, supplierName);
	}*/
	/*public Supplier getBestSupplierForProductAndAmount(ProductInCategory p, int amount){
		HashMap<ProvidedProduct, Integer> productToSupply = new HashMap<ProvidedProduct, Integer>();
		ProvidedProduct pp = new ProvidedProduct(p.getProduct(), p.getManufacturer(), "-1", "-1", "-1", -1);
		productToSupply.put(pp,amount);
		return ((SupplierDiscountTable)supplierDiscountTable).getIDOfBestSupplier(productToSupply);
	}*/


	public void printAllSuppliers()
	{
		List<Supplier> allSuppliers = supplierDBM.getAll();
		System.out.println("All Suppliers");
		for (Supplier supplier : allSuppliers) 
		{
			System.out.println("Name: "+ supplier.getName() +" , ID: "+supplier.getID());
		}
	}

	public void printAllProductsInCategory(int shopID) {
		ArrayList<ProductInCategory> allProductsInCategory = ((ProductInCategoryTable) productInCategoryTable).getAllProductsInCategory(shopID);
		System.out.println("All products in superLi");
		for (ProductInCategory productInCategory : allProductsInCategory) {
			System.out.println("Product Name And Manufacturer : "+ productInCategory.getProduct() +" "+productInCategory.getManufacturer());
		}
	}

	public ProductInCategory getProductInCategoryByNameManufacturerAndShop(String product,String manufacturer,int shopID)
	{
		return ((ProductInCategoryTable)productInCategoryTable).getProductInCategoryByNameManufacturerAndShopID(product, manufacturer, shopID);
	}
	public Double getCostPriceOfProduct(String productName,String manufacturer,String supplierID)
	{
		return productSupplierDBM.getCostPriceOfProduct(productName,manufacturer,supplierID);
	}
	public Map<ProvidedProduct, Integer> getAllProductsInOrder(String orderNumber)
	{
		return supplierOrderDBM.getProducts(orderNumber);
	}
	/**
	 * Get a new order id for a new order.
	 * @return unique order number that doesn't exists yet
	 */
	public String getNewOrderID() 
	{
		int tmp = ordersDBM.getMaxOrderNumber();
		tmp = tmp+1;
		return tmp +"";
	}

	public List<Integer> getOrderDaysByInput(String deliveryDays) {
		return orderDaysDBM.getDaysByInput(deliveryDays);

	}

	public boolean addNewOrder(Order newOrder) {
		return ordersDBM.addOrder(newOrder);
	}

	public void printAllOrders(int shopID)
	{
		ArrayList<Order> allOrders = ordersDBM.getAllOrders(shopID);
		for (Order order : allOrders) {
			System.out.println(order.toString());
		}
	}
	public boolean checkIfOrderExistsByOrderNumber(String orderNumber,int shopID)
	{
		if(ordersDBM.checkIfOrderExistsByOrderNumber(orderNumber,shopID))
			return true;
		System.out.println("Order number : "+orderNumber +" doesn't exists.");
		return false;
	}
	/**
	 * get the order by the order ID
	 * @param orderID
	 * @return Order for the order ID.
	 */
	public Order getOrderByOrderNumber(String orderID,int shopID) {
		return ordersDBM.getByOrderNumber(orderID,shopID);
	}
	public ArrayList<Integer> getSupplyDays(String supplierID)
	{
		return supplier_Supply_DaysTable.getSupplyDays(supplierID);
	}

	public boolean checkValidInputUpdateAmountOfProductInOrder(String input)
	{
		String[] arr = input.split(",");
		if(arr.length != 2)
		{
			System.out.println("Invalid input format");
			return false;
		}
		//check if amount is positive number
		String amount = arr[1];
		if(!checkIfIsPositiveNumber(amount))
			return false;
		return true;

	}
	public boolean checkIfIsPositiveNumber(String toCheck)
	{
		int check;
		try{check = Integer.parseInt(toCheck);}
		catch(Exception e)
		{
			System.out.println(toCheck +" is not a number");
			return false;
		}
		if(check <= 0)
		{
			System.out.println(toCheck +" is not a positive number");
			return false;
		}
		return true;
	}

	public boolean checkValidInputAddProductInOrder(String input) 
	{
		return checkValidInputUpdateAmountOfProductInOrder(input);
	}

	public ProvidedProduct getProvidedProductByCatalogNumberAndSupplier(String catalogNumber, String supplierID) 
	{
		return productSupplierDBM.getProvidedProductByCatalogNumberAndSupplier(catalogNumber, supplierID);
	}

	public boolean checkValidInputDeliveryDays(String dayToRemove) 
	{
		return orderDaysDBM.checkValidInputDeliveryDays(dayToRemove);
	}
	/*public Supplier getIDOfBestSupplier(Map<ProvidedProduct,Integer> products) 
	{
		return ((SupplierDiscountTable)supplierDiscountTable).getIDOfBestSupplier(products);
	}*/
	public boolean updateOrder(Order o)
	{
		return orderDaysDBM.updateOrder(o)&&ordersDBM.upadteOrder(o);
	}
	public List<Integer> getDeliveryDaysByOrderID(String orderID){
		return orderDaysDBM.getOrderDeliveryDays(orderID);
	}

	public void printAllCatalogID(String supplierID) {
		productSupplierDBM.printAllCatalogID(supplierID);
	}

	public boolean updateDailyConsumption(String dailyCons,int shopID) {
		String[] split = dailyCons.split(",");
		if(split.length != 3){
			System.out.println("Invalid input");
			return false;
		}
		if(!((ProductInCategoryTable)productInCategoryTable).checkIfExist(split[0],split[1])){
			System.out.println(split[0]+", "+split[1]+" doesnt exist");
			return false;
		}
		if(!isValidNumber(split[2])){
			System.out.println("Daily consumption needs to be a number!");
			return false;
		}
		((ProductInCategoryTable)productInCategoryTable).updateDailyConsumption(split[0],split[1],split[2], shopID);
		ArrayList<Order> allPermanentOrders =  getPermanentOrdersByProductNameAndManufacturer(split[0],split[1],shopID);
		int avgDeliveryTimeInDays = getAvgDeliveryTimeInDays(allPermanentOrders);
		int minimalAmount = avgDeliveryTimeInDays * Integer.parseInt(split[2]);
		((ProductInCategoryTable)productInCategoryTable).updateMinimalAmount(split[0],split[1],minimalAmount,shopID);
		return true;
	}
	private static boolean isValidNumber(String s) {
		try {
			int num = Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * <Yaniv> : check for shortage in the shop @shopID and print it
	 * @param shopID
	 */
	public void PrintShortageReport(int shopID)
	{
		ShortageReport sr = new ShortageReport();
		sr.shortageForShop(shopID);
		sr.printReport();
	}
	/**
	 * <Yaniv> : check for expired products in the shop @shopID and print a report
	 * @param shopID
	 */
	public void PrintExpiredDateReport(int shopID)
	{
		ExpiredReport er = new ExpiredReport();
		er.expiredProducts(shopID);
		er.printReport();
	}

	public static String getTodayDateFormat()
	{			
		Calendar cal =  Calendar.getInstance();
		int currYear = cal.get(Calendar.YEAR);
		int currMonth = cal.get(Calendar.MONTH) ;
		int currDay = cal.get(Calendar.DAY_OF_MONTH);
		String currDate = currDay+"/"+currMonth+"/"+currYear;
		return currDate;
	}
	public void cycleCount(int shopID)
	{
		System.out.println("---- General Inventory Report ----");
		System.out.println();
		printGeneralInventoryReport(shopID);
		System.out.println();
		PrintShortageReport(shopID);
		System.out.println();
	}
	public boolean validWantedSuperLi(String wantedSuperLi) {
		int wantedShop;
		try{wantedShop=Integer.parseInt(wantedSuperLi);}
		catch(Exception e)
		{
			return false;
		}
		if(((ShopsTable) this.shopsTable).checkExists(wantedShop)){
			return true;
		}
		return false;
	}

	public void showAllSuperLis() {
		this.shopsTable.printAllSuperLis();
	}

	public void insertNewShop(String nextLine) 
	{
		try
		{
			String[] arr = nextLine.split(",");
			this.shopsTable.insertTable(new Shop(-1, arr[0], arr[1], arr[2]));
		}
		catch(Exception e)
		{
			System.out.println("Invalid input");
		}
	}

	public boolean deleteShopById(String nextLine) {
		int shopID;
		try{shopID=Integer.parseInt(nextLine);}
		catch(Exception e)
		{
			System.out.println("invalid input");
			return false;
		}
		if(!this.shopsTable.checkExists(shopID)){
			System.out.println("shop dosent exist");
			return false;
		}
		this.shopsTable.deleteShopById(shopID);
		return true;
	}
	/**
	 * 
	 * Create list of orders, each order is for one supplier
	 * 
	 * @param wantedProducts
	 * @param shopID
	 * @return 
	 */
	public ArrayList<Order> CreateListOfOrdersByProducts(Map<ProvidedProduct, Integer> wantedProducts, int shopID,ArrayList<Integer> delieveryDays) 
	{

		for (ProvidedProduct pp : wantedProducts.keySet()) 
		{
			Supplier bestSupplierForProduct = ((SupplierDiscountTable) supplierDiscountTable).getBestSupplier(pp, wantedProducts.get(pp),shopID,0,delieveryDays);
			if(bestSupplierForProduct!=null)
				pp.setSupplierID(bestSupplierForProduct.getID());
			else return null;
		}
		ArrayList<Order> ordersForListOfProducts = ordersDBM.getListOfOrders(wantedProducts, delieveryDays, shopID);
		return ordersForListOfProducts;
	}

	public boolean addSupplierDays(SupplierSupplyDays supplyDays){
		return supplier_Supply_DaysTable.addSupplierDays(supplyDays);
	}
	public ArrayList<Order> getAllPermanentOrderThatNeedDelievery(int shopID)
	{
		ArrayList<Order> allOrders = ordersDBM.getAllOrders(shopID);
		ArrayList<Order> ans = new ArrayList<Order>();
		for (Order order : allOrders) 
		{
			if((!order.getSuplier().isProvideTransport()) 
					&& (orderDaysDBM.checkIfIsPermanentOrder(order.getOrderNumber())))
			{
				ans.add(order);
			}		
		}
		return ans;
	}

	public void compareDeliveryWithOrder(List<Order> orders,int shopID)
	{
		boolean found;
		int arrivedRatio;
		for (Order order : orders) {
			Order ourOrderToCompare = this.ordersDBM.getByOrderNumber(order.getOrderNumber(), shopID);
			if(ourOrderToCompare==null){
				System.out.println("Order number: "+order.getOrderNumber()+" is not in the system. Do not confirm this delivery!");
				continue;
			}
			for (ProvidedProduct expectedProduct : ourOrderToCompare.getProducts().keySet()) 
			{
				found = false;
				for (ProvidedProduct deliveredProduct : order.getProducts().keySet())
				{
					String catExpected = expectedProduct.getCatalogNumber();
					String catDelievery = deliveredProduct.getCatalogNumber();
					String supExpected = expectedProduct.getSupplierID();
					String supDelievery = deliveredProduct.getSupplierID();
					if(catExpected.equals(catDelievery) &&
							supExpected.equals(supDelievery))
					{
						found=true;
						arrivedRatio = ourOrderToCompare.getProducts().get(expectedProduct) - order.getProducts().get(deliveredProduct);
						Scanner sc = new Scanner(System.in);
						System.out.println("Enter expiration Date for "+expectedProduct.getProduct_name()+" "+expectedProduct.getManufacturer());
						System.out.println("expiration date format: 'dd/mm/yyyy', if doesn't have just type '-' in the place");
						String date = sc.nextLine();
						
						if(arrivedRatio>0)
						{
							System.out.println("Error Amount: Deliver did'nt bring "+arrivedRatio+" "+expectedProduct.getProduct_name()+" "+expectedProduct.getManufacturer());
							addProductsToInventory(expectedProduct.getProduct_name()+","+expectedProduct.getManufacturer()+","+date+","+ ourOrderToCompare.getProducts().get(deliveredProduct),shopID);
						}
						if(arrivedRatio<0)
						{
							System.out.println("Error Amount: Deliver brought "+(arrivedRatio*-1)+" more products of "+expectedProduct.getProduct_name()+" "+expectedProduct.getManufacturer());
							addProductsToInventory(expectedProduct.getProduct_name()+","+expectedProduct.getManufacturer()+","+date+","+ order.getProducts().get(deliveredProduct),shopID);
						}
						if(arrivedRatio==0)
						{
							System.out.println("Success : Deliver brought "+ourOrderToCompare.getProducts().get(expectedProduct)+" products of "+expectedProduct.getProduct_name()+" "+expectedProduct.getManufacturer());
							addProductsToInventory(expectedProduct.getProduct_name()+","+expectedProduct.getManufacturer()+","+date+","+ order.getProducts().get(deliveredProduct),shopID);
						}
					}
				}
				if(!found){
					System.out.println("Error Missing product : Product "+expectedProduct.getProduct_name()+" "+expectedProduct.getManufacturer()+" did not arrive in the delivery");
				}
			}
		}
	}
	public void acceptDelievery(String delieveryID,int shopID)
	{
		try
		{
			int delieveryIDInInt = Integer.parseInt(delieveryID);
			ArrayList<Order> allOrdersInDelievery = pli.getOrdersByDeliveryId(delieveryIDInInt);
			compareDeliveryWithOrder(allOrdersInDelievery, shopID); 
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	public void deleteOrderById(String productName,String manufacturer,int id){
		ordersDBM.deleteOrderById(productName,manufacturer,id);
	}
	public void deleteOrderSupplierById(int id){
		supplierOrderDBM.deleteOrderById(id);
	}
}
