package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modules.Supplier;
import modules.Product;
import modules.ProductInCategory;
import modules.ProvidedProduct;

public class ProductSupplierTable implements IDBManager {

	private Connection conn;
	private String name;

	public ProductSupplierTable() {
		this.conn = null;
		this.name = "PRODUCTS_SUPPLIERS";
	}

	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		createTable(this.name);
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {
		Statement stmt;
		try {
			stmt = this.conn.createStatement();
			String sql = "CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (PRODUCT_NAME    TEXT , "
					+ " MANUFACTURER TEXT , "
					+ " CatalogID TEXT , "
					+ " SupplierID   TEXT , " 
					+ " price            TEXT     NOT NULL, "
					+ " WEIGHT            INT     NOT NULL, "
					+ " primary key (CatalogID , SupplierID)," 
					+ " foreign key (SupplierID) references Suppliers(ID))";

			stmt.executeUpdate(sql);
			stmt.close();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error: cannot create product-supplier table");
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean closeConnection() {
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean add(Supplier s, ProvidedProduct product) {
		try {
			PreparedStatement stmt = conn
					.prepareStatement("insert into " + this.name + " (PRODUCT_NAME , MANUFACTURER,CatalogID,SupplierID, price,WEIGHT) VALUES(?,?,?,?,?,?)");
			stmt.setString(1, product.getProduct_name());
			stmt.setString(2, product.getManufacturer());
			stmt.setString(3, product.getCatalogNumber());
			stmt.setString(4, s.getID());
			stmt.setString(5, String.valueOf(product.getCostPrice()));
			stmt.setInt(6, product.getWeight());
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error: " + this.getClass() + " add fail");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean update(Supplier s, ProvidedProduct product, String fieldName, String input) {
		String sql;
		try {
			sql = "UPDATE PRODUCTS_SUPPLIERS" + " SET " + fieldName + " = '" + input 
					+ "' WHERE PRODUCT_NAME = '"+ product.getProduct_name() 
					+ "' AND SupplierID = '" + s.getID() 
					+ "' AND  MANUFACTURER = '"+ product.getManufacturer()+"'";
			conn.createStatement().executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("Error: cannot update Products-Suppliers table");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<ProvidedProduct> getAll(Supplier s)
	{
		List<ProvidedProduct> ans = new ArrayList<ProvidedProduct>();
		try {
			ResultSet reader = conn.createStatement().executeQuery("Select * FROM PRODUCTS_SUPPLIERS"
					+ " WHERE SupplierID = "+s.getID());
			while (reader.next()) {
				ProvidedProduct row = new ProvidedProduct
						(reader.getString("PRODUCT_NAME"),reader.getString("MANUFACTURER"),
								reader.getString("price"),s.getID(), reader.getString("CatalogID")
								,reader.getInt("WEIGHT"));
				ans.add(row);
			}
		} catch (SQLException e) {
		}
		return ans;
	}
	
	@Override
	public boolean clearTable() {
		try {
			conn.createStatement().executeUpdate("DELETE FROM " + this.name + ";");
			// conn.createStatement().execute("VACUUM;");
		} catch (SQLException e) {
			System.out.println("ERROR: " + this.getClass() + ".clearTable()");
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void insertHardCodedDefaultData() {
		

	}


	public ProvidedProduct getProvidedProductByNameManufacturerAndSupplier
	(String productName, String manufacturer,
			String supplierID) 
	{
		try {
			ResultSet reader = conn.createStatement().executeQuery("Select * FROM PRODUCTS_SUPPLIERS"
					+ " WHERE SupplierID = '"+supplierID+"' AND MANUFACTURER = '"+manufacturer
					+"' AND PRODUCT_NAME = '"+productName+"'");
			while (reader.next()) {
				ProvidedProduct row = new ProvidedProduct
						(productName,manufacturer,reader.getString("price"),supplierID, 
								reader.getString("CatalogID"),reader.getInt("WEIGHT"));
				return row;
			}
		} catch (SQLException e) {
		}
		return null;
	}

	public Double getCostPriceOfProduct(String productName, String manufacturer, String supplierID) {
		try {
			ResultSet reader = conn.createStatement().executeQuery("Select * FROM PRODUCTS_SUPPLIERS"
					+ " WHERE SupplierID = '"+supplierID+"' AND MANUFACTURER = '"+manufacturer+"'"
					+ " AND PRODUCT_NAME = '"+productName+"'");
			while (reader.next()) {
				return Double.parseDouble(reader.getString("price"));
			}
		} catch (SQLException e) {
		}
		return (double) -1;
		
	}
	
	
	public boolean checkIfExistsSupplierForProduct(String productName, String manufacturer) {
		try {
			ResultSet reader = conn.createStatement().executeQuery("Select * FROM PRODUCTS_SUPPLIERS"
					+ " WHERE MANUFACTURER = '"+manufacturer+"'"
					+" AND PRODUCT_NAME = '"+productName+"'");
			if (reader.next()) {
				return true;
			}
		} catch (SQLException e) {
		}
		return false;
		
	}

	public boolean checkIfExist(String productName, String manufacturer) {
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT * "
					+ " FROM "+this.name+
					" WHERE PRODUCT_NAME = '"+productName+"' AND MANUFACTURER = '"+manufacturer+"'");
			// loop through the result set
			if (rs.next()) {
				return true;
				}
			}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return false;
	}

	public ProvidedProduct getProvidedProductByCatalogNumberAndSupplier(String catalogNumber, String supplierID) {
			try {
				ResultSet reader = conn.createStatement().executeQuery("Select * FROM PRODUCTS_SUPPLIERS"
						+ " WHERE SupplierID = '"+supplierID+"' AND CatalogID = '"+catalogNumber+"'");
				while (reader.next()) {
					ProvidedProduct row = new ProvidedProduct
							(reader.getString("PRODUCT_NAME"),reader.getString("MANUFACTURER"),
									reader.getString("price"),supplierID, catalogNumber
									,reader.getInt("WEIGHT"));
					return row;
				}
			} catch (SQLException e) {
			}
			return null;
		}

	public void printAllCatalogID(String supplierID) {
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT PRODUCT_NAME,MANUFACTURER,CatalogID "
					+ " FROM "+this.name
					+ " WHERE SupplierID = '"+supplierID+"'");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Catalog ID: "+rs.getString("CatalogID")+", Product Name: "+rs.getString("PRODUCT_NAME")+", Manufacturer: "+rs.getString("MANUFACTURER"));
			}
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
	}
	
	public boolean checkIfSupplierSupplyProduct(Supplier s,ProvidedProduct p){
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT * "
					+ " FROM "+this.name
					+ " WHERE SupplierID = '"+s.getID()+"' AND PRODUCT_NAME = '"+p.getProduct_name()+"'"
					+ " AND MANUFACTURER = '"+p.getManufacturer()+"'");
			// loop through the result set
			if (rs.next()) {
				return true;
			}
			return false;
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
			return false;
		}
	}
}
