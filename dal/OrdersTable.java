package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.experimental.theories.internal.AllMembersSupplier;

import modules.Order;
import modules.ProductDiscount;
import modules.ProductInCategory;
import modules.ProvidedProduct;
import modules.Supplier;


public class OrdersTable implements IDBManager {

	private Connection conn;
	private String name;

	public OrdersTable() {
		this.conn = null;
		this.name = "ORDERS";
	}
	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		createTable(this.name);
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {
		Statement stmt;
		try {
			stmt = this.conn.createStatement();
			String sql = 
					"CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (PRODUCT_NAME    TEXT      NOT NULL , "
					+ " MANUFACTURER     TEXT    NOT NULL , "
					+ " OrderID           TEXT    NOT NULL , " 
					+ " Amount            TEXT     NOT NULL, "
					+ " SHOP_ID 		INTEGER    NOT NULL, "
					+ " primary key (PRODUCT_NAME , MANUFACTURER , OrderID),"
					+ " foreign key (SHOP_ID) references SHOPS(ID),"
					+ " foreign key (PRODUCT_NAME , MANUFACTURER) references PRODUCTS_IN_SHOP(PRODUCT_NAME , MANUFACTURER),"
					+ " foreign key (OrderID) references SupplierOrder(OrderID))";

			stmt.executeUpdate(sql);
			stmt.close();
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create Orders table");
		}
		return false;
	}
	public boolean addOrder(Order newOrder)
	{
		try {
			if (getByOrderNumber(String.valueOf(newOrder.getOrderNumber()),newOrder.getShopID()) != null)
				return false;
			PreparedStatement stmt = conn.prepareStatement(
					"insert into SUPPLIER_ORDERS (OrderID , SupplierID) VALUES(?,?)");
			stmt.setString(1, newOrder.getOrderNumber());
			stmt.setString(2, newOrder.getSuplier().getID());
			stmt.executeUpdate();
			for(Integer i:newOrder.getDeliveyDays())
			{
				stmt = conn.prepareStatement(
						"insert into ORDERS_DAYS (OrderID , Day) VALUES(?,?)");
				stmt.setString(1, newOrder.getOrderNumber());
				stmt.setString(2, i+"");
				stmt.executeUpdate();
			}
			for(ProvidedProduct p:newOrder.getProducts().keySet())
			{
				stmt = conn.prepareStatement(
						"insert into ORDERS (PRODUCT_NAME , MANUFACTURER,OrderID,Amount,SHOP_ID) VALUES(?,?,?,?,?)");
				stmt.setString(1, p.getProduct_name());
				stmt.setString(2, p.getManufacturer());
				stmt.setString(3, newOrder.getOrderNumber());
				stmt.setString(4, newOrder.getProducts().get(p)+"");
				stmt.setLong(5, newOrder.getShopID());
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean upadteOrder(Order o)
	{
		try {
			if(o.IsDelieveryDayTomorrow())
				return false;
			PreparedStatement stmt = conn.prepareStatement(
					"DELETE FROM "+this.name
					+" WHERE OrderID = '"
					+ o.getOrderNumber()+"'");
			stmt.executeUpdate();
			for(ProvidedProduct p:o.getProducts().keySet())
			{
				stmt = conn.prepareStatement(
						"insert into ORDERS (PRODUCT_NAME , MANUFACTURER,OrderID,Amount,SHOP_ID) VALUES(?,?,?,?,?)");
				stmt.setString(1, p.getProduct_name());
				stmt.setString(2, p.getManufacturer());
				stmt.setString(3, o.getOrderNumber());
				stmt.setString(4, o.getProducts().get(p)+"");
				stmt.setLong(5, o.getShopID());
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;

	}

	/**
	 * check if the order exists
	 * @param orderNumber
	 * @return true if the order exists' otherwise return false.
	 */
	public boolean checkIfOrderExistsByOrderNumber(String orderNumber,int shopID)
	{
		return getByOrderNumber(orderNumber,shopID) != null;
	}
	public Order getByOrderNumber(String orderNumber,int shopID) {
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM SUPPLIER_ORDERS WHERE OrderID = '" + orderNumber+"'");
			// loop through the result set
			while (rs.next()) {
				Order ans = new Order();
				ans.setSuplier(DataBase.getDataBase().getSupplierById(rs.getString("SupplierID")));
				ans.setOrderNumber(rs.getString("OrderID"));
				ans.setDays(DataBase.getDataBase().getDeliveryDaysByOrderID(rs.getString("OrderID")));
				ans.setProducts(DataBase.getDataBase().getAllProductsInOrder(rs.getString("OrderID")));
				ans.setShopID(getShopIdByOrderId(Integer.parseInt(orderNumber)));
				if(ans.getShopID()==shopID)
					return ans;
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public boolean insertTable(Object record) {
		return false;
	}

	@Override
	public boolean closeConnection() {
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	@Override
	public boolean clearTable() {
		try {
			conn.createStatement().executeUpdate("DELETE FROM " + this.name + ";");
			//			conn.createStatement().execute("VACUUM;");
		} catch (SQLException e) {
			System.out.println("ERROR: "+this.getClass()+".clearTable()");
			e.printStackTrace();
		}
		return true;
	}
	@Override
	public void insertHardCodedDefaultData() {


	}
	/**
	 * get all orders for a specific product.
	 * @param productName
	 * @param manufacturer
	 * @return ArrayList<Order> of all the orders that include the product 'productName-manufacturer'.
	 */
	public ArrayList<Order> getAllOrdersByProductNameAndManufacturer(String productName, String manufacturer,int shopID) {
		ArrayList<Order> ans = new ArrayList<Order>();
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery("SELECT * "
					+ "FROM ORDERS "
					+ "WHERE PRODUCT_NAME = '"+productName+"' AND MANUFACTURER = '"+manufacturer+"' AND SHOP_ID = '"+shopID+"'");
			// loop through the result set
			while (rs.next()) {
				{
					Order o = getByOrderNumber(rs.getString("OrderID"),shopID);
					ans.add(o);
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return ans;
	}

	/**
	 * get all orders.
	 * @return ArrayList<Order> of all the orders exists.
	 */
	public ArrayList<Order> getAllOrders(int shopID) {
		ArrayList<Order> ans = new ArrayList<Order>();
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery("SELECT DISTINCT(OrderID) "
					+ " FROM ORDERS"
					+ " WHERE SHOP_ID = '"+shopID+"'");
			// loop through the result set
			while (rs.next()) {
				Order o = getByOrderNumber(rs.getString("OrderID"),shopID);
				ans.add(o);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return ans;
	}
	/**
	 * get the max orderID number
	 * @return max orderID , if orderTable is empty, return 0.
	 */
	public int getMaxOrderNumber() 
	{
		try {
			Statement stmt1  = conn.createStatement();
			//fix error
			ResultSet rs1    = stmt1.executeQuery("SELECT max(length(OrderID)) as LENGTH_OF_STRING FROM "+this.name);
			rs1.next();
			long Length = rs1.getLong("LENGTH_OF_STRING");
			Statement stmt2  = conn.createStatement();
			ResultSet rs2    = stmt2.executeQuery("SELECT MAX(OrderID) AS max FROM "+this.name+" WHERE length(OrderID) = "+Length);
			if (rs2.next()==true) 
			{
				if(rs2.getString("max")!=null)
					return Integer.parseInt(rs2.getString("max"));
				return 0;
			}
			return 0;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}

	public int getShopIdByOrderId(int orderID){
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery("SELECT SHOP_ID"
					+ " FROM ORDERS"
					+ " WHERE OrderID = '"+orderID+"'");
			// loop through the result set
			if (rs.next()) {
				return rs.getInt("SHOP_ID");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return -1;
	}
	
	public ArrayList<Order> getListOfOrders(Map<ProvidedProduct, Integer> wantedProducts,ArrayList<Integer> delieveryDays, int shopID)
	{
		DataBase dbBase = DataBase.getDataBase();
		ArrayList<Order> ans = new ArrayList<Order>();
		HashMap<String, ArrayList<ProvidedProduct>> productsForSupplier = new HashMap<String, ArrayList<ProvidedProduct>>();
		ArrayList<Supplier> allSupplier =  dbBase.supplierDBM.getAll();
		for (Supplier supplier : allSupplier) 
		{
			ArrayList<ProvidedProduct> init = new ArrayList<ProvidedProduct>();
			productsForSupplier.put(supplier.getID(), init);
		}
		for (ProvidedProduct pp : wantedProducts.keySet()) 
		{
			Supplier s = dbBase.supplierDBM.getByID(pp.getSupplierID());
			ArrayList<ProvidedProduct> newArray = productsForSupplier.get(s.getID());
			newArray.add(pp);
			productsForSupplier.put(s.getID(),newArray);
		}
		
		
		int counterOrderID = Integer.parseInt(dbBase.getNewOrderID()) ;
		for (String supplierID : productsForSupplier.keySet()) 
		{
			Supplier supplier = DataBase.getDataBase().supplierDBM.getByID(supplierID);
			if(productsForSupplier.get(supplierID).size() > 0)
			{
				Order o = new Order();
				o.setOrderNumber(counterOrderID +"");
				o.setSuplier(supplier);
				o.setShopID(shopID);
				o.setDays(delieveryDays);
				HashMap<ProvidedProduct, Integer> productsInOrder = new HashMap<ProvidedProduct,Integer>();
				for (ProvidedProduct pp : productsForSupplier.get(supplierID))
				{
					productsInOrder.put(pp, wantedProducts.get(pp));
				}
				o.setProducts(productsInOrder);
				ans.add(o);
				counterOrderID++;
			}
		}
		return ans;
	}
	public void deleteOrderById(String productName,String manufacturer,int id){
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeQuery(" DELETE FROM "+this.name+" WHERE PRODUCT_NAME = '"+productName+"'"
							+ " AND MANUFACTURER = '"+manufacturer+"' AND OrderID = '"+id+"'");
		} catch (SQLException e) {
			//System.out.println(e.getMessage());
		}
	}
}
