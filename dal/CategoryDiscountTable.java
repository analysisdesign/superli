package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modules.*;
import java.sql.Connection;

public class CategoryDiscountTable implements IDBManager{

	private Connection conn;
	private String name;

	public String getName() {
		return name;
	}

	public CategoryDiscountTable() {
		this.conn = null;
		this.name = "CATEGORY_DISCOUNTS";
	}

	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {

		//System.out.println("Creating table...");

		Statement stmt;
		try {
			stmt = this.conn.createStatement();

			String sql = 
					"CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (CATEGORY_NAME TEXT NOT NULL, " 
					+ " DISCOUNT INT NOT NULL,"
					+ " START_DATE TEXT NOT NULL,"
					+ " END_DATE TEXT NOT NULL,"
					+ " PRIMARY KEY (CATEGORY_NAME, START_DATE, END_DATE),"
					+ " FOREIGN KEY(CATEGORY_NAME) REFERENCES CATEGORIES(NAME))";
			stmt.executeUpdate(sql);
			stmt.close();

			//System.out.println("CATEGORY_DISCOUNTS table created!");
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create CATEGORY_DISCOUNTS table");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		try {
			//need to check the dates.
			CategoryDiscount p = (CategoryDiscount)record;
			PreparedStatement stmt = conn.prepareStatement
					("insert into " + this.name+ " (CATEGORY_NAME,DISCOUNT,START_DATE,END_DATE) VALUES(?,?,?,?)");
			stmt.setString(1, p.getCategory());
			stmt.setString(2, p.getDiscount()+"");
			stmt.setString(3, p.getStartDate());
			stmt.setString(4, p.getEndDate());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}



	public boolean closeConnection()
	{
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean checkIfIsNumber(String toCheck)
	{
		try{Integer.parseInt(toCheck);}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}
	public ArrayList<CategoryDiscount> getAllCategoryDiscoutByCatName(String catName)
	{
		ArrayList<CategoryDiscount> ans = new ArrayList<CategoryDiscount>();
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery("SELECT * "
					+ "FROM "+this.name+""
					+ " WHERE CATEGORY_NAME = '" + catName + "'"
					);
			// loop through the result set
			while (rs.next()) {
				CategoryDiscount cdt = new CategoryDiscount(catName ,rs.getInt("DISCOUNT"),rs.getString("START_DATE"), rs.getString("END_DATE"));
				if(!ans.add(cdt))
				{
					System.out.println("Didnt add cdt to list");
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return ans;
	}
	ArrayList<CategoryDiscount> getAllCategoryAndParentCategoryDiscoutByCatName(String catName)
	{
		String[] arr = catName.split(" ");
		ArrayList<CategoryDiscount> ans = new ArrayList<CategoryDiscount>();
		String SubCategory = "";
		try {
			for(int i = 0; i < arr.length;i++)
			{
				for(int j = 0; j <=i; j++)
				{
					if(j == 0)
					{
						SubCategory = arr[j];
					}
					else
					{
						SubCategory = SubCategory +" "+arr[j];
					}
				}
				Statement stmt  = conn.createStatement();
				ResultSet rs    = stmt.executeQuery("SELECT * "
						+ "FROM "+this.name+""
						+ " WHERE CATEGORY_NAME = '"+SubCategory+"'"
						);
				// loop through the result set
				while (rs.next()) {
					CategoryDiscount cdt = new CategoryDiscount(SubCategory ,rs.getInt("DISCOUNT"),rs.getString("START_DATE"), rs.getString("END_DATE"));
					if(!ans.add(cdt))
					{
						System.out.println("Didnt add cdt to list");
					}
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return ans;
	}

	public boolean checkCollision(String startDate, String endDate, String catName) {
		ArrayList<CategoryDiscount> listCd = getAllCategoryDiscoutByCatName(catName);
		for (CategoryDiscount categoryDiscount : listCd) 
		{
			//--s1---s2---e2--e1---
			//start1 <= start2
			//end1 >= end2
			if((compareBetweenDates(startDate, categoryDiscount.getStartDate()) <= 0) &&
					(compareBetweenDates(endDate, categoryDiscount.getEndDate()) >= 0))
				{
				return false;
				}
			// ----s1----s2----e1----e2
			//start1 <= start2
			//end1 >= start2
			//end1 <= end2
			if((compareBetweenDates(startDate, categoryDiscount.getStartDate()) <= 0) &&
					(compareBetweenDates(endDate, categoryDiscount.getStartDate()) >= 0) &&
					(compareBetweenDates(endDate, categoryDiscount.getEndDate()) <= 0))
				{
				return false;
				}
			// ----s2----s1----e2----e1
			//start1 <= start2
			//end1 >= start2
			//end1 <= end2
			if((compareBetweenDates(startDate, categoryDiscount.getStartDate()) >= 0) &&
					(compareBetweenDates(startDate, categoryDiscount.getEndDate()) <= 0) &&
					(compareBetweenDates(endDate, categoryDiscount.getEndDate()) >= 0))
				{
				return false;
			}
			// ----s2----s1----e1----e2
			//start1 <= start2
			//end1 >= start2
			//end1 <= end2
			if((compareBetweenDates(startDate, categoryDiscount.getStartDate()) >= 0) &&
					(compareBetweenDates(startDate, categoryDiscount.getEndDate()) <= 0) &&
					(compareBetweenDates(endDate, categoryDiscount.getEndDate()) <= 0))
			{
				return false;
			}

		}
		return true;
	}

	public int compareBetweenDates(String date1, String date2) 
	{
		String[] arrDate1 = date1.split("/");	
		String[] arrDate2 = date2.split("/");
		if(Integer.compare(Integer.parseInt(arrDate1[2]),Integer.parseInt(arrDate2[2])) != 0)
			return 		Integer.compare(Integer.parseInt(arrDate1[2]),Integer.parseInt(arrDate2[2])) ;
		if(Integer.compare(Integer.parseInt(arrDate1[1]),Integer.parseInt(arrDate2[1])) != 0)
			return 		Integer.compare(Integer.parseInt(arrDate1[1]),Integer.parseInt(arrDate2[1])) ;
		if(Integer.compare(Integer.parseInt(arrDate1[0]),Integer.parseInt(arrDate2[0])) != 0)
			return 		Integer.compare(Integer.parseInt(arrDate1[0]),Integer.parseInt(arrDate2[0])) ;
		return 0;
	}

	public ArrayList<CategoryDiscount> getAllCategoryDiscountByCategoryAndDate(String category, String currDate) 
	{
		ArrayList<CategoryDiscount> listCategoryDiscount = getAllCategoryDiscoutByCatName(category);
		ArrayList<CategoryDiscount> ans = new ArrayList<CategoryDiscount>();
		for (CategoryDiscount categoryDiscount : listCategoryDiscount) {
			if(!checkIfDiscountIsRelevant(currDate, categoryDiscount.getStartDate(),categoryDiscount.getEndDate()))
				{
					ans.add(categoryDiscount);
				}
			}
		return ans;
	}
	private boolean checkIfDiscountIsRelevant(String currDate, String  startDate, String endDate) 
	{
		//the period for discount is in the future
		if(compareBetweenDates(currDate, startDate) < 0)
			return false;
		//the period  for discount finished
		if(compareBetweenDates(endDate, currDate) < 0)
			return false;
		return true;
	}

	public boolean checkIfDiscExist(String catName){
		if(getAllCategoryDiscoutByCatName(catName).size() != 0)
			return true;
		return false;
	}
	
	public boolean removeDiscountByCategoryNameStartTimeEndTime(String catName,String startTime,String endTime){
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeUpdate("DELETE FROM "+this.name
					+ " WHERE CATEGORY_NAME = '" + catName + "' AND START_DATE ='"+startTime+"'"
					+ " AND END_DATE ='"+endTime+"'");
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean clearTable() {
		return false;
	}

	@Override
	public void insertHardCodedDefaultData() {
		
	}

}
