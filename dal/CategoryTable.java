package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modules.*;

import java.sql.Connection;

public class CategoryTable implements IDBManager {

	private static Connection conn;
	private String name;

	public String getName() {
		return name;
	}

	public CategoryTable() {
		this.conn = null;
		this.name = "CATEGORIES";
	}

	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {

		//System.out.println("Creating table...");

		Statement stmt;
		try {
			stmt = this.conn.createStatement();

			String sql = 
					"CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (NAME TEXT NOT NULL PRIMARY KEY, " 
					+ " PARENT TEXT,"
					+ " FOREIGN KEY(PARENT) REFERENCES "+tableName+"(NAME) )";
			stmt.executeUpdate(sql);
			stmt.close();

			//System.out.println("Category table created!");
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create Category table");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		try {
			Category p = (Category)record;
			PreparedStatement stmt = conn.prepareStatement
					("insert into CATEGORIES (NAME,PARENT) VALUES(?,?)");
			stmt.setString(1, p.getCategoryName());
			stmt.setString(2, p.getParent_Category());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	public static ArrayList<Category> getAllCategories(){
		ArrayList<Category> ans = new ArrayList<Category>();
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery("SELECT * FROM CATEGORIES");
			// loop through the result set
			while (rs.next()) {
				ans.add(new Category(rs.getString("NAME"), rs.getString("PARENT")));	
			}
			return ans;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}


	public boolean closeConnection()
	{
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean checkIfIsNumber(String toCheck)
	{
		try{Integer.parseInt(toCheck);}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}

	public boolean checkIfExist(String categoryName)
	{
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT * "
					+ " FROM "+this.name+
					" WHERE NAME = '"+categoryName+"'");
			// loop through the result set
			if (rs.next()) {
				return true;
				}
			}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return false;	
	}

	@Override
	public boolean clearTable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void insertHardCodedDefaultData() {
		// TODO Auto-generated method stub
		
	}

}
