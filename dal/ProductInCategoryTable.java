package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modules.*;

public class ProductInCategoryTable implements IDBManager{

	private static Connection conn;
	private static String name;

	public String getName() {
		return name;
	}

	public ProductInCategoryTable() {
		this.conn = null;
		this.name = "PRODUCT_CATEGORIES";
	}

	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {

		Statement stmt;
		try {
			stmt = this.conn.createStatement();

			String sql = 
					"CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (PRODUCT_NAME TEXT NOT NULL,"
					+ " CATEGORY_NAME TEXT NOT NULL,"
					+ " MANUFACTURER TEXT NOT NULL," 
					+ " SELL_PRICE REAL NOT NULL,"
					+ " MINIMAL_AMOUNT INT NOT NULL,"
					+ " DAILY_CONSUMPTION INT NOT NULL,"
					+ " WEIGHT INT NOT NULL,"
					+ " SHOP_ID INT NOT NULL, "
					+ "	FOREIGN KEY (SHOP_ID) REFERENCES SHOPS(ID),"
					+ " PRIMARY KEY (PRODUCT_NAME,MANUFACTURER,SHOP_ID),"
					+ "	FOREIGN KEY (PRODUCT_NAME,MANUFACTURER) REFERENCES PRODUCTS_SUPPLIERS (PRODUCT_NAME,MANUFACTURER) ,"
					+ "	FOREIGN KEY (CATEGORY_NAME) REFERENCES CATEGORIES (NAME))";

			stmt.executeUpdate(sql);
			stmt.close();

			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create PRODUCT_CATEGORIES table");
			System.out.println(e.getMessage());
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) 
	{
		try {
			ProductInCategory p = (ProductInCategory)record;
			PreparedStatement stmt = conn.prepareStatement
					("insert into "+this.name+" (PRODUCT_NAME, CATEGORY_NAME, MANUFACTURER, SELL_PRICE, MINIMAL_AMOUNT"
							+ ",DAILY_CONSUMPTION,WEIGHT,SHOP_ID) VALUES(?,?,?,?,?,?,?,?)");
			stmt.setString(1, p.getProduct());
			stmt.setString(2, p.getCategory());
			stmt.setString(3, p.getManufacturer());
			stmt.setString(4, p.getSellPrice()+"");
			stmt.setString(5, p.getMinimalAmount()+"");
			stmt.setString(6, p.getDailyConsumption()+"");
			stmt.setLong(7, p.getWeight());
			stmt.setLong(8, p.getShopID());




			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}


	public boolean closeConnection()
	{
		try {
			this.conn.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	public boolean checkIfIsNumber(String toCheck)
	{
		try{Integer.parseInt(toCheck);}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}
	//if not exist return false.
	public boolean checkIfExist(String productName, String manufacturer)
	{
		if(getCategoryByProductNameAndManufacturer(productName, manufacturer) == null)
			return false;
		return true;
	}

	public boolean checkifIsDoubleAndPossitive(String toCheck) {
		try{Double.parseDouble(toCheck);}
		catch(Exception e)
		{
			return false;
		}
		if(Double.parseDouble(toCheck) <= 0)
			return false;
		return true;
	}
	//if product doesn't exists, return -1;
	//-1 is invalid sell price!
	public double getSellPriceByProductNameAndManufacturer(String product, String manufacturer) 
	{
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT * "
					+ " FROM "+this.name+
					" WHERE PRODUCT_NAME = '"+product+"' AND MANUFACTURER = '"+manufacturer+"'");
			// loop through the result set
			if (rs.next()) {
				return rs.getDouble("SELL_PRICE");
				}
			}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return -1;	
	}

	/**
	 * Update the Sell price for a product in the table product In Category 
	 * Assumption : sell price is the same at all shops
	 */
	public boolean updatePriceByNameAndManufacturer(String productName, String manufacturer, double updatePrice){
		String update = "SELL_PRICE";
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeUpdate("UPDATE "+this.name
							+ " SET "+update+" = '"+updatePrice+"'"
							+ " WHERE PRODUCT_NAME = '"+productName+"' AND MANUFACTURER = '"+manufacturer+"'");
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
	
	public static String getCategoryByProductNameAndManufacturer(String product, String manufacturer) 
		{
			try {
				Statement stmt  = conn.createStatement();
				ResultSet rs  = stmt.executeQuery("SELECT * "
						+ " FROM "+name+
						" WHERE PRODUCT_NAME = '"+product+"' AND MANUFACTURER = '"+manufacturer+"'");
				// loop through the result set
				if (rs.next()) {
					return rs.getString("CATEGORY_NAME");
					}
				}
			catch(SQLException e){
				System.out.println(e.getMessage());
			}
			return null;	
		}

	//TODO
	///need to check if distinct work.
	public ArrayList<ProductInCategory> getAllProductsInCategory(int shopID) 
	{
		ArrayList<ProductInCategory> ans = new ArrayList<ProductInCategory>();
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT *"
					+ " FROM "+this.name 
					+ " WHERE SHOP_ID = '"+shopID+"'");
			while(rs.next()) 
			{
				ProductInCategory pic = new ProductInCategory
				(rs.getString("PRODUCT_NAME"),rs.getString("CATEGORY_NAME"),rs.getString("MANUFACTURER")
						,rs.getInt("SELL_PRICE"),rs.getInt("MINIMAL_AMOUNT"),rs.getInt("DAILY_CONSUMPTION")
						,rs.getInt("WEIGHT"), -1);
				ans.add(pic);
				}
			}
		catch(SQLException e){
			System.out.println(e.getMessage());
			return null;
		}
		return ans;
	}

	
	
	@Override
	public boolean clearTable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void insertHardCodedDefaultData() {
		// TODO Auto-generated method stub
		
	}

	public static ProductInCategory getProductInCategoryByNameManufacturerAndShopID(String product, String manufacturer,int shopID) 
	{
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT * "
					+ " FROM "+name+
					" WHERE PRODUCT_NAME = '"+product+"' AND MANUFACTURER = '"+manufacturer+"' AND SHOP_ID = '"+shopID+"'");
			// loop through the result set
			if (rs.next()) {
				return new ProductInCategory(rs.getString("PRODUCT_NAME"),rs.getString("CATEGORY_NAME"),rs.getString("MANUFACTURER")
						,rs.getInt("SELL_PRICE"),rs.getInt("MINIMAL_AMOUNT"),rs.getInt("DAILY_CONSUMPTION")
						,rs.getInt("WEIGHT"), rs.getInt("SHOP_ID"));
				}
			}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return null;	
	}

	public void updateDailyConsumption(String productName, String manufacturer, String dailyConsumption,int shopID) {
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeUpdate("UPDATE "+this.name
							+ " SET DAILY_CONSUMPTION = '"+dailyConsumption+"'"
							+ " WHERE SHOP_ID = '"+shopID+"' AND PRODUCT_NAME = '"+productName+"' AND MANUFACTURER = '"+manufacturer+"'");
		} catch (SQLException e) {
		}
	}

	public void updateMinimalAmount(String productName, String manufacturer, int minimalAmount, int shopID) {
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeUpdate("UPDATE "+this.name
							+ " SET MINIMAL_AMOUNT = '"+minimalAmount+"'"
							+ "WHERE SHOP_ID = '"+shopID+"' AND PRODUCT_NAME = '"+productName+"' AND MANUFACTURER = '"+manufacturer+"'");
		} catch (SQLException e) {
		}
	}
	public void deleteProductByNameAndManufacturer(String product,String manufacturer) { 
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeQuery("DELETE FROM "+this.name+" WHERE PRODUCT_NAME='"+product+"' AND MANUFACTURER = '"+manufacturer+"'" );
		} catch (SQLException e) {
			//System.out.println(e.getMessage());
		}
	}
}
