package dal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import modules.*;

public class ShopsTable {
	private Connection conn;
	private String name;

	public String getName() {
		return name;
	}

	public ShopsTable() {
		this.conn = null;
		this.name = "SHOPS";
	}

	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		return this.conn;
	}

	public boolean createTable(String tableName) {

		//System.out.println("Creating table...");

		Statement stmt;
		try {
			stmt = this.conn.createStatement();

			String sql = 
					"CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ " ADDRESS TEXT NOT NULL,"
					+ " CONTACT_NAME TEXT NOT NULL,"
					+ " PHONE_NUMBER TEXT NOT NULL)";

			stmt.executeUpdate(sql);
			stmt.close();

			//System.out.println("Product table created!");
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create Shops table table");
			e.printStackTrace();
		}
		return false;
	}

	public boolean insertTable(Object record) {
		try {
			Shop p = (Shop)record;
			PreparedStatement stmt = conn.prepareStatement
					("insert into "+this.name+"(ADDRESS,CONTACT_NAME,PHONE_NUMBER) VALUES(?,?,?)");
			stmt.setString(1, p.getAddress());
			stmt.setString(2, p.getContact_Name());
			stmt.setString(3, p.getPhone_Number());


			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void printAllSuperLis() {
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT * FROM "+this.name);
			
			while (rs.next()) {
				Shop toPrint = new Shop(rs.getInt("ID"),rs.getString("ADDRESS"),rs.getString("PHONE_NUMBER"),rs.getString("CONTACT_NAME"));
				System.out.println(toPrint);
			}

			System.out.println();
			System.out.println();
		} 
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public boolean checkExists(int wantedShop) {
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM "+this.name+" WHERE ID ='"+wantedShop+"'");
			if(rs.next()){
				return true;
			}
			return false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	public void deleteShopById(int id) { //ID is unique so no need to check in different shops
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeQuery("DELETE FROM "+this.name+" WHERE ID="+id);
		} catch (SQLException e) {
			//System.out.println(e.getMessage());
		}
	}
	
	public String getAddressByID(int wantedShop) 
	{
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT ADDRESS FROM "+this.name+" WHERE ID ='"+wantedShop+"'");
			if(rs.next()){
				return rs.getString("ADDRESS");
			}
			return "";
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return "";
	}
	public String getContactNameByID(int wantedShop) 
	{
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT CONTACT_NAME FROM "+this.name+" WHERE ID ='"+wantedShop+"'");
			if(rs.next()){
				return rs.getString("ADDRESS");
			}
			return "";
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return "";
	}
	public String getPhoneNumberByID(int wantedShop) 
	{
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT PHONE_NUMBER FROM "+this.name+" WHERE ID ='"+wantedShop+"'");
			if(rs.next()){
				return rs.getString("ADDRESS");
			}
			return "";
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return "";
	}

	public Shop getShopByID(int id) 
	{
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM "+this.name+" WHERE ID ='"+id+"'");
			if(rs.next()){
				return new Shop(id,rs.getString("ADDRESS"), rs.getString("PHONE_NUMBER"), rs.getString("CONTACT_NAME"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	public int getMaxShopID() 
	{
		try {
			Statement stmt1  = conn.createStatement();
			ResultSet rs1    = stmt1.executeQuery("SELECT max(length(ID)) as LENGTH_OF_STRING FROM "+this.name);
			rs1.next();
			long Length = rs1.getLong("LENGTH_OF_STRING");
			Statement stmt2  = conn.createStatement();
			ResultSet rs2    = stmt2.executeQuery("SELECT MAX(ID) AS max FROM "+this.name+" WHERE length(ID) = "+Length);
			if (rs2.next()==true) 
			{
				if(rs2.getString("max")!=null)
					return Integer.parseInt(rs2.getString("max"));
				return 0;
			}
			return 0;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}
}
