package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import modules.Contact;
import modules.Supplier;

public class ContactsTable implements IDBManager {

	private Connection conn;
	private String name;

	public ContactsTable() {
		this.conn = null;
		this.name = "CONTACTS";
	}

	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		createTable(this.name);
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {
		Statement stmt;
		try {
			stmt = this.conn.createStatement();
			String sql = "CREATE TABLE IF NOT EXISTS " + tableName + ""
					+ " (ID    TEXT      NOT NULL,"
					+ " FIRSTNAME           TEXT    NOT NULL, " + " "
					+ "LASTNAME            TEXT     NOT NULL, "
					+ " PHONE       TEXT NOT NULL, " + " "
					+ "SUPPLIERID         TEXT," + "primary key (ID, SUPPLIERID),"
					+ "FOREIGN KEY (SUPPLIERID) REFERENCES Suppliers(ID)," + "CONSTRAINT IdUnique UNIQUE (ID))";

			stmt.executeUpdate(sql);
			stmt.close();
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create contacts table");
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean update(Supplier s, Contact c, String fieldName, String input) {
		String sql;
		try {
			sql = "UPDATE Contacts" + " SET " + fieldName + " = '" + input + "' WHERE ID = " + c.getId()
					+ " AND SUPPLIERID = " + s.getID();
			conn.createStatement().executeUpdate(sql);

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error: cannot update contacts table");
			return false;
		}
		return true;
	}

	public boolean add(Supplier s, Contact newcontact) {
		try {
			PreparedStatement stmt = conn.prepareStatement(
					"insert into Contacts (ID,FIRSTNAME, LASTNAME,PHONE,SUPPLIERID) VALUES(?,?,?,?,?)");
			stmt.setString(1, newcontact.getId());
			stmt.setString(2, newcontact.getFirstName());
			stmt.setString(3, newcontact.getLastName());
			stmt.setString(4, newcontact.getPhoneNumber());
			stmt.setString(5, s.getID());
			stmt.executeUpdate();
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean closeConnection() {
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean clearTable() {
		try {
			conn.createStatement().executeUpdate("DELETE FROM " + this.name + ";");
//			conn.createStatement().execute("VACUUM;");
		} catch (SQLException e) {
			System.out.println("ERROR: "+this.getClass()+".clearTable()");
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void insertHardCodedDefaultData() {
	}

}
