package dal;

import java.sql.*;

public interface IDBManager {
	
	/**
	 * Connecting to the db
	 * @param dbName the name of the db
	 * @return null if connection fail
	 */
	public Connection connectToDB();
	
	/**
	 * Creating table in the db
	 * 
	 * - this method may change for different design
	 *  
	 * @param tableName
	 * @return
	 */
	public boolean createTable(String tableName);
	
	/**
	 * Inserts new record (row) into the table
	 * 	- the record is of type that defines inside of
	 * 		module package
	 * @param module
	 * @return
	 */
	public boolean insertTable(Object record);
	
	/**
	 * Clears all records from table
	 * - for testing use only.
	 * @return true case of success
	 */
	public boolean clearTable();

	
	/**
	 * Close the connection
	 */
	public boolean closeConnection();

	/**
	 * @author Idan Izicovich
	 * insert hard coded default data
	 * <li>for tests usage</li>
	 */
	public void insertHardCodedDefaultData();
}
