package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modules.*;
import java.sql.Connection;

public class ProductDiscountTable implements IDBManager{
	private Connection conn;
	private String name;

	public String getName() {
		return name;
	}

	public ProductDiscountTable() {
		this.conn = null;
		this.name = "PRODUCT_DISCOUNTS";
	}

	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {

		//System.out.println("Creating table...");

		Statement stmt;
		try {
			stmt = this.conn.createStatement();

			String sql = 
					"CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (PRODUCT_NAME TEXT NOT NULL,"
					+ " MANUFACTURER TEXT NOT NULL," 
					+ " DISCOUNT INT NOT NULL,"
					+ " START_DATE TEXT NOT NULL,"
					+ " END_DATE TEXT NOT NULL,"
					+ " PRIMARY KEY (PRODUCT_NAME,MANUFACTURER,START_DATE, END_DATE),"
					+ " FOREIGN KEY (PRODUCT_NAME,MANUFACTURER) REFERENCES PRODUCTS_CATEGORIES(PRODUCT_NAME,MANUFACTURER))";
			stmt.executeUpdate(sql);
			stmt.close();

			//System.out.println("PRODUCT_DISCOUNTS table created!");
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create PRODUCT_DISCOUNTS table");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		try {
			ProductDiscount p = (ProductDiscount)record;
			PreparedStatement stmt = conn.prepareStatement
					("insert into " + this.name+ " (PRODUCT_NAME,MANUFACTURER,DISCOUNT,START_DATE,END_DATE) VALUES(?,?,?,?,?)");
			stmt.setString(1, p.getProduct());
			stmt.setString(2, p.getManufacturer());
			stmt.setString(3, p.getDiscount()+"");
			stmt.setString(4, p.getStartDate());
			stmt.setString(5, p.getEndDate());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}



	public boolean closeConnection()
	{
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean checkIfIsNumber(String toCheck)
	{
		try{Integer.parseInt(toCheck);}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}

	public ArrayList<ProductDiscount> getAllProductDiscountByProductNameAndManufacturer(String productName,
			String manufacturer) 
	{
		ArrayList<ProductDiscount> ans = new ArrayList<ProductDiscount>();

		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery("SELECT * "
					+ "FROM "+this.name+" "
					+ "WHERE PRODUCT_NAME = '"+productName+"' AND MANUFACTURER = '"+manufacturer+"'");
			// loop through the result set
			while (rs.next()) {
				ProductDiscount cdt = new ProductDiscount(productName,manufacturer, rs.getInt("DISCOUNT"),rs.getString("START_DATE"), rs.getString("END_DATE"));
				
					if(!ans.add(cdt))
					{
						System.out.println("Didnt add cdt to list");
					}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return ans;
	}
	public ArrayList<ProductDiscount> getAllProductDiscountByProductNameAndManufacturerAndDate(String productName, String manufacturer, String currDate)
	{
		ArrayList<ProductDiscount> listProductDiscount = getAllProductDiscountByProductNameAndManufacturer(productName, manufacturer);
		ArrayList<ProductDiscount> ans = new ArrayList<ProductDiscount>();
		for (ProductDiscount productDiscount : listProductDiscount) {
			if(!checkIfDiscountIsRelevant(currDate, productDiscount.getStartDate(),productDiscount.getEndDate()))
				{
					ans.add(productDiscount);
				}
			}
		return ans;
	}

	private boolean checkIfDiscountIsRelevant(String currDate, String  startDate, String endDate) 
	{
		//the period for discount is in the future
		if(compareBetweenDates(currDate, startDate) < 0)
			return false;
		//the period  for discount finished
		if(compareBetweenDates(endDate, currDate) < 0)
			return false;
		return true;
	}

	
	
	private ArrayList<ProductDiscount> getAllProductDiscount()
	{
		ArrayList<ProductDiscount> ans = new ArrayList<ProductDiscount>();
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery("SELECT * "
					+ "FROM "+this.name+" ");
			// loop through the result set
			while (rs.next()) {
				ProductDiscount cdt = new ProductDiscount(rs.getString("PRODUCT_NAME"),rs.getString("MANUFACTURER"), rs.getInt("DISCOUNT"),rs.getString("START_DATE"), rs.getString("END_DATE"));
				if(!ans.add(cdt))
				{
					System.out.println("Didnt add cdt to list");
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return ans;
	}

	public boolean checkCollision(String startDate, String endDate, String productName, String manufacturer) {
		ArrayList<ProductDiscount> listPd = getAllProductDiscountByProductNameAndManufacturer(productName, manufacturer);
		for (ProductDiscount productDiscount : listPd) 
		{
			//--s1---s2---e2--e1---
			//start1 <= start2
			//end1 >= end2
			if((compareBetweenDates(startDate, productDiscount.getStartDate()) <= 0) &&
					(compareBetweenDates(endDate, productDiscount.getEndDate()) >= 0))
			{
				return false;
			}
			// ----s1----s2----e1----e2
			//start1 <= start2
			//end1 >= start2
			//end1 <= end2
			if((compareBetweenDates(startDate, productDiscount.getStartDate()) <= 0) &&
					(compareBetweenDates(endDate, productDiscount.getStartDate()) >= 0) &&
					(compareBetweenDates(endDate, productDiscount.getEndDate()) <= 0))
			{
				return false;
			}
			// ----s2----s1----e2----e1
			//start1 <= start2
			//end1 >= start2
			//end1 <= end2
			if((compareBetweenDates(startDate, productDiscount.getStartDate()) >= 0) &&
					(compareBetweenDates(startDate, productDiscount.getEndDate()) <= 0) &&
					(compareBetweenDates(endDate, productDiscount.getEndDate()) >= 0))
			{
				return false;
			}
			// ----s2----s1----e1----e2
			//start1 <= start2
			//end1 >= start2
			//end1 <= end2
			if((compareBetweenDates(startDate, productDiscount.getStartDate()) >= 0) &&
					(compareBetweenDates(startDate, productDiscount.getEndDate()) <= 0) &&
					(compareBetweenDates(endDate, productDiscount.getEndDate()) <= 0))
			{
				return false;
			}

		}
		return true;
	}

	

	public static int compareBetweenDates(String date1, String date2) 
	{
		String[] arrDate1 = date1.split("/");	
		String[] arrDate2 = date2.split("/");
		if(Integer.compare(Integer.parseInt(arrDate1[2]),Integer.parseInt(arrDate2[2])) != 0)
			return 		Integer.compare(Integer.parseInt(arrDate1[2]),Integer.parseInt(arrDate2[2])) ;
		if(Integer.compare(Integer.parseInt(arrDate1[1]),Integer.parseInt(arrDate2[1])) != 0)
			return 		Integer.compare(Integer.parseInt(arrDate1[1]),Integer.parseInt(arrDate2[1])) ;
		if(Integer.compare(Integer.parseInt(arrDate1[0]),Integer.parseInt(arrDate2[0])) != 0)
			return 		Integer.compare(Integer.parseInt(arrDate1[0]),Integer.parseInt(arrDate2[0])) ;
		return 0;
	}
	
	public boolean checkIfDiscExist(String productName,String manufacturer){
		if(getAllProductDiscountByProductNameAndManufacturer(productName, manufacturer).size() != 0)
			return true;
		return false;
	}
	
	public boolean removeDiscountByProductNameManufacturerStartTimeEndTime(String productName,String manufacturer,String startTime,String endTime){
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeUpdate("DELETE FROM "+this.name
					+ " WHERE PRODUCT_NAME= '" + productName + "' AND MANUFACTURER ='"+manufacturer+"' AND START_DATE ='"+startTime+"'"
					+ " AND END_DATE ='"+endTime+"'");
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean clearTable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void insertHardCodedDefaultData() {
		// TODO Auto-generated method stub
		
	}

}
