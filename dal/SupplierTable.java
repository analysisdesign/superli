package dal;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modules.Contact;
import modules.ProvidedProduct;
import modules.Supplier;

public class SupplierTable implements IDBManager {

	private Connection conn;
	private String name;

	public SupplierTable() {
		this.conn = null;
		this.name = "SUPPLIERS";
	}

	@Override
	public Connection connectToDB() 
	{
		this.conn = DbConnectionSingelton.getInstance();
		//createTable(this.name);
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {
		Statement stmt;
		try {
			stmt = this.conn.createStatement();
			String sql = "CREATE TABLE IF NOT EXISTS " + tableName + 
					" (ID    TEXT      NOT NULL,"
					+ " NAME           TEXT    NOT NULL, " 
					+ " BankAccount           TEXT    NOT NULL, " 
					+ " TermOfPayment           TEXT     NOT NULL, "
					+ " ProvideTransport       TEXT DEFAULT F, "
					+ " ADDRESS 	TEXT   NOT NULL,"
					+ " primary key (ID))";

			stmt.executeUpdate(sql);
			stmt.close();
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create Supplier table");
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean closeConnection() {
		/*try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;*/
		return false;
	}

	public boolean addSupplier(Supplier newSupplier) {
		try {
			if (getByID(String.valueOf(newSupplier.getID())) != null)
				return false;
			PreparedStatement stmt = conn.prepareStatement(
					"insert into Suppliers (ID , NAME , BankAccount, TermOfPayment, ProvideTransport,ADDRESS) VALUES(?,?,?,?,?,?)");
			stmt.setString(1, newSupplier.getID());
			stmt.setString(2, newSupplier.getName());
			stmt.setString(3, newSupplier.getBankAccount());
			stmt.setString(4, newSupplier.getTermOfPayment());
			if (newSupplier.isProvideTransport()) {
				stmt.setString(5, "T");
			} else {
				stmt.setString(5, "F");
			}
			stmt.setString(6, newSupplier.getAddress());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public ArrayList<Supplier> getAll()
	{
		ArrayList<Supplier> ans = new ArrayList<Supplier>();
		try {
			Statement stmt = conn.createStatement();
			ResultSet reader = stmt.executeQuery("SELECT * FROM "+this.name);
			while (reader.next()) {
				Supplier row = new Supplier(reader.getString("ID"),reader.getString("NAME"),reader.getString("BankAccount"),
						reader.getString("TermOfPayment"), false,reader.getString("ADDRESS"));
				if (reader.getString("ProvideTransport").equals("T")) {
					row.setProvideTransport(true);
				}
				row.setContacts(getContacts(row.getID()));
				ans.add(row);
			}
		} catch (SQLException e) {
		}
		return ans;
	}
	/**
	 * return true if the supplier exists, else return false
	 * @param supplierID
	 * @return
	 */
	public boolean checkIfExists(String supplierID)
	{
		return getByID(supplierID) != null;
	}

	/**
	 * Retrieves the supplier from database by its id.
	 * 
	 * @param id
	 *            the id of the user
	 * @return null case not found
	 */
	public Supplier getByID(String id) {
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM Suppliers WHERE ID='" + id+"'");
			// loop through the result set
			while (rs.next()) {
				Supplier ans = new Supplier(rs.getString("ID"),rs.getString("NAME"), rs.getString("BankAccount"),
						rs.getString("TermOfPayment"), false,rs.getString("ADDRESS"));
				if (rs.getString("ProvideTransport").equals("T")) {
					ans.setProvideTransport(true);
				}
				ans.setContacts(getContacts(ans.getID()));
				return ans;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private List<Contact> getContacts(String SupplierId) {
		List<Contact> ans = new ArrayList<Contact>();
		try {
			ResultSet reader = conn.createStatement()
					.executeQuery("Select * FROM Contacts WHERE SUPPLIERID = " + SupplierId);
			while (reader.next()) {
				Contact row = new Contact(reader.getString("ID"), reader.getString("FIRSTNAME"),
						reader.getString("LASTNAME"), reader.getString("PHONE"));
				ans.add(row);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ans;
	}

	public boolean update(Supplier s, String fieldName, String input) {
		String sql;
		try {
			sql = "UPDATE Suppliers" + " SET " + fieldName + " = '" + input + "' WHERE ID = " + s.getID();
			conn.createStatement().executeUpdate(sql);

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error: cannot update supplier table");
			return false;
		}
		return true;
	}

	@Override
	public boolean clearTable() {
		try {
			conn.createStatement().executeUpdate("DELETE FROM " + this.name + ";");
//			conn.createStatement().execute("VACUUM;");
			
		} catch (SQLException e) {
			System.out.println("ERROR: "+this.getClass()+".clearTable()");
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void insertHardCodedDefaultData() {

	}
	
	public ArrayList<Supplier> getAllSuppliersForProducts(ProvidedProduct p) 
	{
		ProductSupplierTable pst =DataBase.getDataBase().productSupplierDBM;
		
		ArrayList<Supplier> allSupplier = (ArrayList<Supplier>)getAll();
		ArrayList<Supplier> ans =  new ArrayList<Supplier>();
		boolean supplyAll;
		for (Supplier supplier : allSupplier) 
		{
			supplyAll = true;
				if(!pst.checkIfSupplierSupplyProduct(supplier, p))
				{
					supplyAll = false;
				}
			if(supplyAll)
				ans.add(supplier);
		}
		return ans;
		
	}

}
