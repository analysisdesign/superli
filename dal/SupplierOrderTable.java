package dal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modules.Order;
import modules.Supplier;
import modules.ProductInCategory;
import modules.ProvidedProduct;


public class SupplierOrderTable implements IDBManager {

	private Connection conn;
	private String name;
	public SupplierOrderTable() {
		this.conn = null;
		this.name = "SUPPLIER_ORDERS";
	}
	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		//createTable(this.name);
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {
		Statement stmt;
		try {
			stmt = this.conn.createStatement();
			String sql = 
					"CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (OrderID    TEXT      NOT NULL ,"
					+ " SupplierID           TEXT    NOT NULL, "
					+ "primary key (OrderID),"
					+ "foreign key (SupplierID) references Suppliers(ID))";

			stmt.executeUpdate(sql);
			stmt.close();
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create Supplier-Order table");
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean closeConnection() {
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public List<Order> getAll(Supplier s) {
		List <Order>ans=new ArrayList<Order>();
		try {
			ResultSet reader=conn.createStatement().executeQuery("Select * FROM SupplierOrder");
			while(reader.next())
			{
				Order row = new Order();
				row.setSuplier(s);
				row.setOrderNumber(reader.getString("OrderID"));
				row.setDays(getDays(row.getOrderNumber()));
			    row.setProducts(getProducts(row.getOrderNumber()));
			    ans.add(row);
			}
		} catch (SQLException e) {}
		return ans;
	}
	private List<Integer> getDays(String orderNumber) {
		List<Integer> ans=new ArrayList<Integer>();
		try {
			ResultSet reader=conn.createStatement().executeQuery("Select * FROM ORDERS_DAYS WHERE OrderID = "+orderNumber);
			while(reader.next())
			{
				ans.add(Integer.valueOf(reader.getString("Day")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return ans;
	}
	public Map<ProvidedProduct, Integer> getProducts(String orderNumber) {
		Map<ProvidedProduct, Integer>ans=new HashMap<ProvidedProduct, Integer>();
		try {
			ResultSet reader=conn.createStatement().executeQuery("Select * FROM Orders WHERE OrderID = '"+orderNumber+"'");
			while(reader.next())
			{
				DataBase dbBase=DataBase.getDataBase();
				//Supplier s=dbBase.getSupplierById(getSupplierIDFromOrderID(orderNumber));
				ProvidedProduct row=dbBase.getProvidedProductByNameManufacturerAndSupplier
						(reader.getString("PRODUCT_NAME"),reader.getString("MANUFACTURER"),getSupplierIDFromOrderID(orderNumber));
				ans.put(row, Integer.parseInt(reader.getString("Amount")));
			}
		} catch (SQLException e) {}
		return ans;
	}
	@Override
	public boolean clearTable() {
		try {
			conn.createStatement().executeUpdate("DELETE FROM " + this.name + ";");
//			conn.createStatement().execute("VACUUM;");
		} catch (SQLException e) {
			System.out.println("ERROR: "+this.getClass()+".clearTable()");
			e.printStackTrace();
		}
		return true;	}
	@Override
	public void insertHardCodedDefaultData() {
		// TODO Auto-generated method stub
		
	}
	private String getSupplierIDFromOrderID(String OrderID)
	{
		try {
			ResultSet reader=conn.createStatement().executeQuery("Select * FROM SUPPLIER_ORDERS WHERE OrderID = "+OrderID);
			while(reader.next())
			{
				return reader.getString("SupplierID");
			}
		} catch (SQLException e) {}
		return null;
	}

	public void deleteOrderById(int OrderId){
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeQuery(" DELETE FROM "+this.name+" WHERE OrderID = '"+OrderId+"'");
		} catch (SQLException e) {
			//System.out.println(e.getMessage());
		}
	}
	
}
