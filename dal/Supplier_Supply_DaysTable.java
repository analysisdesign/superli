package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modules.Contact;
import modules.Order;
import modules.ProvidedProduct;
import modules.SupplierSupplyDays;

public class Supplier_Supply_DaysTable implements IDBManager {

	private Connection conn;
	public String name;

	public Supplier_Supply_DaysTable() {
		this.conn = null;
		this.name = "SUPPLIER_SUPPLY_DAYS";
	}
	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		createTable(this.name);
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {
		Statement stmt;
		try {
			stmt = this.conn.createStatement();
			String sql = "CREATE TABLE IF NOT EXISTS SUPPLIER_SUPPLY_DAYS "
					+ "(DAY    INT      NOT NULL,"
					+ "SUPPLIER_ID           TEXT    NOT NULL, " 
					+ "primary key (DAY, SUPPLIER_ID)," 
					+ "foreign key (SUPPLIER_ID) references Suppliers(ID))";

			stmt.executeUpdate(sql);
			stmt.close();
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create Supplier supply days table");
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean clearTable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean closeConnection() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void insertHardCodedDefaultData() {
		// TODO Auto-generated method stub

	}


	/**
	 * Add Supplier supply days to db
	 * @param days
	 * @param supplierID
	 * @return
	 */
	public boolean addSupplierDays(SupplierSupplyDays supplyDays)
	{
		try {
			for (Integer day : supplyDays.getDays())
			{
				PreparedStatement stmt = conn.prepareStatement(
						"insert into "+ this.name +"  (DAY , SUPPLIER_ID) VALUES(?,?)");
				stmt.setInt(1, day );
				stmt.setString(2, supplyDays.getID());
				stmt.executeUpdate();
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean updateSupplierDays(SupplierSupplyDays supplyDays)
	{
		if(deleteSupplierDays(supplyDays.getID()))
		{
			return addSupplierDays(supplyDays);
		}
		System.out.println("Error deleting supplier supply days");
		return false;
	}
	public boolean deleteSupplierDays(String supplierID) 
	{
		try {
			PreparedStatement stmt = conn.prepareStatement(
					"DELETE FROM "+this.name +" WHERE SUPPLIER_ID = '" +supplierID+"'");
			stmt.executeUpdate();
		} 
		catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public ArrayList<Integer> getSupplyDays(String supplierID)
	{
		ArrayList<Integer> ans = new ArrayList<Integer>();
		try {
			ResultSet reader = conn.createStatement()
					.executeQuery("Select * FROM "+this.name+" WHERE SUPPLIER_ID = '" + supplierID +"'");
			while (reader.next()) {
				ans.add(reader.getInt("DAY"));
			}
		} catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return ans;
	}
	public boolean checkIfExistDayForSupplier(String supplierID, int Day)
	{
		try {
			ResultSet reader = conn.createStatement()
					.executeQuery("Select * FROM "+this.name+" WHERE SUPPLIER_ID = '" + supplierID +"' AND DAY = '"+Day +"'");
			while (reader.next()) {
				return true;
			}
		} catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
}
