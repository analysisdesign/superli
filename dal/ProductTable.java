package dal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import modules.*;

public class ProductTable implements IDBManager{

	private Connection conn;
	private String name;

	public String getName() {
		return name;
	}

	public ProductTable() {
		this.conn = null;
		this.name = "PRODUCTS_IN_SHOP";
	}

	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {

		//System.out.println("Creating table...");

		Statement stmt;
		try {
			stmt = this.conn.createStatement();

			String sql = 
					"CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ " NAME TEXT NOT NULL,"
					+ " MANUFACTURER TEXT NOT NULL, " 
					+ " EXP_DATE TEXT, "
					+ " DAMAGED BOOLEAN ,"                 // 0 is OK and 1 is damaged
					+ " LOCATION TEXT NOT NULL,"
					+ " SHOP_ID INTEGER,"
					+ " FOREIGN KEY (SHOP_ID) REFERENCES SHOPS (ID)"
					+ " FOREIGN KEY (NAME,MANUFACTURER) REFERENCES PRODUCTS_CATEGORIES (PRODUCT_NAME,MANUFACTURER))";

			stmt.executeUpdate(sql);
			stmt.close();

			//System.out.println("Product table created!");
			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create product table");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		try {
			Product p = (Product)record;
			PreparedStatement stmt = conn.prepareStatement
					("insert into PRODUCTS_IN_SHOP (NAME,MANUFACTURER,DAMAGED,EXP_DATE,LOCATION,SHOP_ID) VALUES(?,?,?,?,?,?)");
			stmt.setString(1, p.getName());
			stmt.setString(2, p.getManufacturer());
			stmt.setBoolean(3, p.isDamaged());
			stmt.setString(4, p.getExpDate());
			stmt.setString(5, p.getLocation());
			stmt.setInt(6, p.getShopID());

			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void printGeneralInventoryReport(int shopID){
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT NAME,MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP"
					+ " WHERE SHOP_ID = '"+shopID+"'"
					+ " GROUP BY NAME,MANUFACTURER");

			System.out.println("---------------Products in Superli---------------");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Product [ Name = " + rs.getString("NAME")+", Manufacturer = " + rs.getString("MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
			}

			System.out.println();
			System.out.println();

			rs    = stmt.executeQuery("SELECT NAME,MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP"
					+ " WHERE LOCATION = 'WAREHOUSE' AND SHOP_ID = '"+shopID+"'"
					+ " GROUP BY NAME,MANUFACTURER");
			System.out.println("---------------Products in warehouse---------------");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Product [ Name = " + rs.getString("NAME")+", Manufacturer = " + rs.getString("MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
			}

			System.out.println();
			System.out.println();

			rs  = stmt.executeQuery("SELECT NAME,MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP"
					+ " WHERE LOCATION = 'STORE' AND SHOP_ID = '"+shopID+"'"
					+ " GROUP BY NAME,MANUFACTURER");

			System.out.println("---------------Products in store---------------");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Product [ Name = " + rs.getString("NAME")+", Manufacturer = " + rs.getString("MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void printProductsByNameAndManufacturer(String product,String manufacturer,int shopID) {
		try {
			Statement stmt  = conn.createStatement();					
			ResultSet rs  = stmt.executeQuery("SELECT ID,NAME,MANUFACTURER,EXP_DATE,DAMAGED,LOCATION"
					+ " FROM PRODUCTS_IN_SHOP"
					+ " WHERE NAME = '"+product+"' AND MANUFACTURER = '"+manufacturer+"' AND SHOP_ID = '"+shopID+"'");

			System.out.println("---------------Products in Superli---------------");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Product [ ID = " + rs.getInt("ID") 
											+ ", Name = " + rs.getString("NAME")
											+ ", Manufacturer = " + rs.getString("MANUFACTURER") 
											+ ", Expiration date = " + rs.getString("EXP_DATE")
											+ ", Damaged = " + ((rs.getBoolean("DAMAGED")==true)?"true":"false") 
											+ ", Location = "+rs.getString("LOCATION")+" ]");
			}

			System.out.println();
			System.out.println();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public void printDamagedReport(int shopID) {
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT NAME,MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP"
					+ " WHERE DAMAGED = '1' AND SHOP_ID = '"+shopID+"'"
					+ " GROUP BY NAME,MANUFACTURER");

			System.out.println("---------------Products in Superli---------------");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Product [ Name = " + rs.getString("NAME")+", Manufacturer = " + rs.getString("MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
			}

			System.out.println();
			System.out.println();

			rs    = stmt.executeQuery("SELECT NAME,MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP"
					+ " WHERE LOCATION = 'WAREHOUSE' AND DAMAGED = '1' AND SHOP_ID = '"+shopID+"'"
					+ " GROUP BY NAME,MANUFACTURER");
			System.out.println("---------------Products in warehouse---------------");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Product [ Name = " + rs.getString("NAME")+", Manufacturer = " + rs.getString("MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
			}

			System.out.println();
			System.out.println();

			rs  = stmt.executeQuery("SELECT NAME,MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP"
					+ " WHERE LOCATION = 'STORE' AND DAMAGED = '1' AND SHOP_ID = '"+shopID+"'"
					+ " GROUP BY NAME,MANUFACTURER");

			System.out.println("---------------Products in store---------------");
			// loop through the result set
			while (rs.next()) {
				System.out.println("Product [ Name = " + rs.getString("NAME")+", Manufacturer = " + rs.getString("MANUFACTURER") + ", Number Of Products = " + rs.getInt("NUMBER_OF_PRODUCTS")+" ]");
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}		
	}

	public Product getProductById(String id) {
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery("SELECT * FROM "+this.name+" WHERE ID='"+id+"'");
			// loop through the result set
			while (rs.next()) {
				return new Product(rs.getInt("ID"),rs.getString("Name") , rs.getString("MANUFACTURER"),"", rs.getBoolean("DAMAGED"),rs.getString("LOCATION"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public boolean closeConnection()
	{
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/*
	 * if is number return true
	 */
	public boolean checkIfIsNumber(String toCheck)
	{
		try{Integer.parseInt(toCheck);}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}
	public boolean checkIfDouble(String toCheck)
	{
		try{Double.parseDouble(toCheck);}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}

	public boolean checkIfExistByID(String id) { //ID is unique so no need to check in different shops
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM "+this.name+" WHERE ID="+id);
			if(rs.next()){

				return true;
			}
			return false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	public void removeByID(String id) { //ID is unique so no need to check in different shops
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeQuery("DELETE FROM "+this.name+" WHERE ID="+id);
		} catch (SQLException e) {
			//System.out.println(e.getMessage());
		}
	}

	public ResultSet allValidProducts(int shopID){
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM "+this.name
					+ " WHERE DAMAGED = 0 AND SHOP_ID = '"+shopID+"' AND EXP_DATE NOT NULL");
			return rs;
		} catch (SQLException e) {
			System.out.println(e.getMessage()+" Error in allValidProducts");
		}
		return null;
	}
	
	public boolean updateStatusById(int id,boolean isDamaged,String val){
		if(isDamaged){
			try {
				Statement stmt  = conn.createStatement();
				stmt.executeUpdate("UPDATE "+this.name
								+ " SET DAMAGED = '"+val+"'"
								+ " WHERE ID = '"+id+"'");
				return true;
			} catch (SQLException e) {
				return false;
			}
		}
		else{
			try {
				Statement stmt  = conn.createStatement();
				stmt.executeUpdate("UPDATE "+this.name
								+ " SET LOCATION = '"+val+"'"
								+ " WHERE ID = '"+id+"'");
				return true;
			} catch (SQLException e) {
				return false;
			}
		}
	}
	/*
	 * if is of format dd/mm/yyyy return true
	 */
	public boolean checkIfIsDate(String toCheck)
	{
		String arr[] = toCheck.split("/");
		if(arr.length != 3)
			return false;
		for(int i = 0; i < arr.length; i++)
		{
			if(!checkIfIsNumber(arr[i]))
			{
				return false;
			}
		}
		if(!checkRangeForNumber(Integer.parseInt(arr[0]),1,31))
			return false;
		if(!checkRangeForNumber(Integer.parseInt(arr[1]),1,12))
			return false;
		Calendar cal =  Calendar.getInstance();
		if(!checkRangeForNumber(Integer.parseInt(arr[2]),cal.get(Calendar.YEAR), cal.get(Calendar.YEAR) + 20))
			return false;
		return true;


	}
	/*
	 * if is in range return true
	 */
	private boolean checkRangeForNumber(int number, int start, int end) {
		if(number <= end && number >= start)
			return true;
		return false;
	}

	public boolean addProductsToInventory(String productName, String manufacturer, String expDate, int amount,int shopID) 
	{
		
		//if exp doesn't exist enter null.
		if(expDate.equals("-"))
			expDate = null;
		for(int i = 0; i < amount; i++)
		{
			//check that exactly the amount of products were added.
			if(!insertTable(new Product(productName, manufacturer, expDate, false, "WAREHOUSE",shopID)))
			{
				System.out.println("only "+i+" products were added");
				return false;
			}
		}
		return true;
	}
	
	public int getLastAddedId(){
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT last_insert_rowid() as LAST_INDEX");
			return rs.getInt("LAST_INDEX");
		} catch (SQLException e) {
			return -1;
		}
	}
	
	public ResultSet getAllProductsByShopID(int shopID){
		try {
			Statement stmt  = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT NAME,MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS,LOCATION "
					+ " FROM "+this.name
					+ " WHERE SHOP_ID = '"+shopID+"'"
					+ " GROUP BY NAME,MANUFACTURER,SHOP_ID");
			return rs;
		} catch (SQLException e) {
			System.out.println("bad");
			return null;
		}
	}

	@Override
	public boolean clearTable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void insertHardCodedDefaultData() {
		// TODO Auto-generated method stub
		
	}
}
