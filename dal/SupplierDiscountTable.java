package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import modules.CategoryDiscount;
import modules.Order;
import modules.ProductInCategory;
import modules.ProvidedProduct;
import modules.Supplier;
import modules.SupplierDiscount;

public class SupplierDiscountTable implements IDBManager {


	private Connection conn;
	private String name;

	public SupplierDiscountTable() {
		this.conn = null;
		this.name = "SUPPLIER_DISCOUNTS";
	}

	@Override
	public Connection connectToDB() {
		this.conn = DbConnectionSingelton.getInstance();
		return this.conn;
	}

	@Override
	public boolean createTable(String tableName) {
		Statement stmt;
		try {
			stmt = this.conn.createStatement();

			String sql = 
					"CREATE TABLE IF NOT EXISTS " + tableName 
					+ " (PRODUCT_NAME TEXT NOT NULL, " 
					+ " MANUFACTURER TEXT NOT NULL,"
					+ " SUPPLIER_ID TEXT NOT NULL,"
					+ " START_DATE TEXT NOT NULL,"
					+ " END_DATE TEXT NOT NULL,"
					+ " DISCOUNT TEXT NOT NULL,"
					+ " MINIMAL_AMOUNT INT NOT NULL,"
					+ " PRIMARY KEY (PRODUCT_NAME,MANUFACTURER,SUPPLIER_ID,START_DATE, END_DATE,MINIMAL_AMOUNT),"
					+ " FOREIGN KEY(PRODUCT_NAME,MANUFACTURER) REFERENCES PRODUCT_CATEGORIES(NAME,MANUFACTURER),"
					+ " FOREIGN KEY(SUPPLIER_ID) REFERENCES SUPPLIERS(ID))";
			stmt.executeUpdate(sql);
			stmt.close();

			return true;

		} catch (SQLException e) {
			System.out.println("Error: cannot create "+this.name+" table");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean insertTable(Object record) {
		try {
			SupplierDiscount supplierDiscount = (SupplierDiscount)record;
			PreparedStatement stmt = conn.prepareStatement
					("insert into " + this.name+ " (PRODUCT_NAME,MANUFACTURER,SUPPLIER_ID,DISCOUNT,START_DATE,END_DATE,MINIMAL_AMOUNT) VALUES(?,?,?,?,?,?,?)");
			stmt.setString(1, supplierDiscount.getProduct());
			stmt.setString(2, supplierDiscount.getManufacturer());
			stmt.setString(3, supplierDiscount.getSupplierID());
			stmt.setString(4, supplierDiscount.getDiscount()+"");
			stmt.setString(5, supplierDiscount.getStartDate()+"");
			stmt.setString(6, supplierDiscount.getEndDate()+"");
			stmt.setString(7, supplierDiscount.getMinimalAmount()+"");
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean removeFromTable(Object record) {
		SupplierDiscount sd =  (SupplierDiscount)record;
		try {
			Statement stmt  = conn.createStatement();
			stmt.executeUpdate("DELETE FROM "+this.name+" "
					+ " WHERE PRODUCT_NAME = '"+sd.getProduct()+"' "
					+ " AND MANUFACTURER = '"+sd.getManufacturer()+"' "
					+ " AND SUPPLIER_ID = '"+sd.getSupplierID()+"'"
					+ " AND START_DATE = '"+sd.getStartDate()+"'"
					+ " AND END_DATE = '"+sd.getEndDate()+"'"
					+ " AND DISCOUNT = '"+sd.getDiscount()+"'"
					+ " AND MINIMAL_AMOUNT = '"+sd.getMinimalAmount()+"'");

		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean closeConnection() {
		try {
			this.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean checkIfExist(Object record) {
		SupplierDiscount sd =  (SupplierDiscount)record;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM "+this.name+" "
					+ " WHERE PRODUCT_NAME = '"+sd.getProduct()+"' "
					+ " AND MANUFACTURER = '"+sd.getManufacturer()+"' "
					+ " AND SUPPLIER_ID = '"+sd.getSupplierID()+"'"
					+ " AND START_DATE = '"+sd.getStartDate()+"'"
					+ " AND END_DATE = '"+sd.getEndDate()+"'"
					+ " AND DISCOUNT = '"+sd.getDiscount()+"'"
					+ " AND MINIMAL_AMOUNT = '"+sd.getMinimalAmount()+"'");
			if(rs.next()){
				return true;
			}
			return false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean clearTable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void insertHardCodedDefaultData() {
		// TODO Auto-generated method stub

	}

	public int CalculatePrice(Order selected) {
		int ans = 0;
		for (ProvidedProduct p : selected.getProducts().keySet()) {
			int amount = selected.getProducts().get(p);
			for (SupplierDiscount d : getDiscounts(p.getProduct_name(),p.getManufacturer(), selected.getSuplier().getID())) {
				while (d.getMinimalAmount() <= amount) {
					ans += Double.parseDouble(p.getCostPrice()) * d.getDiscount();
					amount = amount - d.getMinimalAmount();
				}
			}
			if (amount > 0) {
				ans += Double.parseDouble(p.getCostPrice()) * amount;
			}
		}
		return ans;
	}
	public ArrayList<SupplierDiscount> getDiscounts(String productName,String manufacturer, String SupplierID) {
		ArrayList<SupplierDiscount> ans = new ArrayList<SupplierDiscount>();
		try {
			ResultSet reader = conn.createStatement().executeQuery("Select * FROM SUPPLIER_DISCOUNTS WHERE"
					+ " PRODUCT_NAME = '" + productName 
					+ "' AND MANUFACTURER = '" + manufacturer 
					+ "' AND SUPPLIER_ID = '" + SupplierID + "'");
			while (reader.next()) {
				SupplierDiscount row = new SupplierDiscount(productName, manufacturer ,
						SupplierID, Double.parseDouble(reader.getString("DISCOUNT")),
						reader.getString("START_DATE"),reader.getString("END_DATE"),
						Integer.parseInt(reader.getString("MINIMAL_AMOUNT")));
				ans.add(row);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ans;
	}

	public double getSellPriceAfterSupplierDiscount(String productName,String manufacturer,String supplierID,int amount)
	{
		int amountTemp=amount;
		DataBase db= DataBase.getDataBase();
		double costPrice=db.getCostPriceOfProduct(productName, manufacturer, supplierID);
		double currentcost=0;
		for(SupplierDiscount d:getDiscounts(productName,manufacturer, supplierID))
		{
			while (d.getMinimalAmount() <= amount) {
				currentcost = (d.getMinimalAmount())* (currentcost + costPrice*d.getDiscount()/100);
				amount = amount - d.getMinimalAmount();
			}
		}
		if (amount > 0) {
			currentcost += (costPrice * amount);
		}
		return currentcost/amountTemp;
	}
	public Supplier getBestSupplier(ProvidedProduct p , int amount,int shopID,int dailyConsumption,ArrayList<Integer> delieveryDays) 
	{
		SupplierTable st = DataBase.getDataBase().supplierDBM;
		Supplier_Supply_DaysTable ssd = DataBase.getDataBase().supplier_Supply_DaysTable;
		ArrayList<Supplier> tmpSuppliers = st.getAllSuppliersForProducts(p);
		ArrayList<Supplier> allSuppliers = new ArrayList<Supplier>();
		if(delieveryDays==null){
			Calendar cal =  Calendar.getInstance();
			int dayToOrder= cal.get(Calendar.DAY_OF_WEEK)+1;
			for (int i = 0; i < 7; i++) {
				if(dayToOrder==8)
					dayToOrder=1;
				for (Supplier supplier : tmpSuppliers) {
					if(ssd.checkIfExistDayForSupplier(supplier.getID(),dayToOrder))
						allSuppliers.add(supplier);
				}
				if(allSuppliers.size()!=0){
					amount = amount + dailyConsumption*i;
					break;
				}
				dayToOrder++;
			}
		}
		else{
			int totalDays = delieveryDays.size();
			int supplierApprovedDays = 0;
			for (Supplier supplier : tmpSuppliers) 
			{
				supplierApprovedDays = 0;
				for (Integer wantedDayOfDelivery : delieveryDays)
				{
					if(ssd.checkIfExistDayForSupplier(supplier.getID(),wantedDayOfDelivery))
					{
						supplierApprovedDays++;
					}
					else{
						break;
					}
				}
				if(supplierApprovedDays==totalDays)
					allSuppliers.add(supplier);
			}
		}
		if(allSuppliers.size() == 0)
		{
			System.out.println("There is no 1 supplier for this product");
			return null;
		}
		Supplier ans = allSuppliers.get(0);
		double minSum =  Double.POSITIVE_INFINITY;
		for(Supplier s:allSuppliers)
		{
			double sellPrice = getSellPriceAfterSupplierDiscount(p.getProduct_name(), p.getManufacturer(), s.getID(), 1);
			double maxSumForProduct = sellPrice * amount;
			ArrayList<SupplierDiscount> allDiscountForProduct = getDiscounts(p.getProduct_name(),p.getManufacturer(), s.getID());
			for (SupplierDiscount d :  allDiscountForProduct)
			{
				if(amount >= d.getMinimalAmount())
				{
					double newPriceAfterDiscount = (d.getDiscount() * d.getMinimalAmount() / 100.0);
					newPriceAfterDiscount = newPriceAfterDiscount + (amount- d.getMinimalAmount())* sellPrice;
					if(maxSumForProduct > newPriceAfterDiscount) 
						maxSumForProduct = newPriceAfterDiscount;
				}
			}
			if(maxSumForProduct < minSum )
			{
				minSum = maxSumForProduct;
				ans = s;
			}
		}
		return ans;
	}

}
