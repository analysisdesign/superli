package modules;

public class Product {

	private int ID;
	private String name;
	private String manufacturer;
	private String expDate;
	private boolean damaged;
	private String location;
	private int shopID;

	public Product(int iD, String name, String manufacturer, String expDate, boolean damaged, String location,int shopID) {
		ID = iD;
		this.name = name;
		this.manufacturer = manufacturer;
		this.expDate = expDate;
		this.damaged = damaged;
		this.location = location;
		this.setShopID(shopID);
	}

	public Product(String name, String manufacturer, String expDate, boolean damaged, String location,int shopID) {
		this.name = name;
		this.manufacturer = manufacturer;
		this.expDate = expDate;
		this.damaged = damaged;
		this.location = location;
		this.setShopID(shopID);
	}
	
	public Product(int id,String name, String manufacturer, String expDate, boolean damaged, String location) {
		this.ID = id;
		this.name = name;
		this.manufacturer = manufacturer;
		this.expDate = expDate;
		this.damaged = damaged;
		this.location = location;
	}
	
	public int getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public String getExpDate() {
		return expDate;
	}

	public boolean isDamaged() {
		return damaged;
	}

	public String getLocation() {
		return this.location;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public void setDamaged(boolean damaged) {
		this.damaged = damaged;
	}


	public void setLocation(String location){
		this.location = location;
	}

	public int getShopID() {
		return shopID;
	}

	public void setShopID(int shopID) {
		this.shopID = shopID;
	}

}
