package modules;

public class Contact extends Person {

	private String PhoneNumber;
	
	public Contact(String id, String firstName, String lastName,String PhoneNumber) {
		super(id, firstName, lastName);
		this.PhoneNumber=PhoneNumber;
	}
	public String getPhoneNumber() {
		return PhoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}
	@Override
	public String toString() {
		return "ID: " + id + " ,first name: " + firstName + " , last name: " + lastName + " ,PhoneNumber: " + PhoneNumber;
	}
	

}
