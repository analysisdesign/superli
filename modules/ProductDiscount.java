package modules;

public class ProductDiscount {

	private String product;
	private String manufacturer;
	private int discount;
	private String startDate;
	private String endDate;
	
	public ProductDiscount(String product,String manufacturer, int discount, String startDate, String endDate) {
		super();
		this.product = product;
		this.manufacturer = manufacturer;
		this.discount = discount;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getProduct() {
		return product;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public int getDiscount() {
		return discount;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setProduct(String product) {
		this.product = product;
	}
	
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
