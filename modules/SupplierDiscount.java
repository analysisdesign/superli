package modules;

public class SupplierDiscount {
	
	private String product;
	private String manufacturer;
	private String supplierID;
	private double discount;
	private int minimalAmount;
	private String startDate;
	private String endDate;
	
	public SupplierDiscount
	(String product, String manufacturer, String supplierID, double discount
			,String startDate,String endDate,int minimalAmount) {
		this.product = product;
		this.manufacturer = manufacturer;
		this.discount = discount;
		this.supplierID = supplierID;
		this.startDate = startDate;
		this.endDate = endDate;
		this.minimalAmount = minimalAmount;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getSupplierID() {
		return supplierID;
	}
	public void setSupplierID(String supplierID) {
		this.supplierID = supplierID;
	}
	public int getMinimalAmount() {
		return minimalAmount;
	}
	public void setMinimalAmount(int minimalAmount) {
		this.minimalAmount = minimalAmount;

	}

}
