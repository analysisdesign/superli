package modules;

public class Category {
	private String categoryName;
	private String parent_Category;
	
	public Category(String categoryName, String parent_Category) {
		super();
		this.categoryName = categoryName;
		this.parent_Category = parent_Category;
	}
	public Category() {
	}
	public String getCategoryName() {
		return categoryName;
	}
	public String getParent_Category() {
		return parent_Category;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public void setParent_Category(String parent_Category) {
		this.parent_Category = parent_Category;
	}	
}
