package modules;

public class Shop {
	private int id;
	private String address;
	private String phone_Number; 
	private String contact_Name;
		
	
	public Shop(int id, String address,String phone_number, String contact_name) {
		this.id = id;
		this.address = address;
		this.setPhone_Number(phone_number);
		this.setContact_Name(contact_name);
	}
	
	public Shop(){
		this.id=-1;
		this.address=null;
		this.phone_Number = null;
		this.contact_Name = null;
	}

	public int getId() {
		return id;
	}

	public String getAddress() {
		return address;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContact_Name() {
		return contact_Name;
	}

	public void setContact_Name(String contact_Name) {
		this.contact_Name = contact_Name;
	}

	public String getPhone_Number() {
		return phone_Number;
	}

	public void setPhone_Number(String phone_Number) {
		this.phone_Number = phone_Number;
	}

	@Override
	public String toString(){
		return "ID = " +id+", Address = " +address+", Contact = "+contact_Name+", Phone Number = "+phone_Number ;
	}
}
