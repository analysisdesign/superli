package modules;

public class ProvidedProduct {
	private String catalogNumber;
	private String product_name;
	private String manufacturer;
	private String cost_price;
	private String supplierID;
	private int weight;

	public ProvidedProduct(String product_name, String manufacturer, String cost_price, 
			String supplierID,String catalogNumber, int weight) {
		super();
		this.catalogNumber = catalogNumber;
		this.product_name = product_name;
		this.manufacturer = manufacturer;
		this.cost_price = cost_price;
		this.supplierID = supplierID;
		this.weight = weight;
	}
	
	@Override
	public String toString() {
		return "catalog Number = " + catalogNumber + ", name = " + product_name + ", manufacturer = "
				+ manufacturer + ", cost price = " + cost_price;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getCostPrice() {
		return cost_price;
	}

	public void setCostPrice(String cost_price) {
		this.cost_price = cost_price;
	}

	public String getSupplierID() {
		return supplierID;
	}

	public void setSupplierID(String supplierID) {
		this.supplierID = supplierID;
	}

	public String getCatalogNumber() {
		return catalogNumber;
	}

	public void setCatalogNumber(String catalogNumber) {
		this.catalogNumber = catalogNumber;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	
		
}
