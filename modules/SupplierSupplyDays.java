package modules;

import java.util.List;

public class SupplierSupplyDays {
	private String ID;
	private List<Integer> days;
	
	public SupplierSupplyDays(String iD, List<Integer> days) {
		this.ID = iD;
		this.days = days;
	}

	public String getID() {
		return ID;
	}

	public List<Integer> getDays() {
		return days;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public void setDays(List<Integer> days) {
		this.days = days;
	}
}
