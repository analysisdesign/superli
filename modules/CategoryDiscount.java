package modules;

public class CategoryDiscount {

	private String category;
	private int discount;
	private String startDate;
	private String endDate;
	
	public CategoryDiscount(String category, int discount, String startDate, String endDate) {
		super();
		this.category = category;
		this.discount = discount;
		this.startDate = startDate;
		this.endDate = endDate;
	}


	public String getCategory() {
		return category;
	}

	public int getDiscount() {
		return discount;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
