package modules;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dal.DataBase;

public class Order {
	private Map<ProvidedProduct,Integer>products;
	private String OrderNumber;
	private List<Integer>days;
	private Supplier suplier;
	private int shopID;

	public Order(Map<ProvidedProduct, Integer> products, String orderNumber, List<Integer> days, Supplier suplier, int shopID) {
		this.products = products;
		this.OrderNumber = orderNumber;
		this.days = days;
		this.suplier = suplier;
		this.shopID = shopID;
	}

	public Order() {
		this.products = new HashMap<ProvidedProduct,Integer>();
		this.OrderNumber = "";
		this.days = new ArrayList<Integer>();
		this.suplier = null;
	}

	public Order(Order o)
	{
		this.products = o.products;
		this.OrderNumber = o.OrderNumber;
		this.days = o.days;
		this.suplier = o.suplier;
	}

	public Map<ProvidedProduct,Integer> getProducts() {
		return products;
	}
	public void setProducts(Map<ProvidedProduct,Integer> products) {
		this.products = products;
	}
	public String getOrderNumber() {
		return OrderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		OrderNumber = orderNumber;
	}
	public List<Integer> getDeliveyDays() {
		return days;
	}
	public void setDays(List<Integer> days) {
		this.days = days;
	}
	public void addProduct(ProvidedProduct p,int amount)
	{
		if (products==null)
		{
			products=new HashMap<ProvidedProduct,Integer>();
		}
		if(products.containsKey(p))
		{
			Integer currentAmount=products.get(p);
			products.replace(p, currentAmount, currentAmount+amount);
		}
		else{
			products.put(p, amount);
		}
	}
	public Supplier getSuplier() {
		return suplier;
	}
	public void setSuplier(Supplier suplier) {
		this.suplier = suplier;
	}

	@Override
	public String toString() {
		String ans="OrderNumber: "+OrderNumber+"\n";
		if(!days.isEmpty()){
			ans=ans+ "Receive an order regularly\n";
		}
		else{
			ans=ans+ "Receives an order by appointment\n";
		}
		ans=ans+ "Products:\n";
		int i=1;
		for(ProvidedProduct p:products.keySet())
		{
			ans=ans+i+") "+p.getProduct_name()+" - "+ products.get(p)+"\n";
			i++;
		}
		ans=ans+"Delivery days are: ";
		for (Integer integer : days) {
			switch (integer){
			case 1:
				ans=ans+"Sunday ";
				break;
			case 2:
				ans=ans+"Monday ";
				break;
			case 3:
				ans=ans+"Tuesday ";
				break;
			case 4:
				ans=ans+"Wednesday ";
				break;
			case 5:
				ans=ans+"Thursday ";
				break;
			case 6:
				ans=ans+"Friday ";
				break;
			case 7:
				ans=ans+"Saturday ";
				break;
			}
		}
		ans=ans+"\n";
		return ans;
	}

	public int getShopID() {
		return shopID;
	}

	public void setShopID(int shopID) {
		this.shopID = shopID;
	}
	/**
	 * Get the total weight for the order
	 * @return
	 */
	public int getTotalWeight()
	{
		int sum = 0;
		for (ProvidedProduct pd : products.keySet())
		{
			sum = sum + pd.getWeight() * products.get(pd);
		}
		return sum;
	}

	public boolean IsDelieveryDayTomorrow() 
	{
		Calendar cal =  Calendar.getInstance();
		int todayDayInWeek = cal.get(Calendar.DAY_OF_WEEK);
		int tommorowDay = (todayDayInWeek % 7 + 1);
		if(days.contains(tommorowDay))
			return true;
		return false;
	}
	


}
