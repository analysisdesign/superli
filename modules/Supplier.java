package modules;


import java.util.ArrayList;
import java.util.List;

public class Supplier {
	
	private String ID;
	private String name;
	private String BankAccount;
	private String TermOfPayment;
	private boolean ProvideTransport;
	private List<Contact>Contacts;
	private String address;
	
	
	
	public Supplier(String ID,String name, String bankAccount, String termOfPayment, boolean provideTransport, List<Contact> contacts,String address) {
		this.ID = ID;
		this.name=name;
		this.BankAccount = bankAccount;
		this.TermOfPayment = termOfPayment;
		this.ProvideTransport = provideTransport;
		this.Contacts = contacts;
		this.address = address;
	}
	public Supplier(String ID,String name, String bankAccount, String termOfPayment,boolean ProvideTransport,String address)
	{
		this.ID = ID;
		this.name=name;
		this.BankAccount = bankAccount;
		this.TermOfPayment = termOfPayment;
		this.ProvideTransport = ProvideTransport;
		this.Contacts = new ArrayList<Contact>();
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getBankAccount() {
		return BankAccount;
	}
	public void setBankAccount(String bankAccount) {
		BankAccount = bankAccount;
	}
	public String getTermOfPayment() {
		return TermOfPayment;
	}
	public void setTermOfPayment(String termOfPayment) {
		TermOfPayment = termOfPayment;
	}
	public boolean isProvideTransport() {
		return ProvideTransport;
	}
	public void setProvideTransport(boolean provideTransport) {
		ProvideTransport = provideTransport;
	}
	public List<Contact> getContacts() {
		return Contacts;
	}
	public void setContacts(List<Contact> contacts) {
		Contacts = contacts;
	}
	@Override
	public String toString() {
		if(ProvideTransport)
		{
		return "supplier Identifier: " + ID + " ,Name: " + name+" ,Bank Account: " + BankAccount + ", Term Of Payment: "
				+ TermOfPayment+ ", provide transport.";
		}
		return "supplier Identifier: " + ID + " ,Name: " + name+ " ,Bank Account: " + BankAccount + ", Term Of Payment: "
		+ TermOfPayment+ ", not provide transport.";
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

}
