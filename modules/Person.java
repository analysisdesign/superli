package modules;

/**
 * A Person
 *
 */
public abstract class Person {
	
	/**
	 * ID
	 */
	protected String id;
	/**
	 * First name
	 */
	protected String firstName;
	/**
	 * Last name
	 */
	protected String lastName;
	
	/**
	 * Constructor
	 * @param id
	 * @param firstName
	 * @param lastName
	 */
	public Person(String id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public String getId() {
		return id;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
