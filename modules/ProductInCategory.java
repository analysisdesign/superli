package modules;

public class ProductInCategory {

	private String product;
	private String category;
	private String manufacturer;
	private double sellPrice;
	private int minimalAmount;
	private int dailyConsumption;
	private int shopID;
	private int weight;
	
	public ProductInCategory(String product, String category, String manufacturer, double sellPrice, int minimalAmount, int dailyConsumption
			,int weight, int shopID) {
		super();
		this.product = product;
		this.category = category;
		this.manufacturer = manufacturer;
		this.sellPrice = sellPrice;
		this.minimalAmount = minimalAmount;
		this.dailyConsumption = dailyConsumption;
		this.setWeight(weight);
		this.setShopID(shopID);
	}
	public String getProduct() {
		return product;
	}
	public String getCategory() {
		return category;
	}
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	public double getSellPrice() {
		return sellPrice;
	}
	public int getMinimalAmount() {
		return minimalAmount;
	}
	
	public int getDailyConsumption() {
		return dailyConsumption;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public void setSellPrice(double sellPrice) {
		this.sellPrice = sellPrice;
	}
	public void setMinimalAmount(int minimalAmount) {
		this.minimalAmount = minimalAmount;
	}
	public void setDailyConsumption(int dailyConsumption) {
		this.dailyConsumption = dailyConsumption;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getShopID() {
		return shopID;
	}
	public void setShopID(int shopID) {
		this.shopID = shopID;
	}
}
