package Adding;

import DataAccessLayer.DBAccess;
import Deliveries.Entities.LicenseType;
import Employees.BusinessLayer.Constraint;
import Employees.BusinessLayer.Employee;
import Employees.BusinessLayer.Role;
import Employees.BusinessLayer.Shift;
import dal.DataBase;
import dal.DbConnectionSingelton;
import modules.*;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AlexFile
{
    public AlexFile()
    {
    	
        Connection c = null;
        Statement stmt = null;
        try
        {
        	
            c = DbConnectionSingelton.getInstance();
            
                
            
            stmt = c.createStatement();
            String exe = "CREATE TABLE IF NOT EXISTS  Constraints " +
                    "(ID                 INT      REFERENCES Employees(ID)       NOT NULL," +
                    " Date               TEXT                        NOT NULL, " +
                    " DayPart            TEXT                        NOT NULL, " +
                    " PRIMARY  KEY (ID,Date,DayPart) );";

            stmt.executeUpdate(exe);
            
            System.out.println("Constraints");
            exe = "CREATE TABLE IF NOT EXISTS  EmployeeRoles " +
                    "(ID                 INT      REFERENCES Employees(ID)       NOT NULL," +
                    " Role          TEXT                        NOT NULL, " +
                    " PRIMARY  KEY (ID,Role) );";

            stmt.executeUpdate(exe);

            System.out.println("EmployeeRoles");
            exe = "CREATE TABLE IF NOT EXISTS  Employees " +
                    "(ID                 INT     PRIMARY KEY         NOT NULL," +
                    " FirstName          TEXT                        NOT NULL, " +
                    " LastName           TEXT                        NOT NULL, " +
                    " Bank               TEXT                        NOT NULL, " +
                    " Branch             INT                         NOT NULL, " +
                    " AccountNumber      INT                         NOT NULL, " +
                    " MonthlyPayment     REAL                        NOT NULL, " +
                    " StartingDate       TEXT   DEFAULT NULL, "+
                    " Status             BOOLEAN   DEFAULT true, "+
                    " LeavingDate        TEXT   DEFAULT NULL, "+
                    " SiteID             INT   REFERENCES SHOPS(ID), "+
                    " EmploymentTerms    TEXT   DEFAULT 'Employer: Super-Lee\n" +
                    "Manager: None\n" +
                    "Day Length: 8\n" +
                    "Resting Day: Saturday\n" +
                    "Pension: None\n" +
                    "Foundation: None') ";
            stmt.executeUpdate(exe);

            System.out.println("Employees");
            exe = "CREATE TABLE IF NOT EXISTS  Roles " +
                    " (Role    TEXT     PRIMARY KEY    NOT NULL);";
            stmt.executeUpdate(exe);

            System.out.println("Roles");
            exe = "CREATE TABLE IF NOT EXISTS  Shifts " +
                    "(ID                 INT       REFERENCES Employees(ID)       NOT NULL," +
                    " Date          TEXT                        NOT NULL, " +
                    " DayPart           TEXT                        NOT NULL, " +
                    " Role           TEXT                        NOT NULL, " +
                    " PRIMARY  KEY (ID,Date,DayPart) );";

            stmt.executeUpdate(exe);
            
            System.out.println("Shifts");
            exe = "CREATE TABLE IF NOT EXISTS  ShiftsStandards " +
                    "(DayPart                 TEXT              NOT NULL," +
                    " Role                    TEXT              NOT NULL, " +
                    " Count                   INT               NOT NULL, " +
                    " PRIMARY  KEY (DayPart,Role) );";
            stmt.executeUpdate(exe);
            
            System.out.println("ShiftsStandards");
            exe = "CREATE TABLE IF NOT EXISTS  Products " +
                    "(ID         INT     PRIMARY KEY         NOT NULL," +
                    " Weight     INT	                     NOT NULL) ";
            stmt.executeUpdate(exe);
            
            System.out.println("Products");
            exe = "CREATE TABLE IF NOT EXISTS  Drivers " +
                    "(ID      			 INT     REFERENCES Employees(ID) PRIMARY KEY         NOT NULL," +
                    " License_Type		 TEXT						 NOT NULL)";
            stmt.executeUpdate(exe);
            
            System.out.println("Drivers");
             exe = "CREATE TABLE IF NOT EXISTS  Trucks " +
                    "(ID        		 INT     PRIMARY KEY         NOT NULL," +
                    " Model     	 	 TEXT	                     NOT NULL,"+
                    " License_Type		 TEXT						 NOT NULL,"+
                    " Net_Weight     	 INT	                     NOT NULL,"+
                     "LeavingDate    	 TEXT	                     DEFAULT NULL,"+
                    " Max_Weight     	 INT	                     NOT NULL) ";
            stmt.executeUpdate(exe);
            
            System.out.println("Trucks");
            exe = "CREATE TABLE IF NOT EXISTS SiteDocs " +
                    "(ID      		  	 INTEGER     PRIMARY KEY     AUTOINCREMENT," +
                    " Dest_Site_ID 		 INT	                     NOT NULL," +
                    "Delivery_ID          INT                        NOT NULL,"+
					" OrderID	 		 INT	                     NOT NULL," +
					" FOREIGN KEY(OrderID) REFERENCES SUPPLIER_ORDERS(OrderID)," +
					" FOREIGN KEY(Delivery_ID) REFERENCES Deliveries(ID)," +
                    " FOREIGN KEY(Dest_Site_ID) REFERENCES Suppliers(ID))";
            stmt.executeUpdate(exe);
                
            System.out.println("SiteDocs");
            exe = "CREATE TABLE IF NOT EXISTS  SiteDocToProducts " +
                    "(Site_Doc_ID      	 INT     			    	 NOT NULL," +
                    " CatalogID 		 INT	                     NOT NULL," +
					" SupplierID 		 INT	                     NOT NULL," +
                    " Quantity  		 INT	                     NOT NULL," +
                    " FOREIGN KEY(Site_Doc_ID) REFERENCES SiteDocs(ID)  ON DELETE CASCADE," +
                    " FOREIGN KEY(CatalogID,SupplierID) REFERENCES PRODUCTS_SUPPLIERS(CatalogID,SupplierID),"+
                    " PRIMARY KEY (Site_Doc_ID,CatalogID,SupplierID))";
            stmt.executeUpdate(exe);
            
            System.out.println("SiteDocToProducts");
            exe = "CREATE TABLE IF NOT EXISTS  Deliveries " +
            		"(ID      		  	 INTEGER     PRIMARY KEY     AUTOINCREMENT," +
                    " Date   	      	 TEXT   			    	 NOT NULL," +
                    " DateInMillis   	 INT    			    	 NOT NULL," +
					" Time   	      	 TEXT   			    	 NOT NULL," +
                    " Driver_ID	 		 INT	                     NOT NULL," +
                    " Truck_ID	 		 INT	                     NOT NULL," +
                    " Site_ID	 		 INT	                     NOT NULL," +
                    " Weight	 		 INT	                     NOT NULL," +
                    " FOREIGN KEY(Driver_ID) REFERENCES Drivers(ID)," +
                    " FOREIGN KEY(Truck_ID) REFERENCES Trucks(ID) ON DELETE CASCADE,"+
                    " FOREIGN KEY(Site_ID) REFERENCES SHOPS(ID))";
            stmt.executeUpdate(exe);
            
            System.out.println("Deliveries");
            
            exe = "CREATE TABLE IF NOT EXISTS  DeliveryToSiteDocs " +
                    "(Delivery_ID      	 INT     			    	 NOT NULL," +
                    " Site_Doc_ID 		 INT	                     NOT NULL," +
                    " FOREIGN KEY(Delivery_ID) REFERENCES Deliveries(ID) ON DELETE CASCADE," +
                    " FOREIGN KEY(Site_Doc_ID) REFERENCES SiteDocs(ID) ON DELETE CASCADE,"+
                    " PRIMARY KEY (Delivery_ID, Site_Doc_ID))";
            stmt.executeUpdate(exe);
			
            System.out.println("DeliveryToSiteDocs");
			
			/*Create Inventory and Suppliers Tables*/
			
			
			
			
			String sql = 
					"CREATE TABLE IF NOT EXISTS CATEGORY_DISCOUNTS "
					+ " (CATEGORY_NAME TEXT NOT NULL, " 
					+ " DISCOUNT INT NOT NULL,"
					+ " START_DATE TEXT NOT NULL,"
					+ " END_DATE TEXT NOT NULL,"
					+ " PRIMARY KEY (CATEGORY_NAME, START_DATE, END_DATE),"
					+ " FOREIGN KEY(CATEGORY_NAME) REFERENCES CATEGORIES(NAME))";
			stmt.executeUpdate(sql);
			
			System.out.println("CATEGORY_DISCOUNTS");
			
			sql = 
					"CREATE TABLE IF NOT EXISTS CATEGORIES "
					+ " (NAME TEXT NOT NULL PRIMARY KEY, " 
					+ " PARENT TEXT,"
					+ " FOREIGN KEY(PARENT) REFERENCES CATEGORIES(NAME) )";
			stmt.executeUpdate(sql);
			
			System.out.println("CATEGORIES");
			
			sql = "CREATE TABLE IF NOT EXISTS CONTACTS "
					+ " (ID    TEXT      NOT NULL,"
					+ " FIRSTNAME           TEXT    NOT NULL, " + " "
					+ "LASTNAME            TEXT     NOT NULL, "
					+ " PHONE       TEXT NOT NULL, " + " "
					+ "SUPPLIERID         TEXT," + "primary key (ID, SUPPLIERID),"
					+ "FOREIGN KEY (SUPPLIERID) REFERENCES Suppliers(ID)," + "CONSTRAINT IdUnique UNIQUE (ID))";

			stmt.executeUpdate(sql);
			
			System.out.println("CONTACTS");
			
						sql = 
					"CREATE TABLE IF NOT EXISTS ORDERS_DAYS"
					+ " (OrderID    TEXT      NOT NULL , "
					+ " Day     TEXT    NOT NULL , "
					+ "primary key (OrderID , DAY ),"
					+ "foreign key (OrderID) references SupplierOrder(OrderID))";

			stmt.executeUpdate(sql);
			
			System.out.println("ORDERS_DAYS");
			
						sql = 
					"CREATE TABLE IF NOT EXISTS ORDERS" 
					+ " (PRODUCT_NAME    TEXT      NOT NULL , "
					+ " MANUFACTURER     TEXT    NOT NULL , "
					+ " OrderID           TEXT    NOT NULL , " 
					+ " Amount            TEXT     NOT NULL, "
					+ " SHOP_ID 		INTEGER    NOT NULL, "
					+ " primary key (PRODUCT_NAME , MANUFACTURER , OrderID),"
					+ " foreign key (SHOP_ID) references SHOPS(ID),"
					+ " foreign key (PRODUCT_NAME , MANUFACTURER) references PRODUCTS_IN_STORE(PRODUCT_NAME , MANUFACTURER),"
					+ " foreign key (OrderID) references SupplierOrder(OrderID))";

			stmt.executeUpdate(sql);
			
			System.out.println("ORDERS");
			
						sql = 
					"CREATE TABLE IF NOT EXISTS PRODUCT_DISCOUNTS"
					+ " (PRODUCT_NAME TEXT NOT NULL,"
					+ " MANUFACTURER TEXT NOT NULL," 
					+ " DISCOUNT INT NOT NULL,"
					+ " START_DATE TEXT NOT NULL,"
					+ " END_DATE TEXT NOT NULL,"
					+ " PRIMARY KEY (PRODUCT_NAME,MANUFACTURER,START_DATE, END_DATE),"
					+ " FOREIGN KEY (PRODUCT_NAME,MANUFACTURER) REFERENCES PRODUCTS_CATEGORIES(PRODUCT_NAME,MANUFACTURER))";
			stmt.executeUpdate(sql);
			
			System.out.println("PRODUCT_DISCOUNTS");
			
						sql = 
					"CREATE TABLE IF NOT EXISTS PRODUCT_CATEGORIES" 
					+ " (PRODUCT_NAME TEXT NOT NULL,"
					+ " CATEGORY_NAME TEXT NOT NULL,"
					+ " MANUFACTURER TEXT NOT NULL," 
					+ " SELL_PRICE REAL NOT NULL,"
					+ " MINIMAL_AMOUNT INT NOT NULL,"
					+ " DAILY_CONSUMPTION INT NOT NULL,"
					+ " WEIGHT INT NOT NULL,"
					+ " SHOP_ID INT NOT NULL, "
					+ "	FOREIGN KEY (SHOP_ID) REFERENCES SHOPS(ID),"
					+ " PRIMARY KEY (PRODUCT_NAME,MANUFACTURER,SHOP_ID),"
					+ "	FOREIGN KEY (PRODUCT_NAME,MANUFACTURER) REFERENCES PRODUCTS_SUPPLIERS (PRODUCT_NAME,MANUFACTURER) ,"
					+ "	FOREIGN KEY (CATEGORY_NAME) REFERENCES CATEGORIES (NAME))";

			stmt.executeUpdate(sql);
			
			System.out.println("PRODUCT_CATEGORIES");
			
						sql = "CREATE TABLE IF NOT EXISTS PRODUCTS_SUPPLIERS"
					+ " (PRODUCT_NAME    TEXT , "
					+" MANUFACTURER TEXT , "
					+" CatalogID TEXT , "
					+ " SupplierID   TEXT , " 
					+ " price            TEXT     NOT NULL, "
					+ " WEIGHT            INT     NOT NULL, "
					+ "primary key (CatalogID , SupplierID)," 
					+ "foreign key (SupplierID) references Suppliers(ID))";

			stmt.executeUpdate(sql);
			
			System.out.println("PRODUCTS_SUPPLIERS");
			
						sql = 
					"CREATE TABLE IF NOT EXISTS  PRODUCTS_IN_SHOP" 
					+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ " NAME TEXT NOT NULL,"
					+ " MANUFACTURER TEXT NOT NULL, " 
					+ " EXP_DATE TEXT, "
					+ " DAMAGED BOOLEAN ,"                 // 0 is OK and 1 is damaged
					+ " LOCATION TEXT NOT NULL,"
					+ " SHOP_ID INTEGER,"
					+ " FOREIGN KEY (SHOP_ID) REFERENCES SHOPS (ID)"
					+ " FOREIGN KEY (NAME,MANUFACTURER) REFERENCES PRODUCTS_CATEGORIES (PRODUCT_NAME,MANUFACTURER))";

			stmt.executeUpdate(sql);
			
			System.out.println("PRODUCTS_IN_SHOP");
			
						sql = 
					"CREATE TABLE IF NOT EXISTS SHOPS "
					+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ " ADDRESS TEXT NOT NULL,"
					+ " CONTACT_NAME TEXT NOT NULL ,"
					+ " PHONE_NUMBER TEXT NOT NULL)";

			stmt.executeUpdate(sql);
			
			System.out.println("SHOPS");
			
						sql = 
					"CREATE TABLE IF NOT EXISTS  SUPPLIER_DISCOUNTS "
					+ " (PRODUCT_NAME TEXT NOT NULL, " 
					+ " MANUFACTURER TEXT NOT NULL,"
					+ " SUPPLIER_ID TEXT NOT NULL,"
					+ " START_DATE TEXT NOT NULL,"
					+ " END_DATE TEXT NOT NULL,"
					+ " DISCOUNT TEXT NOT NULL,"
					+ " MINIMAL_AMOUNT INT NOT NULL,"
					+ " PRIMARY KEY (PRODUCT_NAME,MANUFACTURER,SUPPLIER_ID,START_DATE, END_DATE,MINIMAL_AMOUNT),"
					+ " FOREIGN KEY(PRODUCT_NAME,MANUFACTURER) REFERENCES PRODUCT_CATEGORIES(NAME,MANUFACTURER),"
					+ " FOREIGN KEY(SUPPLIER_ID) REFERENCES SUPPLIERS(ID))";
			stmt.executeUpdate(sql);
			
			System.out.println("SUPPLIER_DISCOUNTS");
			
						sql = 
					"CREATE TABLE IF NOT EXISTS SUPPLIER_ORDERS "
					+ " (OrderID    TEXT      NOT NULL ,"
					+ " SupplierID           TEXT    NOT NULL, "
					+ "primary key (OrderID),"
					+ "foreign key (SupplierID) references Suppliers(ID))";

			stmt.executeUpdate(sql);
			
			System.out.println("SUPPLIER_ORDERS");
			
			sql = "CREATE TABLE IF NOT EXISTS SUPPLIERS "
					+ " (ID    TEXT      NOT NULL,"
					+ " NAME           TEXT    NOT NULL, " 
					+ " BankAccount           TEXT    NOT NULL, " 
					+ " TermOfPayment           TEXT     NOT NULL, "
					+ " ProvideTransport       TEXT DEFAULT F, "
					+ " ADDRESS TEXT NOT NULL,"
					+ " primary key (ID))";

			stmt.executeUpdate(sql);
			
			sql = "CREATE TABLE IF NOT EXISTS SUPPLIER_SUPPLY_DAYS "
					+ "(DAY    INT      NOT NULL,"
					+ "SUPPLIER_ID           TEXT    NOT NULL, " 
					+ "primary key (DAY, SUPPLIER_ID)," 
					+ "foreign key (SUPPLIER_ID) references Suppliers(ID))";

			stmt.executeUpdate(sql);
			
			System.out.println("SUPPLIER_SUPPLY_DAYS");
			
            
            exe =     "INSERT INTO Trucks (ID,Model,License_Type,Net_Weight,Max_Weight) VALUES ('1000','Volvo', 'A' , '200', '2000');"
            		+ "INSERT INTO Trucks (ID,Model,License_Type,Net_Weight,Max_Weight) VALUES ('2000','BMW', 'B' , '400', '2500');"
            		+ "INSERT INTO Trucks (ID,Model,License_Type,Net_Weight,Max_Weight) VALUES ('3000','Toyota', 'C' , '400', '3000');";
            stmt.executeUpdate(exe);

            
            
            stmt.close();
            
            DBAccess dbAccess=new DBAccess();
            dbAccess.addEmployee(new Employee(1000,"Kuti","Levi","Discount",669,25,1000,"01/04/2013", 1));
            dbAccess.addEmployee(new Employee(1111,"Kuti","Levi","Discount",669,25,1000,"01/04/2013", 1));
            dbAccess.addEmployee(new Employee(2222,"Tomer","Cohen","Hapoalim",888,67,1500,"12/11/2005", 1));
            dbAccess.addEmployee(new Employee(3333,"Avi","Levi","Discount",669,25,2000,"26/12/2001", 1));
            dbAccess.addEmployee(new Employee(4444,"Yossi","Cohen","Hapoalim",888,19,2500,"31/10/2010", 1));
            dbAccess.addEmployee(new Employee(5555,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(6666,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 1));
            dbAccess.addEmployee(new Employee(7777,"Kuti","Levi","Discount",669,25,1000,"01/04/2013", 1));
            dbAccess.addEmployee(new Employee(8888,"Tomer","Cohen","Hapoalim",888,67,1500,"12/11/2005", 1));
            dbAccess.addEmployee(new Employee(9999,"Avi","Levi","Discount",669,25,2000,"26/12/2001", 1));
            dbAccess.addEmployee(new Employee(1010,"Yossi","Cohen","Hapoalim",888,19,2500,"31/10/2010", 1));
            dbAccess.addEmployee(new Employee(1112,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(1113,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(1114,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(1115,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(1116,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(1117,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(1118,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(1119,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(1120,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(1121,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));
            dbAccess.addEmployee(new Employee(1122,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 1));

            dbAccess.addEmployee(new Employee(1212,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 2));
            dbAccess.addEmployee(new Employee(1313,"Kuti","Levi","Discount",669,25,1000,"01/04/2013", 2));
            dbAccess.addEmployee(new Employee(1414,"Tomer","Cohen","Hapoalim",888,67,1500,"12/11/2005", 2));
            dbAccess.addEmployee(new Employee(1415,"Tomer","Cohen","Hapoalim",888,67,1500,"12/11/2005", 2));
            dbAccess.addEmployee(new Employee(1416,"Kuti","Levi","Discount",669,25,1000,"01/04/2013", 2));
            dbAccess.addEmployee(new Employee(1417,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 2));
            dbAccess.addEmployee(new Employee(1418,"Yossi","Cohen","Hapoalim",888,19,2500,"31/10/2010", 2));
            dbAccess.addEmployee(new Employee(1515,"Avi","Levi","Discount",669,25,2000,"26/12/2001", 2));
            dbAccess.addEmployee(new Employee(1616,"Yossi","Cohen","Hapoalim",888,19,2500,"31/10/2010", 2));
            dbAccess.addEmployee(new Employee(1717,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 2));
            dbAccess.addEmployee(new Employee(1818,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 2));


            dbAccess.addEmployee(new Employee(1919,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 3));
            dbAccess.addEmployee(new Employee(2020,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 3));
            dbAccess.addEmployee(new Employee(2121,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 3));
            dbAccess.addEmployee(new Employee(2223,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 3));
            dbAccess.addEmployee(new Employee(2224,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 3));
            dbAccess.addEmployee(new Employee(2225,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 3));
            dbAccess.addEmployee(new Employee(2226,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 3));
            dbAccess.addEmployee(new Employee(2323,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 3));
            dbAccess.addEmployee(new Employee(2424,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 3));
            dbAccess.addEmployee(new Employee(2525,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 3));
            dbAccess.addEmployee(new Employee(2626,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 3));

            dbAccess.addEmployee(new Employee(2727,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 4));
            dbAccess.addEmployee(new Employee(2828,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 4));
            dbAccess.addEmployee(new Employee(2929,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 4));
            dbAccess.addEmployee(new Employee(3030,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 4));
            dbAccess.addEmployee(new Employee(3131,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 4));
            dbAccess.addEmployee(new Employee(3232,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 4));
            dbAccess.addEmployee(new Employee(3334,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 4));
            dbAccess.addEmployee(new Employee(3434,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 4));
            dbAccess.addEmployee(new Employee(3535,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 4));
            dbAccess.addEmployee(new Employee(3636,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 4));
            dbAccess.addEmployee(new Employee(3737,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 4));

            dbAccess.addEmployee(new Employee(9898,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 5));
            dbAccess.addEmployee(new Employee(9090,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 5));
            dbAccess.addEmployee(new Employee(9292,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 5));
            dbAccess.addEmployee(new Employee(9393,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 5));
            dbAccess.addEmployee(new Employee(9494,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 5));
            dbAccess.addEmployee(new Employee(9595,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 5));
            dbAccess.addEmployee(new Employee(9696,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 5));
            dbAccess.addEmployee(new Employee(9797,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 5));
            dbAccess.addEmployee(new Employee(9898,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 5));
            dbAccess.addEmployee(new Employee(8181,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 5));
            dbAccess.addEmployee(new Employee(8080,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 5));
            dbAccess.addEmployee(new Employee(8282,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 5));

            dbAccess.addEmployee(new Employee(1234,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 6));
            dbAccess.addEmployee(new Employee(1235,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 6));
            dbAccess.addEmployee(new Employee(1236,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 6));
            dbAccess.addEmployee(new Employee(1237,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 6));
            dbAccess.addEmployee(new Employee(1238,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 6));
            dbAccess.addEmployee(new Employee(1239,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 6));
            dbAccess.addEmployee(new Employee(1240,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 6));
            dbAccess.addEmployee(new Employee(1241,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 6));
            dbAccess.addEmployee(new Employee(1242,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 6));
            dbAccess.addEmployee(new Employee(1243,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 6));
            dbAccess.addEmployee(new Employee(1244,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 6));

            dbAccess.addEmployee(new Employee(1245,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 7));
            dbAccess.addEmployee(new Employee(1246,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 7));
            dbAccess.addEmployee(new Employee(1247,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 7));
            dbAccess.addEmployee(new Employee(1248,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 7));
            dbAccess.addEmployee(new Employee(1249,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 7));
            dbAccess.addEmployee(new Employee(1250,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 7));
            dbAccess.addEmployee(new Employee(1251,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 7));
            dbAccess.addEmployee(new Employee(1252,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 7));
            dbAccess.addEmployee(new Employee(1253,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 7));
            dbAccess.addEmployee(new Employee(1254,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 7));
            dbAccess.addEmployee(new Employee(1255,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 7));
            dbAccess.addEmployee(new Employee(1256,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 7));

            dbAccess.addEmployee(new Employee(1257,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 8));
            dbAccess.addEmployee(new Employee(1258,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 8));
            dbAccess.addEmployee(new Employee(1259,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 8));
            dbAccess.addEmployee(new Employee(1260,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 8));
            dbAccess.addEmployee(new Employee(1261,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 8));
            dbAccess.addEmployee(new Employee(1262,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 8));
            dbAccess.addEmployee(new Employee(1263,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 8));
            dbAccess.addEmployee(new Employee(1264,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 8));
            dbAccess.addEmployee(new Employee(1265,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 8));
            dbAccess.addEmployee(new Employee(1266,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 8));
            dbAccess.addEmployee(new Employee(1267,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 8));
            dbAccess.addEmployee(new Employee(1268,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 8));
            dbAccess.addEmployee(new Employee(1269,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 8));
            dbAccess.addEmployee(new Employee(1270,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 8));

            dbAccess.addEmployee(new Employee(1271,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 9));
            dbAccess.addEmployee(new Employee(1272,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 9));
            dbAccess.addEmployee(new Employee(1273,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 9));
            dbAccess.addEmployee(new Employee(1274,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 9));
            dbAccess.addEmployee(new Employee(1275,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 9));
            dbAccess.addEmployee(new Employee(1276,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 9));
            dbAccess.addEmployee(new Employee(1277,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 9));
            dbAccess.addEmployee(new Employee(1278,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 9));
            dbAccess.addEmployee(new Employee(1279,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 9));
            dbAccess.addEmployee(new Employee(1280,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 9));
            dbAccess.addEmployee(new Employee(1281,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 9));
            dbAccess.addEmployee(new Employee(1282,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 9));
            dbAccess.addEmployee(new Employee(1283,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 9));

            dbAccess.addEmployee(new Employee(1284,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 10));
            dbAccess.addEmployee(new Employee(1285,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 10));
            dbAccess.addEmployee(new Employee(1286,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 10));
            dbAccess.addEmployee(new Employee(1287,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 10));
            dbAccess.addEmployee(new Employee(1288,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 10));
            dbAccess.addEmployee(new Employee(1289,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 10));
            dbAccess.addEmployee(new Employee(1290,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 10));
            dbAccess.addEmployee(new Employee(1291,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 10));
            dbAccess.addEmployee(new Employee(1292,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 10));
            dbAccess.addEmployee(new Employee(1293,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 10));
            dbAccess.addEmployee(new Employee(1294,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 10));
            dbAccess.addEmployee(new Employee(1295,"Alon","Levi","Discount",669,28,3000,"17/03/1999", 10));
            dbAccess.addEmployee(new Employee(1296,"Shay","Cohen","Hapoalim",888,17,3500,"09/06/2015", 10));

			System.out.println("Add Employees");

            dbAccess.addRole(new Role("Cashier"));
            dbAccess.addRole(new Role("Driver"));
            dbAccess.addRole(new Role("Stock Keeper"));
            dbAccess.addRole(new Role("Secretary"));
            dbAccess.addRole(new Role("Shift Manager"));
            dbAccess.addRole(new Role("Human Resources Manager"));
            dbAccess.addRole(new Role("Branch Manager"));
            dbAccess.addRole(new Role("Company Manager"));
            dbAccess.addRole(new Role("Logistics Manager"));

			System.out.println("Add Roles");

            //Site 1:
            dbAccess.addRoleToID("1111",new Role("Company Manager"));
            dbAccess.addRoleToID("2222",new Role("Secretary"));
            dbAccess.addRoleToID("3333",new Role("Human Resources Manager"));
            
            dbAccess.addRoleToID("1000",new Role("Logistics Manager"));

            dbAccess.addRoleToID("4444",new Role("Shift Manager"));
            dbAccess.addRoleToID("5555",new Role("Shift Manager"));
            dbAccess.addRoleToID("6666",new Role("Shift Manager"));
            dbAccess.addRoleToID("7777",new Role("Shift Manager"));
            dbAccess.addRoleToID("8888",new Role("Shift Manager"));
            dbAccess.addRoleToID("9999",new Role("Shift Manager"));
            dbAccess.addRoleToID("1010",new Role("Shift Manager"));
            dbAccess.addRoleToID("1112",new Role("Shift Manager"));
            dbAccess.addRoleToID("1113",new Role("Shift Manager"));
            dbAccess.addRoleToID("1114",new Role("Shift Manager"));
            dbAccess.addRoleToID("1115",new Role("Shift Manager"));
            dbAccess.addRoleToID("1116",new Role("Shift Manager"));
            dbAccess.addRoleToID("1117",new Role("Shift Manager"));
            dbAccess.addRoleToID("1118",new Role("Shift Manager"));
            dbAccess.addRoleToID("1119",new Role("Shift Manager"));
            dbAccess.addRoleToID("1120",new Role("Shift Manager"));
            dbAccess.addRoleToID("1121",new Role("Shift Manager"));
            dbAccess.addRoleToID("1122",new Role("Shift Manager"));

            dbAccess.addRoleToID("4444",new Role("Cashier"));
            dbAccess.addRoleToID("5555",new Role("Cashier"));
            dbAccess.addRoleToID("6666",new Role("Cashier"));
            dbAccess.addRoleToID("7777",new Role("Cashier"));
            dbAccess.addRoleToID("8888",new Role("Cashier"));
            dbAccess.addRoleToID("9999",new Role("Cashier"));
            dbAccess.addRoleToID("1010",new Role("Cashier"));
            dbAccess.addRoleToID("1112",new Role("Cashier"));
            dbAccess.addRoleToID("1113",new Role("Cashier"));
            dbAccess.addRoleToID("1114",new Role("Cashier"));
            dbAccess.addRoleToID("1115",new Role("Cashier"));
            dbAccess.addRoleToID("1116",new Role("Cashier"));
            dbAccess.addRoleToID("1117",new Role("Cashier"));
            dbAccess.addRoleToID("1118",new Role("Cashier"));
            dbAccess.addRoleToID("1119",new Role("Cashier"));
            dbAccess.addRoleToID("1120",new Role("Cashier"));
            dbAccess.addRoleToID("1121",new Role("Cashier"));
            dbAccess.addRoleToID("1122",new Role("Cashier"));

            dbAccess.addRoleToID("4444",new Role("Driver"));
            dbAccess.addDriver(4444, LicenseType.B);
            dbAccess.addRoleToID("5555",new Role("Driver"));
            dbAccess.addDriver(5555, LicenseType.C);
            dbAccess.addRoleToID("6666",new Role("Driver"));
            dbAccess.addDriver(6666, LicenseType.A);
            dbAccess.addRoleToID("7777",new Role("Driver"));
            dbAccess.addDriver(7777, LicenseType.B);
            dbAccess.addRoleToID("8888",new Role("Driver"));
            dbAccess.addDriver(8888, LicenseType.C);
            dbAccess.addRoleToID("9999",new Role("Driver"));
            dbAccess.addDriver(9999, LicenseType.A);
            dbAccess.addRoleToID("1010",new Role("Driver"));
            dbAccess.addDriver(1010, LicenseType.B);
            dbAccess.addRoleToID("1112",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.C);
            dbAccess.addRoleToID("1113",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.A);
            dbAccess.addRoleToID("1114",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.B);
            dbAccess.addRoleToID("1115",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.C);
            dbAccess.addRoleToID("1116",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.A);
            dbAccess.addRoleToID("1117",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.B);
            dbAccess.addRoleToID("1118",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.C);
            dbAccess.addRoleToID("1119",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.A);
            dbAccess.addRoleToID("1120",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.B);
            dbAccess.addRoleToID("1121",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.C);
            dbAccess.addRoleToID("1122",new Role("Driver"));
            dbAccess.addDriver(1112, LicenseType.A);

            dbAccess.addRoleToID("4444",new Role("Stock Keeper"));
            dbAccess.addRoleToID("5555",new Role("Stock Keeper"));
            dbAccess.addRoleToID("6666",new Role("Stock Keeper"));
            dbAccess.addRoleToID("7777",new Role("Stock Keeper"));
            dbAccess.addRoleToID("8888",new Role("Stock Keeper"));
            dbAccess.addRoleToID("9999",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1010",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1112",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1113",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1114",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1115",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1116",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1117",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1118",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1119",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1120",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1121",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1122",new Role("Stock Keeper"));

            dbAccess.addConstraints(4444, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(4444, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(5555, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(5555, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(6666, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(6666, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(7777, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(7777, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(8888, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(8888, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(9999, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(9999, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1010, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1010, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1112, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1112, new Constraint("05/05/2017","Evening"));

            dbAccess.addConstraints(4444, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(4444, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(5555, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(5555, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(6666, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(6666, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(7777, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(7777, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(8888, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(8888, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(9999, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(9999, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1010, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1010, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1112, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1112, new Constraint("06/05/2017","Evening"));
			System.out.println("Add Constraint");


            dbAccess.AssignEmployeeToShift("4444",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("4444",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("5555",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("5555",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("6666",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("6666",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("7777",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("7777",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("8888",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("8888",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("9999",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("9999",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1010",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1010",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1112",new Shift("05/05/2017","Morning"),new Role("Shift Manager"));
            dbAccess.AssignEmployeeToShift("1112",new Shift("05/05/2017","Evening"),new Role("Shift Manager"));

            //Site 2:
            dbAccess.addRoleToID("1212",new Role("Branch Manager"));
            dbAccess.addRoleToID("1313",new Role("Secretary"));
            dbAccess.addRoleToID("1414",new Role("Human Resources Manager"));

            dbAccess.addRoleToID("1415",new Role("Shift Manager"));
            dbAccess.addRoleToID("1416",new Role("Shift Manager"));
            dbAccess.addRoleToID("1417",new Role("Shift Manager"));
            dbAccess.addRoleToID("1418",new Role("Shift Manager"));
            dbAccess.addRoleToID("1515",new Role("Shift Manager"));
            dbAccess.addRoleToID("1616",new Role("Shift Manager"));
            dbAccess.addRoleToID("1717",new Role("Shift Manager"));
            dbAccess.addRoleToID("1818",new Role("Shift Manager"));

            dbAccess.addRoleToID("1415",new Role("Cashier"));
            dbAccess.addRoleToID("1416",new Role("Cashier"));
            dbAccess.addRoleToID("1417",new Role("Cashier"));
            dbAccess.addRoleToID("1418",new Role("Cashier"));
            dbAccess.addRoleToID("1515",new Role("Cashier"));
            dbAccess.addRoleToID("1616",new Role("Cashier"));
            dbAccess.addRoleToID("1717",new Role("Cashier"));
            dbAccess.addRoleToID("1818",new Role("Cashier"));

            dbAccess.addRoleToID("1415",new Role("Driver"));
            dbAccess.addDriver(1415, LicenseType.A);
            dbAccess.addRoleToID("1416",new Role("Driver"));
            dbAccess.addDriver(1416, LicenseType.B);
            dbAccess.addRoleToID("1417",new Role("Driver"));
            dbAccess.addDriver(1417, LicenseType.C);
            dbAccess.addRoleToID("1418",new Role("Driver"));
            dbAccess.addDriver(1418, LicenseType.A);
            dbAccess.addRoleToID("1515",new Role("Driver"));
            dbAccess.addDriver(1515, LicenseType.B);
            dbAccess.addRoleToID("1616",new Role("Driver"));
            dbAccess.addDriver(1616, LicenseType.C);
            dbAccess.addRoleToID("1717",new Role("Driver"));
            dbAccess.addDriver(1717, LicenseType.A);
            dbAccess.addRoleToID("1818",new Role("Driver"));
            dbAccess.addDriver(1818, LicenseType.B);

            dbAccess.addRoleToID("1415",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1416",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1417",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1418",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1515",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1616",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1717",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1818",new Role("Stock Keeper"));

            dbAccess.addConstraints(1415, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1415, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1416, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1416, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1417, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1417, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1418, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1418, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1515, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1515, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1616, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1616, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1717, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1717, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1818, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1818, new Constraint("05/05/2017","Evening"));

            dbAccess.addConstraints(1415, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1415, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1416, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1416, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1417, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1417, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1418, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1418, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1515, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1515, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1616, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1616, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1717, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1717, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1818, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1818, new Constraint("06/05/2017","Evening"));

            dbAccess.AssignEmployeeToShift("1415",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1415",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1416",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1416",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1417",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1417",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1418",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1418",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1515",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1515",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1616",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1616",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1717",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1717",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1818",new Shift("05/05/2017","Morning"),new Role("Shift Manager"));
            dbAccess.AssignEmployeeToShift("1818",new Shift("05/05/2017","Evening"),new Role("Shift Manager"));

            //Site 3:
            dbAccess.addRoleToID("1919",new Role("Branch Manager"));
            dbAccess.addRoleToID("2020",new Role("Secretary"));
            dbAccess.addRoleToID("2121",new Role("Human Resources Manager"));

            dbAccess.addRoleToID("2223",new Role("Shift Manager"));
            dbAccess.addRoleToID("2224",new Role("Shift Manager"));
            dbAccess.addRoleToID("2225",new Role("Shift Manager"));
            dbAccess.addRoleToID("2226",new Role("Shift Manager"));
            dbAccess.addRoleToID("2323",new Role("Shift Manager"));
            dbAccess.addRoleToID("2424",new Role("Shift Manager"));
            dbAccess.addRoleToID("2525",new Role("Shift Manager"));
            dbAccess.addRoleToID("2626",new Role("Shift Manager"));

            dbAccess.addRoleToID("2223",new Role("Cashier"));
            dbAccess.addRoleToID("2224",new Role("Cashier"));
            dbAccess.addRoleToID("2225",new Role("Cashier"));
            dbAccess.addRoleToID("2226",new Role("Cashier"));
            dbAccess.addRoleToID("2323",new Role("Cashier"));
            dbAccess.addRoleToID("2424",new Role("Cashier"));
            dbAccess.addRoleToID("2525",new Role("Cashier"));
            dbAccess.addRoleToID("2626",new Role("Cashier"));

            dbAccess.addRoleToID("2223",new Role("Driver"));
            dbAccess.addDriver(2223, LicenseType.A);
            dbAccess.addRoleToID("2224",new Role("Driver"));
            dbAccess.addDriver(2224, LicenseType.B);
            dbAccess.addRoleToID("2225",new Role("Driver"));
            dbAccess.addDriver(2225, LicenseType.C);
            dbAccess.addRoleToID("2226",new Role("Driver"));
            dbAccess.addDriver(2226, LicenseType.A);
            dbAccess.addRoleToID("2323",new Role("Driver"));
            dbAccess.addDriver(2323, LicenseType.B);
            dbAccess.addRoleToID("2424",new Role("Driver"));
            dbAccess.addDriver(2424, LicenseType.C);
            dbAccess.addRoleToID("2525",new Role("Driver"));
            dbAccess.addDriver(2525, LicenseType.A);
            dbAccess.addRoleToID("2626",new Role("Driver"));
            dbAccess.addDriver(2626, LicenseType.B);

            dbAccess.addRoleToID("2223",new Role("Stock Keeper"));
            dbAccess.addRoleToID("2224",new Role("Stock Keeper"));
            dbAccess.addRoleToID("2225",new Role("Stock Keeper"));
            dbAccess.addRoleToID("2226",new Role("Stock Keeper"));
            dbAccess.addRoleToID("2323",new Role("Stock Keeper"));
            dbAccess.addRoleToID("2424",new Role("Stock Keeper"));
            dbAccess.addRoleToID("2525",new Role("Stock Keeper"));
            dbAccess.addRoleToID("2626",new Role("Stock Keeper"));

            dbAccess.addConstraints(2223, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(2223, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(2224, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(2224, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(2225, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(2225, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(2226, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(2226, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(2323, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(2323, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(2424, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(2424, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(2525, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(2525, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(2626, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(2626, new Constraint("05/05/2017","Evening"));

            dbAccess.addConstraints(2223, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(2223, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(2224, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(2224, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(2225, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(2225, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(2226, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(2226, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(2323, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(2323, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(2424, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(2424, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(2525, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(2525, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(2626, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(2626, new Constraint("06/05/2017","Evening"));

            dbAccess.AssignEmployeeToShift("2223",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("2223",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("2224",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("2224",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("2225",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("2225",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("2226",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("2226",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("2323",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("2323",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("2424",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("2424",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("2525",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("2525",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("2626",new Shift("05/05/2017","Morning"),new Role("Shift Manager"));
            dbAccess.AssignEmployeeToShift("2626",new Shift("05/05/2017","Evening"),new Role("Shift Manager"));

            //Site 4:
            dbAccess.addRoleToID("2727",new Role("Branch Manager"));
            dbAccess.addRoleToID("2828",new Role("Secretary"));
            dbAccess.addRoleToID("2929",new Role("Human Resources Manager"));

            dbAccess.addRoleToID("3030",new Role("Shift Manager"));
            dbAccess.addRoleToID("3131",new Role("Shift Manager"));
            dbAccess.addRoleToID("3232",new Role("Shift Manager"));
            dbAccess.addRoleToID("3334",new Role("Shift Manager"));
            dbAccess.addRoleToID("3434",new Role("Shift Manager"));
            dbAccess.addRoleToID("3535",new Role("Shift Manager"));
            dbAccess.addRoleToID("3636",new Role("Shift Manager"));
            dbAccess.addRoleToID("3737",new Role("Shift Manager"));

            dbAccess.addRoleToID("3030",new Role("Cashier"));
            dbAccess.addRoleToID("3131",new Role("Cashier"));
            dbAccess.addRoleToID("3232",new Role("Cashier"));
            dbAccess.addRoleToID("3334",new Role("Cashier"));
            dbAccess.addRoleToID("3434",new Role("Cashier"));
            dbAccess.addRoleToID("3535",new Role("Cashier"));
            dbAccess.addRoleToID("3636",new Role("Cashier"));
            dbAccess.addRoleToID("3737",new Role("Cashier"));

            dbAccess.addRoleToID("3030",new Role("Driver"));
            dbAccess.addDriver(3030, LicenseType.B);
            dbAccess.addRoleToID("3131",new Role("Driver"));
            dbAccess.addDriver(3131, LicenseType.C);
            dbAccess.addRoleToID("3232",new Role("Driver"));
            dbAccess.addDriver(3232, LicenseType.A);
            dbAccess.addRoleToID("3334",new Role("Driver"));
            dbAccess.addDriver(3334, LicenseType.B);
            dbAccess.addRoleToID("3434",new Role("Driver"));
            dbAccess.addDriver(3434, LicenseType.C);
            dbAccess.addRoleToID("3535",new Role("Driver"));
            dbAccess.addDriver(3535, LicenseType.A);
            dbAccess.addRoleToID("3636",new Role("Driver"));
            dbAccess.addDriver(3636, LicenseType.B);
            dbAccess.addRoleToID("3737",new Role("Driver"));
            dbAccess.addDriver(3737, LicenseType.C);

            dbAccess.addRoleToID("3030",new Role("Stock Keeper"));
            dbAccess.addRoleToID("3131",new Role("Stock Keeper"));
            dbAccess.addRoleToID("3232",new Role("Stock Keeper"));
            dbAccess.addRoleToID("3334",new Role("Stock Keeper"));
            dbAccess.addRoleToID("3434",new Role("Stock Keeper"));
            dbAccess.addRoleToID("3535",new Role("Stock Keeper"));
            dbAccess.addRoleToID("3636",new Role("Stock Keeper"));
            dbAccess.addRoleToID("3737",new Role("Stock Keeper"));

            dbAccess.addConstraints(3030, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(3030, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(3131, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(3131, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(3232, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(3232, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(3334, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(3334, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(3434, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(3434, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(3535, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(3535, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(3636, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(3636, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(3737, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(3737, new Constraint("05/05/2017","Evening"));

            dbAccess.addConstraints(3030, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(3030, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(3131, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(3131, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(3232, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(3232, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(3334, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(3334, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(3434, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(3434, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(3535, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(3535, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(3636, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(3636, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(3737, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(3737, new Constraint("06/05/2017","Evening"));

            dbAccess.AssignEmployeeToShift("3030",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("3030",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("3131",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("3131",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("3232",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("3232",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("3334",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("3334",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("3434",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("3434",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("3535",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("3535",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("3636",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("3636",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("3737",new Shift("05/05/2017","Morning"),new Role("Shift Manager"));
            dbAccess.AssignEmployeeToShift("3737",new Shift("05/05/2017","Evening"),new Role("Shift Manager"));
			System.out.println("Before Add Site 5");

            //Site 5:
            dbAccess.addRoleToID("9898",new Role("Branch Manager"));
            dbAccess.addRoleToID("9090",new Role("Secretary"));
            dbAccess.addRoleToID("9292",new Role("Human Resources Manager"));

            dbAccess.addRoleToID("9393",new Role("Shift Manager"));
            dbAccess.addRoleToID("9494",new Role("Shift Manager"));
            dbAccess.addRoleToID("9595",new Role("Shift Manager"));
            dbAccess.addRoleToID("9696",new Role("Shift Manager"));
            dbAccess.addRoleToID("9797",new Role("Shift Manager"));
            dbAccess.addRoleToID("9898",new Role("Shift Manager"));
            dbAccess.addRoleToID("8181",new Role("Shift Manager"));
            dbAccess.addRoleToID("8080",new Role("Shift Manager"));
            dbAccess.addRoleToID("8282",new Role("Shift Manager"));

            dbAccess.addRoleToID("9393",new Role("Cashier"));
            dbAccess.addRoleToID("9494",new Role("Cashier"));
            dbAccess.addRoleToID("9595",new Role("Cashier"));
            dbAccess.addRoleToID("9696",new Role("Cashier"));
            dbAccess.addRoleToID("9797",new Role("Cashier"));
            dbAccess.addRoleToID("9898",new Role("Cashier"));
            dbAccess.addRoleToID("8181",new Role("Cashier"));
            dbAccess.addRoleToID("8080",new Role("Cashier"));
            dbAccess.addRoleToID("8282",new Role("Cashier"));

            dbAccess.addRoleToID("9393",new Role("Driver"));
            dbAccess.addDriver(9393, LicenseType.B);
            dbAccess.addRoleToID("9494",new Role("Driver"));
            dbAccess.addDriver(9494, LicenseType.C);
            dbAccess.addRoleToID("9595",new Role("Driver"));
            dbAccess.addDriver(9595, LicenseType.A);
            dbAccess.addRoleToID("9696",new Role("Driver"));
            dbAccess.addDriver(9696, LicenseType.B);
            dbAccess.addRoleToID("9797",new Role("Driver"));
            dbAccess.addDriver(9797, LicenseType.C);
            dbAccess.addRoleToID("9898",new Role("Driver"));
            dbAccess.addDriver(9898, LicenseType.A);
            dbAccess.addRoleToID("8181",new Role("Driver"));
            dbAccess.addDriver(8181, LicenseType.B);
            dbAccess.addRoleToID("8080",new Role("Driver"));
            dbAccess.addDriver(8080, LicenseType.C);
            dbAccess.addRoleToID("8282",new Role("Driver"));
            dbAccess.addDriver(8282, LicenseType.A);

            dbAccess.addRoleToID("9393",new Role("Stock Keeper"));
            dbAccess.addRoleToID("9494",new Role("Stock Keeper"));
            dbAccess.addRoleToID("9595",new Role("Stock Keeper"));
            dbAccess.addRoleToID("9696",new Role("Stock Keeper"));
            dbAccess.addRoleToID("9797",new Role("Stock Keeper"));
            dbAccess.addRoleToID("9898",new Role("Stock Keeper"));
            dbAccess.addRoleToID("8181",new Role("Stock Keeper"));
            dbAccess.addRoleToID("8080",new Role("Stock Keeper"));
            dbAccess.addRoleToID("8282",new Role("Stock Keeper"));

            dbAccess.addConstraints(9393, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(9393, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(9494, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(9494, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(9595, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(9595, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(9696, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(9696, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(9797, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(9797, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(9898, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(9898, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(8181, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(8181, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(8080, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(8080, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(8282, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(8282, new Constraint("05/05/2017","Evening"));

            dbAccess.addConstraints(9393, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(9393, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(9494, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(9494, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(9595, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(9595, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(9696, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(9696, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(9797, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(9797, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(9898, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(9898, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(8181, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(8181, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(8080, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(8080, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(8282, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(8282, new Constraint("06/05/2017","Evening"));

            dbAccess.AssignEmployeeToShift("9393",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("9393",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("9494",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("9494",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("9595",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("9595",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("9696",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("9696",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("9797",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("9797",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("9898",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("9898",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("8181",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("8181",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("8080",new Shift("05/05/2017","Morning"),new Role("Shift Manager"));
            dbAccess.AssignEmployeeToShift("8080",new Shift("05/05/2017","Evening"),new Role("Shift Manager"));

            //Site 6:
            dbAccess.addRoleToID("1234",new Role("Branch Manager"));
            dbAccess.addRoleToID("1235",new Role("Secretary"));
            dbAccess.addRoleToID("1236",new Role("Human Resources Manager"));

            dbAccess.addRoleToID("1237",new Role("Shift Manager"));
            dbAccess.addRoleToID("1238",new Role("Shift Manager"));
            dbAccess.addRoleToID("1239",new Role("Shift Manager"));
            dbAccess.addRoleToID("1240",new Role("Shift Manager"));
            dbAccess.addRoleToID("1241",new Role("Shift Manager"));
            dbAccess.addRoleToID("1242",new Role("Shift Manager"));
            dbAccess.addRoleToID("1243",new Role("Shift Manager"));
            dbAccess.addRoleToID("1244",new Role("Shift Manager"));

            dbAccess.addRoleToID("1237",new Role("Cashier"));
            dbAccess.addRoleToID("1238",new Role("Cashier"));
            dbAccess.addRoleToID("1239",new Role("Cashier"));
            dbAccess.addRoleToID("1240",new Role("Cashier"));
            dbAccess.addRoleToID("1241",new Role("Cashier"));
            dbAccess.addRoleToID("1242",new Role("Cashier"));
            dbAccess.addRoleToID("1243",new Role("Cashier"));
            dbAccess.addRoleToID("1244",new Role("Cashier"));

            dbAccess.addRoleToID("1237",new Role("Driver"));
            dbAccess.addDriver(1237, LicenseType.B);
            dbAccess.addRoleToID("1238",new Role("Driver"));
            dbAccess.addDriver(1238, LicenseType.C);
            dbAccess.addRoleToID("1239",new Role("Driver"));
            dbAccess.addDriver(1239, LicenseType.A);
            dbAccess.addRoleToID("1240",new Role("Driver"));
            dbAccess.addDriver(1240, LicenseType.B);
            dbAccess.addRoleToID("1241",new Role("Driver"));
            dbAccess.addDriver(1241, LicenseType.C);
            dbAccess.addRoleToID("1242",new Role("Driver"));
            dbAccess.addDriver(1242, LicenseType.A);
            dbAccess.addRoleToID("1243",new Role("Driver"));
            dbAccess.addDriver(1243, LicenseType.B);
            dbAccess.addRoleToID("1244",new Role("Driver"));
            dbAccess.addDriver(1244, LicenseType.C);

            dbAccess.addRoleToID("1237",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1238",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1239",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1240",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1241",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1242",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1243",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1244",new Role("Stock Keeper"));

            dbAccess.addConstraints(1237, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1237, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1238, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1238, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1239, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1239, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1240, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1240, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1241, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1241, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1242, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1242, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1243, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1243, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1244, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1244, new Constraint("05/05/2017","Evening"));

            dbAccess.addConstraints(1237, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1237, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1238, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1238, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1239, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1239, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1240, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1240, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1241, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1241, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1242, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1242, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1243, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1243, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1244, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1244, new Constraint("06/05/2017","Evening"));

            dbAccess.AssignEmployeeToShift("1237",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1237",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1238",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1238",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1239",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1239",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1240",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1240",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1241",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1241",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1242",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1242",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1243",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1243",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1244",new Shift("05/05/2017","Morning"),new Role("Shift Manager"));
            dbAccess.AssignEmployeeToShift("1244",new Shift("05/05/2017","Evening"),new Role("Shift Manager"));

            //Site 7:
            dbAccess.addRoleToID("1245",new Role("Branch Manager"));
            dbAccess.addRoleToID("1246",new Role("Secretary"));
            dbAccess.addRoleToID("1247",new Role("Human Resources Manager"));

            dbAccess.addRoleToID("1248",new Role("Shift Manager"));
            dbAccess.addRoleToID("1249",new Role("Shift Manager"));
            dbAccess.addRoleToID("1250",new Role("Shift Manager"));
            dbAccess.addRoleToID("1251",new Role("Shift Manager"));
            dbAccess.addRoleToID("1252",new Role("Shift Manager"));
            dbAccess.addRoleToID("1253",new Role("Shift Manager"));
            dbAccess.addRoleToID("1254",new Role("Shift Manager"));
            dbAccess.addRoleToID("1255",new Role("Shift Manager"));
            dbAccess.addRoleToID("1256",new Role("Shift Manager"));

            dbAccess.addRoleToID("1248",new Role("Cashier"));
            dbAccess.addRoleToID("1249",new Role("Cashier"));
            dbAccess.addRoleToID("1250",new Role("Cashier"));
            dbAccess.addRoleToID("1251",new Role("Cashier"));
            dbAccess.addRoleToID("1252",new Role("Cashier"));
            dbAccess.addRoleToID("1253",new Role("Cashier"));
            dbAccess.addRoleToID("1254",new Role("Cashier"));
            dbAccess.addRoleToID("1255",new Role("Cashier"));
            dbAccess.addRoleToID("1256",new Role("Cashier"));

            dbAccess.addRoleToID("1248",new Role("Driver"));
            dbAccess.addDriver(1248, LicenseType.B);
            dbAccess.addRoleToID("1249",new Role("Driver"));
            dbAccess.addDriver(1249, LicenseType.C);
            dbAccess.addRoleToID("1250",new Role("Driver"));
            dbAccess.addDriver(1250, LicenseType.A);
            dbAccess.addRoleToID("1251",new Role("Driver"));
            dbAccess.addDriver(1251, LicenseType.B);
            dbAccess.addRoleToID("1252",new Role("Driver"));
            dbAccess.addDriver(1252, LicenseType.C);
            dbAccess.addRoleToID("1253",new Role("Driver"));
            dbAccess.addDriver(1253, LicenseType.A);
            dbAccess.addRoleToID("1254",new Role("Driver"));
            dbAccess.addDriver(1254, LicenseType.B);
            dbAccess.addRoleToID("1255",new Role("Driver"));
            dbAccess.addDriver(1255, LicenseType.C);
            dbAccess.addRoleToID("1256",new Role("Driver"));
            dbAccess.addDriver(1256, LicenseType.A);

            dbAccess.addRoleToID("1248",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1249",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1250",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1251",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1252",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1253",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1254",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1255",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1256",new Role("Stock Keeper"));

            dbAccess.addConstraints(1248, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1248, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1249, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1249, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1250, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1250, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1251, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1251, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1252, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1252, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1253, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1253, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1254, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1254, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1255, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1255, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1256, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1256, new Constraint("05/05/2017","Evening"));

            dbAccess.addConstraints(1248, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1248, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1249, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1249, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1250, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1250, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1251, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1251, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1252, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1252, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1253, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1253, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1254, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1254, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1255, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1255, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1256, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1256, new Constraint("06/05/2017","Evening"));

            dbAccess.AssignEmployeeToShift("1248",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1248",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1249",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1249",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1250",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1250",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1251",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1251",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1252",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1252",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1253",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1253",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1254",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1254",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1255",new Shift("05/05/2017","Morning"),new Role("Shift Manager"));
            dbAccess.AssignEmployeeToShift("1255",new Shift("05/05/2017","Evening"),new Role("Shift Manager"));
			System.out.println("Before Add Site 8");

            //Site 8:
            dbAccess.addRoleToID("1257",new Role("Branch Manager"));
            dbAccess.addRoleToID("1258",new Role("Secretary"));
            dbAccess.addRoleToID("1259",new Role("Human Resources Manager"));

            dbAccess.addRoleToID("1260",new Role("Shift Manager"));
            dbAccess.addRoleToID("1261",new Role("Shift Manager"));
            dbAccess.addRoleToID("1262",new Role("Shift Manager"));
            dbAccess.addRoleToID("1263",new Role("Shift Manager"));
            dbAccess.addRoleToID("1264",new Role("Shift Manager"));
            dbAccess.addRoleToID("1265",new Role("Shift Manager"));
            dbAccess.addRoleToID("1266",new Role("Shift Manager"));
            dbAccess.addRoleToID("1267",new Role("Shift Manager"));
            dbAccess.addRoleToID("1268",new Role("Shift Manager"));
            dbAccess.addRoleToID("1269",new Role("Shift Manager"));
            dbAccess.addRoleToID("1270",new Role("Shift Manager"));

            dbAccess.addRoleToID("1260",new Role("Cashier"));
            dbAccess.addRoleToID("1261",new Role("Cashier"));
            dbAccess.addRoleToID("1262",new Role("Cashier"));
            dbAccess.addRoleToID("1263",new Role("Cashier"));
            dbAccess.addRoleToID("1264",new Role("Cashier"));
            dbAccess.addRoleToID("1265",new Role("Cashier"));
            dbAccess.addRoleToID("1266",new Role("Cashier"));
            dbAccess.addRoleToID("1267",new Role("Cashier"));
            dbAccess.addRoleToID("1268",new Role("Cashier"));
            dbAccess.addRoleToID("1269",new Role("Cashier"));
            dbAccess.addRoleToID("1270",new Role("Cashier"));

            dbAccess.addRoleToID("1260",new Role("Driver"));
            dbAccess.addDriver(1260, LicenseType.B);
            dbAccess.addRoleToID("1261",new Role("Driver"));
            dbAccess.addDriver(1261, LicenseType.C);
            dbAccess.addRoleToID("1262",new Role("Driver"));
            dbAccess.addDriver(1262, LicenseType.A);
            dbAccess.addRoleToID("1263",new Role("Driver"));
            dbAccess.addDriver(1263, LicenseType.B);
            dbAccess.addRoleToID("1264",new Role("Driver"));
            dbAccess.addDriver(1264, LicenseType.C);
            dbAccess.addRoleToID("1265",new Role("Driver"));
            dbAccess.addDriver(1265, LicenseType.A);
            dbAccess.addRoleToID("1266",new Role("Driver"));
            dbAccess.addDriver(1266, LicenseType.B);
            dbAccess.addRoleToID("1267",new Role("Driver"));
            dbAccess.addDriver(1267, LicenseType.C);
            dbAccess.addRoleToID("1268",new Role("Driver"));
            dbAccess.addDriver(1268, LicenseType.A);
            dbAccess.addRoleToID("1269",new Role("Driver"));
            dbAccess.addDriver(1269, LicenseType.B);
            dbAccess.addRoleToID("1270",new Role("Driver"));
            dbAccess.addDriver(1270, LicenseType.C);

            dbAccess.addRoleToID("1260",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1261",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1262",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1263",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1264",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1265",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1266",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1267",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1268",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1269",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1270",new Role("Stock Keeper"));

            dbAccess.addConstraints(1260, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1260, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1261, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1261, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1262, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1262, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1263, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1263, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1264, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1264, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1265, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1265, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1266, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1266, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1267, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1267, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1268, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1268, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1269, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1269, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1270, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1270, new Constraint("05/05/2017","Evening"));

            dbAccess.addConstraints(1260, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1260, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1261, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1261, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1262, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1262, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1263, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1263, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1264, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1264, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1265, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1265, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1266, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1266, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1267, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1267, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1268, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1268, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1269, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1269, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1270, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1270, new Constraint("06/05/2017","Evening"));

            dbAccess.AssignEmployeeToShift("1260",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1260",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1261",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1261",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1262",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1262",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1263",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1263",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1264",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1264",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1265",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1265",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1266",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1266",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1267",new Shift("05/05/2017","Morning"),new Role("Shift Manager"));
            dbAccess.AssignEmployeeToShift("1267",new Shift("05/05/2017","Evening"),new Role("Shift Manager"));
			System.out.println("Before Add Site 9");

            //Site 9:
            dbAccess.addRoleToID("1271",new Role("Branch Manager"));
            dbAccess.addRoleToID("1272",new Role("Secretary"));
            dbAccess.addRoleToID("1273",new Role("Human Resources Manager"));

            dbAccess.addRoleToID("1274",new Role("Shift Manager"));
            dbAccess.addRoleToID("1275",new Role("Shift Manager"));
            dbAccess.addRoleToID("1276",new Role("Shift Manager"));
            dbAccess.addRoleToID("1277",new Role("Shift Manager"));
            dbAccess.addRoleToID("1278",new Role("Shift Manager"));
            dbAccess.addRoleToID("1279",new Role("Shift Manager"));
            dbAccess.addRoleToID("1280",new Role("Shift Manager"));
            dbAccess.addRoleToID("1281",new Role("Shift Manager"));
            dbAccess.addRoleToID("1282",new Role("Shift Manager"));
            dbAccess.addRoleToID("1283",new Role("Shift Manager"));

            dbAccess.addRoleToID("1274",new Role("Cashier"));
            dbAccess.addRoleToID("1275",new Role("Cashier"));
            dbAccess.addRoleToID("1276",new Role("Cashier"));
            dbAccess.addRoleToID("1277",new Role("Cashier"));
            dbAccess.addRoleToID("1278",new Role("Cashier"));
            dbAccess.addRoleToID("1279",new Role("Cashier"));
            dbAccess.addRoleToID("1280",new Role("Cashier"));
            dbAccess.addRoleToID("1281",new Role("Cashier"));
            dbAccess.addRoleToID("1282",new Role("Cashier"));
            dbAccess.addRoleToID("1283",new Role("Cashier"));

            dbAccess.addRoleToID("1274",new Role("Driver"));
            dbAccess.addDriver(1274, LicenseType.B);
            dbAccess.addRoleToID("1275",new Role("Driver"));
            dbAccess.addDriver(1275, LicenseType.C);
            dbAccess.addRoleToID("1276",new Role("Driver"));
            dbAccess.addDriver(1276, LicenseType.A);
            dbAccess.addRoleToID("1277",new Role("Driver"));
            dbAccess.addDriver(1277, LicenseType.B);
            dbAccess.addRoleToID("1278",new Role("Driver"));
            dbAccess.addDriver(1278, LicenseType.C);
            dbAccess.addRoleToID("1279",new Role("Driver"));
            dbAccess.addDriver(1279, LicenseType.A);
            dbAccess.addRoleToID("1280",new Role("Driver"));
            dbAccess.addDriver(1280, LicenseType.B);
            dbAccess.addRoleToID("1281",new Role("Driver"));
            dbAccess.addDriver(1281, LicenseType.C);
            dbAccess.addRoleToID("1282",new Role("Driver"));
            dbAccess.addDriver(1282, LicenseType.A);
            dbAccess.addRoleToID("1283",new Role("Driver"));
            dbAccess.addDriver(1283, LicenseType.B);

            dbAccess.addRoleToID("1274",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1275",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1276",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1277",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1278",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1279",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1280",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1281",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1282",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1283",new Role("Stock Keeper"));

            dbAccess.addConstraints(1274, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1274, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1275, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1275, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1276, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1276, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1277, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1277, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1278, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1278, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1279, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1279, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1280, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1280, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1281, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1281, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1282, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1282, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1283, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1283, new Constraint("05/05/2017","Evening"));

            dbAccess.addConstraints(1274, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1274, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1275, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1275, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1276, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1276, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1277, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1277, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1278, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1278, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1279, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1279, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1280, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1280, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1281, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1281, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1282, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1282, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1283, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1283, new Constraint("06/05/2017","Evening"));

            dbAccess.AssignEmployeeToShift("1274",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1274",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1275",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1275",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1276",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1276",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1277",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1277",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1278",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1278",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1279",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1279",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1280",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1280",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1281",new Shift("05/05/2017","Morning"),new Role("Shift Manager"));
            dbAccess.AssignEmployeeToShift("1281",new Shift("05/05/2017","Evening"),new Role("Shift Manager"));

			System.out.println("Before Add Site 10");

            //Site 10:
            dbAccess.addRoleToID("1284",new Role("Branch Manager"));
            dbAccess.addRoleToID("1285",new Role("Secretary"));
            dbAccess.addRoleToID("1286",new Role("Human Resources Manager"));

            dbAccess.addRoleToID("1287",new Role("Shift Manager"));
            dbAccess.addRoleToID("1288",new Role("Shift Manager"));
            dbAccess.addRoleToID("1289",new Role("Shift Manager"));
            dbAccess.addRoleToID("1290",new Role("Shift Manager"));
            dbAccess.addRoleToID("1291",new Role("Shift Manager"));
            dbAccess.addRoleToID("1292",new Role("Shift Manager"));
            dbAccess.addRoleToID("1293",new Role("Shift Manager"));
            dbAccess.addRoleToID("1294",new Role("Shift Manager"));
            dbAccess.addRoleToID("1295",new Role("Shift Manager"));
            dbAccess.addRoleToID("1296",new Role("Shift Manager"));

            dbAccess.addRoleToID("1287",new Role("Cashier"));
            dbAccess.addRoleToID("1288",new Role("Cashier"));
            dbAccess.addRoleToID("1289",new Role("Cashier"));
            dbAccess.addRoleToID("1290",new Role("Cashier"));
            dbAccess.addRoleToID("1291",new Role("Cashier"));
            dbAccess.addRoleToID("1292",new Role("Cashier"));
            dbAccess.addRoleToID("1293",new Role("Cashier"));
            dbAccess.addRoleToID("1294",new Role("Cashier"));
            dbAccess.addRoleToID("1295",new Role("Cashier"));
            dbAccess.addRoleToID("1296",new Role("Cashier"));

            dbAccess.addRoleToID("1287",new Role("Driver"));
            dbAccess.addDriver(1287, LicenseType.B);
            dbAccess.addRoleToID("1288",new Role("Driver"));
            dbAccess.addDriver(1288, LicenseType.C);
            dbAccess.addRoleToID("1289",new Role("Driver"));
            dbAccess.addDriver(1289, LicenseType.A);
            dbAccess.addRoleToID("1290",new Role("Driver"));
            dbAccess.addDriver(1290, LicenseType.B);
            dbAccess.addRoleToID("1291",new Role("Driver"));
            dbAccess.addDriver(1291, LicenseType.C);
            dbAccess.addRoleToID("1292",new Role("Driver"));
            dbAccess.addDriver(1292, LicenseType.A);
            dbAccess.addRoleToID("1293",new Role("Driver"));
            dbAccess.addDriver(1293, LicenseType.B);
            dbAccess.addRoleToID("1294",new Role("Driver"));
            dbAccess.addDriver(1294, LicenseType.C);
            dbAccess.addRoleToID("1295",new Role("Driver"));
            dbAccess.addDriver(1295, LicenseType.A);
            dbAccess.addRoleToID("1296",new Role("Driver"));
            dbAccess.addDriver(1296, LicenseType.B);

            dbAccess.addRoleToID("1287",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1288",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1289",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1290",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1291",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1292",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1293",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1294",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1295",new Role("Stock Keeper"));
            dbAccess.addRoleToID("1296",new Role("Stock Keeper"));

            dbAccess.addConstraints(1287, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1287, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1288, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1288, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1289, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1289, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1290, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1290, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1291, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1291, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1292, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1292, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1293, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1293, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1294, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1294, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1295, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1295, new Constraint("05/05/2017","Evening"));
            dbAccess.addConstraints(1296, new Constraint("05/05/2017","Morning"));
            dbAccess.addConstraints(1296, new Constraint("05/05/2017","Evening"));

            dbAccess.addConstraints(1287, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1287, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1288, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1288, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1289, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1289, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1290, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1290, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1291, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1291, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1292, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1292, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1293, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1293, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1294, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1294, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1295, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1295, new Constraint("06/05/2017","Evening"));
            dbAccess.addConstraints(1296, new Constraint("06/05/2017","Morning"));
            dbAccess.addConstraints(1296, new Constraint("06/05/2017","Evening"));

            dbAccess.AssignEmployeeToShift("1287",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1287",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1288",new Shift("05/05/2017","Morning"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1288",new Shift("05/05/2017","Evening"),new Role("Cashier"));
            dbAccess.AssignEmployeeToShift("1289",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1289",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1290",new Shift("05/05/2017","Morning"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1290",new Shift("05/05/2017","Evening"),new Role("Stock Keeper"));
            dbAccess.AssignEmployeeToShift("1291",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1291",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1292",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1292",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1293",new Shift("05/05/2017","Morning"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1293",new Shift("05/05/2017","Evening"),new Role("Driver"));
            dbAccess.AssignEmployeeToShift("1294",new Shift("05/05/2017","Morning"),new Role("Shift Manager"));
            dbAccess.AssignEmployeeToShift("1294",new Shift("05/05/2017","Evening"),new Role("Shift Manager"));


            dbAccess.insertStandard("Morning", new Role("Cashier"), 2);
            dbAccess.insertStandard("Morning", new Role("Shift Manager"), 1);
            dbAccess.insertStandard("Morning", new Role("Driver"), 3);
            dbAccess.insertStandard("Morning", new Role("Stock Keeper"), 2);

            dbAccess.insertStandard("Evening", new Role("Cashier"), 2);
            dbAccess.insertStandard("Evening", new Role("Shift Manager"), 1);
            dbAccess.insertStandard("Evening", new Role("Driver"), 3);
            dbAccess.insertStandard("Evening", new Role("Stock Keeper"), 2);
            
            DataBase db = DataBase.getDataBase();
            
            db.insertNewShop("ramhal 1,05052221113,Roy");
            db.insertNewShop("ramhal 2,05213541342,Yaniv");
            db.insertNewShop("ramhal 3,05123554145,Kuti");
            db.insertNewShop("ramhal 4,05296847315,Alex");

            

			System.out.println("Add Category");


    		db.addCategory("Dairy,-");
    		db.addCategory("Dairy Milk,Dairy");
    		db.addCategory("Dairy Cheese,Dairy");
    		db.addCategory("Dairy Yogurt,Dairy");
    		db.addCategory("Snack,-");
    		db.addCategory("Meat,-");
    		db.addCategory("Meat Sliced,Meat");		
    		System.out.println("After Add Category");


    		db.addSupplier(new Supplier("1", "Roy&Co", "001", "Credit", false,"Rager 32 Beer Sheva"));
    		db.addSupplier(new Supplier("2", "HotBrazil", "002", "Credit", false,"Rio de 12 Jenro Brazil"));
    		db.addSupplier(new Supplier("3", "Orsh", "003", "Credit", true,"Ha Arbaa 2 Tel Aviv"));
    		db.addSupplier(new Supplier("4", "Idanoosh", "004", "Credit", true,"Hatuna"));

    		

    		
    		db.addContact(new Supplier("1", "", "", "", false,""), new Contact("1", "Roy", "Ygael", "050512341"));
    		db.addContact(new Supplier("2", "", "", "", false,""), new Contact("2", "Yaniv", "Daye", "051512341"));
    		db.addContact(new Supplier("3", "", "", "", false,""), new Contact("3", "Or", "Ben Shushan", "052512341"));
    		db.addContact(new Supplier("4", "", "", "", false,""), new Contact("4", "Idan", "Izcovitch", "053512341"));

    		db.addProduct(new Supplier("1", "", "", "", false,""), new ProvidedProduct("Milk 1L", "Tara", "2", "1", "1",1));
    		db.addProduct(new Supplier("1", "", "", "", false,""), new ProvidedProduct("Koteg 500gr", "Tnuva", "3", "1", "2",1));
    		db.addProduct(new Supplier("1", "", "", "", false,""), new ProvidedProduct("Koteg 100gr", "Tnuva", "1", "1", "3",1));
    		db.addProduct(new Supplier("1", "", "", "", false,""), new ProvidedProduct("Danny 100gr", "Tara", "3", "1", "4",1));
    		db.addProduct(new Supplier("1", "", "", "", false,""), new ProvidedProduct("Bamba 100gr", "Osem", "1.5", "1", "5",1));
    		db.addProduct(new Supplier("1", "", "", "", false,""), new ProvidedProduct("Bamba 100gr", "Shush", "1", "1", "6",1));
    		db.addProduct(new Supplier("1", "", "", "", false,""), new ProvidedProduct("Bisly 100gr", "Osem", "1.5", "1", "7",1));
    		db.addProduct(new Supplier("1", "", "", "", false,""), new ProvidedProduct("Salami 200gr", "Zogloveck", "2", "1", "8",1));


    		db.addProduct(new Supplier("2", "", "", "", false,""), new ProvidedProduct("Milk 1L", "Tara", "2", "1", "1",2));
    		List<Integer> k = new LinkedList<Integer>();
    		k.add(1);
    		k.add(2);
    		k.add(3);
    		k.add(5);
    		k.add(6);
    		List<Integer> t = new LinkedList<Integer>();
    		t.add(1);
    		t.add(4);
    		t.add(5);
    		t.add(7);
    		db.addSupplierDays(new SupplierSupplyDays("1", k));
    		db.addSupplierDays(new SupplierSupplyDays("2", t));
    		db.addProduct(new Supplier("2", "", "", "", false,""), new ProvidedProduct("Koteg 500gr", "Tnuva", "3", "1", "2",2));
    		db.addProduct(new Supplier("2", "", "", "", false,""), new ProvidedProduct("Koteg 100gr", "Tnuva", "1", "1", "3",2));
    		db.addProduct(new Supplier("2", "", "", "", false,""), new ProvidedProduct("Danny 100gr", "Tara", "3", "1", "4",2));
    		db.addProduct(new Supplier("2", "", "", "", false,""), new ProvidedProduct("Bamba 100gr", "Osem", "1", "1", "5",2));
    		db.addProduct(new Supplier("2", "", "", "", false,""), new ProvidedProduct("Bamba 100gr", "Shush", "1", "1", "6",2));
    		db.addProduct(new Supplier("2", "", "", "", false,""), new ProvidedProduct("Bisly 100gr", "Osem", "1", "1", "7",2));
    		db.addProduct(new Supplier("2", "", "", "", false,""), new ProvidedProduct("Salami 200gr", "Zogloveck", "2", "1", "8",2));

			System.out.println("after Add Suppliers");


    		db.addSupplierDiscount("Milk 1L,Tara,2,50,25/05/2017,14/06/2017,10");
    		db.addSupplierDiscount("Koteg 100gr,Tnuva,2,50,25/05/2017,14/06/2017,10");

			System.out.println("after Add Suppliers Discount");

    		db.addProductInCategory("Milk 1L,Dairy Milk,Tara,4,5,1",1);
    		db.addProductInCategory("Koteg 500gr,Dairy Cheese,Tnuva,6,1,1",1);
    		db.addProductInCategory("Koteg 100gr,Dairy Cheese,Tnuva,3,2,1",1);
    		db.addProductInCategory("Danny 100gr,Dairy Yogurt,Tara,6,2,1",1);
    		db.addProductInCategory("Bamba 100gr,Snack,Osem,4,10,1",1);
    		db.addProductInCategory("Bamba 100gr,Snack,Shush,3,5,1",1);
    		db.addProductInCategory("Salami 200gr,Meat Sliced,Zogloveck,3,11,1",1);
    		db.addProductInCategory("Bisly 100gr,Snack,Osem,4,10,1",1);

    		db.addProductInCategory("Milk 1L,Dairy Milk,Tara,4,5,1",2);
    		db.addProductInCategory("Koteg 500gr,Dairy Cheese,Tnuva,6,1,1",2);
    		db.addProductInCategory("Koteg 100gr,Dairy Cheese,Tnuva,3,2,1",2);
    		db.addProductInCategory("Danny 100gr,Dairy Yogurt,Tara,6,2,1",2);
    		db.addProductInCategory("Bamba 100gr,Snack,Osem,4,10,1,1",2);
    		db.addProductInCategory("Bamba 100gr,Snack,Shush,3,5,1",2);
    		db.addProductInCategory("Salami 200gr,Meat Sliced,Zogloveck,3,11,1",2);
    		db.addProductInCategory("Bisly 100gr,Snack,Osem,4,10,1",2);

    		db.addProductInCategory("Milk 1L,Dairy Milk,Tara,4,5,1",3);
    		db.addProductInCategory("Koteg 500gr,Dairy Cheese,Tnuva,6,1,1",3);
    		db.addProductInCategory("Koteg 100gr,Dairy Cheese,Tnuva,3,2,1",3);
    		db.addProductInCategory("Danny 100gr,Dairy Yogurt,Tara,6,2,1",3);
    		db.addProductInCategory("Bamba 100gr,Snack,Osem,4,10,1",3);
    		db.addProductInCategory("Bamba 100gr,Snack,Shush,3,5,1",3);
    		db.addProductInCategory("Salami 200gr,Meat Sliced,Zogloveck,3,11,1",3);
    		db.addProductInCategory("Bisly 100gr,Snack,Osem,4,10,1",3);

    		db.addProductInCategory("Milk 1L,Dairy Milk,Tara,4,5,1",4);
    		db.addProductInCategory("Koteg 500gr,Dairy Cheese,Tnuva,6,1,1",4);
    		db.addProductInCategory("Koteg 100gr,Dairy Cheese,Tnuva,3,2,1",4);
    		db.addProductInCategory("Danny 100gr,Dairy Yogurt,Tara,6,2,1",4);
    		db.addProductInCategory("Bamba 100gr,Snack,Osem,4,10,1",4);
    		db.addProductInCategory("Bamba 100gr,Snack,Shush,3,5,1",4);
    		db.addProductInCategory("Salami 200gr,Meat Sliced,Zogloveck,3,11,1",4);
    		db.addProductInCategory("Bisly 100gr,Snack,Osem,4,10,1",4);
    		
    		db.addCategoryDiscount("Dairy Cheese,30,30/04/2017,30/05/2017");
    		db.addProductDiscount("Salami 200gr,Zogloveck,50,30/04/2017,30/05/2017");
    		
    		db.addProductsToInventory("Milk 1L,Tara,10/05/2017,100",1);
            db.addProductsToInventory("Koteg 500gr,Tnuva,10/05/2017,20",1);
            db.addProductsToInventory("Koteg 100gr,Tnuva,10/05/2017,50",1);
            db.addProductsToInventory("Danny 100gr,Tara,10/05/2017,30",1);
            db.addProductsToInventory("Bamba 100gr,Osem,22/05/2017,100",1);
            db.addProductsToInventory("Bamba 100gr,Shush,22/05/2017,50",1);
            db.addProductsToInventory("Bisly 100gr,Osem,22/05/2017,100",1);
            db.addProductsToInventory("Salami 200gr,Zogloveck,15/05/2017,100",1);

            db.addProductsToInventory("Milk 1L,Tara,10/05/2017,100",2);
            db.addProductsToInventory("Koteg 500gr,Tnuva,10/05/2017,20",2);
            db.addProductsToInventory("Koteg 100gr,Tnuva,10/05/2017,50",2);
            db.addProductsToInventory("Danny 100gr,Tara,10/05/2017,30",2);
            db.addProductsToInventory("Bamba 100gr,Osem,22/05/2017,100",2);
            db.addProductsToInventory("Bamba 100gr,Shush,22/05/2017,50",2);
            db.addProductsToInventory("Bisly 100gr,Osem,22/05/2017,100",2);
            db.addProductsToInventory("Salami 200gr,Zogloveck,15/05/2017,100",2);

            db.addProductsToInventory("Milk 1L,Tara,10/05/2017,100",3);
            db.addProductsToInventory("Koteg 500gr,Tnuva,10/05/2017,20",3);
            db.addProductsToInventory("Koteg 100gr,Tnuva,10/05/2017,50",3);
            db.addProductsToInventory("Danny 100gr,Tara,10/05/2017,30",3);
            db.addProductsToInventory("Bamba 100gr,Osem,22/05/2017,100",3);
            db.addProductsToInventory("Bamba 100gr,Shush,22/05/2017,50",3);
            db.addProductsToInventory("Bisly 100gr,Osem,22/05/2017,100",3);
            db.addProductsToInventory("Salami 200gr,Zogloveck,15/05/2017,100",3);
            
            db.addProductsToInventory("Milk 1L,Tara,10/05/2017,100",4);
            db.addProductsToInventory("Koteg 500gr,Tnuva,10/05/2017,20",4);
            db.addProductsToInventory("Koteg 100gr,Tnuva,10/05/2017,50",4);
            db.addProductsToInventory("Danny 100gr,Tara,10/05/2017,30",4);
            db.addProductsToInventory("Bamba 100gr,Osem,22/05/2017,100",4);
            db.addProductsToInventory("Bamba 100gr,Shush,22/05/2017,50",4);
            db.addProductsToInventory("Bisly 100gr,Osem,22/05/2017,100",4);
            db.addProductsToInventory("Salami 200gr,Zogloveck,15/05/2017,100",4);
			System.out.println("Add Products");
			
			System.out.println("Before adding Orders");
			int shop=1;
			Map<ProvidedProduct,Integer> wantedProducts = db.inserNewOrder("Milk 1L:Tara:20,Koteg 500gr:Tnuva:10,Danny 100gr:Tara:5",shop);
			ArrayList<Integer> delieveryDays = (ArrayList<Integer>) db.getOrderDaysByInput("4,5"); 					
			ArrayList<Order> listOfOrders = db.CreateListOfOrdersByProducts(wantedProducts,shop, delieveryDays);
			if(listOfOrders != null)
			{
				System.out.println("The order is from "+listOfOrders.size() +" suppliers");
				for (Order order : listOfOrders) 
				{
					if(db.addNewOrder(order))
						System.out.println("Order has added successfully");
					else
						System.out.println("An error occourd, Order didn't added");		
				}	
			}
			else
			{
				System.out.println("Please try other order");
			}
			
			shop = 2;
			wantedProducts = db.inserNewOrder("Bamba 100gr:Osem:30,Bisly 100gr:Osem:15",shop);
			delieveryDays = (ArrayList<Integer>) db.getOrderDaysByInput("1,5"); 					
			listOfOrders = db.CreateListOfOrdersByProducts(wantedProducts,shop, delieveryDays);
			if(listOfOrders != null)
			{
				System.out.println("The order is from "+listOfOrders.size() +" suppliers");
				for (Order order : listOfOrders) 
				{
					if(db.addNewOrder(order))
						System.out.println("Order has added successfully");
					else
						System.out.println("An error occourd, Order didn't added");		
				}	
			}
			else
			{
				System.out.println("Please try other order");
			}

			dbAccess.AssignEmployeeToShift("4444",new Shift("01/06/2017","Morning"),new Role("Driver"));
        } catch ( Exception e )
        {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        
        }
        System.out.println("Tables created successfully");
    }
    
}