package app.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import DataAccessLayer.DBAccess;
import dal.DataBase;
import dal.OrderDaysTable;
import dal.Supplier_Supply_DaysTable;
import modules.*;

public class MainSuppliers {

	private static Scanner sc = new Scanner(System.in);
	private static DataBase dbBase = null;// DataBase.getDataBase();
	private static int shop;

	/*public static void main(String[] args) {
		dbBase = DataBase.getDataBase();
		run();
		dbBase.close();
	}*/

	public static void clearScreen() {
		System.out.println();
	}

	/*public static void run() {
		MainMenu();
		System.out.println("Have a nice day ;) ");
		sc.nextLine();
	}*/

	/*public static void MainMenu(){
		String UserSelect = null;
		String wantedSuperLi = null;
		do {
			System.out.println("--------------- Welcome to the Super-li Menu ---------------");
			System.out.println();
			System.out.println("0) Exit");
			System.out.println("1) Choose a Super-li by ID");
			System.out.println("2) Show all Super-li's IDs");
			System.out.println("3) Insert new Shop");
			System.out.println("4) Delete a Shop by ID");
			UserSelect = sc.nextLine();
			switch (UserSelect) {
			// the menu
			case "0": // Exit
				return;
			case "1": 
				System.out.println("Please enter the wanted Super-Li's ID");
				wantedSuperLi=sc.nextLine();
				if(dbBase.validWantedSuperLi(wantedSuperLi)){
					shop=Integer.parseInt(wantedSuperLi);
					System.out.println();
					System.out.println();
					MainMenuAfterChoosing();
				}
				break;
			case "2": 
				dbBase.showAllSuperLis();
				break;
			case "3":
				System.out.println("Please enter the new Super-Li in the next format: 'Address,Phone,Contact'");
				dbBase.insertNewShop(sc.nextLine());
				break;
			case "4":
				System.out.println("Please enter the ID of Super-Li you want to delete");
				dbBase.deleteShopById(sc.nextLine());
				break;
			default:
				System.out.println("Incorrect input!");
				break;
			}
		} while (true);
	}*/

	/*public static void MainMenuAfterChoosing(){
		String UserSelect = null;
		do {
			System.out.println("--------------- Welcome to the Main Menu ---------------");
			System.out.println();
			System.out.println("0) Exit");
			System.out.println("1) Inventory Menu");
			System.out.println("2) Suppliers Menu");
			System.out.println("3) Orders Menu");
			UserSelect = sc.nextLine();
			switch (UserSelect) {
			// the menu
			case "0": // Exit
				return;
			case "1": // Inventory Menu
				System.out.println();
				System.out.println();
				//MainMenuInventory();
				break;
			case "2": // Suppliers Menu
				System.out.println();
				System.out.println();
				MainMenuSuppliers();
				break;
			case "3": // Order Menu
				System.out.println();
				System.out.println();
				MainMenuOrder(shop);
				break;
			default:
				System.out.println("Incorrect input!");
				break;
			}
		} while (true);
	}*/

	public static void MainMenuOrder(int shopID) {
		dbBase = DataBase.getDataBase();
		shop=shopID;
		String UserSelect = null;
		do {
			System.out.println("--------------- Welcome to the Main Menu of the Orders ---------------");
			System.out.println();
			System.out.println("0) Exit");
			System.out.println("1) Insert new static order");
			System.out.println("2) Show all orders");
			System.out.println("3) Update order by ID");
			System.out.println("4) Print order by ID");
			UserSelect = sc.nextLine();
			switch (UserSelect) {
			// the menu
			case "0": // Exit
				return;
			case "1": // Insert new order
				System.out.println("Please enter the wanted products and amounts in the next format: 'product name1:manufacturer1:amount1,product name2:manufacturer2:amount2,...'");
				System.out.println("Options:");
				dbBase.printAllProductsInCategory(shop);
				String orderdProducts = sc.nextLine();
				Map<ProvidedProduct,Integer> wantedProducts = dbBase.inserNewOrder(orderdProducts,shop); 
				if(wantedProducts == null){// error in the input
					sc.nextLine();
				}
				else
				{
					System.out.println("Please enter the wanted days for delievery in the next format '1','3','7'");
					System.out.println("Options:");
					printMenuDays();
					String deliveryDays = sc.nextLine();
					ArrayList<Integer> delieveryDays = (ArrayList<Integer>) dbBase.getOrderDaysByInput(deliveryDays);
					ArrayList<Order> listOfOrders = dbBase.CreateListOfOrdersByProducts(wantedProducts,shop, delieveryDays);
					if(listOfOrders != null)
					{
						System.out.println("The order is from "+listOfOrders.size() +" suppliers");
						for (Order order : listOfOrders) 
						{
							if(dbBase.addNewOrder(order))
								System.out.println("Order has added successfully");
							else
								System.out.println("An error occourd, Order didn't added");		
						}	
					}
					else
					{
						System.out.println("Please try other order");
						MainMenuOrder(shopID);
					}
				}
				break;
			case "2": // Show all orders
				System.out.println();
				System.out.println();
				dbBase.printAllOrders(shop);
				break;
			case "3": // Update order
				System.out.println();
				System.out.println();
				System.out.println("Please enter the order Number to update");
				String orderNumber = sc.nextLine();
				if(dbBase.checkIfOrderExistsByOrderNumber(orderNumber,shop))
					UpdateOrderMenu(orderNumber);
				break;
			case "4": //print order
				System.out.println();
				System.out.println();
				System.out.println("Please enter the order number");
				String orderNum = sc.nextLine();
				if(dbBase.checkIfOrderExistsByOrderNumber(orderNum,shop))
					dbBase.printOrder(orderNum,shop);
				break;
			default:
				System.out.println("Incorrect input!");
				break;
			}
		} while (true);

	}

	private static void UpdateOrderMenu(String orderID) 
	{
		String input;
		Order orderToUpdate = dbBase.getOrderByOrderNumber(orderID,shop);
		String UserSelect = null;
		do {
			System.out.println("All products that are provided by the Supplier of the order are:");
			dbBase.printAllCatalogID(orderToUpdate.getSuplier().getID());
			System.out.println("--------------- Welcome to the Update Order Menu ---------------");
			System.out.println("Please enter one of the following:");
			System.out.println();
			System.out.println("Updating Order Number: "+orderID);
			System.out.println();
			System.out.println("0) Exit");
			System.out.println("1) Update amount of product");
			System.out.println("2) Add product to order");
			System.out.println("3) Remove product from order");
			System.out.println("4) Add delivery day");
			System.out.println("5) Remove delivery day");

			UserSelect = sc.nextLine();
			switch (UserSelect) {
			// the menu
			case "0": // Exit
				return;

			case "1": // Update amount of product
				System.out.println
				("Please enter the catalog number of the product to update and amount in the next format 'catalog number,new amount'");
				input = sc.nextLine();
				if(!dbBase.checkValidInputUpdateAmountOfProductInOrder(input))
				{
					System.out.println("invalid input");
					return;
				}
				String catalogNumber = input.split(",")[0];
				Integer amountToUpdate = Integer.parseInt(input.split(",")[1]);
				HashMap<ProvidedProduct, Integer> productsInOrder =  (HashMap<ProvidedProduct, Integer>) orderToUpdate.getProducts();
				for (ProvidedProduct productInOrder :productsInOrder.keySet())
				{
					if(productInOrder.getCatalogNumber().equals(catalogNumber))
					{
						Integer oldAmount = orderToUpdate.getProducts().get(productInOrder);
						orderToUpdate.getProducts().put(productInOrder, amountToUpdate);
						if(dbBase.updateOrder(orderToUpdate))
							System.out.println("amount is updated successfully");
						else
							System.out.println("An error occourd, amount didn't changed");

						if(oldAmount > amountToUpdate)
						{
							if(!orderToUpdate.getSuplier().isProvideTransport())
								MainMenu.dbAccess.RemoveProductFromOrderInDelivery(orderToUpdate);
						}
						else
						{
							dbBase.makeOrderAfterUpdate(productInOrder, shop, amountToUpdate - oldAmount);
						}

						return;
					}
				}
				//catalog number doesn't exists.
				System.out.println("Catalog number doesn't exists.");
				return;

			case "2": // Add product to order
				Supplier s = dbBase.getOrderByOrderNumber(orderID,shop).getSuplier();
				printAllProductBySupplierID(s);
				
				System.out.println("Please enter the catalog number and amount in the next format 'catalog number,amount'");
				input = sc.nextLine();
				if(!dbBase.checkValidInputAddProductInOrder(input))
				{
					System.out.println("invalid input");
					return;
				}
				catalogNumber = input.split(",")[0];
				Integer amount = Integer.parseInt(input.split(",")[1]);
				ProvidedProduct newProductInOrder = dbBase.getProvidedProductByCatalogNumberAndSupplier(catalogNumber, orderToUpdate.getSuplier().getID());
				if(newProductInOrder == null)
					System.out.println("Catalog number doesn't exists.");
				orderToUpdate.getProducts().put(newProductInOrder, amount);
				if(dbBase.updateOrder(orderToUpdate))
					System.out.println("product is added successfully to the order");
				else
					System.out.println("An error occourd, product didn't added");
				dbBase.makeOrderAfterUpdate(newProductInOrder, shop, amount);
				return;


			case "3": // Remove product from order
				System.out.println
				("Please enter the catalog number of product to remove");
				catalogNumber = sc.nextLine();
				ProvidedProduct productToRemove = null;
				for (ProvidedProduct product : orderToUpdate.getProducts().keySet()) 
				{
					if(product.getCatalogNumber().equals(catalogNumber))
						productToRemove = product;
				}
				if(productToRemove == null)
					System.out.println("Catalog number doesn't exists.");
				orderToUpdate.getProducts().remove(productToRemove);
				if(dbBase.updateOrder(orderToUpdate))
					System.out.println("product is removed successfully from the order");
				else
					System.out.println("An error occourd, product didn't removed");
				return;
			case "4": // Add delivery day
				System.out.println
				("Please enter the delivery day you want to add");
				printMenuDays();
				input = sc.nextLine();
				if(!dbBase.checkValidInputDeliveryDays(input))
				{
					System.out.println("invalid input");
					return;
				}
				Integer dayToAdd = Integer.parseInt(input);
				boolean foundDeliveryDay = false;
				for (Integer deliveryDay : orderToUpdate.getDeliveyDays()) {
					if(deliveryDay.equals(dayToAdd))
					{
						foundDeliveryDay = true;
					}
				}
				if(foundDeliveryDay)
				{
					System.out.println("delivery day already exists");
					return;
				}
				orderToUpdate.getDeliveyDays().add(dayToAdd);
				if(dbBase.updateOrder(orderToUpdate))
					System.out.println("delivery day is added successfully");
				else
					System.out.println("An error occourd, delivery day didn't added");

				if(!orderToUpdate.getSuplier().isProvideTransport())
					MainMenu.dbAccess.RemoveProductFromOrderInDelivery(orderToUpdate);


				return;
			case "5": // Remove delivery day
				System.out.println
				("Please enter the delivery day you want to remove");
				printMenuDays();
				input = sc.nextLine();
				if(!dbBase.checkValidInputDeliveryDays(input))
				{
					System.out.println("invalid input");
					return;
				}
				Integer dayToRemove = Integer.parseInt(input);
				foundDeliveryDay = false;
				for (Integer deliveryDay : orderToUpdate.getDeliveyDays()) {
					if(deliveryDay.equals(dayToRemove))
					{
						foundDeliveryDay = true;
					}
				}
				if(foundDeliveryDay)
				{
					orderToUpdate.getDeliveyDays().remove(dayToRemove);
					if(dbBase.updateOrder(orderToUpdate))
						System.out.println("delivery day is removed successfully");
					else
						System.out.println("An error occourd, delivery day didn't removed");
					return;
				}
				System.out.println("delivery day doesn't exists");
				return;
			default:
				System.out.println("Incorrect input!");
				continue;
			}
		} while (true);
	}


	private static void printMenuDays() 
	{
		System.out.println("Sunday - 1");
		System.out.println("Monday - 2");
		System.out.println("Tuesday - 3");
		System.out.println("Wednesday - 4");
		System.out.println("Thursday - 5");
		System.out.println("Friday - 6");
		System.out.println("Saturday - 7");
	}

	/**
	 * the main menu for Inventory
	 */
	public static void MainMenuInventory(int shopID) {
		dbBase = DataBase.getDataBase();
		shop=shopID;
		String UserSelect = null;
		do {
			System.out.println("--------------- Welcome to the Main Menu of the Inventory ---------------");
			System.out.println();
			System.out.println("0) Exit");
			System.out.println("1) Show General inventory report");
			System.out.println("2) Show Categorized inventory report by a specific category");
			System.out.println("3) Show Categorized inventory report by multiple categories");
			System.out.println("4) Show damaged invenotry report");
			System.out.println("5) Show price for product");
			System.out.println("6) Show all product details");
			System.out.println("7) Add product to invenotry by de");
			System.out.println("8) Add discount for product");
			System.out.println("9) Add discount for category");
			System.out.println("10) Add new category");
			System.out.println("11) Add new product details");
			System.out.println("12) Update Damaged/Location of a product by ID");
			System.out.println("13) Update product's sell price");
			System.out.println("14) Update daily consumption of a product");
			System.out.println("15) Remove product");
			System.out.println("16) Remove supplier discount");
			System.out.println("17) Accept Delievery");
			System.out.println("18) Show Shortage");


			UserSelect = sc.nextLine();
			switch (UserSelect) {
			// the menu
			case "0": // Exit
				return;

			case "1": // General report
				dbBase.printGeneralInventoryReport(shop);
				break;
			case "2": // Specific categorized report
				System.out.println("Please enter a category");
				System.out.println("Options:");
				dbBase.printAllCategories();
				if(!dbBase.printSpecificCategorizedReport(sc.nextLine(),shop)){
					System.out.println("Category does'nt exist! enter any key to continue to main menu");
					sc.nextLine();
				}
				break;
			case "3": // Multiple categories report
				System.out.println("Please enter the wanted categories in the next format: 'category1,category2,category3...'");
				System.out.println("Options:");
				dbBase.printAllCategories();
				if(!dbBase.printMultipleCategorizedReport(sc.nextLine(),shop)){
					sc.nextLine();
				}
				break;
			case "4": // Damaged report
				dbBase.printDamagedReport(shop);
				break;
			case "5":// Show price for product
				System.out.println("Please enter product in the next format: 'product name,manufacturer'");
				dbBase.printProductPrice(sc.nextLine());
				break;
			case "6": // show IDs 
				System.out.println("Please enter product in the next format: 'product name,manufacturer'");
				dbBase.printProductWithID(sc.nextLine(),shop);
				break;
			case "7": // Add products 
				System.out.println("Please enter product in the next format: 'product name,manufacturer,expiration date,amount'");
				System.out.println("expiration date format: 'dd/mm/yyyy', if doesn't have just type '-' in the place");
				if(dbBase.addProductsToInventory(sc.nextLine(),shop))
				{
					System.out.println("Products added succesfully");
				}
				else
				{
					System.out.println("An error accourd");
				}

				break;
			case "8": // Add Discount product
				System.out.println("Please enter discount for product in the next format: 'product name,manufacturer,discount,start date,end date'");
				System.out.println("date format: 'dd/mm/yyyy'");
				dbBase.printAllProductsInCategory(shop);
				if(dbBase.addProductDiscount(sc.nextLine()))
				{
					System.out.println("Discount Product added succesfully");
				}
				else
				{
					System.out.println("An error accourd");
				}
				break;
			case "9": // Add Discount category 
				System.out.println("Please enter discount for category in the next format: 'category name,discount,start date,end date'");
				System.out.println("date format: 'dd/mm/yyyy'");
				dbBase.printAllCategories();
				if(dbBase.addCategoryDiscount(sc.nextLine()))
				{
					System.out.println("Discount for category has added succesfully");
				}
				else
				{
					System.out.println("An error accourd");
				}
				break;
			case "10":// Add new category
				System.out.println("Please enter new category name and parent category: 'category name,parent category name'");
				System.out.println("if this category doesn't have parent category enter '-' in parent category name space");
				System.out.println("Options for parent category:");
				dbBase.printAllCategories();
				if(dbBase.addCategory(sc.nextLine()))
				{
					System.out.println("category has added succesfully");
				}
				else
				{
					System.out.println("An error accourd");
				}
				break;
			case "11": // Add new product details
				System.out.println(
						"Please enter new product: 'product name,category name,manufacturer"
								+ ",sellPrice,dailyConsumption,weight'");
				System.out.println("Options for category:");
				dbBase.printAllCategories();
				if(dbBase.addProductInCategory(sc.nextLine(),shop))
				{
					System.out.println("product details has added succesfully");
				}
				else
				{
					System.out.println("An error accourd");
				}
				break;
			case "12": // Update Damaged/Location
				System.out.println("Please enter the ID of the product you want to update its Damage or Location");
				String idInput = sc.nextLine();
				if(!isValidNumber(idInput)){
					System.out.println("invalid input");
					break;
				}
				if(!dbBase.checkIfExistByID(idInput)){
					System.out.println("ID does'nt exist! enter any key to continue to main menu");
					sc.nextLine();
				}
				else{
					updateStatusMenu(idInput);
				}
				break;
			case "13": //Update sell price
				System.out.println("Please enter the name and the manufacturer of the product you want to update its sell price in the next format:");
				System.out.println("'product name','manufacturer'");
				String productInput = sc.nextLine();
				if(!dbBase.checkIfExistByNameAndManufacturer(productInput)){
					System.out.println("Enter any key to continue to main menu");
					sc.nextLine();
				}
				else{
					System.out.println("Please enter the price you want to update");
					String updatePrice = sc.nextLine();
					if(!dbBase.checkIfIsPrice(updatePrice)){
						System.out.println("This is not a price! enter any key to continue to main menu");
						sc.nextLine();
					}
					else{
						if(dbBase.updatePriceByNameAndManufacturer(productInput,Double.parseDouble(updatePrice),true)){
							System.out.println("Successfully updated price!");
						}
						else{
							System.out.println("Enter any key to continue to main menu");
							sc.nextLine();
						}
					}
				}
				break;
			case "14": // Update Daily consumption
				System.out.println("Please enter the product name, manufacturer and daily consumption in the next format:'Product name,Manufacturer,daily consumption'");
				String dailyCons = sc.nextLine();
				if(!dbBase.updateDailyConsumption(dailyCons,shop)){
					System.out.println("An error has occurred. Daily consumption didn't update");
					break;
				}
				System.out.println("Daily consumption has been changed successfully");
				break;
			case "15": // Remove product by ID
				System.out.println("Please enter the products ID that you would like to remove");
				String itemID = sc.nextLine();
				if(!isValidNumber(itemID)){
					System.out.println("invalid input");
					break;
				}
				if(!dbBase.removeProductById(itemID)){
					System.out.println("ID does'nt exist! enter any key to continue to main menu");
					sc.nextLine();
				}
				else{
					System.out.println("Item with ID: "+itemID+ " has been successfully removed");
				}
				break;	
			case "16": // remove supplier discount
				System.out.println(
						"Please enter supplier discount to remove in the next format: 'product name,manufacturer,supplier ID"
								+ ",discount,start date, end date, amount'");
				System.out.println("date format: 'dd/mm/yyyy'");
				if(dbBase.removeSupplierDiscount(sc.nextLine()))
				{
					System.out.println("supplier discount has deleted succesfully");
				}
				else
				{
					System.out.println("An error accourd");
				}
				break;
			case "17": // accept delievery
				System.out.println("Please enter delievery id");
				dbBase.acceptDelievery(sc.nextLine(),shopID);
				break;
			case "18": // show shortage
				System.out.println();
				dbBase.cycleCount(shop);
				System.out.println();
				break;
			default:
				System.out.println("Incorrect input!");
				break;
			}
		} while (true);
	}

	public static void updateStatusMenu(String id){
		String UserSelect = null;
		do {
			System.out.println("--------------- Welcome to the Update Status Menu ---------------");
			System.out.println();
			System.out.println("Updating product: "+id);
			System.out.println();
			System.out.println("0) Exit");
			System.out.println("1) Update damaged field of the product");
			System.out.println("2) Update location field of the product");

			UserSelect = sc.nextLine();
			switch (UserSelect) {
			// the menu
			case "0": // Exit
				return;

			case "1": // Update damaged
				System.out.println("Please enter one of the following:");
				System.out.println();
				System.out.println("0) Exit to Update Status Menu");
				System.out.println("1) Update product to be damaged");
				System.out.println("2) Update product to be not! damaged");
				String val = sc.nextLine();
				String outPutString="";
				if(val.equals("0")){
					continue;
				}
				if(!val.equals("1") && !val.equals("2")){
					System.out.println("Incorrect input! enter any key to return to Update Status Menu");
					sc.nextLine();
					continue;
				}
				if(val.equals("1")){
					outPutString = "damaged";
				}
				else{
					val="0";
					outPutString = "not damaged";
				}
				if(dbBase.updateStatusById(Integer.parseInt(id),true,val)){
					System.out.println("Updated product "+id+" to be "+outPutString);
				}
				else{
					System.out.println("Somthing went wrong please try again.");
					continue;
				}
				break;
			case "2": // Update location
				System.out.println("Please enter one of the following:");
				System.out.println();
				System.out.println("0) Exit to Update Status Menu");
				System.out.println("1) Update product's location to Store");
				System.out.println("2) Update product's location to Warehouse");
				String val2 = sc.nextLine();
				if(val2.equals("0")){
					continue;
				}
				if(!val2.equals("1") && !val2.equals("2")){
					System.out.println("Incorrect input! enter any key to return to Update Status Menu");
					sc.nextLine();
					continue;
				}
				if(val2.equals("1")){
					val2="STORE";
				}
				else{
					val2="WAREHOUSE";
				}
				if(dbBase.updateStatusById(Integer.parseInt(id),false,val2)){
					System.out.println("Updated product "+id+" to the "+val2);
				}
				break;	
			default:
				System.out.println("Incorrect input!");
				continue;
			}
		} while (true);
	}

	public static void MainMenuSuppliers() {
		dbBase = DataBase.getDataBase();
		boolean flag = false; // this flag true if the user enter a worng choice
		String UserSelect = null;
		do {
			System.out.println("--------------- Welcome to the Main Menu of the Suppliers ---------------");
			System.out.println();
			System.out.println("0) Exit");
			System.out.println("1) INSERT new supplier");
			System.out.println("2) GET supplier details by ID");
			System.out.println("3) GET all suppliers");
			System.out.println("4) GET all products of supplier");
			System.out.println("5) ADD supplier Discount");
			System.out.println("6) GET supplier supplies days");
			UserSelect = sc.nextLine();
			switch (UserSelect) {
			// the menu
			case "0": // Exit
				return;

			case "1": // INSERT
				insertMenu();
				break;
			case "2": // GET
				getMenu();
				break;
			case "3": // get all
				getAll();
				break;
			case "4": // get all
				System.out.println("Please enter the supplier ID");
				String IDSupplier = sc.nextLine();
				if(!isValidNumber(IDSupplier)){
					System.out.println("invalid input");
					break;
				}
				if(dbBase.getSupplierById(IDSupplier)==null){
					System.out.println("Supplier "+IDSupplier+" doesnt exist");
					break;
				}
				dbBase.printAllCatalogID(IDSupplier);
				break;
			case"5":
				System.out.println(
						"Please enter new supplier discount: 'product name,manufacturer,supplier id"
								+ ",discount,start date, end date, amount'");
				System.out.println("date format: 'dd/mm/yyyy'");
				//print a list of all supplier in the format 'Supplier Name, Supplier ID'
				dbBase.printAllSuppliers();
				if(dbBase.addSupplierDiscount(sc.nextLine()))
				{
					System.out.println("supplier discount has added succesfully");
				}
				else
				{
					System.out.println("An error accourd");
				}
				break;
			case"6": //print supplier supply days
				System.out.println("Please enter the supplier ID");
				IDSupplier = sc.nextLine();
				if(!isValidNumber(IDSupplier))
				{
					System.out.println("invalid input");
					break;
				}
				for (Integer day : dbBase.getSupplyDays(IDSupplier))
				{
					System.out.println("Day Number "+day);
				}
				
				break;
			default:
				flag = true;
				break;
			}

		} while (flag);
	}

	public static void insertMenu() {
		clearScreen();
		System.out.println("Insert supplier details in format 'ID','name','Bank Account','Term of payment','T/F','Delivery days','address'");
		System.out.println("'T/F' field represents whether the supplier gives transport");
		System.out.println("'Delivery days' field is inputed in format 'day1:day2:...");
		String tmp = sc.nextLine();
		String[] ans = tmp.split(",");
		if (ans.length != 7) { // validate input
			System.out.println("incorrect input.");
			System.out.println("To return to Main Menu type '0'");
			System.out.println("To try again type any key");
			tmp = sc.nextLine();
			if (tmp.equals("0")) {
				MainMenuSuppliers();
				return;
			}
			insertMenu();
			return;
		}
		String[] stringDeliveryDays = ans[5].split(":");
		ArrayList<Integer> deliveryDays = new ArrayList<Integer>();
		for (String string : stringDeliveryDays) {
			deliveryDays.add(Integer.parseInt(string));
		}
		Supplier newSupplier = null;
		try {
			if (ans[4].equals("T")) {
				newSupplier = new Supplier(ans[0], ans[1], ans[2],ans[3], true,ans[6]);
			} else {
				newSupplier = new Supplier(ans[0], ans[1], ans[2],ans[3], false,ans[6]);
			}
		} catch (Exception e) {
		}
		if (!isValidNumber(ans[0]) || !isValidNumber(ans[2])) {
			newSupplier = null;
		}

		if (newSupplier != null) {
			if (dbBase.addSupplier(newSupplier)) { // make sure id dosen't exist
				if(!dbBase.addSupplierDays(new SupplierSupplyDays(ans[0], deliveryDays)))
					System.out.println("Only days where added without the new supplier");
				System.out.println("Supplier added SUCCESSFULLY!");
				System.out.println("Tap any key to continue.");
				sc.nextLine();
				MainMenuSuppliers();
			} else {
				System.out.println("ID of Supplier already exists.");
				System.out.println("Tap any key to try again.");
				sc.nextLine();
				insertMenu();
			}
		} else {
			System.out.println("ID or bank account is't nummeric.");
			System.out.println("Tap any key to try again.");
			sc.nextLine();
			insertMenu();
		}
	}

	public static void getMenu() {
		clearScreen();
		System.out.println("Insert supplier ID");
		String input = sc.nextLine();
		Supplier ans = dbBase.getSupplierById(input);
		if (ans == null) { // validate input
			System.out.println("ID dosen't exist");
			System.out.println("To return to Main Menu type '0'");
			System.out.println("To try again type any key");
			input = sc.nextLine();
			if (input.equals("0")) {
				MainMenuSuppliers();
				return;
			}
			getMenu();
			return;
		}
		System.out.println();
		System.out.println();
		detailsMenu(ans);
	}

	public static void getAll() {
		clearScreen();
		List<Supplier> ans = dbBase.getAllSuppliers();
		for (int i = 0; i < ans.size(); i++) {
			System.out.println(i + 1 + ") " + ans.get(i).getID());
		}
		System.out.println("If you like to get more details or update one of the suppliers please enter his number");
		System.out.println("otherwise enter zero:");
		String input = sc.nextLine();
		int Iinput = -1;
		try {
			Iinput = Integer.parseInt(input);
		} catch (Exception e) {
			System.out.println("input is't nummeric.");
			System.out.println("Tap any key to try again.");
			sc.nextLine();
			getAll();
		}
		if (Iinput > ans.size() + 1 || Iinput < 0) {
			System.out.println("input is't valid.");
			System.out.println("Tap any key to try again.");
			sc.nextLine();
			getAll();
		} else {
			if (Iinput == 0) {
				MainMenuSuppliers();
				return;
			}

			detailsMenu(ans.get(Iinput - 1));
		}
	}

	private static void detailsMenu(Supplier s) {
		System.out.println(s);
		System.out.println("0) Exit");
		System.out.println("1) Update details");
		System.out.println("2) View contacts");
		System.out.println("3) View all products");
		System.out.println("4) View current orders");
		String input = sc.nextLine();
		switch (input) {
		case "0": {
			MainMenuSuppliers();
			return;
		}
		case "1": {
			updatesuplier(s);
		}
		break;
		case "2": {
			allContacts(s);
		}
		break;
		case "3": {
			allProducts(s);
		}
		break;
		case "4": {
			currentOrders(s);
		}
		break;
		default: {
			System.out.println("Invalid Input");
			System.out.println("To try again type any key");
			sc.nextLine();
			detailsMenu(s);
		}
		break;
		}
	}

	private static void currentOrders(Supplier s) {
		System.out.println();
		List<Order> ans = dbBase.getAllOrders(s);
		if (ans.isEmpty()) {
			System.out.println("there is no orders for this suplier enter any key to continue");
			sc.nextLine();
			MainMenuSuppliers();
			return;
		} else {
			int i;
			for (i = 0; i < ans.size(); i++) {
				System.out.println(i + 1 + ") " + ans.get(i).getOrderNumber());
			}
			System.out.println("If you like to get more details of order please enter his number");
			System.out.println("otherwise enter zero:");
			String input = sc.nextLine();
			int Iinput = -1;
			try {
				Iinput = Integer.parseInt(input);
			} catch (Exception e) {
				System.out.println("input is't nummeric.");
				System.out.println("Tap any key to try again.");
				sc.nextLine();
				currentOrders(s);
			}
			if (Iinput > ans.size() || Iinput < 0) {
				System.out.println("input is't valid.");
				System.out.println("Tap any key to try again.");
				sc.nextLine();
				currentOrders(s);
			} else {
				if (Iinput == 0) {
					MainMenuSuppliers();
					return;
				} else {
					Order selected = ans.get(Iinput - 1);
					System.out.println(selected);
					int price = 0;
					for (ProvidedProduct p : selected.getProducts().keySet()) {
						price += Integer.parseInt(p.getCostPrice()) * selected.getProducts().get(p);
					}
					System.out.println("Price before discount " + price);
					System.out.println("Price after discount " + dbBase.orderPrice(selected));
					System.out.println("Tap any key to return to Main Menu.");
					sc.nextLine();
					MainMenuSuppliers();
					return;
				}
			}
		} // there is orders

	}

	private static void printAllProductBySupplierID(Supplier s)
	{
		List<ProvidedProduct> ans = dbBase.getAllProducts(s);
		for (ProvidedProduct providedProduct : ans) 
		{
			System.out.println("Catalog ID : "+ providedProduct.getCatalogNumber()+" Product "
					+ ":"+ providedProduct.getProduct_name() +" "+providedProduct.getManufacturer());
		}
	}
	private static void allProducts(Supplier s) {
		System.out.println();
		List<ProvidedProduct> ans = dbBase.getAllProducts(s);
		if (ans.isEmpty()) {
			System.out.println("there is no products for this supplier if you like to add one enter 1");
		} else {
			int i;
			for (i = 0; i < ans.size(); i++) {
				System.out.println(i + 1 + ") " + ans.get(i));
			}
			System.out.println(i + 1 + ") Add new Proudct");
			System.out.println("If you like to  update the price of the product please enter his number");
		}
		System.out.println("otherwise enter zero:");
		String input = sc.nextLine();
		int Iinput = -1;
		try {
			Iinput = Integer.parseInt(input);
		} catch (Exception e) {
			System.out.println("input is't nummeric.");
			System.out.println("Tap any key to try again.");
			sc.nextLine();
			allProducts(s);
		}
		if (Iinput > ans.size() + 1 || Iinput < 0) {
			System.out.println("input is't valid.");
			System.out.println("Tap any key to try again.");
			sc.nextLine();
			allProducts(s);
		} else {
			if (Iinput == 0) {
				MainMenuSuppliers();
				return;
			} else {
				if (Iinput == ans.size() + 1) {
					System.out.println("Insert product details in format 'name','manufactorer','price','catalog Number','weight'");
					String tmp = sc.nextLine();
					String[] ans2 = tmp.split(",");
					if (ans2.length != 5) { // validate input
						System.out.println("incorrect input.");
						System.out.println("To return to Main Menu type '0'");
						System.out.println("To try again type any key");
						tmp = sc.nextLine();
						if (tmp.equals("0")) {
							MainMenuSuppliers();
							return;
						}
						allProducts(s);
						return;
					}
					ProvidedProduct product = null;
					try {
						Integer.parseInt(ans2[2]);
						product = new ProvidedProduct(ans2[0], ans2[1], ans2[2],s.getID(),ans2[3],Integer.parseInt(ans2[4]));
					} catch (Exception e) {
					}
					if (!isValidNumber(ans2[3])) {
						product = null;
					}
					if (product != null) {
						if (dbBase.addProduct(s, product)) { // make sure
							// Catalog
							// Number
							// dosen't exist
							System.out.println("product added SUCCESSFULLY!");
							System.out.println("Tap any key to continue.");
							sc.nextLine();
							MainMenuSuppliers();
						} else {
							System.out.println("Catalog Number of product already exists.");
							System.out.println("Tap any key to try again.");
							sc.nextLine();
							allProducts(s);
						}
					} else {
						System.out.println("Catalog Number or price is't nummeric.");
						System.out.println("Tap any key to try again.");
						sc.nextLine();
						allProducts(s);
					}
				} // add
				else {// update
					System.out.println();
					System.out.println("Insert the new price");
					input = sc.nextLine();
					if (isValidNumber(input)) {
						if (dbBase.updateProductPrice(s, ans.get(Iinput - 1), input)) {
							System.out.println("price updated SUCCESSFULLY!");
							System.out.println("Tap any key to continue.");
							sc.nextLine();
							MainMenuSuppliers();
						} else {
							System.out.println("Illegal input");
							System.out.println("To return to Main Menu type '0'");
							System.out.println("To try again type any key");
							String input2 = sc.nextLine();
							if (input2.equals("0")) {
								MainMenuSuppliers();
								return;
							}
							allProducts(s);
							return;
						}
					} else {
						System.out.println("Illegal input");
						System.out.println("To return to Main Menu type '0'");
						System.out.println("To try again type any key");
						String input2 = sc.nextLine();
						if (input2.equals("0")) {
							MainMenuSuppliers();
							return;
						}
						allProducts(s);
						return;
					}
				} // update
			} // not exit
		} // valid input
	}

	private static void allContacts(Supplier s) {
		System.out.println();
		List<Contact> ans = s.getContacts();
		if (ans.isEmpty()) {
			System.out.println("there is no contacts if you like to add one enter 1 otherwise enter 0:");
		} else {
			int i;
			for (i = 0; i < ans.size(); i++) {
				System.out.println(i + 1 + ") " + ans.get(i));
			}
			System.out.println(i + 1 + ") Add new contact");
			System.out.println("If you like to  update one of the contacts please enter his number");
			System.out.println("otherwise enter zero:");
		}
		String input = sc.nextLine();
		int Iinput = -1;
		try {
			Iinput = Integer.parseInt(input);
		} catch (Exception e) {
			System.out.println("input is't nummeric.");
			System.out.println("Tap any key to try again.");
			sc.nextLine();
			allContacts(s);
		}
		if (Iinput > ans.size() + 1 || Iinput < 0) {
			System.out.println("input is't valid.");
			System.out.println("Tap any key to try again.");
			sc.nextLine();
			allContacts(s);
		} else {
			if (Iinput == 0) {
				MainMenuSuppliers();
				return;
			} else {
				if (Iinput == ans.size() + 1) {
					System.out.println("Insert cantact details in format 'ID','first name','last name','phone number'");
					String tmp = sc.nextLine();
					String[] ans2 = tmp.split(",");
					if (ans2.length != 4) { // validate input
						System.out.println("incorrect input.");
						System.out.println("To return to Main Menu type '0'");
						System.out.println("To try again type any key");
						tmp = sc.nextLine();
						if (tmp.equals("0")) {
							MainMenuSuppliers();
							return;
						}
						allContacts(s);
						return;
					}
					Contact newcontact = null;
					if (isValidNumber(ans2[0]) && isValidNumber(ans2[3])) {
						newcontact = new Contact(ans2[0], ans2[1], ans2[2], ans2[3]);
					}

					if (newcontact != null) {
						if (dbBase.addContact(s, newcontact)) { // make sure ID
							// dosen't exist
							System.out.println("Contact added SUCCESSFULLY!");
							System.out.println("Tap any key to continue.");
							sc.nextLine();
							MainMenuSuppliers();
						} else {
							System.out.println("ID of contact already exists.");
							System.out.println("Tap any key to try again.");
							sc.nextLine();
							allContacts(s);
						}
					}
				} // add new contact
				else {
					System.out.println();
					System.out.println("Select wanted field to update:");
					System.out.println();
					System.out.println("0) Exit");
					System.out.println("1) ID");
					System.out.println("2) First Name");
					System.out.println("3) Last Name");
					System.out.println("4) Phone Number");
					String fieldName = sc.nextLine();
					boolean chackNumeric = false;
					switch (fieldName) {
					case "0": // Exit
						return;
					case "1": // ID
						fieldName = "ID";
						chackNumeric = true;
						break;
					case "2": // First Name
						fieldName = "FIRSTNAME";
						break;
					case "3": // Last Name
						fieldName = "LASTNAME";
						break;
					case "4": // Phone Number
						fieldName = "PHONE";
						chackNumeric = true;
						break;
					default:
						System.out.println("Illegal input");
						System.out.println("To return to Main Menu type '0'");
						System.out.println("To try again type any key");
						input = sc.nextLine();
						if (input.equals("0")) {
							MainMenuSuppliers();
							return;
						}
						allContacts(s);
						return;
					}
					System.out.println("Insert the new value");
					input = sc.nextLine();
					if (chackNumeric) {
						if (!isValidNumber(input)) {
							System.out.println("Illegal input");
							System.out.println("To return to Main Menu type '0'");
							System.out.println("To try again type any key");
							String input2 = sc.nextLine();
							if (input2.equals("0")) {
								MainMenuSuppliers();
								return;
							}
							updatesuplier(s);
							return;
						}
					}
					if (dbBase.updateContact(s, ans.get(Iinput - 1), fieldName, input)) {
						System.out.println("Contact updated SUCCESSFULLY!");
						System.out.println("Tap any key to continue.");
						sc.nextLine();
						MainMenuSuppliers();
					} else {
						System.out.println("Illegal input");
						System.out.println("To return to Main Menu type '0'");
						System.out.println("To try again type any key");
						String input2 = sc.nextLine();
						if (input2.equals("0")) {
							MainMenuSuppliers();
							return;
						}
						updatesuplier(s);
						return;
					}
				} // update
			} // not exit
		}

	}

	private static void updatesuplier(Supplier s) {
		System.out.println();
		System.out.println("Select wanted field to update:");
		System.out.println();
		System.out.println("0) Exit");
		System.out.println("1) ID");
		System.out.println("2) Name");
		System.out.println("3) Bank Account");
		System.out.println("4) Term Of Payment");
		System.out.println("5) Provide Transport (T/F)");
		String fieldName = sc.nextLine();
		boolean chackNumeric = false;
		switch (fieldName) {
		case "0": // Exit
			MainMenuSuppliers();
			return;

		case "1": // ID
			fieldName = "ID";
			chackNumeric = true;
			break;
		case"2":
			fieldName = "NAME";
			break;
		case "3": // Bank Account
			fieldName = "BankAccount";
			chackNumeric = true;
			break;
		case "4": // Term Of Payment
			fieldName = "TermOfPayment";
			break;
		case "5": // Provide Transport
			fieldName = "ProvideTransport";
			break;
		default:
			System.out.println("illigal input");
			System.out.println("To return to Main Menu type '0'");
			System.out.println("To try again type any key");
			String input = sc.nextLine();
			if (input.equals("0")) {
				MainMenuSuppliers();
				return;
			}
			updatesuplier(s);
			return;
		}
		System.out.println("Insert the new value");
		String input = sc.nextLine();
		if (chackNumeric && !isValidNumber(input)) {
			System.out.println("Illegal input");
			System.out.println("To return to Main Menu type '0'");
			System.out.println("To try again type any key");
			String input2 = sc.nextLine();
			if (input2.equals("0")) {
				MainMenuSuppliers();
				return;
			}
			updatesuplier(s);
			return;
		}
		if (dbBase.updateSupplier(s, fieldName, input)) {
			System.out.println("Supplier updated SUCCESSFULLY!");
			System.out.println("Tap any key to continue.");
			sc.nextLine();
			MainMenuSuppliers();
		} else {
			System.out.println("Illegal input");
			System.out.println("To return to Main Menu type '0'");
			System.out.println("To try again type any key");
			String input2 = sc.nextLine();
			if (input2.equals("0")) {
				MainMenuSuppliers();
				return;
			}
			updatesuplier(s);
			return;
		}

	}

	private static boolean isValidNumber(String s) {
		try {
			int num = Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
