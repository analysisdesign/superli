package app.main;
import DataAccessLayer.DBAccess;
import Deliveries.Layers.PLI;
import Employees.BusinessLayer.InputTesting;
import Employees.PresentationLayer.Menu;
import dal.DataBase;
import modules.Order;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.*;

import Adding.AlexFile;

import static java.time.temporal.TemporalAdjusters.next;

public class MainMenu
{
    public static DBAccess dbAccess=new DBAccess();
    private static int shop;
    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args)
    {
    	//AlexFile init = new AlexFile();
    	boolean toContinue = true;
    	PLI p = new PLI(dbAccess);
    	
        while(toContinue)
        {
            System.out.println("Press 1 For Employees");
            System.out.println("Press 2 For Deliveries");
            System.out.println("Press 3 For Inventory And Orders");
            System.out.println("Press 4 For Suppliers");
            System.out.println("Press 5 For Exit");
            System.out.println("Press 6 For Update The System(Only On Sunday Mornings)");
            int opt=getOptionFromUser(6);
            switch (opt){
            
            case 1:
            	/*Menu m=new Menu(dbAccess);
                m.runMenu();*/
            	break;
            case 2:
                p.runMenu();
            	break;
            case 3:
            	chooseShop();
            	break;
            case 4:
            	MainSuppliers.MainMenuSuppliers();
            	break;
            case 5:
            	toContinue = false;
            	break;
            case 6:
                 updateSystem(dbAccess);
                 break;
            }
        }
        dbAccess.closeConnection();
    }
       
    private static void chooseShop() {
    	DataBase dbBase = DataBase.getDataBase();
    	String UserSelect = null;
		String wantedSuperLi = null;
		do {
			System.out.println("--------------- Welcome to the Super-li Menu ---------------");
			System.out.println();
			System.out.println("0) Exit");
			System.out.println("1) Choose a Super-li by ID");
			System.out.println("2) Show all Super-li's IDs");
			System.out.println("3) Insert new Shop");
			System.out.println("4) Delete a Shop by ID");
			UserSelect = sc.nextLine();
			switch (UserSelect) {
			// the menu
			case "0": // Exit
				return;
			case "1": 
				System.out.println("Please enter the wanted Super-Li's ID");
				wantedSuperLi=sc.nextLine();
				if(dbBase.validWantedSuperLi(wantedSuperLi)){
					shop=Integer.parseInt(wantedSuperLi);
					System.out.println();
					System.out.println();
					MainMenuAfterChoosing();
				}
				break;
			case "2": 
				dbBase.showAllSuperLis();
				break;
			case "3":
				System.out.println("Please enter the new Super-Li in the next format: 'Address,Phone,Contact'");
				dbBase.insertNewShop(sc.nextLine());
				break;
			case "4":
				System.out.println("Please enter the ID of Super-Li you want to delete");
				dbBase.deleteShopById(sc.nextLine());
				break;
			default:
				System.out.println("Incorrect input!");
				break;
			}
		} while (true);
	}
    
    public static void MainMenuAfterChoosing(){
		String UserSelect = null;
		do {
			System.out.println("--------------- Welcome to the Main Menu ---------------");
			System.out.println();
			System.out.println("0) Exit");
			System.out.println("1) Inventory Menu");
			System.out.println("2) Orders Menu");
			UserSelect = sc.nextLine();
			switch (UserSelect) {
			// the menu
			case "0": // Exit
				return;
			case "1": // Inventory Menu
				System.out.println();
				System.out.println();
				MainSuppliers.MainMenuInventory(shop);
				break;
			case "2": // Order Menu
				System.out.println();
				System.out.println();
				MainSuppliers.MainMenuOrder(shop);
				break;
			default:
				System.out.println("Incorrect input!");
				break;
			}
		} while (true);
	}

	public static int getOptionFromUser(int upperBound)
    {
        String optionStr;
        int optionNum;
        Scanner in=new Scanner(System.in);
        while(true)
        {
            optionStr=in.nextLine();
            if(InputTesting.isValidNumber(optionStr))
            {
                optionNum=Integer.parseInt(optionStr);
                if((optionNum>=1 && optionNum<=upperBound))
                {
                    return optionNum;
                }
                else
                {
                    System.out.println("-You Have Entered An Invalid Number, Please Enter A Number :");
                }
            }
            else
            {
                System.out.println("-You Haven't Entered A Number, Please Enter A Number :");
            }
        }
    }

    public static void updateSystem(DBAccess dbAccess)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        List<Integer> shops=dbAccess.getAllShops();
        for(int k=0;k<shops.size();k++)
        {
        	List<Order> orders= DataBase.getDataBase().getAllPermanentOrderThatNeedDelievery(shops.get(k));/*Yaniv Function*/
        	for(int i=0;i<orders.size();i++)
        	{
        		for(int j=0;j<orders.get(i).getDeliveyDays().size();j++)
        		{
        			Calendar c = Calendar.getInstance();
        			Date today=c.getTime();
        			c.add(Calendar.DATE,orders.get(i).getDeliveyDays().get(j)-1);
        			if(PLI.helpAllOrders(orders.get(i),dateFormat.format(c.getTime()),dbAccess)==null)
        			{
        				dbAccess.checkDriverShiftAndTruck(orders.get(i),orders.get(i).getShopID(),dateFormat.format(c.getTime()));
        				dbAccess.addEmptyDeliveryWithOrderAndDate(orders.get(i),dateFormat.format(c.getTime()));
        			}
        		}
        	}
        }
    }
}
