package reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dal.DbConnectionSingelton;
import dal.ProductInCategoryTable;
import modules.Category;
import modules.Order;
import modules.ProductInCategory;
import modules.ProvidedProduct;
import modules.Supplier;

public class ShortageReport implements Report {

	
	private Map<ProductInCategory,Integer> shortageProducts;
	
	public ShortageReport(Map<ProductInCategory, Integer> shortageProducts) {
		super();
		this.shortageProducts = shortageProducts;
	}
	public ShortageReport() {
		super();
		this.shortageProducts = new HashMap<ProductInCategory, Integer>();
	}
	public Map<ProductInCategory, Integer> getShortageProducts() {
		return shortageProducts;
	}
	public void setShortageProducts(Map<ProductInCategory, Integer> shortageProducts) {
		this.shortageProducts = shortageProducts;
	}
	public void printReport() 
	{
		System.out.println("---- Shortage Report ----");
		for (ProductInCategory pp : shortageProducts.keySet()) 
		{
			System.out.println("Name : "+pp.getProduct() +" Manufacturer : "+pp.getManufacturer() +" "
					+ "Current Amount :"+ shortageProducts.get(pp) +" Minimal Amount : "+pp.getMinimalAmount());
		}
	}
	/*public Map<Category,Integer> shortageByCategory(int shopID)
	{
		Map<Category,Integer> ans = new HashMap<Category, Integer>();
		int minimalAmount = 0;
		int numOfProduct = 0;
		String productName = null;
		String manufacturer = null;
		ArrayList<Category> allCategories = CategoryTable.getAllCategories();
		for (Category cat : allCategories) 
		{
			ans.put(cat, 0);
		}
		try
		{
			Statement stmt  = DbConnectionSingelton.getInstance().createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT NAME,MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS WHERE SHOP_ID = '"+shopID+"' "
					+ " GROUP BY NAME,MANUFACTURER");
			while (rs.next()) {
				numOfProduct =  rs.getInt("NUMBER_OF_PRODUCTS");
				productName = rs.getString("NAME");
				manufacturer = rs.getString("MANUFACTURER");
				ProductInCategory pic = ProductInCategoryTable.getProductInCategoryByNameManufacturerAndShopID(productName, manufacturer, shopID);
				Category cat = CategoryTable.getCategoryByID(pic.getCategory());
				minimalAmount = pic.getMinimalAmount();
				if(numOfProduct < minimalAmount)
				{
					int oldShortahe = ans.get(cat);
					int newShortage = oldShortahe + minimalAmount - numOfProduct;
					ans.put(cat, newShortage );
				}
			}
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
	}
*/
	public void shortageByNameAndManufacturer(String productName,String manufaturer, int shopID)
	{
		int minimalAmount = 0;
		int numOfProduct = 0;
		String manufacturer = null;
		try
		{
			Statement stmt  = DbConnectionSingelton.getInstance().createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT NAME,MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP WHERE NAME = '"+productName+"' AND "
					+ "MANUFACTURER = '"+ manufacturer+"' SHOP_ID = '"+shopID+"' "
					+ " GROUP BY NAME, MANUFACTURER");
			while (rs.next()) {
				numOfProduct =  rs.getInt("NUMBER_OF_PRODUCTS");
				ProductInCategory pic = ProductInCategoryTable.getProductInCategoryByNameManufacturerAndShopID(productName, manufacturer, shopID);
				minimalAmount = pic.getMinimalAmount();
				if(numOfProduct < minimalAmount)
				{
					shortageProducts.put(pic, minimalAmount - numOfProduct);
				}
			}
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	
	public void shortageForShop(int shopID)
	{
		int minimalAmount = 0;
		int numOfProduct = 0;
		String productName = null;
		String manufacturer = null;
		try
		{
			Statement stmt  = DbConnectionSingelton.getInstance().createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT NAME,MANUFACTURER,COUNT(*) AS NUMBER_OF_PRODUCTS "
					+ " FROM PRODUCTS_IN_SHOP WHERE SHOP_ID = '"+shopID+"' "
					+ " GROUP BY NAME, MANUFACTURER");
			while (rs.next()) {
				numOfProduct =  rs.getInt("NUMBER_OF_PRODUCTS");
				productName = rs.getString("NAME");
				manufacturer = rs.getString("MANUFACTURER");
				ProductInCategory pic = ProductInCategoryTable.getProductInCategoryByNameManufacturerAndShopID(productName, manufacturer, shopID);
				minimalAmount = pic.getMinimalAmount();
				if(numOfProduct < minimalAmount)
				{
					shortageProducts.put(pic, minimalAmount - numOfProduct);
				}
			}
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
	}
}
