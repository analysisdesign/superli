package reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import dal.DataBase;
import dal.DbConnectionSingelton;
import dal.ProductDiscountTable;
import modules.Product;
import modules.ProductInCategory;

public class ExpiredReport implements Report {
 
	private ArrayList<Product> expiredProducts;
	
	
	public ExpiredReport()
	{
		this.expiredProducts = new ArrayList<Product>();
	}
	public ArrayList<Product> getExpiredProducts() {
		return expiredProducts;
	}
	public void setExpiredProducts(ArrayList<Product> expiredProducts) {
		this.expiredProducts = expiredProducts;
	}
	
	public ExpiredReport(ArrayList<Product> expiredProducts) {
		super();
		this.expiredProducts = expiredProducts;
	}
	@Override
	public void printReport() {
		System.out.println("Expired Date Report");
		for (Product pp : expiredProducts) 
		{
			System.out.println("Name : "+pp.getName() +" Manufacturer : "+pp.getManufacturer() +" "
					+ "Expired Date :"+ pp.getExpDate() +" Location : "+pp.getLocation());
		}

	}
	public void expiredProducts(int shopID)
	{
		try {
			Statement stmt  = DbConnectionSingelton.getInstance().createStatement();
			ResultSet rs  = stmt.executeQuery("SELECT * "
					+ " FROM PRODUCTS"
					+ " WHERE SHOP_ID = '"+shopID+"'");

			// loop through the result set
			while (rs.next()) {
				String expDate = rs.getString("EXP_DATE");
				String productName = rs.getString("NAME");
				String manufacturer = rs.getString("MANUFACTURER");
				int productID = rs.getInt("ID");
				boolean demaged = rs.getBoolean("DEMAGED");
				String location = rs.getString("LOCATION");

				Product p = new Product(productID,productName, manufacturer,expDate, demaged, location);
				if(IsExpired(expDate))
				{
					expiredProducts.add(p);
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}	
	}
	private boolean IsExpired(String expDate) 
	{
		String todayDate = DataBase.getTodayDateFormat();
		int ans = ProductDiscountTable.compareBetweenDates(expDate, todayDate);
		return ans == 1;
	}

}
