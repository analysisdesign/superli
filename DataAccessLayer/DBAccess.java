package DataAccessLayer;
import Deliveries.Entities.*;
import Deliveries.Entities.Driver;
import Deliveries.Layers.PLI;
import Employees.BusinessLayer.Constraint;
import Employees.BusinessLayer.Employee;
import Employees.BusinessLayer.Role;
import Employees.BusinessLayer.Shift;
import dal.DataBase;
import dal.DbConnectionSingelton;
import modules.Contact;
import modules.Order;
import modules.ProvidedProduct;
import modules.Shop;
import modules.Supplier;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class DBAccess
{
    Connection c;
    public DBAccess()
    {
    	c=DbConnectionSingelton.getInstance();
    	/*
        try
        {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:DataBase.db");
            c.setAutoCommit(false);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }*/
    }
    public boolean addConstraints(int ID, Constraint constraint)
    {
        return executeCommand("INSERT INTO Constraints (ID,Date,DayPart) VALUES ("+ID+",'"+constraint.getDate()+"','"+constraint.getDayPart()+"')");
    }
    public boolean checkIfEmpInShift(String id ,Shift shift)
    {
        Statement stmt = null;
        boolean b=false;
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "Select Employees.ID From  Shifts  Where Shifts.Date='"+shift.getDate()+"' AND Shifts.DayPart='"+shift.getDayPart()+"';");
            if (rs.next())
            {
                b= true;
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return b;
        }
        return b;
    }
    public LinkedList<String> getRoleByID(String ID)
    {
        Statement stmt = null;
        LinkedList<String> roles=new LinkedList<>();
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT Role FROM EmployeeRoles WHERE ID="+ID+";");
            while (rs.next())
            {
                roles.add(rs.getString("Role"));
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return null;
        }
        return roles;
    }
    public boolean addRoleToID(String ID, Role Role)
    {
        return executeCommand("INSERT INTO EmployeeRoles (ID,Role) VALUES ("+ID+",'"+Role.getRole()+"')");
    }
    public boolean addEmployee(Employee emp)
    {
        String sql = "INSERT INTO Employees (ID,FirstName,LastName,Bank,Branch,AccountNumber,MonthlyPayment,StartingDate,SiteID) " +
                "VALUES ("+emp.getId()+",'"+emp.getFirstName()+"','"+emp.getLastName()+"','"+emp.getBank()+"','"+emp.getBranch()+"',"+emp.getAccountNumber()
                +","+emp.getMonthlypayment()+",'"+emp.getStartingDate()+"',"+emp.getSiteID()+")";
        return executeCommand(sql);
    }

    public boolean RehireEmployee(String id)
    {
        return executeCommand("UPDATE EMPLOYEES set STATUS ='" +true +"' where ID='"+id+"';");
    }
    public boolean FireEmployee(String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set STATUS ='" +false +"' where ID='"+ID+"';");
    }
    public boolean checkIfEmployeeExists(String ID)
    {
        Statement stmt = null;
        boolean b=false;
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Employees WHERE ID="+ID+";");
            if (rs.next())
            {
                b=true;
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return b;
        }
        return b;
    }

    public boolean getStatus(String ID)
    {
        Statement stmt = null;
        boolean toReturn=false;
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT Status FROM Employees WHERE ID="+ID+";");
            if (rs.next())
            {
                toReturn=Boolean.parseBoolean(rs.getString("Status"));
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return false;
        }
        return toReturn;
    }

    public boolean updateFirstName(String name,String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set FirstName ='" +name +"' where ID='"+ID+"';");
    }


    public boolean updateLastName(String name,String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set LastName ='" +name +"' where ID='"+ID+"';");
    }

    public boolean updateBank(String bank,String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set Bank ='" +bank +"' where ID='"+ID+"';");
    }

    public boolean updateBranch(int branch,String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set Branch ='" +branch +"' where ID='"+ID+"';");
    }

    public boolean updateAccountNumber(int number,String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set AccountNumber =" +number +" where ID="+ID+";");
    }

    public boolean updateMonthlyPayment(double number,String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set MonthlyPayment ='" +number +"' where ID='"+ID+"';");
    }

    public boolean updateSiteID(int siteID,String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set SiteID =" +siteID +" where ID='"+ID+"';");
    }

    public boolean updateStatus(String status,String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set Status ='" +status +"' where ID='"+ID+"';");
    }
    public boolean updateEmploymentTerms(String terms,String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set EmploymentTerms ='" +terms +"' where ID='"+ID+"';");
    }

    public LinkedList getEmployeeDetails(String ID)
    {
        Statement stmt = null;
        LinkedList<String> toReturn=new LinkedList<String>();
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "Select FirstName,LastName,Bank,Branch,AccountNumber,MonthlyPayment,StartingDate,Status,EmploymentTerms,Role,SiteID,LeavingDate From Employees JOIN EmployeeRoles ON Employees.ID=EmployeeRoles.ID Where Employees.ID="+ID+";");
            if(rs.next())
            {
                toReturn.add(""+ID);
                toReturn.add(rs.getString("FirstName"));
                toReturn.add(rs.getString("LastName"));
                toReturn.add(rs.getString("Bank"));
                toReturn.add(""+rs.getInt("Branch"));
                toReturn.add(""+rs.getInt("AccountNumber"));
                toReturn.add(""+rs.getDouble("MonthlyPayment"));
                toReturn.add(rs.getString("StartingDate"));
                toReturn.add(""+rs.getString("Status"));
                if(toReturn.get(8).equals("false"))
                {
                    toReturn.add(""+rs.getString("LeavingDate"));
                }
                toReturn.add(rs.getString("EmploymentTerms"));
                toReturn.add(rs.getString("SiteID"));
                toReturn.add(rs.getString("Role"));
            }
            while (rs.next())
            {
                toReturn.add(rs.getString("Role"));
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return null;
        }
        return toReturn;
    }
    public LinkedList<String> getOptionalRoles()
    {
        Statement stmt = null;
        LinkedList<String> roles=new LinkedList<>();
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT Role FROM Roles;");
            while (rs.next())
            {
                roles.add(rs.getString("Role"));
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return null;
        }
        return roles;
    }

    public boolean addRole(Role Role)
    {
        return executeCommand("INSERT INTO Roles (Role) VALUES ('"+Role.getRole()+"')");
    }
    public LinkedList getEmployeesForShift(Shift shift, Role Role,int HumanID)
    {
        LinkedList toReturn = new LinkedList<>();
        Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            String query="Select Employees.ID,FirstName,LastName FROM Employees,EmployeeRoles,Constraints Where Employees.ID=Constraints.ID AND Employees.ID=EmployeeRoles.ID AND Employees.SiteID="+getEmployeeSiteID(HumanID)+" AND Employees.Status='"+true+"' AND Constraints.Date='"+shift.getDate()+"' AND Constraints.DayPart='"+shift.getDayPart()+"' AND EmployeeRoles.Role='"+Role.getRole()+
                    "' EXCEPT Select Employees.ID,FirstName,LastName FROM Employees JOIN Shifts ON Employees.ID=Shifts.ID WHERE Shifts.Date='"+shift.getDate()+"' AND Shifts.DayPart='"+shift.getDayPart()+"'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next())
            {
                toReturn.add(rs.getString("ID"));
                toReturn.add(rs.getString("FirstName"));
                toReturn.add(rs.getString("LastName"));
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return null;
        }
        return toReturn;
    }
    private int getEmployeeSiteID(int ID)
    {
        Statement stmt = null;
        int ans=0;
        try
        {
            stmt = c.createStatement();
            String query="Select SiteID From Employees WHERE ID="+ID;
            ResultSet rs = stmt.executeQuery(query);
            if (rs.next())
            {
                ans=rs.getInt("SiteID");
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return 0;
        }
        return ans;
    }
    public boolean AssignEmployeeToShift(String ID,Shift shift,Role Role)
    {
        return executeCommand("INSERT INTO Shifts (ID,Date,DayPart,Role) VALUES ("+ID+",'"+shift.getDate()+"','"+shift.getDayPart()+"','"+Role.getRole()+"')");
    }

    public LinkedList getShift(Shift shift)
    {
        Statement stmt = null;
        LinkedList toReturn=new LinkedList();
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "Select Employees.ID,FirstName,LastName,Role From Employees JOIN Shifts ON Employees.ID=Shifts.ID Where Shifts.Date='"+shift.getDate()+"' AND Shifts.DayPart='"+shift.getDayPart()+"';");
            while (rs.next())
            {
                toReturn.add(rs.getString("ID"));
                toReturn.add(rs.getString("FirstName"));
                toReturn.add(rs.getString("LastName"));
                toReturn.add(rs.getString("Role"));
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return null;
        }
        return toReturn;
    }
    public boolean insertStandard(String Part, Role Role,int count)
    {
        return executeCommand("INSERT INTO ShiftsStandards (DayPart,Role,Count) VALUES ('"+Part+"','"+Role.getRole()+"',"+count+")");
    }

    public boolean updateStandard(String Part,Role Role,int count)
    {
        return  executeCommand("UPDATE ShiftsStandards set Count ='" +count +"' where DayPart='"+Part+"' AND Role='"+Role.getRole()+"';");
    }
    public boolean eraseStandard(String Part,Role Role)
    {
        return  executeCommand("DELETE FROM ShiftsStandards where DayPart='"+Part+"' AND Role='"+Role.getRole()+"';");
    }

    public LinkedList<String> getOptionalRolesToAdd(String ID)
    {
        Statement stmt = null;
        LinkedList<String> roles=new LinkedList<>();
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT Role FROM Roles EXCEPT SELECT Role From EmployeeRoles WHERE ID="+ID+";");
            while (rs.next())
            {
                roles.add(rs.getString("Role"));
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return null;
        }
        return roles;
    }
    public LinkedList getStandards(String Part)
    {
        Statement stmt = null;
        LinkedList toReturn=new LinkedList();
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "Select Role,Count From ShiftsStandards Where ShiftsStandards.DayPart='"+Part+"';");
            while (rs.next())
            {
                toReturn.add(rs.getString("Role"));
                toReturn.add(rs.getString("Count"));
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return toReturn;
        }
        return toReturn;
    }
    public boolean executeCommand(String sqlQuery)
    {
        Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            stmt.executeUpdate(sqlQuery);
            stmt.close();
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }
    public boolean checkExistingRole(String Role)
    {
        Statement stmt = null;
        boolean b=false;
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Roles WHERE Role='"+Role+"';");
            if (rs.next())
            {
                b=true;
            }
            rs.close();
            stmt.close();
        }
        catch ( Exception e )
        {
            return b;
        }
        return b;
    }
    public boolean updateLeavingDate(String date,String ID)
    {
        return executeCommand("UPDATE EMPLOYEES set LeavingDate ='" +date +"' where ID='"+ID+"';");
    }

    public boolean updateDeliveryDate(long newDate, int id)
    {
        return executeCommand("UPDATE Deliveries set Date_Hour ='" +newDate +"' where ID='"+id+"';");
    }
    public boolean updateDeliveryDriver(int newDriverId, int id)
    {
        return executeCommand("UPDATE Deliveries set Driver_ID ='" +newDriverId +"' where ID='"+id+"';");
    }
    public boolean updateDeliveryTruck(int newTruckId, int id)
    {
        return executeCommand("UPDATE Deliveries set Truck_ID ='" +newTruckId +"' where ID='"+id+"';");
    }
    public Delivery getDelivery(int id)
    {
        Delivery toReturn = null;
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try
        {
            Statement lclStmt = c.createStatement();
            ResultSet rs = lclStmt.executeQuery( "SELECT * FROM Deliveries WHERE ID="+id+";");
            while (rs.next())
            {
                int ID = rs.getInt("ID");
                String  date = rs.getString("Date");
                int drv_id = rs.getInt("Driver_ID");
                Driver drv = getDriver(drv_id);
                int trck_id = rs.getInt("Truck_ID");
                Truck trk = getTruck(trck_id);
                int site_id = rs.getInt("Site_ID");
                Shop site = getShop(site_id);
                int weight = rs.getInt("Weight");
                String time = rs.getString("Time");
                List<SiteDoc> siteDocs = getSiteDocsPerDelivery(ID);

                toReturn = new Delivery(dateFormat.parse(date), drv, trk,weight, site,siteDocs, time);
                toReturn.set_deliveryId(ID);
            }
            rs.close();
            lclStmt.close();
        }
        catch (Exception e)
        {
            System.out.println("SiteDoc return with error..");
            return toReturn;
        }
        return toReturn;
    }
    
    public List<SiteDoc> getSiteDocsPerDelivery(int deliveryID)
    {
    	List<SiteDoc> siteDocs=new LinkedList<SiteDoc>();
    	Statement stmt;
         try
         {
             stmt = c.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM SiteDocs WHERE Delivery_ID="+deliveryID);
             while(rs.next())
             {
            	 siteDocs.add(new SiteDoc(deliveryID, getOrder(rs.getInt("ID"),rs.getInt("OrderID")), ""));
             }
             stmt.close();
             rs.close();
         }
         catch(Exception e){}
         return siteDocs;
    	
    }
    public Order getOrderForSiteDoc(int siteDocId,int shopID)
    {
        Order toReturn = null;
        try
        {
            Statement lclStmt = c.createStatement();
            ResultSet rs = lclStmt.executeQuery( "SELECT * FROM SiteDocToProducts WHERE Site_Doc_ID="+siteDocId+";");
            Map<ProvidedProduct,Integer> mp=new HashMap<>();
            while (rs.next())
            {
                int prodId = rs.getInt("SupplierID");
                ProvidedProduct prodToAdd =getProduct(rs.getInt("CatalogID"), rs.getInt("SupplierID"));
                int  quan = rs.getInt("Quantity");
                mp.put(prodToAdd, quan);
            }
            toReturn=new Order(mp,"",null,null,0);
            rs.close();
            lclStmt.close();
        }
        catch (Exception e)
        {
            System.out.println("List Of Pairs of Products and quantity return with error..");
            return toReturn;
        }
        return toReturn;
    }

    public List<Product> getAllProducts()
    {
        Statement stmt = null;
        List<Product> toReturn = new ArrayList<Product>();
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Products;");
            while (rs.next())
            {
                int id = rs.getInt("ID");
                int  weight = rs.getInt("Weight");
                toReturn.add(new Product(id, weight));
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e) {
            System.out.println("List Of Product return with error..");
            return toReturn;
        }
        return toReturn;
    }
    public boolean updateSiteDocDest(int siteDocId, int newDestId)
    {
        return executeCommand("UPDATE SiteDocs set Dest_Site_ID ='" +newDestId +"' where ID='"+siteDocId+"';");
    }
    public boolean deleteSiteDoc(int siteDocId)
    {
        String sql = "DELETE FROM SiteDocs WHERE ID = ?";
        try (PreparedStatement pstmt = c.prepareStatement(sql)) {
            // set the corresponding param
            pstmt.setInt(1, siteDocId);
            // execute the delete statement
            pstmt.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }


    public LinkedList getEmployeesForStockKeeper(int siteDoc, java.util.Date date)
    {
        LinkedList ls=new LinkedList();
        String shift="Evening";
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int hours = cal.get(Calendar.HOUR_OF_DAY);
        if(hours>=8 && hours<=16)
        {
            shift="Morning";
        }
        try
        {
            Statement lclStmt = c.createStatement();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String sql="SELECT Employees.ID,FirstName,LastName FROM Employees JOIN Shifts ON Shifts.ID=Employees.ID WHERE Employees.SiteID="+siteDoc+" AND Shifts.Date='"+sdf.format(date)+"' AND Shifts.DayPart='"+shift+"' AND Shifts.Role='Stock Keeper' AND Status='true'";
            ResultSet rs = lclStmt.executeQuery( sql);
            while(rs.next())
            {
                ls.add(rs.getInt("ID"));
                ls.add(rs.getString("FirstName"));
                ls.add(rs.getString("LastName"));
            }
            rs.close();
            lclStmt.close();
        }
        catch (Exception e)
        {
            return ls;
        }
        return ls;
    }

    public boolean update_quantity_of_Product_of_SiteDoc(int siteDocId, int productId, int newQuan)
    {
        return executeCommand("UPDATE SiteDocToProducts set Quantity ='" +newQuan +"' where Site_Doc_ID = '"+siteDocId+"' AND Product_ID = '"+ productId +"';");
    }
    public List<Site> getAllSites()
    {
        List<Site> toReturn = new ArrayList<Site>();
        Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Sites;");
            while (rs.next())
            {
                int id = rs.getInt("ID");
                String adrs = rs.getString("Address");
                int num = rs.getInt("Phone");
                String name = rs.getString("Name");

                String region = rs.getString("Region");
                Region enumType = Region.valueOf(region);
                String type = rs.getString("Type");
                toReturn.add(new Site(id, adrs, num, name, enumType, type));
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("List Of Sites return with error..");
            return toReturn;
        }
        return toReturn;
    }

    public Date getDateOfDelivery(int delID)
    {
        Statement stmt = null;
        Date date=null;
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT Date_Hour FROM Deliveries WHERE Deliveries.ID="+delID+";");
            if (rs.next())
            {
                date=new Date(rs.getLong("Date_Hour"));
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("List Of Sites return with error..");
            return date;
        }
        return date;
    }
    public Site getSite(int id)
    {
        Site toReturn = null;
        Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Sites WHERE ID="+id+";");
            while (rs.next())
            {
                int ID = rs.getInt("ID");
                String adrs = rs.getString("Address");
                int num = rs.getInt("Phone");
                String name = rs.getString("Name");

                String region = rs.getString("Region");
                Region enumType = null;
                if(region.equals("NORTH"))
                    enumType = Region.NORTH;
                else if(region.equals("SOUTH"))
                    enumType = Region.SOUTH;
                else if(region.equals("CENTER"))
                    enumType = Region.CENTER;
                else if(region.equals("EAST"))
                    enumType = Region.EAST;
                else if(region.equals("WEST"))
                    enumType = Region.WEST;

                String type = rs.getString("Type");
                toReturn = new Site(ID, adrs, num, name, enumType, type);
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("Site return with error..");
            return toReturn;
        }
        return toReturn;
    }
    public List<Truck> getAllTrucks(Date date)
    {
        Statement stmt = null;
        List<Truck> toReturn = new ArrayList<Truck>();
        try
        {
            stmt = c.createStatement();
            String sql;
            if(date!=null)
            {
                SimpleDateFormat fromUser = new SimpleDateFormat("dd/MM/yyyy");
                long dateStart= PLI.makeDate( fromUser.format(date),"00:00").getTime();
                long dateFinish= PLI.makeDate( fromUser.format(date),"23:59").getTime();
                sql="SELECT Trucks.ID,Model,License_Type,Net_Weight,Max_Weight FROM Trucks Where LeavingDate IS NULL EXCEPT SELECT Trucks.ID,Model,License_Type,Net_Weight,Max_Weight FROM Trucks,Deliveries,SiteDocs,DeliveryToSiteDocs WHERE LeavingDate IS NULL AND Deliveries.ID=DeliveryToSiteDocs.Delivery_ID AND DeliveryToSiteDocs.Site_Doc_ID=SiteDocs.ID AND Trucks.ID=Deliveries.Truck_ID AND Deliveries.Date_Hour>"+dateStart+" AND Deliveries.Date_Hour<"+dateFinish+";";
            }
            else
            {
                sql="SELECT * FROM Trucks WHERE LeavingDate IS NULL ;";
            }
            ResultSet rs = stmt.executeQuery(sql);
            int id;
            while (rs.next())
            {
                if(date!=null)
                {
                    id = rs.getInt("Trucks.ID");
                }
                else
                {
                    id = rs.getInt("ID");
                }
                String mod = rs.getString("Model");
                String type = rs.getString("License_Type");
                LicenseType enumType =LicenseType.valueOf(type);
                int net = rs.getInt("Net_Weight");
                int max = rs.getInt("Max_Weight");
                toReturn.add(new Truck(id, mod, enumType, net, max));
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("List Of Trucks return with error..");
            return toReturn;
        }
        return toReturn;
    }
    public Truck getTruck(int ID)
    {
        Statement stmt = null;
        Truck toReturn = null;
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Trucks WHERE ID="+ID+";");
            while (rs.next())
            {
                int id = rs.getInt("ID");
                String mod = rs.getString("Model");
                String type = rs.getString("License_Type");
                LicenseType enumType = LicenseType.valueOf(type);
                int net = rs.getInt("Net_Weight");
                int max = rs.getInt("Max_Weight");
                toReturn = new Truck(id, mod, enumType, net, max);
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("Truck return with error..");
            return toReturn;
        }
        return toReturn;
    }
    public boolean updateTruckModel(int id ,String newModel)
    {
        return executeCommand("UPDATE Trucks set Model ='" +newModel +"' where ID='"+id+"';");
    }
    public boolean updateTruckLicense(int id ,LicenseType lice)
    {
        return executeCommand("UPDATE Trucks set License_Type ='" +lice.name() +"' where ID='"+id+"';");
    }
    public boolean deleteSiteDocToDelivery(int siteDoc)
    {
        return executeCommand("DELETE FROM DeliveryToSiteDocs WHERE Site_Doc_ID="+siteDoc);
    }
    public int getSiteDocFromSiteID(int delID,int siteID)
    {
        Statement stmt = null;
        int ans=0;
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT SiteDocs.ID FROM SiteDocs,Deliveries,DeliveryToSiteDocs WHERE SiteDocs.ID=DeliveryToSiteDocs.Site_Doc_ID AND Deliveries.ID=DeliveryToSiteDocs.Delivery_ID AND SiteDocs.Dest_Site_ID="+siteID+" AND Deliveries.ID="+delID);
            if (rs.next())
            {
                ans= rs.getInt("ID");
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("Truck return with error..");
            return ans;
        }
        return ans;
    }
    public boolean deleteTruck(int id)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        return executeCommand("UPDATE Trucks set LeavingDate ='" +dateFormat.format(cal.getTime()) +"' where ID='"+id+"';");
    }
    public boolean addTruck(Truck trck)
    {
        String sql = "INSERT INTO Trucks (ID,Model,License_Type,Net_Weight,Max_Weight) " +
                "VALUES ('"+trck.get_truckId()+"','"+trck.get_TruckModel()+"','"+trck.get_licenseType().toString()+"','"+trck.get_netTruckWeight()+"','"+trck.get_maxTruckWeight()+"')";
        return executeCommand(sql);
    }
    public boolean updateTruckCleanWeight(int id ,int newClean)
    {
        return executeCommand("UPDATE Trucks set Net_Weight ='" +newClean +"' where ID='"+id+"';");
    }
    public boolean updateTruckMaxWeight(int id ,int newMax)
    {
        return executeCommand( "UPDATE Trucks set Max_Weight ='" +newMax +"' where ID='"+id+"';");
    }
    public Driver getDriver(int id)
    {
        Driver toReturn = null;
        Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Drivers WHERE ID="+id+";");
            while (rs.next())
            {
                int ID = rs.getInt("ID");
                String type = rs.getString("License_Type");
                LicenseType enumType = LicenseType.valueOf(type);
                toReturn = new Driver(ID, enumType);
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("Driver return with error..");
            return toReturn;
        }
        return toReturn;
    }
    public boolean addDriver(int id,LicenseType licenseType)
    {
        return executeCommand("INSERT INTO Drivers (ID,License_Type) " + "VALUES ("+id+",'"+licenseType.name()+"')");
    }
    public LinkedList<Driver> getAllDrivers(Date date,int SiteID)
    {
        LinkedList<Driver> toReturn = new LinkedList<Driver>();
        Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            long dateStart= PLI.makeDate( sdf.format(date),"00:00").getTime();
            long dateFinish= PLI.makeDate( sdf.format(date),"23:59").getTime();
            String sql="SELECT Drivers.ID,License_Type FROM Drivers,Shifts,Employees Where Employees.ID=Drivers.ID AND Shifts.ID=Drivers.ID AND Shifts.Date='"+sdf.format(date)+"' AND Shifts.DayPart='Morning' AND Employees.SiteID="+SiteID+" EXCEPT SELECT Drivers.ID,License_Type FROM Deliveries JOIN Drivers ON Drivers.ID=Deliveries.Driver_ID WHERE Deliveries.Date_Hour<"+dateFinish+" AND Deliveries.Date_Hour>"+dateStart+";";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next())
            {
                int id = rs.getInt("Drivers.ID");
                String type = rs.getString("License_Type");
                LicenseType enumType = LicenseType.valueOf(type);
                toReturn.add(new Driver(id, enumType));
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("List Of Drivers return with error..");
            return toReturn;
        }
        return toReturn;
    }
    public boolean addDeliveryToSiteDocs(int insertedDeliveryId,int insertedSD)
    {
        return executeCommand("INSERT INTO DeliveryToSiteDocs (Delivery_ID,Site_Doc_ID) VALUES ('"+insertedDeliveryId+"','"+insertedSD+"')");
    }

    public LinkedList getDriversForShift(String date)
    {
        LinkedList toReturn = new LinkedList<>();
        Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            String sql="SELECT ID,FirstName,LastName FROM (SELECT Drivers.ID,FirstName,LastName FROM Drivers,Employees,Constraints WHERE Employees.ID=Drivers.ID AND Constraints.ID=Drivers.ID AND Constraints.Date='"+date+"' GROUP BY Drivers.ID,FirstName,LastName HAVING COUNT(Drivers.ID)>1 EXCEPT SELECT Employees.ID,FirstName,LastName FROM Employees JOIN Shifts ON Employees.ID=Shifts.ID WHERE Shifts.Date='"+date+"');";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next())
            {
                toReturn.add(rs.getString("ID"));
                toReturn.add(rs.getString("FirstName"));
                toReturn.add(rs.getString("LastName"));
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            return toReturn;
        }
        return toReturn;
    }
    public boolean deleteAllProductFromSiteDoc(int delID,int SiteID)
    {
        return executeCommand("DELETE FROM SiteDocToProducts WHERE Site_Doc_ID in (SELECT SiteDocs.ID FROM SiteDocs JOIN DeliveryToSiteDocs ON SiteDocs.ID=DeliveryToSiteDocs.Site_Doc_ID WHERE Dest_Site_ID="+SiteID+" AND Delivery_ID="+delID+") ");
    }
    public boolean deleteProductFromSiteDoc(int SiteDocID,int proID)
    {
        return executeCommand("DELETE FROM SiteDocToProducts WHERE Site_Doc_ID="+SiteDocID+" AND Product_ID="+proID);
    }
    public boolean deleteSiteDoc(int delID,int SiteID)
    {
        return executeCommand("DELETE FROM SiteDocs WHERE ID in (SELECT SiteDocs.ID FROM SiteDocs JOIN DeliveryToSiteDocs ON SiteDocs.ID=DeliveryToSiteDocs.Site_Doc_ID WHERE Dest_Site_ID="+SiteID+" AND Delivery_ID="+delID+") ");
    }

    public LinkedList<Site> getDestinationWithout(int src,int delID)
    {
        LinkedList<Site> toReturn = new LinkedList<Site>();
        Statement stmt = null;
        try
        {
            String sql="SELECT Sites.ID FROM Sites WHERE Sites.ID <> "+src+" EXCEPT SELECT Dest_Site_ID FROM DeliveryToSiteDocs JOIN SiteDocs ON DeliveryToSiteDocs.Site_Doc_ID=SiteDocs.ID WHERE Delivery_ID="+delID+";";
            stmt=c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next())
            {
                int id = rs.getInt("Sites.ID");
                toReturn.add(getSite(id));
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("List Of Drivers return with error..");
            return toReturn;
        }
        return toReturn;
    }

    public boolean reuseTruck(int id)
    {
        return executeCommand("UPDATE Trucks set LeavingDate = null where ID = "+id+";");
    }

    public void closeConnection()
    {
        try
        {
            this.c.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public boolean ifDriverAlreadyAssigned(Shift shift,String HumanID)
    {
        Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            String query="Select * FROM Shifts Where Shifts.ID="+HumanID+" AND Shifts.Date='"+shift.getDate()+"' AND Shifts.DayPart='"+shift.getDayPart()+"'";
            ResultSet rs = stmt.executeQuery(query);
            boolean ans=false;
            if (rs.next())
            {
                ans = true;
            }
            else
            {
                ans = false;
            }
            rs.close();
            stmt.close();
            return ans;
        }
        catch ( Exception e )
        {
            return false;
        }
    }

    public int deliveryAvailableForDay(String day,int weight,int siteID)
    {
        Statement stmt = null;
        int ans;
        try
        {
            stmt = c.createStatement();
            String query="Select Deliveries.ID FROM Deliveries JOIN Trucks ON Trucks.ID=Deliveries.Truck_ID Where Date='"+day+"' AND (Trucks.Max_Weight-Deliveries.Weight>="+weight+") AND Deliveries.Site_ID="+siteID;
            ResultSet rs = stmt.executeQuery(query);
            if (rs.next())
            {
                ans = rs.getInt("ID");
            }
            else
            {
                ans = -1;
            }
            rs.close();
            stmt.close();
            return ans;
        }
        catch ( Exception e )
        {
            return -1;
        }
    }

    public Delivery driverAvailableDelivery(String day,Order order)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Delivery del=null;
        Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            String query="Select Drivers.ID,Trucks.ID FROM Drivers,Shifts,Employees,Trucks WHERE Trucks.License_Type=Drivers.License_Type AND Drivers.ID=Shifts.ID AND Shifts.ID=Employees.ID AND Shifts.Role='Driver' AND Shifts.Date='"+day+"' AND Employees.SiteID="+order.getShopID()+" AND Trucks.Max_Weight>"+order.getTotalWeight()
               +     " Except SELECT Drivers.ID,Trucks.ID FROM Deliveries,Trucks,Drivers WHERE Date='"+day+"' AND (Deliveries.Truck_ID=Trucks.ID OR Deliveries.Driver_ID=Drivers.ID)";
            ResultSet rs = stmt.executeQuery(query);
            boolean flag=true;
            Driver driver=null;
            Truck truck=null;
            if (rs.next())
            {
                flag = false;
                driver=getDriver(rs.getInt(1));
                truck=getTruck(rs.getInt(2));
            }
            rs.close();
            stmt.close();
            if(!flag)
            {
                List<SiteDoc> list = new LinkedList<>();
                list.add(new SiteDoc(addSiteDoc(order,0), order, day));
                for (Map.Entry<ProvidedProduct,Integer> pair : order.getProducts().entrySet())
                {
                    ProvidedProduct p = ((ProvidedProduct) pair.getKey());
                    addSiteDocToProducts(list.get(0).getSiteDocId(), Integer.parseInt(p.getCatalogNumber()), Integer.parseInt(p.getSupplierID()), (Integer) pair.getValue());
                }
                del = new Delivery(dateFormat.parse(day),driver , truck, order.getTotalWeight(), getShop(order.getShopID()), list, "08:00");
                updateSiteDocDelivery(addDelivery(del),list.get(0).getSiteDocId());
            }
            return del;
        }
        catch ( Exception e )
        {
            return null;
        }
    }
    public boolean updateSiteDocDelivery(int deliveryID,int siteDocID)
    {
    	return executeCommand("Update SiteDocs SET Delivery_ID="+deliveryID+" WHERE ID="+siteDocID);
    }
    public int addDelivery(Delivery del)
    {
        Statement stmt = null;
        Date date = del.get_date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dateToAdd = dateFormat.format(date);
        int driverIdToAdd = del.get_driver().get_DriverId();
        int truckIdToAdd = del.get_truck().get_truckId();
        int originIdToAdd = del.get_originSite().getId();
        int weight = del.get_weight();
        String time=del.getTime();
        int insertedDeliveryId = 0;
        try
        {
            stmt = c.createStatement();
            String sql = "INSERT INTO Deliveries (Date,DateInMillis,Time,Driver_ID,Truck_ID,Site_ID,Weight) "
                    + "VALUES ('"+dateToAdd+"','"+dateFormat.parse(dateToAdd).getTime()+ "','"+ time +"','"+ driverIdToAdd +"','"+truckIdToAdd+"','" +originIdToAdd+"','"+ weight +"')";
            PreparedStatement statement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            insertedDeliveryId = 0;
            if (generatedKeys.next()) {
                insertedDeliveryId = generatedKeys.getInt(1);
            }
            stmt.close();
        }
        catch (Exception e)
        {
            return insertedDeliveryId;
        }
        return insertedDeliveryId;
    }

    public Shop getShop(int shopID)
    {
        Shop toReturn = null;
        Statement stmt = null;
        try
        {
            String sql="SELECT * FROM SHOPS WHERE ID="+shopID+";";
            stmt=c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next())
            {
                toReturn=new Shop(rs.getInt("ID"),rs.getString("ADDRESS"),rs.getString("PHONE_NUMBER"),rs.getString("CONTACT_NAME"));
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
            System.out.println("List Of Drivers return with error..");
            return toReturn;
        }
        return toReturn;
    }

    public void addToDelivery(Order order,int deliveryID)
    {
        if(!order.getProducts().isEmpty())
        {
            int siteDocID = 0;
            siteDocID = addSiteDoc(order,deliveryID);
            for (Map.Entry<ProvidedProduct,Integer> pair : order.getProducts().entrySet())
            {
                ProvidedProduct p = ((ProvidedProduct) pair.getKey());
                addSiteDocToProducts(siteDocID, Integer.parseInt(p.getCatalogNumber()), Integer.parseInt(p.getSupplierID()), ((Integer) pair.getValue()).intValue());
            }
            updateDeliveryWeightPlus(deliveryID,order.getTotalWeight());
        }
    }
    public boolean addSiteDocToProducts(int insertedSiteDocId, int catalogID,int supplierID,int amount)
    {
        return executeCommand("INSERT INTO SiteDocToProducts (Site_Doc_ID,CatalogID,SupplierID,Quantity) VALUES ('"+insertedSiteDocId+"','"+catalogID+"','" +supplierID+"','"+ amount+"')");
    }
    public boolean updateDeliveryWeightPlus(int deliveryID,int weight)
    {
        return executeCommand("Update Deliveries SET Weight=Weight+"+weight+" WHERE ID="+deliveryID);
    }
    public boolean updateDeliveryWeightMinus(int deliveryID,int weight)
    {
        return executeCommand("Update Deliveries SET Weight=Weight-"+weight+" WHERE ID="+deliveryID);
    }
    public int addSiteDoc(Order o,int deliveryID)
    {
        int insertedSiteDocId = 0;
        try
        {
            String sql = "INSERT INTO SiteDocs (Dest_Site_ID,OrderID,Delivery_ID) " + "VALUES ('"+o.getSuplier().getID()+"',"+o.getOrderNumber()+","+deliveryID+")";
            PreparedStatement statement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                insertedSiteDocId = generatedKeys.getInt(1);
            }
            else
            {
                throw new SQLException("adding sitedoc failed, no ID obtained.");
            }
            statement.close();
        }
        catch (Exception e)
        {
            return insertedSiteDocId;
        }
        return insertedSiteDocId;
    }

    public void addEmptyDeliveryWithOrder(Order order)
    {
        List<SiteDoc> list = new LinkedList<>();
        list.add(new SiteDoc(addSiteDoc(order,0), order, ""));
        for (Map.Entry<ProvidedProduct,Integer> pair : order.getProducts().entrySet())
        {
            ProvidedProduct p = ((ProvidedProduct) pair.getKey());
            addSiteDocToProducts(list.get(0).getSiteDocId(), Integer.parseInt(p.getCatalogNumber()), Integer.parseInt(p.getSupplierID()), (Integer) pair.getValue());
        }
        updateSiteDocDelivery(addEmptyDelivery(order.getTotalWeight(),order.getShopID()),list.get(0).getSiteDocId());
    }
    
    public void addEmptyDeliveryWithOrderAndDate(Order order,String day)
    {
        List<SiteDoc> list = new LinkedList<>();
        list.add(new SiteDoc(addSiteDoc(order,0), order, ""));
        for (Map.Entry<ProvidedProduct,Integer> pair : order.getProducts().entrySet())
        {
            ProvidedProduct p = ((ProvidedProduct) pair.getKey());
            addSiteDocToProducts(list.get(0).getSiteDocId(), Integer.parseInt(p.getCatalogNumber()), Integer.parseInt(p.getSupplierID()), (Integer) pair.getValue());
        }
        updateSiteDocDelivery(addEmptyDeliveryWithDate(order.getTotalWeight(),order.getShopID(),day),list.get(0).getSiteDocId());
    }
    
    public int addEmptyDeliveryWithDate(int weight,int shop,String day)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	Statement stmt = null;
        int insertedDeliveryId = 0;
        try
        {
            stmt = c.createStatement();
            String sql = "INSERT INTO Deliveries (Time,Site_ID,Weight,Date,DateInMillis) "
                    + "VALUES ('08:00','"+shop+"','"+ weight +"','"+day+"','"+dateFormat.parse(day).getTime()+"')";
            PreparedStatement statement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            insertedDeliveryId = 0;
            if (generatedKeys.next())
            {
                insertedDeliveryId = generatedKeys.getInt(1);
            }
            stmt.close();
        }
        catch (Exception e)
        {
            return insertedDeliveryId;
        }
        return insertedDeliveryId;
    }
    public int addEmptyDelivery(int weight,int shop)
    {
        Statement stmt = null;
        int insertedDeliveryId = 0;
        try
        {
            stmt = c.createStatement();
            String sql = "INSERT INTO Deliveries (Time,Site_ID,Weight) "
                    + "VALUES ('08:00','"+shop+"','"+ weight +"')";
            PreparedStatement statement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            insertedDeliveryId = 0;
            if (generatedKeys.next())
            {
                insertedDeliveryId = generatedKeys.getInt(1);
            }
            stmt.close();
        }
        catch (Exception e)
        {
            return insertedDeliveryId;
        }
        return insertedDeliveryId;
    }

    public void deliveryForEmpty()
    {
        Statement stmt = null;
        try
        {
            LinkedList<Integer> ls=new LinkedList<Integer>();
            stmt = c.createStatement();
            String query="Select ID FROM Deliveries WHERE Date IS NULL";
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next())
            {
                ls.add(rs.getInt("ID"));
                ls.add(rs.getInt("Weight"));
            }
            rs.close();
            stmt.close();
            for(int i=0;i<ls.size();i+=2)
            {
                fillDelivery(ls.get(i),ls.get(i+1));
            }
        }
        catch ( Exception e )
        {

        }
    }

    private  void fillDelivery(int deliveryNum,int weight)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            String query = "Select Drivers.ID,Trucks.ID,Shifts.Date FROM Drivers,Trucks,Shifts Where Trucks.Max_Weight>"+weight +
                    "Except Select Drivers.ID,Trucks.ID,Shifts.Date From Drivers,Trucks,Shifts,Deliveries Where Shifts.ID=Drivers.ID AND Shifts.Date=Deliveries.Date AND (Drivers.ID=Deliveries.Driver_ID OR Deliveries.Truck_ID=Trucks.ID)";
            ResultSet rs = stmt.executeQuery(query);
            String toExecute="";
            while(rs.next())
            {
                Date date=dateFormat.parse(rs.getString("Shifts.Date"));
                Calendar c1=Calendar.getInstance();
                if(date.compareTo(c1.getTime())>0)
                {
                    toExecute="UPDATE Deliveries SET Date='"+rs.getString("Shifts.Date")
                            +"', DateInMillis="+date.getTime()+", Truck_ID="+rs.getInt("Trucks.ID")+", Drivers.ID="+rs.getInt("Drivers.ID")+" WHERE ID="+deliveryNum;
                    break;
                }
            }
            rs.close();
            stmt.close();
            executeCommand(toExecute);
        }
        catch(Exception e){}
    }
    public List<Integer> getAllShops()
    {
    	List<Integer> list=new LinkedList<Integer>();
    	Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            String query = "Select ID From SHOPS";
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next())
            {
            	list.add(rs.getInt("ID"));
            }
            rs.close();
            stmt.close();
        }
        catch(Exception e){return null;}
        return list;
    }
    
    
    public ArrayList<Order> getAllOrdersInDelivery(int deliveryNum)
    {
    	ArrayList<Order> list=new ArrayList<Order>();
    	List<Integer> siteDocs=new LinkedList<Integer>();
    	Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            String query = "Select ID,OrderID FROM SiteDocs WHERE Delivery_ID="+deliveryNum;
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next())
            {
            	siteDocs.add(rs.getInt("ID"));
            	siteDocs.add(rs.getInt("OrderID"));
            }
            rs.close();
            stmt.close();
            for(int i=0;i<siteDocs.size();i+=2)
            {
            	list.add(getOrder(siteDocs.get(i),siteDocs.get(i+1)));
            }
            return list;
        }
        catch(Exception e){return null;}
    }
    
    public Order getOrder(int siteDocID,int orderID)
    {
    	Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            String query = "Select * FROM SiteDocToProducts WHERE Site_Doc_ID="+siteDocID;
            ResultSet rs = stmt.executeQuery(query);
            Order o=null;
            while(rs.next())
            {
            	if(o==null)
            	{
            		o=new Order();
            		o.setOrderNumber(orderID+"");
                	o.setSuplier(getSupplier(rs.getInt("SupplierID")));/*get supplier with contacts*/
            	}
            	o.addProduct(getProduct(rs.getInt("CatalogID"),rs.getInt("SupplierID")), rs.getInt("Quantity"));
            }
            rs.close();
            stmt.close();
            return o;
        }
        catch(Exception e){return null;}
    }
    
    public Supplier getSupplier(int supplierID)
    {
    	Statement stmt = null;
    	List<Contact> contacts=new LinkedList<Contact>();
        try
        {
            stmt = c.createStatement();
            String query = "Select * FROM CONTACTS WHERE SUPPLIERID="+supplierID;
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next())
            {
            	String id=rs.getString("ID");
            	String fName=rs.getString("FIRSTNAME");
            	String lName=rs.getString("LASTNAME");
            	String phone=rs.getString("PHONE");
            	contacts.add(new Contact(id,fName,lName,phone));
            }
            stmt.close();
            rs.close();
            stmt = c.createStatement();
            query="SELECT * FROM SUPPLIERS WHERE ID="+supplierID;
            ResultSet rs2=stmt.executeQuery(query);
            Supplier sup=null;
            if(rs2.next())
            {
            	boolean isProvide=true;
            	if(rs2.getString("ProvideTransport").equals("F"))
            	{
            		isProvide=false;
            	}
            	sup=new Supplier(rs2.getString("ID"),rs2.getString("NAME"),rs2.getString("BankAccount"),rs2.getString("TermOfPayment"),isProvide,contacts,rs2.getString("ADDRESS"));
            }
            stmt.close();
            rs.close();
            return sup;
        }
        catch(Exception e){return null;}
    }
    
    public ProvidedProduct getProduct(int catalogID,int supplierID)
    {
    	ProvidedProduct p=null;
    	Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            String query = "Select * FROM PRODUCTS_SUPPLIERS WHERE CatalogID='"+catalogID+"' AND SupplierID='"+supplierID+"'";
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
            {
            	p=new ProvidedProduct(rs.getString("PRODUCT_NAME"),rs.getString("MANUFACTURER"),rs.getString("price"),supplierID+"",catalogID+"",rs.getInt("WEIGHT"));
            }
            rs.close();
            stmt.close();
            return p;
        }
        catch(Exception e){return null;}
    }
    
    
    public boolean checkDriverShiftAndTruck(Order order,int shopID,String day)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            String query = "Select Drivers.ID,Trucks.ID,Constraints.Date FROM Drivers,Trucks,Constraints,Employees Where Constraints.Date='"+day+"' AND Drivers.License_Type=Trucks.License_Type AND Employees.ID=Drivers.ID AND Drivers.ID=Constraints.ID AND Trucks.Max_Weight>"+order.getTotalWeight()+
            		" AND Employees.SiteID="+shopID+" AND Trucks.ID NOT IN (Select Truck_ID FROM Deliveries WHERE Date='"+day+"') AND Drivers.ID NOT IN (SELECT ID From Shifts WHERE Date='"+day+"')";
            ResultSet rs = stmt.executeQuery(query);
            Driver driver=null;
            Truck truck=null;
            if(rs.next())
            {
            	driver=getDriver(rs.getInt(1));
            	truck=getTruck(rs.getInt(2));
            }
            rs.close();
            stmt.close();
            if(driver!=null)
            {
            	executeCommand("INSERT INTO Shifts (ID,Date,DayPart,Role) VALUES ("+driver.get_DriverId()+",'"+day+"','Morning','Driver')");
            	List<SiteDoc> list = new LinkedList<>();
                list.add(new SiteDoc(addSiteDoc(order,0), order, day));
                for (Map.Entry<ProvidedProduct,Integer> pair : order.getProducts().entrySet())
                {
                    ProvidedProduct p = ((ProvidedProduct) pair.getKey());
                    addSiteDocToProducts(list.get(0).getSiteDocId(), Integer.parseInt(p.getCatalogNumber()), Integer.parseInt(p.getSupplierID()), (Integer) pair.getValue());
                }
                Delivery del = new Delivery(dateFormat.parse(day),driver , truck, order.getTotalWeight(), getShop(order.getShopID()), list, "08:00");
                updateSiteDocDelivery(addDelivery(del),list.get(0).getSiteDocId());
            	return true;
            }
            
        }
        catch(Exception e){return false;}
        return false;
    }
    
    public void cancelOrder(int orderID)
    {
    	int weight=0;
    	Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            Calendar calendar=Calendar.getInstance();
            String query = "Select Delivery_ID,SiteDocToProducts.CatalogID,SiteDocToProducts.SupplierID,Quantity,SiteDocs.ID,PRODUCTS_SUPPLIERS.WEIGHT FROM SiteDocs,SiteDocToProducts,Deliveries,PRODUCTS_SUPPLIERS WHERE PRODUCTS_SUPPLIERS.CatalogID=SiteDocToProducts.CatalogID  AND PRODUCTS_SUPPLIERS.SupplierID=SiteDocToProducts.SupplierID AND SiteDocs.Delivery_ID=Deliveries.ID AND SiteDocs.ID=SiteDocToProducts.Site_Doc_ID AND OrderID="+orderID+" AND Deliveries.DateInMillis>"+calendar.getTimeInMillis();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next())
            {
            	int delID=rs.getInt("Delivery_ID");
            	int catID=rs.getInt("CatalogID");
            	int supID=rs.getInt("SupplierID");
            	int quan=rs.getInt("Quantity");
            	weight=weight+quan*rs.getInt(6);
            	updateDeliveryWeightMinus(delID,weight);
            	deleteProductFromDelivery(delID,catID,supID,quan,rs.getInt(5));
            }
            rs.close();
            stmt.close();
            executeCommand("DELETE FROM SiteDocs WHERE OrderID="+orderID+" AND Delivery_ID IN (SELECT Delivery_ID FROM SiteDocs JOIN Deliveries ON SiteDocs.Delivery_ID=Deliveries.ID WHERE DateInMillis>"+calendar.getTimeInMillis()+" AND OrderID="+orderID+")");
            executeCommand("DELETE FROM Deliveries WHERE Deliveries.ID NOT IN (SELECT Deliveries.ID FROM SiteDocs JOIN Deliveries ON Deliveries.ID=SiteDocs.Delivery_ID)");
        }
        catch(Exception e)
        {
        	System.out.println("error");
        }
    }
    public void deleteProductFromDelivery(int deliveryID,int catalogID,int supplierID,int quantity,int siteDoc)
    {
    	Statement stmt = null;
    	int weight=0;
        try
        {
            stmt = c.createStatement();
            String query = "Select WEIGHT FROM PRODUCTS_SUPPLIERS Where CatalogID="+catalogID+" AND SupplierID="+supplierID;
            ResultSet rs = stmt.executeQuery(query);
            stmt.close();
            if(rs.next())
            {
            	weight=rs.getInt("WEIGHT");
            }
            rs.close();
        }
        catch(Exception e){}
        updateDeliveryWeightMinus(deliveryID,weight*quantity);
        executeCommand("DELETE FROM SiteDocToProducts WHERE Site_Doc_ID="+siteDoc);
    }
    
    public void RemoveProductFromOrderInDelivery(Order order)
    {
    	
    	Statement stmt = null;
        try
        {
            stmt = c.createStatement();
            Calendar calendar=Calendar.getInstance();
            String query = "Select Delivery_ID FROM Deliveries JOIN SiteDocs ON Deliveries.ID=SiteDocs.Delivery_ID Where OrderID="+order.getOrderNumber()+" AND DateInMillis>"+calendar.getTimeInMillis();
            ResultSet rs = stmt.executeQuery(query);
            stmt.close();
            cancelOrder(Integer.parseInt(order.getOrderNumber()));
            while(rs.next())
            {
            	addToDelivery(order,rs.getInt("Delivery_ID"));
            }
        }
        catch(Exception e){}
    }
    
    public boolean existsDeliveryWithOrder(int orderID)
    {
    	Statement stmt = null;
    	boolean ans=false;
        try
        {
            stmt = c.createStatement();
            Calendar calendar=Calendar.getInstance();
            String query = "Select * FROM Deliveries JOIN SiteDocs ON Deliveries.ID=SiteDocs.Delivery_ID Where OrderID="+orderID+" AND DateInMillis>"+calendar.getTimeInMillis();
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
            {
            	ans=true;
            }
            rs.close();
            stmt.close();
        }
        catch(Exception e){return false;}
        return ans;
    }
    
    public int getShopID(int ID)
    {
    	Statement stmt = null;
    	int ans=0;
        try
        {
            stmt = c.createStatement();
            String query = "Select * FROM Employees Where ID="+ID;
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
            {
            	ans=rs.getInt("SiteID");
            }
            rs.close();
            stmt.close();
        }
        catch(Exception e)
        {
        	return 0;
        }
        return ans;
    }
    
    public Connection getConnection()
    {
        return this.c;
    }
}