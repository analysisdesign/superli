package unitTests;







import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.internal.runners.statements.Fail;

import dal.DataBase;
import dal.ProductInCategoryTable;
import modules.Contact;
import modules.Order;
import modules.ProductInCategory;
import modules.ProvidedProduct;
import modules.Shop;
import modules.Supplier;


public class Tests
{

	public static void main(String[] args) 
	{

	}
	private DataBase dbBase;
	
	
	
	
	@Test
	public void testAddNewSupplier1()
	{
		dbBase=DataBase.getDataBase();
		String ID = "12345";
		String name = "Urugay supplier";
		String bankAccount = "99999";
		String termOfPayment = "Cash";
		boolean ProvideTransport = false;
		String address = "Montevideo";
		Supplier s=new Supplier(ID, name, bankAccount, termOfPayment, ProvideTransport, address);
		try{
			dbBase.addSupplier(s);
		}
		catch(Exception e){

		}
		Supplier dataSupplier=dbBase.getSupplierById(ID);
		assertEquals(dataSupplier.getID(),ID);
		assertEquals(dataSupplier.getBankAccount(),bankAccount);
		assertEquals(dataSupplier.isProvideTransport(),ProvideTransport);
	}
	@Test
	public void testAddNewSupplier2()
	{
		dbBase=DataBase.getDataBase();
		String ID = "12346";
		String name = "Yeman supplier";
		String bankAccount = "1111";
		String termOfPayment = "Cash";
		boolean ProvideTransport = true;
		String address = "Rosh Hayin";
		Supplier s=new Supplier(ID, name, bankAccount, termOfPayment, ProvideTransport, address);
		try{
			dbBase.addSupplier(s);
		}
		catch(Exception e){

		}
		Supplier dataSupplier=dbBase.getSupplierById(ID);
		assertEquals(dataSupplier.getID(),ID);
		assertEquals(dataSupplier.getBankAccount(),bankAccount);
		assertEquals(dataSupplier.isProvideTransport(),ProvideTransport);
	}
	@Test
	public void testAddNewContact1()
	{
		dbBase=DataBase.getDataBase();
		String ID = "12346";
		String name = "Yeman supplier";
		String bankAccount = "1111";
		String termOfPayment = "Cash";
		boolean ProvideTransport = true;
		String address = "Rosh Hayin";
		Supplier s=new Supplier(ID, name, bankAccount, termOfPayment, ProvideTransport, address);
		Contact c=new Contact("13579","israel","israeli","0549876543");
		List<Contact> listContacts = new ArrayList<Contact>();
		listContacts.add(c);
		s.setContacts(listContacts);
		try{
			dbBase.addSupplier(s);
			dbBase.addContact(s, c);
		}
		catch(Exception e){}
		Supplier supplierDB =dbBase.getSupplierById(ID);
		boolean flag=false;
		for(Contact curr1:supplierDB.getContacts())
		{
			for(Contact curr2:s.getContacts())
			{
				if(curr2.getId().equals(curr1.getId()))
				{
					flag=true;
				}
			}
		}
		assertEquals(flag,true);
		assertEquals(s.getTermOfPayment(),supplierDB.getTermOfPayment());
	}
	@Test
	public void testUpdateContact()

	{
		dbBase=DataBase.getDataBase();
		String ID = "12346";
		String name = "Yeman supplier";
		String bankAccount = "1111";
		String termOfPayment = "Cash";
		boolean ProvideTransport = true;
		String address = "Rosh Hayin";
		Supplier s=new Supplier(ID, name, bankAccount, termOfPayment, ProvideTransport, address);
		Contact c=new Contact("13579","israel","israeli","0549876543");
		List<Contact> listContacts = new ArrayList<Contact>();
		listContacts.add(c);
		s.setContacts(listContacts);
		try{
			dbBase.addSupplier(s);
			dbBase.addContact(s, c);
			dbBase.updateContact(s, c, "FIRSTNAME", "israel2");

		}
		catch(Exception e){}
		Supplier supplierDB =dbBase.getSupplierById(ID);
		boolean found = false;
		c.setFirstName("israel2");
		ArrayList<Contact> allContact = (ArrayList<Contact>) dbBase.getAllContactsBySupplierID(ID);
		for (Contact contact : allContact) {
			if(contact.getId().equals(c.getId()))
			{
				found = true;
				assertEquals(c.getFirstName(),allContact.get(0).getFirstName());
				assertEquals(c.getId(),allContact.get(0).getId());
				assertEquals(c.getLastName(),allContact.get(0).getLastName());
				assertEquals(c.getPhoneNumber(),allContact.get(0).getPhoneNumber());
			}
		}
		assertEquals(found,true);
	}

	@Test
	public void testAddNewShop()
	{
		dbBase=DataBase.getDataBase();
		int id = -1;
		String phone_number = "03-1234567";
		String contact_name = "Hershale";
		String address = "Ramat Aviv";
		Shop s=new Shop(id, address, phone_number, contact_name);
		try{
			dbBase.shopsTable.insertTable(s);
			id = dbBase.shopsTable.getMaxShopID();

		}
		catch(Exception e)
		{
		}
		Shop dbShop = dbBase.shopsTable.getShopByID(id);
		assertEquals(address, dbShop.getAddress());
		assertEquals(contact_name, dbShop.getContact_Name());
		assertEquals(phone_number, dbShop.getPhone_Number());
		dbBase.shopsTable.deleteShopById(id);
	}


	@Test
	public void testDeleteShop()

	{
		dbBase=DataBase.getDataBase();
		int id = -1;
		String phone_number = "03-1234567";
		String contact_name = "Hershale";
		String address = "Ramat Aviv";
		Shop s=new Shop(-1, address, phone_number, contact_name);
		try{
			dbBase.shopsTable.insertTable(s);
			 id = dbBase.shopsTable.getMaxShopID();
			dbBase.shopsTable.deleteShopById(id);
		}
		catch(Exception e){
		}
		Shop dbShop = dbBase.shopsTable.getShopByID(id);
		assertEquals(null, dbShop);
	}
	
	@Test
	public void testAddProductInCategory()

	{
		dbBase=DataBase.getDataBase();
		String product = "Klik";
		String category = "Snacks";
		String manufacturer = "Osem";
		int sellPrice = 7;
		int minimalAmount = 10;
		int dailyConsumption = 10;
		int weight = 1;
		int shopID = 1;
		ProductInCategory pp=new ProductInCategory(product, category, manufacturer, sellPrice, minimalAmount, dailyConsumption, weight, shopID);
		try{
			dbBase.productInCategoryTable.insertTable(pp);
		}
		catch(Exception e)
		{
		}
		ProductInCategory pppDB = ((ProductInCategoryTable) dbBase.productInCategoryTable).getProductInCategoryByNameManufacturerAndShopID(product, manufacturer, shopID);
		assertEquals(pppDB.getCategory(), pp.getCategory());
		assertEquals(pppDB.getDailyConsumption(), pp.getDailyConsumption());
		assertEquals(pppDB.getMinimalAmount(), pp.getMinimalAmount());
		
		((ProductInCategoryTable) dbBase.productInCategoryTable).deleteProductByNameAndManufacturer(pppDB.getProduct(), pppDB.getManufacturer());


	}
	
	@Test
	public void addRemoveOrder()
	{
		dbBase=DataBase.getDataBase();
		Map<ProvidedProduct,Integer>products = new HashMap<ProvidedProduct,Integer>();
		List<Integer> day = new ArrayList<Integer>(); 
		String OrderNumber="9999";
		Supplier suplier = new Supplier("9999", "", "", "", false, "");
		int shopID=1;
		ProvidedProduct pp = new ProvidedProduct("Milk","Tara" , "1", "1", "1", 1);
		products.put(pp, 3);
		Order newOrder= new Order(products,OrderNumber,day,suplier,shopID);
		if(!dbBase.addNewOrder(newOrder)){
			fail("didnt add order!");
		}
		dbBase.deleteOrderById("Milk","Tara",Integer.parseInt(OrderNumber));
		dbBase.deleteOrderSupplierById(Integer.parseInt(OrderNumber));
	}
	
	@Test
	public void addRemoveProductDiscount()
	{
		dbBase=DataBase.getDataBase();
		String productName = "Milk 1L";
		String manufacturer = "Tara";
		int discount = 50;
		String startDate = "12/05/2017";
		String endDate = "20/05/2017";
		if(!dbBase.addProductDiscount(productName+","+manufacturer+","+discount+","+startDate+","+endDate)){
			fail("didnt add discount!");
		}
		if(!dbBase.checkIfProductDiscExist(productName, manufacturer)){
			fail("didnt find discount!");
		}
		dbBase.removeDiscountByProductNameManufacturerStartTimeEndTime(productName, manufacturer, startDate, endDate);
	}
	
	@Test
	public void addRemoveCategoryDiscount()
	{
		dbBase=DataBase.getDataBase();
		String category = "Dairy Milk";
		int discount = 50;
		String startDate = "12/05/2017";
		String endDate = "20/05/2017";
		if(!dbBase.addCategoryDiscount(category+","+discount+","+startDate+","+endDate)){
			fail("didnt add discount!");
		}
		if(!dbBase.checkIfCategoryDiscExist(category)){
			fail("didnt find discount!");
		}
		dbBase.removeDiscountByCategoryNameStartTimeEndTime(category, startDate, endDate);
	}
}