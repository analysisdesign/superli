package Deliveries.Layers;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import DataAccessLayer.DBAccess;
import Deliveries.Entities.*;
import Employees.BusinessLayer.InputTesting;
import Employees.PresentationLayer.Views;
import dal.DataBase;
import modules.Order;

public class PLI
{
	DBAccess dbAccess;
	private Scanner in;
	public PLI(DBAccess dbAccess)
	{
		this.dbAccess=dbAccess;
	}
	public void runMenu()
	{
		
		boolean run = true;
		in = new Scanner(System.in);
		System.out.println("Welcome to the Delivery system");
		while(run)
		{
			System.out.println("Please choose an option:");
			System.out.println("1) Add a new truck");
			System.out.println("2) View previous deliveries");
			System.out.println("3) Deprecate Or Reuse existing truck");
			System.out.println("4) Cancel Delivery Of Order");
			System.out.println("5) Exit");
			int num =0;
			while(true)
			{
				try
				{
					num = Integer.parseInt(in.nextLine());
					if(num > 0 && num < 9)
						break;
					else
						System.out.println("Enter digit 1-8.");

				}catch (Exception e) {
					System.out.println("Enter digit 1-8.");
				}
			}
			switch (num){
			case 1:{//Add a new truck
				System.out.println("Enter the register number of the truck:");
				int regNum;
				while(true){
					try{
						regNum = Integer.parseInt(in.nextLine());
						if(regNum < 0)
							System.out.println("Enter Positive register number");
						else if(dbAccess.getTruck(regNum) != null)
							System.out.println("This Truck is already exist. re-enter");
						else
							break;
					}catch (Exception e) {
						System.out.println("Incorrect register number. re-enter.");
					}
				}
				System.out.println("Enter the model of the truck:");
				String modelNum;
				while(true){
					try{
						modelNum = in.nextLine();
						break;
					}catch (Exception e) {
						System.out.println("Incorrect truck model. re-enter.");
					}
				}
				System.out.println("Enter the weight of the truck in Kg:");
				int weight;
				while(true){
					try{
						weight = Integer.parseInt(in.nextLine());
						if(weight > 0)
							break;
						else
							System.out.println("Enter Positive weight.");
					}catch (Exception e) {
						System.out.println("Incorrect truck weight. re-enter.");
					}
				}
				System.out.println("Enter the max possible carry weight of the truck in Kg:");
				int maxWeight;
				while(true){
					try{
						maxWeight = Integer.parseInt(in.nextLine());
						if(maxWeight > 0)
							break;
						else
							System.out.println("Enter Positive weight.");
					}catch (Exception e) {
						System.out.println("Incorrect max possible carry weight of the truck. re-enter.");
					}
				}
				System.out.println("Enter the proper license type of the truck:");
				String lycType;
				while(true){
					try{
						lycType = in.nextLine();
						if(lycType.equals("A") || lycType.equals("B") || lycType.equals("C"))
							break;
						else System.out.println("Incorrect license type. re-enter.");
					}catch (Exception e) {
						System.out.println("Incorrect license type. re-enter.");
					}
				}

				if(dbAccess.addTruck(new Truck(regNum, modelNum, LicenseType.valueOf(lycType), weight, maxWeight))){
					System.out.println("Truck was added successfully");
				}else{
					System.out.println("Truck wasn't added successfully.. try again");
				}

			}break;

			case 2:{//View previous deliveries
				System.out.println("Enter the delivery ID:");
				int delID;
				while(true){
					try{
						delID = Integer.parseInt(in.nextLine());
						break;
					}catch (Exception e) {
						System.out.println("Incorrect delivery ID. re-enter.");
					}
				}
				if(!printDeliveryDetails(delID,dbAccess))
					System.out.println("Such delivery ID doesn't exists");
			}break;
			case 3:{//Delete existing truck
				while(true)
				{
					System.out.println("Press 1 For Deprecate Truck");
					System.out.println("Press 2 For Reuse Truck");
					System.out.println("Press 3 For Exit");
					int opt= Views.getOptionFromUser(3);
					System.out.println("Enter the register number of the truck:");
					int regNum = Integer.parseInt(in.nextLine());
					if(!truckExists(regNum))
					{
						System.out.println("The Truck Does Not Exist");
						break;
					}
					if(opt==1)
					{
						deleteTruck(regNum);
					}
					else
					{
						reuseTruck(regNum);
					}
					System.out.println("Back to menu? (Yes/No)");
					if(in.nextLine().equals("Yes"))
						break;
				}//while
			}break;
			case 4:{
				System.out.println("Enter Order Number:");
				String orderID=in.nextLine();
				if(dbAccess.existsDeliveryWithOrder(Integer.parseInt(orderID)))
				{
					dbAccess.cancelOrder(Integer.parseInt(orderID));
					System.out.println("Delivery Of Order Was Successfully Deleted");
				}
				else
				{
					System.out.println("Delivery Of Order Was Not Found");
				}
				break;
			}
			case 5 :{
				run = false;
				break;
			}

			default:
				System.out.println("invalid input, choose again");
				break;

			}}}

	private void deleteTruck(int regNum) 
	{
		if(dbAccess.deleteTruck(regNum))
			System.out.println("Truck was Deprecate successfully");
		else
			System.out.println("Something went wrong");
	}

	private void reuseTruck(int regNum) 
	{
		if(dbAccess.reuseTruck(regNum))
			System.out.println("Truck was Reuse successfully");
		else
			System.out.println("Something went wrong");
	}

	private boolean truckExists(int regNum) 
	{
		return dbAccess.getTruck(regNum) != null;
	}

	public static boolean printDeliveryDetails(int delID,DBAccess dbAccess) 
	{
		Delivery del = dbAccess.getDelivery(delID);
		if(del == null)
			return false;
		else
			System.out.println(del.toString());

		return true;
	}
	public static Date makeDate(String date, String time)
	{
		int year = Integer.parseInt(date.substring(6)) - 1900;
		int month = Integer.parseInt(date.substring(3, 5)) - 1;
		int day = Integer.parseInt(date.substring(0, 2));
		int hours = Integer.parseInt(time.substring(0, 2));
		int minutes = Integer.parseInt(time.substring(3));
		Date ddate = new Date(year,month,day,hours,minutes);
		return ddate;
	}
    public static int getOptionFromUser(int lowerBound,int upperBound)
    {
        String optionStr;
        int optionNum;
        Scanner in=new Scanner(System.in);
        while(true)
        {
            optionStr=in.nextLine();
            if(InputTesting.isValidNumber(optionStr))
            {
                optionNum=Integer.parseInt(optionStr);
                if((optionNum>=lowerBound && optionNum<=upperBound))
                {
                    return optionNum;
                }
                else
                {
                    System.out.println("You Have Entered An Invalid Number, Please Enter A Number :");
                }
            }
            else
            {
                System.out.println("You Haven't Entered A Number, Please Enter A Number :");
            }
        }
    }

	public Order AddUrgentOrderToDelivery(HashMap<Order,Integer> ordersByDays)
	{
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        for(Map.Entry<Order, Integer> pair : ordersByDays.entrySet())
        {
            Calendar c = Calendar.getInstance();
            if(c.get(Calendar.DAY_OF_WEEK)>=pair.getValue())
            {
            	continue;
            }
            c.set(Calendar.DAY_OF_WEEK,pair.getValue());
            if(c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
            {
                continue;
            }
            if(helpAllOrders(pair.getKey(),dateFormat.format(c.getTime()),dbAccess)!=null)
            {
                return pair.getKey();
            }
        }
        return null;
	}
	public static Order helpAllOrders(Order o,String day,DBAccess dbAccess)
    {
        int deliveryAvailable=dbAccess.deliveryAvailableForDay(day,o.getTotalWeight(),o.getShopID());
        if(deliveryAvailable!=-1)
        {
            dbAccess.addToDelivery(o,deliveryAvailable);
            return o;
        }
        else
        {
            Delivery deliveryBuildAvailable=dbAccess.driverAvailableDelivery(day,o);
            if(deliveryBuildAvailable!=null)
            {
                return o;
            }
        }
        return null;
    }
	
	public boolean changeDriversShift(HashMap<Order,Integer> orders)
	{
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		for(Map.Entry<Order, Integer> pair: orders.entrySet())
		{
			Calendar c=Calendar.getInstance();
			if(c.get(Calendar.DAY_OF_WEEK)>=pair.getValue())
            {
            	continue;
            }
            c.set(Calendar.DAY_OF_WEEK,pair.getValue());
            if(c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
            {
                continue;
            }
			if(dbAccess.checkDriverShiftAndTruck(pair.getKey(),pair.getKey().getShopID(),dateFormat.format(c.getTime())))
			{
				return true;
			}
		}
		return false;
	}
	
	public ArrayList<Order> getOrdersByDeliveryId(int deliveryID)
	{
		return dbAccess.getAllOrdersInDelivery(deliveryID);
	}
}