package Deliveries.Entities;

import modules.ProvidedProduct;
import modules.Shop;
import modules.Supplier;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Delivery {

    private int _deliveryId;
    private Date _date_hour;
    private Driver _driver;
    private Truck _truck;
    private int _weight;
    private Shop _originSite;
    private List<SiteDoc> _sitesOnTheWay;
    private String time;
    public Delivery(Date date, Driver driver, Truck truck, int weight,
                    Shop origin, List<SiteDoc> sitesOneTheWay,String time){
        _date_hour = date;
        _driver = driver;
        _truck = truck;
        _weight = weight;
        _originSite = origin;
        _sitesOnTheWay = sitesOneTheWay;
        this.time=time;
    }

    public int get_deliveryId() {
        return _deliveryId;
    }

    public void set_deliveryId(int _deliveryId) {
        this._deliveryId = _deliveryId;
    }

    public Date get_date() {
        return _date_hour;
    }

    public void set_date(Date _date) {
        this._date_hour = _date;
    }

    public Driver get_driver() {
        return _driver;
    }

    public void set_driver(Driver _driver) {
        this._driver = _driver;
    }

    public Truck get_truck() {
        return _truck;
    }

    public void set_truck(Truck _truck) {
        this._truck = _truck;
    }

    public int get_weight() {
        return _weight;
    }

    public void set_weight(int _weight) {
        this._weight = _weight;
    }

    public Shop get_originSite() {
        return _originSite;
    }

    public void set_originSite(Shop _originSite) {
        this._originSite = _originSite;
    }

    public List<SiteDoc> get_sitesOnTheWay() {
        return _sitesOnTheWay;
    }

    public void set_sitesOnTheWay(List<SiteDoc> _sitesOnTheWay) {
        this._sitesOnTheWay = _sitesOnTheWay;
    }

    public String toString(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - hh:mm");

        String toReturn = this.get_deliveryId() + ". " + "Scheduled for: " + sdf.format(get_date()) + "\n" +
                "Driver id: " + _driver.get_DriverId() + "\n" +
                "Truck id: " + _truck.get_truckId() + "\n" +
                "Truck weight: " + _weight + "\n" +
                "Shop id: " +_originSite.getId()+"\n" +
                "Shop Address: " +_originSite.getAddress()+"\n" +
                "Shop Contact Name: " +_originSite.getContact_Name()+"\n" +
                "Shop Phone Number: " +_originSite.getPhone_Number()+"\n" +
                "Suppliers: \n" +
                getAllStopsAsString();
        return toReturn;
    }

    private String getAllStopsAsString() {
        String toRet = "";
        for(int i = 0; i < _sitesOnTheWay.size(); i++){
        	Supplier sup=_sitesOnTheWay.get(i).getOrder().getSuplier();
            toRet = toRet+"Supplier Name:"+ sup.getName() +",Address: "+sup.getAddress()+",Contact Name:"+
                    sup.getContacts().get(0).getFirstName()+sup.getContacts().get(0).getLastName()
                    +",Phone Number:"+ sup.getContacts().get(0).getPhoneNumber()
                    +" \nProducts:\n"+
                    getAllProductsPerSite(i);
        }


        return toRet;
    }

    private String getAllProductsPerSite(int i) {
        String toRet = "";
        SiteDoc tempSiteDoc = _sitesOnTheWay.get(i);
        Map<ProvidedProduct,Integer> hm=tempSiteDoc.getOrder().getProducts();
        for(Map.Entry<ProvidedProduct, Integer> pair: hm.entrySet()){

            toRet = toRet + pair.getValue()+ " " + pair.getKey().getProduct_name()+"\n";
        }
        return toRet;
    }

   /* private int get_total_weight() {
        int toRet = 0;
        for(int i = 0 ; i < _sitesOnTheWay.size() ; i++){
            SiteDoc tempSiteDoc = _sitesOnTheWay.get(i);
            List<Pair<Product, Integer>> tempListOfProducts = tempSiteDoc.get_listOfProducts();
            for(int j = 0 ; j < tempListOfProducts.size() ; j++){
                toRet = toRet + (tempListOfProducts.get(j).getLeft().get_productWeight() * tempListOfProducts.get(j).getRight());
            }
        }
        return toRet + _truck.get_netTruckWeight();
    }*/


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
