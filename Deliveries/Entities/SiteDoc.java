package Deliveries.Entities;

import modules.Order;

import java.util.Date;

public class SiteDoc {
	private int siteDocId;
	private Order order;
	private String dateToDestination;
	public SiteDoc(int siteDocId,Order order,String toDestination)
	{
		this.siteDocId = siteDocId;
		this.order=order;
		this.dateToDestination=toDestination;
	}

	public int getSiteDocId()
	{
		return siteDocId;
	}

	public void setSiteDocId(int siteDocId)
	{
		this.siteDocId = siteDocId;
	}

	public Order getOrder()
	{
		return order;
	}

	public void setOrder(Order order)
	{
		this.order = order;
	}

	public String getToDestination()
	{
		return dateToDestination;
	}

	public void setToDestination(String toDestination)
	{
		this.dateToDestination = toDestination;
	}
}
