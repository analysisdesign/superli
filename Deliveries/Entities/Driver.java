package Deliveries.Entities;

public class Driver {
	private int _driverId;
	private LicenseType _licenseType; // A, B or C
	
	public Driver(int _id, LicenseType __licenseType) {
		this._driverId = _id;
		this._licenseType = __licenseType;
	}

	public int get_DriverId() {
		return _driverId;
	}

	public void set_driverId(int _DriverId) {
		this._driverId = _DriverId;
	}

	public LicenseType get_licenseType() {
		return _licenseType;
	}

	public void set_licenseType(LicenseType __licenseType) {
		this._licenseType = __licenseType;
	}
	
	public String toString(){
		String toRet = "";
		toRet = "Driver id: " + get_DriverId() + ", License type: " +
				get_licenseType() ;
		
		return toRet;
	}
}
