package Deliveries.Entities;

public class Site {
	
	private int _siteId;
	private String _siteAddress;
	private int _contactNumber;
	private String _contactName;
	private Region _region;
	private String _typeOfSite; //SUPP for supplier or STORE for STORE
	
	public Site(int siteId, String _siteAddress, int _contactNumber, String _contactName, Region _region, String _typeOfSite) {
		this._siteId = siteId;
		this._siteAddress = _siteAddress;
		this._contactNumber = _contactNumber;
		this._contactName = _contactName;
		this._region = _region;
		this._typeOfSite = _typeOfSite;
	}

	public int get_siteId() {
		return _siteId;
	}

	public void set_siteId(int _siteId) {
		this._siteId = _siteId;
	}

	public String get_siteAddress() {
		return _siteAddress;
	}

	public void set_siteAddress(String _siteAddress) {
		this._siteAddress = _siteAddress;
	}

	public int get_contactNumber() {
		return _contactNumber;
	}

	public void set_contactNumber(int _contactNumber) {
		this._contactNumber = _contactNumber;
	}

	public String get_contactName() {
		return _contactName;
	}

	public void set_contactName(String _contactName) {
		this._contactName = _contactName;
	}

	public Region get_region() {
		return _region;
	}

	public void set_region(Region _region) {
		this._region = _region;
	}

	public String get_typeOfSite() {
		return _typeOfSite;
	}

	public void set_typeOfSite(String _typeOfSite) {
		this._typeOfSite = _typeOfSite;
	}
	
	public String toString(){
		String toRet = "";
		toRet = "Site id: " + get_siteId() +"\n"+
				"Adress: " + get_siteAddress() +"\n"+
				"Contact Number: " + get_contactNumber() + "\n"+
				"Contact Name: " + get_contactName() + "\n"+
				"Region: " +get_region() + "\n";
		return toRet;
	}
}
