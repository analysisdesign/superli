package Deliveries.Entities;


public class Truck {
	
	private int _truckId;
	private String _TruckModel;
	private LicenseType _licenseType;
	private int _netTruckWeight;
	private int _maxTruckWeight;
	
	public Truck(int id, String model, LicenseType licenseType, int netWeight, int maxWeight){
		_truckId = id;
		_TruckModel = model;
		_licenseType = licenseType;
		_netTruckWeight = netWeight;
		_maxTruckWeight = maxWeight;
	}

	public int get_truckId() {
		return _truckId;
	}

	public void set_truckId(int _truckId) {
		this._truckId = _truckId;
	}

	public String get_TruckModel() {
		return _TruckModel;
	}

	public void set_TruckModel(String _TruckModel) {
		this._TruckModel = _TruckModel;
	}

	public LicenseType get_licenseType() {
		return _licenseType;
	}

	public void set_licenseType(LicenseType _licenseType) {
		this._licenseType = _licenseType;
	}

	public int get_netTruckWeight() {
		return _netTruckWeight;
	}

	public void set_netTruckWeight(int _netTruckWeight) {
		this._netTruckWeight = _netTruckWeight;
	}

	public int get_maxTruckWeight() {
		return _maxTruckWeight;
	}

	public void set_maxTruckWeight(int _maxTruckWeight) {
		this._maxTruckWeight = _maxTruckWeight;
	}
	
	public String toString(){
		String toRet = "";
		toRet = "Track id: " + get_truckId() + ", Model: " + get_TruckModel() + ", Net weight: " + get_netTruckWeight() +
				", Max weight: " + get_maxTruckWeight() + ", License type: " + get_licenseType();
		return toRet;
	}
}
