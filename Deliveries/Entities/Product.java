package Deliveries.Entities;

public class Product {
	private int _productId;
	private int _productWeight;
	
	public Product(int _productId, int _productWe) {
		this._productId = _productId;
		this._productWeight = _productWe;
	}
		
	public int get_productId() {
		return _productId;
	}

	public void set_productId(int _productId) {
		this._productId = _productId;
	}

	public int get_productWeight() {
		return _productWeight;
	}

	public void set_productWeight(int _productWeight) {
		this._productWeight = _productWeight;
	}
	
	public String toString(){
		return "Product id: " + get_productId() + ", Product weight: " + get_productWeight();
	}
	
}
