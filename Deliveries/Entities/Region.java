package Deliveries.Entities;

public enum Region {
	NORTH, SOUTH, CENTER, WEST, EAST
}
