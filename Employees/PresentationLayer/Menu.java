package Employees.PresentationLayer;
import Employees.BusinessLayer.InputTesting;
import dal.DataBase;
import modules.Contact;
import modules.Order;
import modules.ProvidedProduct;
import modules.Supplier;
import DataAccessLayer.DBAccess;
import Deliveries.Layers.PLI;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import Adding.AlexFile;

public class Menu
{
    public static DBAccess dbAccess;
    public static void main(String[] args) 
    {
    	dbAccess=new DBAccess();
    	runMenu();
    }
    public static void runMenu()
    {
    	//new AlexFile();
    	/*PLI pli = new PLI(dbAccess);
    	HashMap<Order,Integer> hmap=new HashMap<Order,Integer>();
    	HashMap<ProvidedProduct,Integer> hmap2=new HashMap<ProvidedProduct,Integer>();
    	hmap2.put(new ProvidedProduct("Milky","Elite","10","1","5",4), 10);
    	List<Integer> days=new LinkedList<Integer>();
    	days.add(5);
    	List<Contact> contacts=new LinkedList<Contact>();
    	hmap.put(new Order(hmap2,"4",days,new Supplier("1","danona","5653","hfgj",false,contacts,"gfgh"),1), 5);
    	pli.changeDriversShift(hmap);*/
        Views views=new Views(dbAccess);
        
        while(true)
        {
            int option=0;
            while(option==0)
            {
                System.out.println("1) Press 1 To Enter The System.");
                System.out.println("2) Press 2 To Exit.");
                option=Views.getOptionFromUser(2);
            }
            if(option==2)
            {
                break;
            }
            else
            {
                System.out.println("Please Enter An ID :");
                Scanner in=new Scanner(System.in);
                String id=in.nextLine();
                if(!InputTesting.isValidNumber(id))
                {
                    System.out.println("You Have Entered An Invalid ID, And Returned To The Main Menu.");
                    continue;
                }
                if(!dbAccess.checkIfEmployeeExists(id))
                {
                    System.out.println("The ID Doesn't Exist, You Have Been Returned To The Main Menu.");
                    continue;
                }
                if(!dbAccess.getStatus(id))
                {
                    System.out.println("The Employee Has Been Fired, You Have Been Returned To The Main Menu.");
                    continue;
                }
                int ID=Integer.parseInt(id);
                LinkedList<String> roles=dbAccess.getRoleByID(id);
                if(roles.size()==0)
                {
                    System.out.println("The Employee Has No Role, The System Shut Down.");
                    break;
                }
                String role=roles.get(0);
                for(int i=0;i<roles.size();i++)
                {
                    if(roles.get(i).equals("Stock Keeper") || roles.get(i).equals("Logistics Manager") || roles.get(i).equals("Company Manager") || roles.get(i).equals("Branch Manager") || roles.get(i).equals("Secretary") || roles.get(i).equals("Human Resources Manager"))
                    {
                        role=roles.get(i);
                        break;
                    }
                }
                if(role.equals("Company Manager"))
                {
                    views.ManagementView(id,"Company Manager");
                }
                else
                {
                    if(role.equals("Branch Manager"))
                    {
                        views.ManagementView(id,"Branch Manager");
                    }
                    else
                    {
                        if(role.equals("Secretary"))
                        {
                            views.ManagementView(id,"Secretary");
                        }
                        else
                        {
                            if(role.equals("Human Resources Manager"))
                            {
                                views.HumanResourcesManagerView(ID);
                            }
                            else
                            {
                            	if(role.equals("Stock Keeper"))
                                {
                            		views.StockKeeperView(ID);
                                }
                            	else
                            	{
                            		if(role.equals("Logistics Manager"))
                                    {
                            			//views.logisticsManagerView(dbAccess.getShopID(ID));
                            			views.logisticsManagerView();
                                    }
                            		else
                            		{
                                        views.RegularEmployeesView(ID);
                            		}
                            	}
                            }
                        }
                    }
                }
            }
        }
    }
}
