package Employees.PresentationLayer;

import Deliveries.Entities.LicenseType;
import Deliveries.Layers.PLI;
import Employees.BusinessLayer.*;
import app.main.MainMenu;
import app.main.MainSuppliers;
import dal.DataBase;
import modules.Contact;
import modules.Order;
import modules.ProvidedProduct;
import modules.Supplier;
import DataAccessLayer.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Views
{
	private DBAccess dbAccess;
	private DataBase db;
	public Views(DBAccess dbAccess)
	{
		this.dbAccess=dbAccess;
		db = DataBase.getDataBase();
	}
	public void HumanResourcesManagerView(int HumanID)
	{
		Scanner in=new Scanner(System.in);
		boolean shouldStop=false;
		while(!shouldStop)
		{
			int optionNum=0;
			while(optionNum==0)
			{
				System.out.println("1) Please Enter 1 To Set Weekly Shifts.");
				System.out.println("2) Please Enter 2 To Update Employees Details.");
				System.out.println("3) Please Enter 3 To Show Employee Details.");
				System.out.println("4) Please Enter 4 To Show Shifts History.");
				System.out.println("5) Please Enter 5 To Manage Shifts Positions.");
				System.out.println("6) Please Enter 6 To Exit.");
				optionNum=getOptionFromUser(6);
			}
			switch(optionNum)
			{
			case 1:
				System.out.println("1) Please Enter 1 To Set Weekly Shifts For Regular Employees.");
				System.out.println("2) Exit.");
				int opt=getOptionFromUser(2);
				String date,shift;
				switch (opt)
				{
				case 1:
					System.out.println("Please Enter Shift Date :");
					date=in.nextLine();
					while(!InputTesting.isValidDate(date))
					{
						System.out.println("You Have Entered An Invalid Date, Please Enter A Valid Date :");
						date=in.nextLine();
					}
					System.out.println("Please Enter Shift : (Morning Or Evening)");
					shift=in.nextLine();
					while(!shift.equals("Morning") && !shift.equals("Evening"))
					{
						System.out.println("You Have Entered An Invalid Shift, Please Enter A Valid Shift :");
						shift=in.nextLine();
					}
					if(canAssign(date,shift,HumanID))
					{
						assignEmployeeToShift(date,shift,HumanID);
					}
					break;
				case 2:
					break;
				}
				break;
			case 2:
				String id;
				System.out.println("Please Enter An Employee ID :");
				id = in.nextLine();
				while(!InputTesting.isValidNumber(id))
				{
					System.out.println("You Have Entered An Invalid ID, Please Enter A New One :");
					id=in.nextLine();
				}
				if(!dbAccess.checkIfEmployeeExists(id))
				{
					System.out.println("The ID Does'nt Exists, You Have Been Returned To The Main Menu.");
					continue;
				}
				LinkedList<String> roles=dbAccess.getRoleByID(id);
				for(int i=0;i<roles.size();i++)
				{
					if(roles.get(i).equals("Branch Manager")|| roles.get(i).equals("Company Manager"))
					{
						System.out.println("You Don't Have Permission To Update This Employee.");
						continue;
					}
				}
				changeEmployee(String.valueOf(HumanID),id);
				break;
			case 3:
				System.out.println("Please Enter An Employee ID :");
				id = in.nextLine();
				while(!InputTesting.isValidNumber(id))
				{
					System.out.println("You Have Entered An Invalid ID, Please Enter A New One :");
					id=in.nextLine();
				}
				if(!dbAccess.checkIfEmployeeExists(id))
				{
					System.out.println("The ID Does'nt Exists, You Have Been Returned To The Main Menu.");
					continue;
				}
				printDetailsOfEmployee(id);
				break;
			case 4:
				String part;
				System.out.println("Please Enter Shift Date :");
				date=in.nextLine();
				while(!InputTesting.isValidDate(date))
				{
					System.out.println("You Have Entered An Invalid Date, Please Enter A Valid Date :");
					date=in.nextLine();
				}
				System.out.println("Please Enter Shift Type:(Morning Or Evening)");
				part=in.nextLine();
				while(!InputTesting.isValidName(part))
				{
					System.out.println("You Have Entered An Invalid Type, Please Enter A Valid Type :");
					part=in.nextLine();
				}
				LinkedList lst= dbAccess.getShift(new Shift(date,part));
				if(lst.size()==0)
				{
					System.out.println("There Is No Shift In This Date And Day Part.");
				}
				for(int i=0;i<lst.size();i+=4)
				{
					System.out.println("First Name :"+lst.get(i));
					System.out.println("Last Name :"+lst.get(i+1));
					System.out.println("ID :"+lst.get(i+2));
					System.out.println("Role :"+lst.get(i+3));
				}
				break;
			case 5:
				boolean stopMe=false;
				while(!stopMe)
				{
					System.out.println("1) Please Enter 1 To Insert New Shift Position.");
					System.out.println("2) Please Enter 2 To Update Shift Position.");
					System.out.println("3) Please Enter 3 To Delete Shift Position.");
					System.out.println("4) Please Enter 4 To Exit.");
					optionNum=getOptionFromUser(4);
					switch (optionNum)
					{
					case 1:
						String count;
						int num;
						System.out.println("Please Enter Shift Part : (Morning Or Evening)");
						part=in.nextLine();
						while(!InputTesting.isValidName(part))
						{
							System.out.println("You Have Entered An Invalid Part, Please Enter A Valid Part :");
							part=in.nextLine();
						}
						System.out.println("Please Enter A Role: ");
						String role=in.nextLine();
						while(!dbAccess.checkExistingRole(role) || role.equals("Secretary") || role.equals("Branch Manager") || role.equals("Company Manager") || role.equals("Human Resources Manager"))
						{
							System.out.println("You Have Entered An Invalid Role, Please Enter A Valid Role :");
							PrintSimpleRoles();
							role=in.nextLine();
						}
						System.out.println("Please Enter A Role Count: ");
						count=in.nextLine();
						while(!InputTesting.isValidNumber(count))
						{
							System.out.println("You Have Entered An Invalid Count, Please Enter A Valid Count :");
							count=in.nextLine();
						}
						num=Integer.parseInt(count);
						dbAccess.insertStandard(part,new Role(role),num);
						break;
					case 2:
						System.out.println("Please Enter Shift Part : (Morning Or Evening)");
						part=in.nextLine();
						while(!InputTesting.isValidName(part))
						{
							System.out.println("You Have Entered An Invalid Part, Please Enter A Valid Part :");
							part=in.nextLine();
						}
						System.out.println("Please Enter A Role: ");
						role=in.nextLine();
						while(!InputTesting.isValidName(role))
						{
							System.out.println("You Have Entered An Invalid Role, Please Enter A Valid Role :");
							role=in.nextLine();
						}
						System.out.println("Please Enter A Role Count: ");
						count=in.nextLine();
						while(!InputTesting.isValidNumber(count))
						{
							System.out.println("You Have Entered An Invalid Count, Please Enter A Valid Count :");
							count=in.nextLine();
						}
						num=Integer.parseInt(count);
						dbAccess.updateStandard(part,new Role(role),num);
						break;
					case 3:
						System.out.println("Please Enter Shift Part : (Morning Or Evening)");
						part=in.nextLine();
						while(!InputTesting.isValidName(part))
						{
							System.out.println("You Have Entered An Invalid Part, Please Enter A Valid Part :");
							part=in.nextLine();
						}
						System.out.println("Please Enter A Role: ");
						role=in.nextLine();
						while(!InputTesting.isValidName(role))
						{
							System.out.println("You Have Entered An Invalid Role, Please Enter A Valid Role :");
							role=in.nextLine();
						}
						dbAccess.eraseStandard(part,new Role(role));
						break;
					case 4:
						stopMe=true;
						break;
					}
				}
				break;
			case 6:
				shouldStop=true;
			}
		}
	}
	private boolean canAssign(String date,String shift,int HumanID)
	{
		LinkedList employees;
		LinkedList<String> standards=dbAccess.getStandards(shift);
		for(int i=0;i<standards.size();i+=2)
		{
			employees= dbAccess.getEmployeesForShift(new Shift(date,shift),new Role(standards.get(i)),HumanID);
			if(employees.size()/3<Integer.parseInt(standards.get(i+1)))
			{
				System.out.println("There Are "+(employees.size()/3)+" Available "+standards.get(i)+", "+standards.get(i+1)+" Are Needed");
				return false;
			}
		}
		return true;
	}
	private void assignEmployeeToShift(String date,String shift,int HumanID)
	{
		Scanner in=new Scanner(System.in);
		LinkedList employees=null;
		String employeeID;
		LinkedList<String> standards=dbAccess.getStandards(shift);
		for(int i=0;i<standards.size();i+=2)
		{
			employees= dbAccess.getEmployeesForShift(new Shift(date,shift),new Role(standards.get(i)),HumanID);
			System.out.println("The Role is :"+standards.get(i));
			printEmployees(employees);
			System.out.println("Please Enter "+standards.get(i+1)+" "+standards.get(i)+" :");
			for(int h=0;h<Integer.parseInt(standards.get(i+1));h++)
			{
				employeeID=in.nextLine();
				boolean exist=false,inShift=false;
				while (true)
				{
					exist=checkExistingEmployee(employees,employeeID);
					inShift=dbAccess.checkIfEmpInShift(employeeID,new Shift(date,shift));
					if(!exist)
					{
						System.out.println("There Is No Employee With That ID.");
					}
					if(inShift)
					{
						System.out.println("This Employee Already Assigned To This Shift.");
					}
					if(inShift || !exist)
					{
						employeeID=in.nextLine();
					}
					else
					{
						break;
					}
				}
				if(standards.get(i).equals(("Driver")))
				{
					if(!dbAccess.ifDriverAlreadyAssigned(new Shift(date,shift),employeeID))
					{
						dbAccess.AssignEmployeeToShift(employeeID,new Shift(date,"Morning"),new Role(standards.get(i)));
						dbAccess.AssignEmployeeToShift(employeeID,new Shift(date,"Evening"),new Role(standards.get(i)));
					}
				}
				else
				{
					dbAccess.AssignEmployeeToShift(employeeID,new Shift(date,shift),new Role(standards.get(i)));
				}

			}
		}
		System.out.println("The Shift Assignment Passed Successfully.");
	}
	public void RegularEmployeesView(int ID)
	{
		Scanner in=new Scanner(System.in);
		int optionNum;
		while(true)
		{
			optionNum=0;
			while(optionNum==0)
			{
				System.out.println("1) Please Enter 1 To Add Your Weekly Shifts.");
				System.out.println("2) Please Enter 2 To Exit.");
				optionNum=getOptionFromUser(2);
			}
			switch(optionNum)
			{
			case 1:
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				String date;
				String shift;
				Date dt = new Date();
				Calendar c = Calendar.getInstance();
				c.add(Calendar.DATE, 1);
				int i=0;
				while(i<7)
				{
					date=df.format(c.getTime());
					System.out.println(date);
					System.out.println("Please Enter Available Shift : (Morning Or Evening Or Both Or None)");
					shift=in.nextLine();
					while(!shift.equals("Morning") && !shift.equals("Evening") && !shift.equals("Both") && !shift.equals("None"))
					{
						System.out.println("Please Enter Available Shift : (Morning Or Evening Or Both Or None)");
						shift=in.nextLine();
					}
					if(shift.equals("Both"))
					{
						dbAccess.addConstraints(ID,new Constraint(date,"Morning"));
						dbAccess.addConstraints(ID,new Constraint(date,"Evening"));
					}
					else
					{
						if(!shift.equals("None"))
						{
							dbAccess.addConstraints(ID,new Constraint(date,shift));
						}
					}
					c.add(Calendar.DATE, 1);
					i++;
				}
				break;
			case 2:
				return;
			}
		}
	}

	public void ManagementView(String admin,String rolement)
	{
		boolean shouldStop=false;
		Scanner in=new Scanner(System.in);
		while(!shouldStop)
		{
			int optionNum=0;
			while(optionNum==0)
			{
				System.out.println("1) Please Enter 1 To Add A New Employee.");
				System.out.println("2) Please Enter 2 To Fire An Employee.");
				System.out.println("3) Please Enter 3 To Update Employee Details.");
				System.out.println("4) Please Enter 4 To Show Employee Details.");
				System.out.println("5) Please Enter 5 To Exit.");
				optionNum=getOptionFromUser(5);
			}
			switch(optionNum)
			{
			case 1:
				String id,date,firstName,lastName,Bank,Branch,AccountNumber,role,Monthly;
				Integer BranchNum,AccountNum;
				Double MonthlyPay;
				System.out.println("Please Enter An Employee ID :");
				id = in.nextLine();
				while(!InputTesting.isValidNumber(id))
				{
					System.out.println("You Have Entered An Invalid ID, Please Enter A New One :");
					id=in.nextLine();
				}
				if(dbAccess.checkIfEmployeeExists(id))
				{
					System.out.println("The ID Exists, You Have Been Returned To The Main Menu.");
					continue;
				}
				System.out.println("Please Enter A First Name :");
				firstName = in.nextLine();
				while(!InputTesting.isValidName(firstName))
				{
					System.out.println("You Have Entered An Invalid Name, Please Enter A New One :");
					firstName=in.nextLine();
				}
				System.out.println("Please Enter A Last Name :");
				lastName = in.nextLine();
				while(!InputTesting.isValidName(lastName))
				{
					System.out.println("You Have Entered An Invalid Name, Please Enter A New One :");
					lastName=in.nextLine();
				}
				System.out.println("Please Enter A Bank Name :");
				Bank = in.nextLine();
				while(!InputTesting.isValidName(Bank))
				{
					System.out.println("You Have Entered An Invalid Bank Name, Please Enter A New One :");
					Bank=in.nextLine();
				}
				System.out.println("Please Enter A Bank Branch :");
				Branch = in.nextLine();
				while(!InputTesting.isValidNumber(Branch))
				{
					System.out.println("You Have Entered An Invalid Branch, Please Enter A New One :");
					Branch=in.nextLine();
				}
				BranchNum = Integer.parseInt(Branch);
				System.out.println("Please Enter A Account Number :");
				AccountNumber = in.nextLine();
				while(!InputTesting.isValidNumber(AccountNumber))
				{
					System.out.println("You Have Entered An Invalid Account Number, Please Enter A New One :");
					AccountNumber=in.nextLine();
				}
				AccountNum = Integer.parseInt(AccountNumber);
				System.out.println("Please Enter A Monthly Payment :");
				Monthly = in.nextLine();
				while(!InputTesting.isValidMonthlyPayment(Monthly))
				{
					System.out.println("You Have Entered An Invalid Monthly Payment, Please Enter A New One :");
					Monthly=in.nextLine();
				}
				MonthlyPay = Double.parseDouble(Monthly);
				System.out.println("Please Enter A Starting Date :");
				date=in.nextLine();
				while(!InputTesting.isValidDate(date))
				{
					System.out.println("You Have Entered An Invalid Date, Please Enter a valid Date :");
					date=in.nextLine();
				}
				System.out.println("Please Enter Site Id :");
				int siteID;
				String siteIDInput;
				while(true)
				{
					siteIDInput=in.nextLine();
					if(!InputTesting.isValidNumber(siteIDInput))
					{
						System.out.println("You Have Entered An Invalid Site Id, Please Enter a valid Site Id :");
						continue;
					}
					siteID=Integer.parseInt(siteIDInput);
					if(dbAccess.getSite(siteID)==null)
					{
						System.out.println("You Have Entered A Non Existent Site ID, Please Enter An Existent Site ID :");
						continue;
					}
					break;
				}
				Employee emp=new Employee(Integer.parseInt(id),firstName,lastName,Bank,BranchNum,AccountNum,MonthlyPay,date, siteID);
				dbAccess.addEmployee(emp);
				System.out.println("Please Enter The Optional Roles Of Employee : (Enter 0 For End)");
				PrintRoles();
				while(true)
				{
					role=in.nextLine();
					if(role.equals("0"))
					{
						System.out.println("The Employee Was Added Successfully.");
						break;
					}
					else
					{
						if(!InputTesting.isValidName(role))
						{
							System.out.println("You Have Entered An Invalid Role, Please Enter A Valid Role :");
							continue;
						}
						else
						{
							if(dbAccess.checkExistingRole(role))
							{
								if(role.equals("Company Manager") && !rolement.equals("Company Manager"))
								{
									System.out.println("You Don't Have The Permission To Add A Company Manager.");
									break;
								}
								if(role.equals("Branch Manager") && !rolement.equals("Company Manager"))
								{
									System.out.println("You Don't Have The Permission To Add A Branch Manager.");
									break;
								}
								if(role.equals("Secretary") && !(rolement.equals("Company Manager")|| rolement.equals("Branch Manager")))
								{
									System.out.println("You Don't Have The Permission To Add A Secretary.");
									break;
								}
								if(role.equals("Driver"))
								{
									String license;
									System.out.println("Please Enter A License Type:");
									while(true)
									{
										license=in.nextLine();
										if(!(license.equals("A")|| license.equals("B") ||license.equals("C")))
										{
											System.out.println("Please Enter A License Type:(A Or B Or C)");
											continue;
										}
										break;
									}
									dbAccess.addDriver(Integer.parseInt(id), LicenseType.valueOf(license));
								}
								if(dbAccess.addRoleToID(id,new Role(role)))
								{
									System.out.println("The Role Was Added Successfully.");
								}
								else
								{
									System.out.println("There Was A Problem.");
								}
								System.out.println("Please Enter The Optional Roles Of Employee : (Enter 0 For End)");
							}
							else
							{
								System.out.println("The Role Doen't Exist");
							}
						}
					}
				}
				break;
			case 2:
				System.out.println("Please Enter An Employee ID :");
				id = in.nextLine();
				while(!InputTesting.isValidNumber(id))
				{
					System.out.println("You Have Entered An Invalid ID, Please Enter A New One :");
					id=in.nextLine();
				}
				if(!dbAccess.checkIfEmployeeExists(id))
				{
					System.out.println("The ID Doesn't Exist, You Have Been Returned To The Main Menu.");
					return;
				}
				if(!dbAccess.getStatus(id))
				{
					System.out.println("The Employee Has Been Fired Already.");
					return;
				}
				LinkedList<String> roles=dbAccess.getRoleByID(id);
				role= roles.get(0);
				for(int i=0;i<roles.size();i++)
				{
					if(roles.get(i).equals("Secretary") || roles.get(i).equals("Branch Manager")|| roles.get(i).equals("Company Manager"))
					{
						role=roles.get(i);
						break;
					}
				}
				if(role.equals("Company Manager") && !rolement.equals("Company Manager"))
				{
					System.out.println("You Don't Have The Permission To Fire A Company Manager.");
					break;
				}
				if(role.equals("Branch Manager") && !rolement.equals("Company Manager"))
				{
					System.out.println("You Don't Have The Permission To Fire A Branch Manager.");
					break;
				}
				if(role.equals("Secretary") && !(rolement.equals("Company Manager")|| rolement.equals("Branch Manager")))
				{
					System.out.println("You Don't Have The Permission To Fire A Secretary.");
					break;
				}
				if(id.equals(admin))
				{
					System.out.println("You Can't Change Your Own Status");
					break;
				}
				if(!dbAccess.FireEmployee(id))
				{
					System.out.println("The Fire Did Not Work.");
				}
				else
				{
					System.out.println("Please Enter Leaving Date:");
					String date1;
					while(true)
					{
						date1=in.nextLine();
						if(!InputTesting.isValidDate(date1))
						{
							System.out.println("Please Enter A Valid Leaving Date:");
						}
						break;
					}
					dbAccess.FireEmployee(id);
					dbAccess.updateLeavingDate(date1,id);
					System.out.println("The Employee Has Been Successfully Fired.");
				}
				break;
			case 3:
				System.out.println("Please Enter An Employee ID :");
				id = in.nextLine();
				while(!InputTesting.isValidNumber(id))
				{
					System.out.println("You Have Entered An Invalid ID, Please Enter A New One :");
					id=in.nextLine();
				}
				if(!dbAccess.checkIfEmployeeExists(id))
				{
					System.out.println("The ID Does'nt Exists, You Have Been Returned To The Main Menu.");
					continue;
				}
				roles=dbAccess.getRoleByID(id);
				role= roles.get(0);
				for(int i=0;i<roles.size();i++)
				{
					if(roles.get(i).equals("Secretary") || roles.get(i).equals("Branch Manager")|| roles.get(i).equals("Company Manager"))
					{
						role=roles.get(i);
						break;
					}
				}
				if(role.equals("Company Manager") && !rolement.equals("Company Manager"))
				{
					System.out.println("You Don't Have The Permission To Add A Company Manager.");
					break;
				}
				if(role.equals("Branch Manager") && !rolement.equals("Company Manager"))
				{
					System.out.println("You Don't Have The Permission To Add A Branch Manager.");
					break;
				}
				if(role.equals("Secretary") && !(rolement.equals("Company Manager")|| rolement.equals("Branch Manager")))
				{
					System.out.println("You Don't Have The Permission To Add A Secretary.");
					break;
				}
				changeEmployee(admin,id);
				break;
			case 4:
				System.out.println("Please Enter An Employee ID :");
				id = in.nextLine();
				while(!InputTesting.isValidNumber(id))
				{
					System.out.println("You Have Entered An Invalid ID, Please Enter A New One :");
					id=in.nextLine();
				}
				if(!dbAccess.checkIfEmployeeExists(id))
				{
					System.out.println("The ID Does'nt Exists, You Have Been Returned To The Main Menu.");
					continue;
				}
				printDetailsOfEmployee(id);
				break;
			case 5:
				shouldStop=true;
				break;
			}
		}
	}
	public static int getOptionFromUser(int upperBound)
	{
		String optionStr;
		int optionNum;
		Scanner in=new Scanner(System.in);
		while(true)
		{
			optionStr=in.nextLine();
			if(InputTesting.isValidNumber(optionStr))
			{
				optionNum=Integer.parseInt(optionStr);
				if((optionNum>=1 && optionNum<=upperBound))
				{
					return optionNum;
				}
				else
				{
					System.out.println("You Have Entered An Invalid Number, Please Enter A Number :");
				}
			}
			else
			{
				System.out.println("You Haven't Entered A Number, Please Enter A Number :");
			}
		}
	}
	private boolean checkExistingEmployee(LinkedList ls,String id)
	{
		for(int i=0;i<ls.size();i+=3)
		{
			if(ls.get(i).equals(id))
			{
				return true;
			}
		}
		return false;
	}



	private void changeEmployee(String changerID,String id)
	{
		Scanner in=new Scanner(System.in);
		boolean shouldStop =false;
		int optionNum;
		while (!shouldStop)
		{
			System.out.println("1) Please Enter 1 To Update Employee First Name.");
			System.out.println("2) Please Enter 2 To Update Employees Last Name.");
			System.out.println("3) Please Enter 3 To Update Employees Bank.");
			System.out.println("4) Please Enter 4 To Update Employees Branch.");
			System.out.println("5) Please Enter 5 To Update Employees Account Number.");
			System.out.println("6) Please Enter 6 To Update Employees Monthly Payment.");
			System.out.println("7) Please Enter 7 To Update Employees Employment Terms.");
			System.out.println("8) Please Enter 8 To Update Employees Site Id.");
			System.out.println("9) Please Enter 9 To Rehire Employees.");
			System.out.println("10) Please Enter 10 To Add Employees Role.");
			System.out.println("11) Please Enter 11 To Exit.");
			optionNum=getOptionFromUser(11);

			switch (optionNum)
			{
			case 1:
				String firstName,lastName,bank,branch,number,pay,manager,dayLength,restingDay,pension,foundation,terms;
				System.out.println("Please Enter A First Name :");
				firstName = in.nextLine();
				while(!InputTesting.isValidName(firstName))
				{
					System.out.println("You Have Entered An Invalid Name, Please Enter A New One :");
					firstName=in.nextLine();
				}
				dbAccess.updateFirstName(firstName,id);
				break;
			case 2:
				System.out.println("Please Enter A Last Name :");
				lastName = in.nextLine();
				while(!InputTesting.isValidName(lastName))
				{
					System.out.println("You Have Entered An Invalid Name, Please Enter A New One :");
					lastName=in.nextLine();
				}
				dbAccess.updateLastName(lastName,id);
				break;

			case 3:
				System.out.println("Please Enter A Bank Name :");
				bank = in.nextLine();
				while(!InputTesting.isValidName(bank))
				{
					System.out.println("You Have Entered An Invalid Name, Please Enter A New One :");
					bank=in.nextLine();
				}
				dbAccess.updateBank(bank,id);
				break;

			case 4:
				System.out.println("Please Enter A Branch Number :");
				branch = in.nextLine();
				while(!InputTesting.isValidNumber(branch))
				{
					System.out.println("You Have Entered An Invalid Number, Please Enter A New One :");
					branch=in.nextLine();
				}
				dbAccess.updateBranch(Integer.parseInt(branch),id);
				break;

			case 5:
				System.out.println("Please Enter A Account Number :");
				number = in.nextLine();
				while(!InputTesting.isValidNumber(number))
				{
					System.out.println("You Have Entered An Invalid Number, Please Enter A New One :");
					number=in.nextLine();
				}
				dbAccess.updateAccountNumber(Integer.parseInt(number),id);
				break;

			case 6:
				System.out.println("Please Enter A Monthly Payment:");
				pay = in.nextLine();
				while(!InputTesting.isValidMonthlyPayment(pay))
				{
					System.out.println("You Have Entered An Invalid Number, Please Enter A New One :");
					pay=in.nextLine();
				}
				dbAccess.updateMonthlyPayment(Double.parseDouble(pay),id);
				break;

			case 7:
				System.out.println("Please Enter Manager Of The Employee :");
				manager = in.nextLine();
				while(!InputTesting.isValidName(manager))
				{
					System.out.println("You Have Entered An Invalid Manager, Please Enter A New One :");
					manager=in.nextLine();
				}
				System.out.println("Please Enter Day Length Of The Employee :");
				dayLength = in.nextLine();
				while(!InputTesting.isValidNumber(dayLength))
				{
					System.out.println("You Have Entered An Invalid Day Length, Please Enter A New One :");
					dayLength=in.nextLine();
				}
				System.out.println("Please Enter Resting Day Of The Employee :");
				restingDay = in.nextLine();
				while(!InputTesting.isValidName(restingDay))
				{
					System.out.println("You Have Entered An Invalid Resting Day, Please Enter A New One :");
					restingDay=in.nextLine();
				}
				System.out.println("Please Enter Pension Of The Employee :");
				pension = in.nextLine();
				while(!InputTesting.isValidName(pension))
				{
					System.out.println("You Have Entered An Invalid Pension, Please Enter A New One :");
					pension=in.nextLine();
				}
				System.out.println("Please Enter Foundation Of The Employee :");
				foundation = in.nextLine();
				while(!InputTesting.isValidName(foundation))
				{
					System.out.println("You Have Entered An Invalid Foundation, Please Enter A New One :");
					foundation=in.nextLine();
				}
				terms="Employer: Super-Lee\n"+"Manager: "+manager+"\nDay Length: "+dayLength+"\nResting Day: "+restingDay+"\nPension: "+pension+"\nFoundation: "+foundation;
				dbAccess.updateEmploymentTerms(terms,id);
				break;
			case 8:
				System.out.println("Please Enter Site Id :");
				int siteID;
				String siteIDInput;
				while(true)
				{
					siteIDInput=in.nextLine();
					if(InputTesting.isValidNumber(siteIDInput))
					{
						System.out.println("You Have Entered An Invalid Site Id, Please Enter a valid Site Id :");
						continue;
					}
					siteID=Integer.parseInt(siteIDInput);
					if(dbAccess.getSite(siteID)==null)
					{
						System.out.println("You Have Entered A Non Existent Site ID, Please Enter An Existent Site ID :");
						continue;
					}
					break;
				}
				dbAccess.updateSiteID(siteID,id);
				break;
			case 9:
				if(changerID.equals(id))
				{
					System.out.println("You Can't Change Your Own Status");
				}
				else
				{
					if(dbAccess.getStatus(id)==false)
					{
						dbAccess.RehireEmployee(id);
						dbAccess.updateLeavingDate(null,id);
					}
					else
					{
						System.out.println("You Cant Rehire An Already Working Employee.");
					}

				}
				break;
			case 10:
				PrintOptionalRolesToAdd(id);
				String role=in.nextLine();
				while(!checkIfRoleIsValid(id,role) || !dbAccess.checkExistingRole(role))
				{
					System.out.println("You Have Entered An Invalid role, Please Enter A New One :");
					role=in.nextLine();
				}
				dbAccess.addRoleToID(id,new Role(role));
				System.out.println("The Role Was Added Successfully.");
			case 11:
				shouldStop=true;
			}
		}
	}

	public void StockKeeperView(int ID)
	{
		while(true)
		{
			System.out.println("Press 1 For Inventory");
			System.out.println("Press 2 For Suppliers");
			System.out.println("Press 3 For Orders");
			System.out.println("Press 4 For Determine Constraints");
			System.out.println("Press 5 For Exit");
			int option=getOptionFromUser(5);
			if(option==5)
			{
				break;
			}
			if(option==1)
			{
				MainSuppliers.MainMenuInventory(dbAccess.getShopID(ID));
				continue;
			}
			if(option==2)
			{
				MainSuppliers.MainMenuSuppliers();
				continue;
			}
			if(option==3)
			{
				MainSuppliers.MainMenuOrder(dbAccess.getShopID(ID));
				continue;
			}
			if(option==4)
			{
				RegularEmployeesView(ID);
			}
		}

	}
	public void logisticsManagerView()
	{
		while(true)
		{
			System.out.println("Press 1 For Deliveries");
			System.out.println("Press 2 For Automatic Delivries Adding");
			System.out.println("Press 3 For Exit");
			int option=getOptionFromUser(3);
			if(option==3)
			{
				break;
			}
			if(option==1)
			{
				PLI p = new PLI(dbAccess);
				p.runMenu();
				continue;
			}
			if(option==2)
			{
				updateSystem(dbAccess);
				continue;
			}
		}
	}
	public void branchManagerView(int shopID)
	{
		boolean cont=true;
		while(cont)
		{
			System.out.println("Press 1 For Suppliers");
			System.out.println("Press 2 To Show Employee Details.");
			System.out.println("Press 3 To Show Shifts History.");
			System.out.println("Press 4 To View previous deliveries");
			System.out.println("Press 5 To View Orders");
			System.out.println("Press 6 To View Inventory");
			System.out.println("Press 7 To View Suppliers");
			System.out.println("Press 8 For Exit");
			int option=getOptionFromUser(8);
			switch (option){
			case 1:
				MainSuppliers.MainMenuSuppliers();
				break;
			case 2:
				Scanner in=new Scanner(System.in);
				System.out.println("Please Enter An Employee ID :");
				String id = in.nextLine();
				while(!InputTesting.isValidNumber(id))
				{
					System.out.println("You Have Entered An Invalid ID, Please Enter A New One :");
					id=in.nextLine();
				}
				if(!dbAccess.checkIfEmployeeExists(id))
				{
					System.out.println("The ID Does'nt Exists, You Have Been Returned To The Main Menu.");
					break;
				}
				printDetailsOfEmployee(id);
				break;
			case 3:
				String part;
				Scanner in1=new Scanner(System.in);
				System.out.println("Please Enter Shift Date :");
				String date=in1.nextLine();
				while(!InputTesting.isValidDate(date))
				{
					System.out.println("You Have Entered An Invalid Date, Please Enter A Valid Date :");
					date=in1.nextLine();
				}
				System.out.println("Please Enter Shift Type:(Morning Or Evening)");
				part=in1.nextLine();
				while(!InputTesting.isValidName(part))
				{
					System.out.println("You Have Entered An Invalid Type, Please Enter A Valid Type :");
					part=in1.nextLine();
				}
				LinkedList lst= dbAccess.getShift(new Shift(date,part));
				if(lst.size()==0)
				{
					System.out.println("There Is No Shift In This Date And Day Part.");
				}
				for(int i=0;i<lst.size();i+=4)
				{
					System.out.println("First Name :"+lst.get(i));
					System.out.println("Last Name :"+lst.get(i+1));
					System.out.println("ID :"+lst.get(i+2));
					System.out.println("Role :"+lst.get(i+3));
				}
				break;
			case 4:
				Scanner in2=new Scanner(System.in);
				System.out.println("Enter the delivery ID:");
				int delID;
				while(true){
					try{
						delID = Integer.parseInt(in2.nextLine());
						break;
					}catch (Exception e) {
						System.out.println("Incorrect delivery ID. re-enter.");
					}
				}
				if(!PLI.printDeliveryDetails(delID,dbAccess))
					System.out.println("Such delivery ID doesn't exists");
				break;
			case 5: //Orders menu 
				viewOrdersMenu(shopID);
				break;
			case 6: //Inventory Menu
				viewInventoryMenu(shopID);
				break;
			case 7: //Suppliers Menu
				viewSuppliersMenu();
				break;
			case 8: //Exit
				cont=false;
				break;
			}
		}
	}
	private void viewSuppliersMenu() {
		Scanner sc=new Scanner(System.in);
		DataBase db = DataBase.getDataBase();
		boolean cont=true;
		do {
			System.out.println("Press 1 To supplier details by ID");
			System.out.println("Press 2 To all suppliers");
			System.out.println("Press 3 For Exit");
			int option=getOptionFromUser(3);
			switch (option) {
			case 1: //supplier details by ID
				getSupplierMenu();
				break;
			case 2: //all suppliers
				List<Supplier> suppliers = db.getAllSuppliers();
				for (Supplier supplier : suppliers) {
					System.out.println(supplier);
					System.out.println();
				}
				System.out.println();
				break;
			case 3:
				cont=false;
				break;
			}
		} while (cont);
	}

	private void getSupplierMenu() {
		Scanner sc=new Scanner(System.in);
		DataBase db = DataBase.getDataBase();
		System.out.println("Insert supplier ID");
		String input = sc.nextLine();
		Supplier ans = db.getSupplierById(input);
		if (ans == null) { // validate input
			System.out.println("ID dosen't exist");
			System.out.println();
			return;
		}
		System.out.println();
		System.out.println();
		detailsMenu(ans);
	}

	private void detailsMenu(Supplier s) {
		Scanner sc=new Scanner(System.in);
		DataBase db = DataBase.getDataBase();
		boolean cont=true;
		do{
			System.out.println(s);
			System.out.println();
			System.out.println("Press 1 To View contacts");
			System.out.println("Press 2 To View all products");
			System.out.println("Press 3 To View current orders");
			System.out.println("Press 4 For Exit");
			String input = sc.nextLine();
			switch (input) {
			case "1": //view contacts
				List<Contact> contacts=db.getAllContactsBySupplierID(s.getID());
				for (Contact contact : contacts) {
					System.out.println(contact);
					System.out.println();
				}
				System.out.println();
				break;
			case "2": //view products
				List<ProvidedProduct> providedProucts=db.getAllProducts(s);
				for (ProvidedProduct providedProduct : providedProucts) {
					System.out.println(providedProduct);
					System.out.println();
				}
				System.out.println();
				break;
			case "3": //view orders
				List<Order> orders = db.getAllOrders(s);
				for (Order order : orders) {
					System.out.println(order);
					System.out.println();
				}
				System.out.println();
				break;
			case "4": 
				cont = false;
				break;
			}
		} while(cont);
	}

	private void viewInventoryMenu(int shopID) {
		Scanner sc=new Scanner(System.in);
		DataBase db = DataBase.getDataBase();
		boolean cont=true;
		do {
			System.out.println("Press 1 To Show General inventory report");
			System.out.println("Press 2 To Show Categorized inventory report by a specific category");
			System.out.println("Press 3 To Show Categorized inventory report by multiple categories");
			System.out.println("Press 4 To Show damaged invenotry report");
			System.out.println("Press 5 To Show price for product");
			System.out.println("Press 6 To Show all product details");
			System.out.println("Press 7 For Exit");
			int option=getOptionFromUser(7);
			switch (option) {
			case 1: //General inventory report
				System.out.println();
				System.out.println();
				db.printGeneralInventoryReport(shopID);
				break;
			case 2: //Categorized inventory specific
				System.out.println("Please enter a category");
				System.out.println("Options:");
				db.printAllCategories();
				if(!db.printSpecificCategorizedReport(sc.nextLine(),shopID)){
					System.out.println("Category does'nt exist! enter any key to continue to main menu");
					sc.nextLine();
				}
				break;
			case 3: //Categorized inventory multiple
				System.out.println("Please enter the wanted categories in the next format: 'category1,category2,category3...'");
				System.out.println("Options:");
				db.printAllCategories();
				if(!db.printMultipleCategorizedReport(sc.nextLine(),shopID)){
					sc.nextLine();
				}
				break;
			case 4: // Damaged report
				db.printDamagedReport(shopID);
				break;
			case 5:// Show price for product
				System.out.println("Please enter product in the next format: 'product name,manufacturer'");
				db.printProductPrice(sc.nextLine());
				break;
			case 6:// show IDs 
				System.out.println("Please enter product in the next format: 'product name,manufacturer'");
				db.printProductWithID(sc.nextLine(),shopID);
				break;
			case 7:
				cont=false;
				break;
			}
		} while (cont);
	}
	private void viewOrdersMenu(int shopID) {
		Scanner sc=new Scanner(System.in);
		DataBase db = DataBase.getDataBase();
		boolean cont=true;
		do {
			System.out.println("Press 1 To Show all orders");
			System.out.println("Press 2 To Print order by ID");
			System.out.println("Press 3 For Exit");
			int option=getOptionFromUser(3);
			switch (option) {
			case 1:
				System.out.println();
				System.out.println();
				db.printAllOrders(shopID);
				break;
			case 2:
				System.out.println();
				System.out.println();
				System.out.println("Please enter the order number");
				String orderNum = sc.nextLine();
				if(db.checkIfOrderExistsByOrderNumber(orderNum,shopID))
					db.printOrder(orderNum,shopID);
				break;
			case 3:
				cont=false;
				break;
			}
		} while (cont);
	}

	private void printEmployees(LinkedList<String> employees)
	{
		System.out.println("The Matching Employees Are :");
		for(int j=0;j<employees.size();j+=3)
		{
			System.out.println((j/3)+1+") ");
			System.out.println("ID :"+employees.get(j));
			System.out.println("First Name :"+employees.get(j+1));
			System.out.println("Last Name :"+employees.get(j+2));
		}
	}

	private void printDetailsOfEmployee(String ID)
	{
		int j=9;
		System.out.println("The Details Are :");
		LinkedList ls=dbAccess.getEmployeeDetails(ID);
		System.out.println("ID : "+ID);
		System.out.println("First Name : "+ls.get(1));
		System.out.println("Last Name : "+ls.get(2));
		System.out.println("Bank : "+ls.get(3));
		System.out.println("Branch : "+ls.get(4));
		System.out.println("Account Number : "+ls.get(5));
		System.out.println("Monthly Payment : "+ls.get(6));
		System.out.println("Starting Date : "+ls.get(7));
		System.out.println("Status : "+ls.get(8));
		if(ls.get(8).equals("false"))
		{
			System.out.println("Leaving Date : "+ls.get(9));
			j++;
		}
		System.out.println("Employment Terms : "+ls.get(j));
		System.out.println("Site Id : "+ls.get(j+1));
		System.out.print("Roles :( ");
		for(int i=j+2;i<ls.size()-1;i++)
		{
			System.out.print(ls.get(i)+", ");
		}
		System.out.println(ls.get(ls.size()-1)+")");
	}


	private void PrintRoles()
	{
		LinkedList<String> roles=dbAccess.getOptionalRoles();
		System.out.print("(");
		for(int i=0;i<roles.size()-1;i++)
		{
			System.out.print(roles.get(i)+", ");

		}
		System.out.println(roles.get(roles.size()-1)+")");
	}

	private void PrintSimpleRoles()
	{
		LinkedList<String> roles=dbAccess.getOptionalRoles();
		System.out.print("(");
		for(int i=0;i<roles.size();i++)
		{
			if(!roles.get(i).equals("Secretary") &&!roles.get(i).equals("Branch Manager") &&!roles.get(i).equals("Company Manager") &&!roles.get(i).equals("Human Resources Manager"))
			{
				System.out.print(roles.get(i)+", ");
			}
		}
		System.out.println(")");
	}

	private void PrintOptionalRolesToAdd(String id)
	{
		LinkedList<String> roles=dbAccess.getOptionalRolesToAdd(id);
		System.out.print("(");
		for(int i=0;i<roles.size()-1;i++)
		{
			System.out.print(roles.get(i)+", ");

		}
		System.out.println(roles.get(roles.size()-1)+")");
	}

	private boolean checkIfRoleIsValid(String id,String role)
	{
		LinkedList lst = dbAccess.getOptionalRolesToAdd(id);
		boolean ans = false;
		for(int i=0;i<lst.size()&&!ans;i++)
		{
			if(lst.get(i).equals(role))
			{
				ans = true;
			}
		}
		return ans;
	}


	public static void updateSystem(DBAccess dbAccess)
	{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		List<Integer> shops=dbAccess.getAllShops();
		for(int k=0;k<shops.size();k++)
		{
			List<Order> orders= DataBase.getDataBase().getAllPermanentOrderThatNeedDelievery(shops.get(k));/*Yaniv Function*/
			for(int i=0;i<orders.size();i++)
			{
				for(int j=0;j<orders.get(i).getDeliveyDays().size();j++)
				{
					Calendar c = Calendar.getInstance();
					Date today=c.getTime();
					c.set(Calendar.DAY_OF_WEEK,orders.get(i).getDeliveyDays().get(j));
					PLI.helpAllOrders(orders.get(i),dateFormat.format(c.getTime()),dbAccess);
				}
			}
		}
	}
}