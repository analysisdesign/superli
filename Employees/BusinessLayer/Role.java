package Employees.BusinessLayer;

public class Role
{
    private String Role;
    private int Priority;

    public Role(String role)
    {
        Role = role;
        setPriority();
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    public int getPriority() {
        return Priority;
    }

    public void setPriority()
    {
        if (getRole().equals("Company Manager"))
        {
            Priority=10;
        }
        else
        {
            if (getRole().equals("Branch Manager")||(getRole().equals("Secretary")))
            {
                Priority=9;
            }
            else
            {
                if (getRole().equals("Human Resources Manager"))
                {
                    Priority=8;
                }
                else
                {
                    Priority=1;
                }
            }
        }
    }
}