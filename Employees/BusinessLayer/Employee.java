package Employees.BusinessLayer;

public class Employee
{
    private int id;
    private String firstName;
    private String LastName;
    private String Bank;
    private int branch;
    private int accountNumber;
    private double monthlypayment;
    private String startingDate;
    private String EmploymentTerms;
    private int SiteID;

    public Employee(int id, String firstName, String lastName, String bank, int branch, int accountNumber, double monthlypayment, String startingDate, int siteID)
    {
        this.id = id;
        this.firstName = firstName;
        LastName = lastName;
        Bank = bank;
        this.branch = branch;
        this.accountNumber = accountNumber;
        this.monthlypayment = monthlypayment;
        this.startingDate = startingDate;
        SiteID = siteID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getBank() {
        return Bank;
    }

    public void setBank(String bank) {
        Bank = bank;
    }

    public int getBranch() {
        return branch;
    }

    public void setBranch(int branch) {
        this.branch = branch;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getMonthlypayment() {
        return monthlypayment;
    }

    public void setMonthlypayment(double monthlypayment) {
        this.monthlypayment = monthlypayment;
    }

    public String getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(String startingDate) {
        this.startingDate = startingDate;
    }





    public int getSiteID() {
        return SiteID;
    }

    public void setSiteID(int siteID) {
        SiteID = siteID;
    }

    public String getEmploymentTerms() {
        return EmploymentTerms;
    }

    public void setEmploymentTerms(String employmentTerms) {
        EmploymentTerms = employmentTerms;
    }
}
