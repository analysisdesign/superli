package Employees.BusinessLayer;

public class InputTesting
{
    public static boolean isValidNumber(String str)
    {
        try
        {
            int test = Integer.parseInt(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }
    public static boolean isValidName(String str)
    {
        if(str.length()==0)
        {
            return false;
        }
        for (int i=0;i<str.length();i++)
        {
            if (!Character.isLetter(str.charAt(i))&&(!(str.charAt(i)==' ')))
            {
                return false;
            }
        }
        return true;
    }
    public static boolean isValidMonthlyPayment(String str)
    {
        try
        {
            double test = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }
    public static boolean isValidDate(String str)
    {
        if(str.compareTo("0")==0)
        {
            return true;
        }
        if(str.length()!=10 || str.charAt(2)!='/' || str.charAt(5)!='/')
        {
            return false;
        }
        try
        {
            int days=Integer.parseInt(str.substring(0,2));
            int month=Integer.parseInt(str.substring(3,5));
            int years=Integer.parseInt(str.substring(6));
            if(days < 1 || days>31 || month < 1 || month>12)
            {
                return false;
            }

        }
        catch (Exception e)
        {
            return false;
        }
        return true;

    }
}
