package Employees.BusinessLayer;

public class Shift
{
    private String Date;
    private String DayPart;
    public Shift(String date, String dayPart)
    {
        Date = date;
        DayPart = dayPart;
    }

    public String getDate()
    {
        return Date;
    }

    public void setDate(String date)
    {
        Date = date;
    }

    public String getDayPart()
    {
        return DayPart;
    }

    public void setDayPart(String dayPart)
    {
        DayPart = dayPart;
    }
}
